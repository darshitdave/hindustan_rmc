<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@indexPage')->name('front.home.page');
Route::get('/another_home_page', 'HomeController@indexAnotherPage')->name('front.home.another.page');
Route::get('/stone-crushing-plant', 'HomeController@stonePlantPage')->name('front.home.stone.page');
//about us pages
Route::get('/company', 'HomeController@aboutUs')->name('front.company.page');
Route::get('/rmc-process', 'HomeController@aboutTechonology')->name('front.about.technology.page');
Route::get('/infrastructure', 'HomeController@aboutInfrastructure')->name('front.about.infrastructure.page');
Route::get('/life-at-hindustan', 'HomeController@aboutLifeAtHindustan')->name('front.about.life_at_hindustan.page');
Route::get('/milestones', 'HomeController@aboutMilestones')->name('front.about.milestones.page');
Route::get('/leadership', 'HomeController@aboutLeadership')->name('front.about.leadership.page');

//product
Route::get('/product', 'HomeController@aboutProduct')->name('front.about_product.page');
Route::get('/sub-product/{id?}', 'HomeController@aboutSubProduct')->name('front.about_sub_product.page');

//projects
Route::match(['get', 'post'],'/projects', 'HomeController@aboutProjects')->name('front.about_projects.page');
Route::get('/sub-project/{id}', 'HomeController@aboutSubProject')->name('front.about_sub_project.page');


//plants
Route::get('/plants-location', 'HomeController@locationPlants')->name('front.location_plants.page');
Route::get('/plants-detail/{id}', 'HomeController@aboutPlants')->name('front.about_plants.page');

//why us
Route::get('/plant-comparison', 'HomeController@plantComparison')->name('front.plant_comparison.page');
Route::get('/usp', 'HomeController@ourUps')->name('front.our_ups.page');

//support
Route::get('/contact-us', 'HomeController@contactUs')->name('front.contact_us.page');

//careers
Route::get('/careers', 'HomeController@comCareer')->name('front.comCareer.page');
Route::get('/careers-image/{id}', 'HomeController@comCareerImage')->name('front.comCareerImage.page');

//faq
Route::get('/faq', 'HomeController@aboutFaq')->name('front.aboutFaq.page');

//blog
Route::get('/blog-list', 'HomeController@blogList')->name('front.blog_list.page');
Route::get('/blog-detail/{id}', 'HomeController@blogDetail')->name('front.blog_detail.page');

//client
Route::get('/clients', 'HomeController@client')->name('front.client.page');

//service details
Route::get('/equipment-on-hire', 'HomeController@serviceDetails')->name('front.service_details.page');

//service details
Route::get('/recognition', 'HomeController@latterOfAppraciation')->name('front.latter_of_appraciation.page');

//calculator
Route::get('/rmc-calculator', 'HomeController@selectCalculation')->name('front.select_calculation.page');
Route::post('get-quote-calculator', 'HomeController@insertRmcCalculator')->name('admin.insertRmcCalculator');

Route::get('quote-calculator', 'HomeController@getQuoteCalculator')->name('admin.getQuoteCalculator');

//
Route::post('get-sub-product', 'HomeController@getSubProduct')->name('admin.getSubProduct');
Route::post('get-sub-product_type_qty', 'HomeController@getSubProductQty')->name('admin.getSubProductQty');

//insert rmc and mail
Route::post('insert-rmc-detail', 'HomeController@insertRmcDetail')->name('admin.insertRmcDetail');

//mail genral contact form route
Route::post('insert-contact-detail', 'HomeController@insertContactDetail')->name('admin.insertContactDetail');

//multiple image of plant
Route::post('/plant-file-upload','Admin\PlantController@multiplePlant')->name('admin.plant.multiple');
Route::DELETE('/plant-id-remove-file-upload','Admin\PlantController@removePlantPhotos')->name('admin.plant.removefile');

//multiple image category
Route::post('/category-image-upload','Admin\ImageCategoryController@multipleCategoryImage')->name('admin.multipleCategoryImage');
Route::DELETE('/category-id-remove-file-upload','Admin\ImageCategoryController@removeCategoryImage')->name('admin.removeCategoryImage');

//news letter
Route::post('/news-letter','HomeController@insertNewsLetterEmail')->name('front.news_letter');

Route::group(['prefix' => 'admin-panel', 'namespace' => 'Admin'], function () {

	// Authentication admin Login Routes
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
	Route::post('login', 'Auth\LoginController@login')->name('admin.postlogin');
	Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');

	//forget and reset password
	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.auth.password.reset');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.passwordemail');
	Route::get('password/reset/{token?}', 'Auth\ResetPasswordController@showResetForm')->name('admin.auth.password.reset');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('admin.resetpassword');

	//Dashboard Route....
	Route::get('/', 'AdminController@index')->name('admin.dashboard');

	//update profile
	Route::get('admin-profile', 'AdminController@adminProfile')->name('admin.profile');
	Route::post('admin-update-profile', 'AdminController@adminProfileUpdate')->name('admin.update.profile');

	//change password
	Route::get('/change-admin-password', 'AdminController@changeAdminPassword')->name('admin.changeAdminPassword');
	Route::post('/update-admin-password', 'AdminController@updateAdminPassword')->name('admin.updateAdminPassword');

	//project
	Route::get('project-list', 'ProjectController@projectList')->name('admin.projectList');
	Route::get('add-project', 'ProjectController@addProject')->name('admin.addProject');
	Route::post('insert-project', 'ProjectController@insertProject')->name('admin.insertProject');
	Route::get('edit-project/{id}', 'ProjectController@editProject')->name('admin.editProject');
	Route::post('update-project', 'ProjectController@updateProject')->name('admin.updateProject');
	Route::get('delete-project/{id}', 'ProjectController@removeProject')->name('admin.removeProject');
	Route::post('change-project-status', 'ProjectController@changeProjectStatus')->name('admin.changeProjectStatus');
	Route::post('admin-category-priority', 'ProjectController@checkCategoryPriority')->name('admin.checkCategoryPriority');

	//plant
	Route::get('plant-list', 'PlantController@plantList')->name('admin.plantList');
	Route::get('add-plant', 'PlantController@addPlant')->name('admin.addPlant');
	Route::post('insert-plant', 'PlantController@insertPlant')->name('admin.insertPlant');
	Route::get('edit-plant/{id}', 'PlantController@editPlant')->name('admin.editPlant');
	Route::post('update-plant', 'PlantController@updatePlant')->name('admin.updatePlant');
	Route::get('delete-plant/{id}', 'PlantController@removePlant')->name('admin.removePlant');
	Route::post('change-plant-status', 'PlantController@changePlantStatus')->name('admin.changePlantStatus');
	Route::post('admin-plant-priority', 'PlantController@checkPlantPriority')->name('admin.checkPlantPriority');

	//testimonial
	Route::get('testimonial-list', 'TestimonialController@testimonialList')->name('admin.testimonialList');
	Route::get('add-letter-of-appreciation', 'TestimonialController@addTestimonial')->name('admin.addTestimonial');
	Route::post('insert-testimonial', 'TestimonialController@insertTestimonial')->name('admin.insertTestimonial');
	Route::get('edit-testimonial/{id}', 'TestimonialController@editTestimonial')->name('admin.editTestimonial');
	Route::post('update-testimonial', 'TestimonialController@updateTestimonial')->name('admin.updateTestimonial');
	Route::get('delete-testimonial/{id}', 'TestimonialController@removeTestimonial')->name('admin.removeTestimonial');
	Route::post('change-testimonial-status', 'TestimonialController@changeTestimonialStatus')->name('admin.changeTestimonialStatus');
	Route::post('admin-testimonial-priority', 'TestimonialController@checkTestimonialPriority')->name('admin.checkTestimonialPriority');

	//testimonial other
	Route::get('testimonial-other-list', 'TestimonialOtherController@testimonialOtherList')->name('admin.testimonialOtherList');
	Route::get('add-other-testimonial', 'TestimonialOtherController@addTestimonialOther')->name('admin.addTestimonialOther');
	Route::post('insert-other-testimonial', 'TestimonialOtherController@insertTestimonialOther')->name('admin.insertTestimonialOther');
	Route::get('edit-other-testimonial/{id}', 'TestimonialOtherController@editTestimonialOther')->name('admin.editTestimonialOther');
	Route::post('update-other-testimonial', 'TestimonialOtherController@updateTestimonialOther')->name('admin.updateTestimonialOther');
	Route::get('delete-other-testimonial/{id}', 'TestimonialOtherController@removeTestimonialOther')->name('admin.removeTestimonialOther');
	Route::post('change-other-testimonial-status', 'TestimonialOtherController@changeTestimonialOtherStatus')->name('admin.changeTestimonialOtherStatus');
	Route::post('admin-other-testimonial-priority', 'TestimonialOtherController@checkTestimonialOtherPriority')->name('admin.checkTestimonialOtherPriority');

	//blog
	Route::get('blog-list', 'BlogController@blogList')->name('admin.blogList');
	Route::get('add-blog', 'BlogController@addBlog')->name('admin.addBlog');
	Route::post('insert-blog', 'BlogController@insertBlog')->name('admin.insertBlog');
	Route::get('edit-blog/{id}', 'BlogController@editBlog')->name('admin.editBlog');
	Route::post('update-blog', 'BlogController@updateBlog')->name('admin.updateBlog');
	Route::get('delete-blog/{id}', 'BlogController@removeBlog')->name('admin.removeBlog');
	Route::post('change-blog-status', 'BlogController@changeBlogStatus')->name('admin.changeBlogStatus');
	Route::post('admin-blog-priority', 'BlogController@blogPriority')->name('admin.blogPriority');	

	Route::get('edit-base-setting', 'BaseSettingController@editBaseSetting')->name('admin.base.setting');
	Route::post('update-base-setting', 'BaseSettingController@insertBaseSetting')->name('admin.baseSetting');

	//product rate
	Route::get('product-setting-list', 'ProductSettingController@productSettingList')->name('admin.productSettingList');
	Route::get('add-product-setting', 'ProductSettingController@addProductSetting')->name('admin.product.setting');
	Route::post('insert-product-setting', 'ProductSettingController@insertProductSetting')->name('admin.productSetting');
	Route::get('edit-product-setting/{id}', 'ProductSettingController@editProductSetting')->name('admin.editProductSetting');
	Route::post('update-product-setting', 'ProductSettingController@updateProductSetting')->name('admin.updateProductSetting');
	Route::get('delete-product-setting/{id}', 'ProductSettingController@removeProductData')->name('admin.removeProductData');
	Route::post('change-product-status', 'ProductSettingController@changeProductStatus')->name('admin.changeProductStatus');

	//inquiry list
	Route::match(['get','post'],'genral-inquiry-list', 'InquiryController@genralInquiryList')->name('admin.genral_inquiry_list');
	Route::match(['get','post'],'rmc-inquiry-list', 'InquiryController@rmcInquiryList')->name('admin.rmc_inquiry_list');
	
	Route::post('genral-inquiry-detail', 'InquiryController@genralInquiryDetail')->name('admin.genralInquiryDetail');
	Route::post('rmc-inquiry-detail', 'InquiryController@rmcInquiryDetail')->name('admin.rmcInquiryDetail');

	//product rate
	Route::get('gallery-list', 'GalleryController@galleryList')->name('admin.galleryList');
	Route::get('add-gallery', 'GalleryController@addGallery')->name('admin.addGallery');
	Route::post('insert-gallery', 'GalleryController@saveGallery')->name('admin.saveGallery');
	Route::get('edit-gallery/{id}', 'GalleryController@editGallery')->name('admin.editGallery');
	Route::post('update-gallery', 'GalleryController@saveEditedGallery')->name('admin.saveEditedGallery');
	Route::get('delete-gallery/{id}', 'GalleryController@deleteGallery')->name('admin.deleteGallery');

	//news letter 
	Route::get('news-letter-subscription-list', 'NewLatterController@newsLetterList')->name('admin.newsLetterList');

	//image categories routes
	Route::get('image-category-list', 'ImageCategoryController@imageCategoryList')->name('admin.imageCategoryList');
	Route::get('add-image-category', 'ImageCategoryController@addImageCategory')->name('admin.addImageCategory');
	Route::post('insert-image-category', 'ImageCategoryController@saveImageCategory')->name('admin.saveImageCategory');
	Route::get('edit-image-category/{id}', 'ImageCategoryController@editImageCategory')->name('admin.editImageCategory');
	Route::post('update-image-category', 'ImageCategoryController@saveEditedImageCategory')->name('admin.saveEditedImageCategory');
	Route::get('delete-image-category/{id}', 'ImageCategoryController@deleteImageCategory')->name('admin.deleteImageCategory');	
	Route::post('change-category-status', 'ImageCategoryController@changeImageCategoryStatus')->name('admin.changeImageCategoryStatus');
	Route::post('admin-image-category-priority', 'ImageCategoryController@ImageCategoryPriority')->name('admin.ImageCategoryPriority');	
	Route::get('view-category-images/{id}', 'ImageCategoryController@viewCategoryImages')->name('admin.viewCategoryImages');	

	//category images
	Route::get('add-category-image/{id}', 'ImageCategoryController@addCategoryImage')->name('admin.addCategoryImage');
	Route::post('insert-category-image', 'ImageCategoryController@saveCategoryImage')->name('admin.saveCategoryImage');
	Route::get('edit-category-image/{id?}/{category_id?}', 'ImageCategoryController@editCategoryImage')->name('admin.editCategoryImage');
	Route::post('update-category-image', 'ImageCategoryController@saveEditedCategoryImage')->name('admin.saveEditedCategoryImage');
	Route::get('delete-category-image/{id?}/{category_id?}', 'ImageCategoryController@deleteCategoryImage')->name('admin.deleteCategoryImage');	
	Route::post('change-category-image-status', 'ImageCategoryController@categoryStatusImage')->name('admin.categoryStatusImage');

	//image categories routes
	Route::get('client-list', 'ClientController@clientList')->name('admin.clientList');
	Route::get('add-client', 'ClientController@addClient')->name('admin.addClient');
	Route::post('insert-client', 'ClientController@saveClient')->name('admin.saveClient');
	Route::get('edit-client/{id}', 'ClientController@editClient')->name('admin.editClient');
	Route::post('update-client', 'ClientController@saveEditedClient')->name('admin.saveEditedClient');
	Route::get('delete-client/{id}', 'ClientController@deleteClient')->name('admin.deleteClient');	
	Route::post('change-client-status', 'ClientController@changeClientStatus')->name('admin.changeClientStatus');
	Route::post('admin-client-priority', 'ClientController@ClientPriority')->name('admin.ClientPriority');
});






