<!DOCTYPE html>
<html class="no-js" lang="zxx" >
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Favicon -->
    <link rel="icon" href="{{asset('img/favicon.png')}}">
    @include('partials.front.header_link') 

</head>

<body>
	<!--====================  preloader ====================-->
    <div class="preloader-activate preloader-active">
        <div class="preloader-area-wrap">
            <div class="spinner d-flex justify-content-center align-items-center h-100">
                <img src="{{ asset('assets/front/img/Rolling-1s-200px.gif') }}" alt="" style="width: 50px;">
            </div>
        </div>
    </div>
    <!--====================  End of preloader  ====================-->
    @include('partials.front.head')

    @yield('content')    
    <div class="footer-area section-space--inner--120 dark-bg--style3" style="padding-bottom: 5px;">
		@include('partials.front.footer')    	
    </div>

    <!--====================  mobile menu overlay ====================-->
    <div class="mobile-menu-overlay" id="mobile-menu-overlay">
    	@include('partials.front.header_mobile_overlay')
    </div>

    <!--====================  scroll top ====================-->
    <a href="#" class="scroll-top" id="scroll-top">
        <i class="ion-android-arrow-up"></i>
    </a>
    <!--====================  End of scroll top  ====================-->
    
    @include('partials.front.footer_link')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @if(route::is('front.select_calculation.page'))
    <!-- Calculator Page Validation -->
    <script src="{{ asset('assets/front/js/calculator_type.js') }}"></script>
    <!-- end -->
    @endif
    @if(route::is('admin.getQuoteCalculator')  || route::is('admin.insertRmcCalculator'))
    <!-- Calculator Page Validation -->
    <script src="{{ asset('assets/front/js/get_quote.js') }}"></script>
    <!-- end -->
    @endif
    @yield('js')
    <script>
        $(document).on('click', '.news_button', function() {
            $(".news_email").parent().next(".validation").remove();
            var user_email = $('.news_email').val();
            var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
            var re1 = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/igm;

            if(user_email != ''){
                if (re.test(user_email) || re1.test(user_email)){
                    $.ajax({
                        type: 'post',
                        url: 'news-letter',
                        data: {
                            'user_email': user_email
                        },
                        success: function(data) {
                           if(data == 'true'){
                                toastr.success('Thank You For Subscription!');
                                $('.news_email').val('');
                            } else {        
                               toastr.success('Something Wrong!');
                            }
                        }
                    });  
                }else{
                    $(".news_email").parent().next(".validation").remove();
                    $(".news_email").parent().after("<div class='validation' style='color:red;margin:-71px 0 10px 6px'>Please Enter Valid Email ID!</div>");    
                }  
            }else{
                $(".news_email").parent().next(".validation").remove();
                $(".news_email").parent().after("<div class='validation' style='color:red;margin:-71px 0 10px 6px'>Please Enter Email ID!</div>");
            }
        });
    
        @if(Session::has('messages'))
            jQuery(document).ready(function() {
                @foreach(Session::get('messages') AS $msg) 
                    toastr['{{$msg["type"]}}']('{{$msg["message"]}}');
                @endforeach
            });
        @endif
        @if (count($errors) > 0) 
            jQuery(document).ready(function() {
                @foreach($errors->all() AS $error)
                    toastr.error('{{$error}}');
                @endforeach     
            });
        @endif
    </script>
</body>
</html>