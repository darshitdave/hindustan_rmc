<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
  border: 1px solid black;
}
</style>
</head>
<body>
<p> Hello Admin,</p>
<p>You've got new message!</p>
<table>
  <tr>
    <td>Name</td>
    <td><?php echo $bodymessage['name']; ?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td><?php echo $bodymessage['email']; ?></td>
  </tr>
@if(isset($bodymessage['phone_number']) && $bodymessage['phone_number'] != '')
  <tr>
    <td>Phone Number</td>
    <td><?php echo $bodymessage['phone_number']; ?></td>
  </tr>
@endif
@if(isset($bodymessage['message']) && $bodymessage['message'] != '')
  <tr>
    <td>Message</td>
    <td><?php echo $bodymessage['message']; ?></td>
  </tr>
@endif
</table>
<p>Thank you!</p>
</body>
</html>
