<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
  border: 1px solid black;
}
</style>
</head>
<body>
<p> Hello <?php echo $bodymessage['name']; ?>,</p>
<p>Greetings from HindustanRMC.</p>
<p>Here is the price quote estimation as per the requirements you've submitted:</p>

<table>
  <tr>
    <th colspan="3">RMC Inquiry</th>
  </tr>
  <!-- <tr>
    <td>Name</td>
    <td><?php echo $bodymessage['name']; ?></td>
  </tr> -->
  <!-- @if(isset($bodymessage['email']) && $bodymessage['email'] != '')
  <tr>
    <td>Email</td>
    <td><?php echo $bodymessage['email']; ?></td>
  </tr>
  @endif
    <tr>
      <td>Phone Number</td>
      <td><?php echo $bodymessage['phone_number']; ?></td>
    </tr>
   -->
@if(isset($bodymessage['address']) && $bodymessage['address'] != '')
  <tr>
    <td>Address</td>
    <td><?php echo $bodymessage['address']; ?></td>
  </tr>
@endif
  <tr>
    <td>Product Name</td>
    <td><?php echo $bodymessage['product_name']; ?></td>
  </tr>
  <tr>
    <td>Grade</td>
    <td><?php echo $bodymessage['grade']; ?></td>
  </tr>
  <tr>
    <td>Cement Quantity</td>
    <td><?php echo $bodymessage['cement_quantity']; ?> kg</td>
  </tr>
  <tr>
    <td>Fly Ash Quantity</td>
    <td><?php echo $bodymessage['fly_ash_quantity']; ?> kg</td>
  </tr>
  <tr>
    <td>Placement Method</td>
    <td><?php echo $bodymessage['placement_type']; ?></td>
  </tr>
  <tr>
    <td><b>Price Without GST</b></td>
    <td><b>₹<?php echo $bodymessage['price_without_gst']; ?></b></td>
  </tr>
  <tr>
    @if($bodymessage['placement_type'] == 'Pumping')
    <td><b>Price With GST</b></td>
    <td><b>₹<?php echo $bodymessage['price_with_gst_price']; ?></b></td>
    @endif
  </tr>
  <tr>
    @if($bodymessage['placement_type'] == 'Pumping')
      <td><b>Total Price</b></td>
      <td><b>₹<?php echo round($bodymessage['price_with_gst'],2).' (Inclusive of ₹150 for Pumping)'; ?></b></td>
    @else
      <td><b>Total Price With GST</b></td>
      <td><b>₹<?php echo round($bodymessage['price_with_gst'],2); ?></b></td>
    @endif
  </tr>
</table>
<p>We will soon get in touch to assist you further.<p>
<p>Thank you!</p>
</body>
</html>
