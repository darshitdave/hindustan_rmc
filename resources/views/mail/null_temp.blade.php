<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
  border: 1px solid black;
}
</style>
</head>
<body>
  <p>Hello <?php echo $bodymessage['name']; ?>,</p>
  <p>Greetings from HindustanRMC.</p>
  <p>Thank you for getting in touch with us, we've received your message.</p>
<table>
  <tr>
    <th colspan="3">Message</th>
  </tr>
  
@if(isset($bodymessage['message']) && $bodymessage['message'] != '')
  <tr>
    <!-- <td>Message</td> -->
    <td><?php echo $bodymessage['message']; ?></td>
  </tr>
@endif
</table>
<p>We will soon get in touch to assist you further.</p>
<p>Thank you!</p>
</body>
</html>
