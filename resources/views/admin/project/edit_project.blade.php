@extends('layouts.admin.admin')
@section('title','Edit Project| Hindustan RMC')
@section('content')
<style type="text/css">
#map-canvas {
margin-top: 15px !important;
margin-bottom: 15px !important;
height: 300px;
}
.gmnoprint .gm-style-mtc,.gmnoprint .gm-style-mtc,.gmnoprint .gm-style-cc, .gmnoprint .gm-style-cc{
    display: none;
}
</style>
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Edit Project</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Edit Project
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.updateProject') }}" enctype="multipart/form-data" id="projectForms">
                            @csrf
                            <div class="form-group">
                                <label for="inputCategoryId">Category Name<span class="mendetory">*</span></label>
                                <select class="form-control category" name="category_id" required="required">
                                    <option value="" selected="selected">Select Category Name</option>
                                    <option value="0" @if($get_project->category_id == 0) selected="selected" @endif>Residential</option>
                                    <option value="1" @if($get_project->category_id == 1) selected="selected" @endif>Commercial</option>
                                    <option value="2" @if($get_project->category_id == 2) selected="selected" @endif>Industrial</option>
                                    <option value="3" @if($get_project->category_id == 3) selected="selected" @endif>Roads & Bridges</option>
                                    <option value="4" @if($get_project->category_id == 4) selected="selected" @endif>Others</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="inputProjectName">Project Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="project_name" id="inputProjectName" placeholder="Project Name" value="{{$get_project->project_name}}" required>
                                <input type="hidden" name="id" id="id" value="{{$get_project->id}}">
                            </div>

                            <div class="form-group">
                                <label for="inputPriority">Priority</label>
                                <input type="tel" class="form-control priority" name="priority" oninvalid="this.setCustomValidity('Please Enter Priority')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputPriority"  placeholder="Enter Priority" value="{{$get_project->priority}}">
                                
                            </div>
                                
                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Project Profile Photo<span class="mendetory">*</span> (370x320)</label>
                                <input type="file" name="project_profile_photo" class="dropify" data-show-remove="false"   @if($get_project->project_profile != '') data-show-remove="false"  data-default-file="{{asset('uploads/project_photo/'.$get_project->project_profile)}}" @endif >
                            </div>

                            

                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Project Gallery (480x480)</label>
                                    <input type="file" class="form-control dropify" data-show-remove="false"  name="project_gallary_one" @if($get_project->gallery_one != '') data-show-remove="false" data-default-file="{{asset('uploads/project_gallary/'.$get_project->gallery_one)}}" @endif><br>
                                    <input type="file" class="form-control dropify" data-show-remove="false"  name="project_gallary_two" @if($get_project->gallery_two != '') data-show-remove="false" data-default-file="{{asset('uploads/project_gallary/'.$get_project->gallery_two)}}" @endif><br>
                                    <input type="file" class="form-control dropify" data-show-remove="false"  name="project_gallary_three" @if($get_project->gallery_three != '') data-show-remove="false" data-default-file="{{asset('uploads/project_gallary/'.$get_project->gallery_three)}}" @endif>
                            </div>

                            <div class="form-group">
                                <label for="inputProjectDescription">Project Description</label>
                                <textarea type="text" class="form-control" name="project_description" placeholder="Project Description" id="inputProjectDescription" spellcheck="false">{{$get_project->project_description}}</textarea>
                            </div>
                            
                            <div class="form-group">
                                <label for="inputDate">Construction Date</label>
                                <input type="text" class="js-datepicker form-control" name="construction_date" placeholder="Construction Date" value="{{$get_project->construction_date}}" >
                            </div>

                            <div class="form-group">
                                <label for="inputBuilderName">Builder Name</label>
                                <input type="text" class="form-control" name="builder_name" id="inputBuilderName" placeholder="Builder Name" value="{{$get_project->builder_name}}">
                            </div>

                            
                            <div class="form-group">
                                <label for="inputConcreteConsume">Concrete Consumed</label>
                                <input type="text" class="form-control" name="concrete_consume" id="inputConcreteConsume" placeholder="Concrete Consumed" value="{{$get_project->concrete_consume}}">
                            </div>

                            <input type="hidden" name="latitude" id="latitude" value="{{$get_project->latitude}}"> 
                  
                            <input type="hidden" name="longitude" id ="longitude" value="{{$get_project->longitude}}">  

                            <input type="hidden" id ="city" name="city_name" />
                          
                            <input type="hidden" id ="state" name="state_name" />

                            <input type="hidden" id ="country" name="country"/>

                            <div class="form-group">
                                <label for="inputLocation">Location</label>
                                <input type="text" class="form-control" name="location" id="pacinput" placeholder="Location" value="{{$get_project->location}}">
                            </div>

                            <div class="form-group">
                                <div id="map-canvas"></div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.projectList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function() {
    $(".priority").keyup(function(){

        $(".priority").parent().next(".validation").remove();

        var category = $(".category").val();
        var priority = $(".priority").val();
        var id = $("#id").val();

        if(priority != ''){
            $.ajax({
                type: 'post',
                url: "{{route('admin.checkCategoryPriority')}}",
                data: {
                    'category': category,
                    'priority': priority,
                    'id' : id
                },
                success: function(data) {
                   if(data == 'true'){

                        $(':input[type="submit"]').prop('disabled', true);
                        if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                        {
                            $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                        }
                        
                        $('.priority').focus();
                   }else{
                        $(':input[type="submit"]').prop('disabled',false);
                        $(".priority").parent().next(".validation").remove();
                   }
                }
            });
        }
    });
});

$(document).on('change', '.category', function() {

    var category = $(".category").val();
    var priority = $(".priority").val();
    var id = $("#id").val();

    if(priority != ''){

            $.ajax({
            type: 'post',
            url: "{{route('admin.checkCategoryPriority')}}",
            data: {
                'category': category,
                'priority': priority,
                'id' : id
            },
            success: function(data) {
               if(data == 'true'){

                    $(':input[type="submit"]').prop('disabled', true);
                    if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                    {
                        $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                    }
                    
               }else{
                    $(':input[type="submit"]').prop('disabled',false);
                    $(".priority").parent().next(".validation").remove();
               }
            }
        });
    }
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDbX_JirTlqgj9BO002nMah8CQSD7f4ypI&signed_in=true&libraries=places"></script>
<script type="text/javascript">
    var isPlaceAuthentic = false;
    var lastPlaceAuthenticated = '';
   
    function initialize() {
        var input = (document.getElementById('pacinput'));
        var autocomplete = new google.maps.places.Autocomplete(input);
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            infowindow.close();
            var place = autocomplete.getPlace();
            isPlaceAuthentic = true;
            lastPlaceAuthenticated = $('#pacinput').val();
            isPlaceAuthentic = false;
            var address = '';
            var toInsertData = place.adr_address;
            var latti = place.geometry.location.lat();
            var longi = place.geometry.location.lng();
            var addressToInsert = toInsertData.substr(0, toInsertData.indexOf('<')).trim();
            if (toInsertData.indexOf('<') !== 1) {
                toInsertData = toInsertData.substr(toInsertData.indexOf('<'));
            }
            var streetAddress = $(toInsertData).filter('.street-address').text().trim();
            var extendedAddress = $(toInsertData).filter('.extended-address').text().trim();
            var cityName = $(toInsertData).filter('.locality').text().trim();
            var stateName = $(toInsertData).filter('.region').text().trim();
            var countryName = $(toInsertData).filter('.country-name').text().trim();

            $("#latitude").val(latti);
            $("#longitude").val(longi);
            $("#city").val(cityName);
            $("#state").val(stateName);
            $("#country").val(countryName);
            initMap(latti,longi);
            var appenedAddress = addressToInsert.concat(streetAddress);
            appenedAddress = appenedAddress.concat(extendedAddress);
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
    $("#area_submit").on('click',function(e){
        if(($("#latitude").val() == "" || $("#longitude").val() == "")){
            alert("Please Enter Valid Address");
            e.preventDefault();
        }
    });
    
    function initMap(latitude,longitude) {
        var myLatLng = {lat: latitude, lng: longitude};
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 18,
          center: myLatLng
        });
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            document.getElementById("lat").value = this.getPosition().lat();
            document.getElementById("long").value = this.getPosition().lng();
        });
    }
</script>
@if($get_project->latitude != '' && $get_project->longitude != '')
    <script type="text/javascript">
        initMap({{ $get_project->latitude }},{{ $get_project->longitude }});
    </script>
@endif
@endsection