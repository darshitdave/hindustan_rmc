@extends('layouts.admin.admin')
@section('title','Add Project | Hindustan RMC')
@section('content')
<style type="text/css">
#map-canvas {
margin-top: 15px !important;
margin-bottom: 15px !important;
height: 300px;
}
.gmnoprint .gm-style-mtc,.gmnoprint .gm-style-mtc,.gmnoprint .gm-style-cc, .gmnoprint .gm-style-cc{
    display: none;
}
</style>
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Add Project</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Add Project
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.insertProject') }}" enctype="multipart/form-data" id="projectForms">
                            @csrf
                            <div class="form-group">
                                <label for="inputCategoryId">Category Name<span class="mendetory">*</span></label>
                                <select class="form-control category" name="category_id" required="required">
                                    <option value="" selected="selected">Select Category Name</option>
                                    <option value="0">Residential</option>
                                    <option value="1">Commercial</option>
                                    <option value="2">Industrial</option>
                                    <option value="3">Roads & Bridges</option>
                                    <option value="4">Others</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="inputProjectName">Project Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="project_name" id="inputProjectName" placeholder="Project Name" required>
                            </div>

                            <div class="form-group">
                                <label for="inputPriority">Priority</label>
                                <input type="tel" class="form-control priority" name="priority" oninvalid="this.setCustomValidity('Please Enter Priority')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputPriority"  placeholder="Enter Priority">
                                
                            </div>
                                
                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Project Profile Photo<span class="mendetory">*</span> (370x320)</label>
                                <input type="file" class="form-control dropify" name="project_profile_photo" required="">
                            </div>

                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Project Gallery (480x480)</label>
                                <input type="file" class="form-control dropify" name="project_gallary_one"><br>
                                <input type="file" class="form-control dropify" name="project_gallary_two"><br>
                                <input type="file" class="form-control dropify" name="project_gallary_three">
                            </div>

                            <div class="form-group">
                                <label for="inputProjectDescription">Project Description</label>
                                <textarea type="text" class="form-control" name="project_description" placeholder="Project Description" id="inputProjectDescription" spellcheck="false"></textarea>
                            </div>
                            
                            <div class="form-group">
                                <label for="inputDate">Construction Date</label>
                                <input type="text" class="js-datepicker form-control" name="construction_date" placeholder="Construction Date">
                            </div>

                            <div class="form-group">
                                <label for="inputBuilderName">Builder Name</label>
                                <input type="text" class="form-control" name="builder_name" id="inputBuilderName" placeholder="Builder Name">
                            </div>

                            <div class="form-group">
                                <label for="inputConcreteConsume">Concrete Consumed</label>
                                <input type="text" class="form-control" name="concrete_consume" id="inputConcreteConsume" placeholder="Concrete Consumed">
                            </div>

                            <input type="hidden" name="latitude" id="latitude"> 
                  
                            <input type="hidden" name="longitude" id ="longitude">  

                            <input type="hidden" id ="city" name="city_name"/>
                          
                            <input type="hidden" id ="state" name="state_name"/>

                            <input type="hidden" id ="country" name="country"/>

                            <div class="form-group">
                                <label for="inputLocation">Location</label>
                                <input type="text" class="form-control" name="location" id="pacinput" placeholder="Location">
                            </div>

                            <div class="form-group">
                                <div id="map-canvas"></div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="submit" name="add_new" value="add_new" class="btn btn-info">Save & Add New</button>
                                <a href="{{route('admin.projectList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function() {
    $(".priority").keyup(function(){

        $(".priority").parent().next(".validation").remove();

        var category = $(".category").val();
        var priority = $(".priority").val();

        if(priority != ''){
            $.ajax({
                type: 'post',
                url: 'admin-category-priority',
                data: {
                    'category': category,
                    'priority': priority
                },
                success: function(data) {
                   if(data == 'true'){

                        $(':input[type="submit"]').prop('disabled', true);
                        if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                        {
                            $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                        }
                        
                        $('.priority').focus();
                   }else{
                        $(':input[type="submit"]').prop('disabled',false);
                        $(".priority").parent().next(".validation").remove();
                   }
                }
            });
        }
    });
});


$(document).on('change', '.category', function() {

    var category = $(".category").val();
    var priority = $(".priority").val();
    if(priority != ''){

            $.ajax({
            type: 'post',
            url: 'admin-category-priority',
            data: {
                'category': category,
                'priority': priority
            },
            success: function(data) {
               if(data == 'true'){

                    $(':input[type="submit"]').prop('disabled', true);
                    if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                    {
                        $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                    }
                    
               }else{
                    $(':input[type="submit"]').prop('disabled',false);
                    $(".priority").parent().next(".validation").remove();
               }
            }
        });
    }
});
</script>

@endsection