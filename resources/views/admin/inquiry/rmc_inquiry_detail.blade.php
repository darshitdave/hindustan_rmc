<div class="container-fluid ">
    <div class="row ">
        <div class="col-md-12">
            <form class="needs-validation"  method="post">
                @csrf

                <div class="form-row">
                    <div class="form-group col-md-12">
                        @if($rmc_details->address != '')
                            <label>Delivery Address</label>
                            <!-- <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="{{$rmc_details->cement}}" required="" aria-invalid="false" readonly="readonly"> -->
                            <textarea placeholder="   Message" name="name" rows="2" id="con_message" autocomplete="off" class="infra_message" readonly="readonly">{{$rmc_details->address}}</textarea>
                        @endif
                        @if($rmc_details->cement != '')
                            <label>Cement Quantity</label>
                            <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="{{$rmc_details->cement}}" required="" aria-invalid="false" readonly="readonly">
                        @endif
                        @if($rmc_details->fly_ash != '')
                            <label>Fly Ash Quantity</label>
                            <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="{{$rmc_details->fly_ash}}" required="" aria-invalid="false" readonly="readonly">
                        @endif
                        @if($rmc_details->placement_type == 1)
                            <label>Placement Method</label>
                            <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="Dumping" required="" aria-invalid="false" readonly="readonly">
                        @elseif($rmc_details->placement_type == 2)
                            <label>Placement Method</label>
                            <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="Pumping" required="" aria-invalid="false" readonly="readonly">
                        @else

                        @endif
                        @if($rmc_details->without_gst_price != '')
                            <label>Without GST Price</label>
                            <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="₹{{$rmc_details->without_gst_price}}" required="" aria-invalid="false" readonly="readonly">
                        @endif
                        @if($rmc_details->placement_type == 2)
                            <label>With GST Price</label>
                            <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="₹{{$rmc_details->with_gst_price}}" required="" aria-invalid="false" readonly="readonly">
                        @endif
                        @if($rmc_details->total_calculation != '')
                            @if($rmc_details->placement_type == 2)
                                <label>Total Price</label>
                                <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="₹<?php echo round($rmc_details->total_calculation,2).' (Inclusive of ₹150 for Pumping)'; ?>" required="" aria-invalid="false" readonly="readonly">
                            @else
                                <label>Total Price With GST</label>
                                <input type="text" class="form-control" name="name" placeholder="name"  autocomplete="off" value="₹<?php echo round($rmc_details->total_calculation,2); ?>" required="" aria-invalid="false" readonly="readonly">
                            @endif
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>