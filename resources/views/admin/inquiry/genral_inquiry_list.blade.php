@extends('layouts.admin.admin')
@section('title','Genral Inquiry List | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        General Inquiry List
                    </h4>
                </div>
            </div>
        </div>
    </div>

     <div class="container  pull-up">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{ route('admin.genral_inquiry_list') }}" id="company">
                            @csrf
                            <div class="form-row">

                                <div class="col-md-4 mb-3">
                                    <label for="">Inquiry Date Range</label>
                                    <input type="text" class="form-control input-daterange" id="date_range" name="date_range" placeholder="Date Range" autocomplete="off" value="{{ $date_range }}" readonly>
                                </div>

                                <div class="col-md-2 mt-4">
                                    <button type="submit" class="btn btn-primary vendors"  style="margin-top: 5px;">Submit</button>
                                </div>
                                @if($filter == 1)
                                    <div class="col-md-2 mt-4">
                                        <a href="{{ route('admin.genral_inquiry_list') }}" class="btn btn-danger mt-1" id="filter" name="save_and_list" value="save_and_list" style="margin-left: -100px;">Reset</a>
                                    </div>
                                @endif
                            </div>
                        </form>
                       
                    </div>
                </div>
            </div>
        </div>
    </div><br> 

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Date Time</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($genral_details))
                                    @foreach($genral_details as $gk => $gv)
                                    
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $gv->name }}</td>
                                            <td>{{ $gv->email }}</td>
                                            <td>{{ $gv->phone_number }}</td>
                                            <td>{{date("d-m-Y h:i:s",strtotime($gv->created_at))}}</td>
                                            <td>
                                                <a href="" class="btn m-b-15 ml-2 mr-2 btn-dark genral_detail" data-id="{{$gv->id}}" onclick="{{$gv->id}}" data-toggle="modal" data-target="#example_02"><i class="fe fe-eye"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade"  id="example_02" tabindex="-1" role="dialog" aria-labelledby="example_02" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Genral Inquiry Details</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modalcontent">
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
        </div>
      </div>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).on('click', '.genral_detail', function() {

    var id = $(this).data('id');
    
    $.ajax({
        type: 'post',
        url: "{{ route('admin.genralInquiryDetail') }}",
        data: {
            'id': id
        },
        success: function(data) {
           $('#modalcontent').html(data);
        }
    });
});
</script>
@endsection