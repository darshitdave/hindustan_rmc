<div class="container-fluid ">
    <div class="row ">
        <div class="col-md-12">
            <form class="needs-validation"  method="post">
                @csrf

                <div class="form-row">
                    <div class="form-group col-md-12">
                        @if($genral_details->message != '')
                        <label>Message</label>
                        
                        <textarea placeholder="   Message" name="name" id="con_message" autocomplete="off" class="infra_message" readonly="readonly">{{$genral_details->message}}</textarea>
                        @else
                            <h4>No Message Found</h4>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>