@extends('layouts.admin.admin')
@section('title','Category Images List | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="" align="center">
                        {{$category_name->category_name}}
                    </h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container pull-up">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                    <a href="{{route('admin.addCategoryImage',$category_id)}}" class="btn m-b-15 ml-2 mr-2 btn-dark" style="text-decoration: none;"><i class="fa fa-plus"></i> Add Category Images</a>
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Category Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($category_images))
                                    @foreach($category_images as $ck => $cv)
                                    
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td><img src="{{ asset('uploads/Category_Sub_Images') }}/{{ $cv->sub_images }}" style="width:10%"/></td>
                                            <td>
                                                @php $checked = ''; @endphp
                                                @if($cv->is_active == 1)
                                                  @php $checked = 'checked'; @endphp
                                                @endif
                                                <label class="cstm-switch ">
                                                    <input type="checkbox" name="option" value="1" class="cstm-switch-input changeProject" data-id="{{ $cv->id }}" {{ $checked }}>
                                                    <span class="cstm-switch-indicator size-md "></span>
                                                    <span class="cstm-switch-description"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <a href="{{route('admin.editCategoryImage',['id' => $cv->id, 'category_id' => $category_id])}}" class="btn m-b-15 ml-2 mr-2 btn-dark"><i class="fe fe-edit"></i></a>
                                                <a href="{{ route('admin.deleteCategoryImage',['id' => $cv->id, 'category_id' => $category_id]) }}" class="btn m-b-15 ml-2 mr-2 btn-dark" onclick="return confirm('Are your sure want to delete this Category Image ?')"><i class="fe fe-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).on('change', '.changeProject', function() {

    var id = $(this).data('id');
    var current_status = $(this).data('value');
    var status;
   
    if($(this).prop("checked") == true){
      status = 1;
    } else {
      status = 0;
    }

    $.ajax({
        type: 'post',
        url: '{{route("admin.categoryStatusImage")}}',
        data: {
            'id': id,
            'status': status
        },
        success: function(data) {
           if(data == 'true'){
                if(status == 1){
                    msg = 'Status Enabled!';
                } else {
                    msg = 'Status Disabled!';
                }
            } else {        
               msg = 'Something Went Wrong';
            }

            $.notify({ title: '', message: msg},{
            placement: {
                align: "right",
                from: "top"
            },
                timer: 500,
                type: 'success',
            });
        }
    });
});
</script>
@endsection