@extends('layouts.admin.admin')
@section('title','Edit Image Category | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Edit Image Category</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Edit Image Category
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.saveEditedCategoryImage') }}" enctype="multipart/form-data" id="">
                            @csrf
                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Category Image<span class="mendetory">*</span> (480x480)</label>
                                <input type="file" class="form-control dropify" name="sub_images"   @if($editCategoryImage->sub_images != '') data-show-remove="false"  data-default-file="{{asset('uploads/Category_Sub_Images/'.$editCategoryImage->sub_images)}}" @endif>
                            </div>
                            <input type="hidden" name="id" value="{{$id}}">
                            <input type="hidden" name="category_id" value="{{$category_id}}">

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.viewCategoryImages',$category_id)}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')

@endsection