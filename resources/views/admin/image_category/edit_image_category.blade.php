@extends('layouts.admin.admin')
@section('title','Edit Image Category | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Edit Image Category</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Edit Image Category
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.saveEditedImageCategory') }}" enctype="multipart/form-data" id="category_image">
                            @csrf
                            <div class="form-group">
                                <label for="inputProjectName">Category Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control category" name="category_name" id="inputProjectName" placeholder="Project Name" value="{{$category_image->category_name}}" required>
                                <input type="hidden" name="id" id="id" value="{{$category_image->id}}">
                            </div>

                            <div class="form-group">
                                <label for="inputPriority">Priority</label>
                                <input type="tel" class="form-control priority" name="priority" oninvalid="this.setCustomValidity('Please Enter Priority')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputPriority"  placeholder="Enter Priority" value="{{$category_image->priority}}">
                                
                            </div>
                                
                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Catergory Cover<span class="mendetory">*</span> (1900x400)</label>
                                <input type="file" class="form-control dropify" name="category_banner"   @if($category_image->category_banner != '') data-show-remove="false"  data-default-file="{{asset('uploads/image_category_banner/'.$category_image->category_banner)}}" @endif>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.imageCategoryList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function() {
    $(".priority").keyup(function(){

        $(".priority").parent().next(".validation").remove();

        /*var category = $(".category").val();*/
        var priority = $(".priority").val();
        var id = $("#id").val();
        
        if(priority != ''){
        
            $.ajax({
                type: 'post',
                url: '{{route("admin.ImageCategoryPriority")}}',
                data: {
                    /*'category': category,*/
                    'priority': priority,
                    'id' : id
                },
                success: function(data) {
                   if(data == 'true'){

                        $(':input[type="submit"]').prop('disabled', true);
                        if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                        {
                            $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                        }
                        
                        $('.priority').focus();
                   }else{
                        $(':input[type="submit"]').prop('disabled',false);
                        $(".priority").parent().next(".validation").remove();
                   }
                }
            });
        }
    });
});


/*$(document).on('focusout', '.category', function() {

    var category = $(".category").val();
    var priority = $(".priority").val();
    var id = $("#id").val();

    if(priority != ''){

            $.ajax({
            type: 'post',
            url: '{{route("admin.ImageCategoryPriority")}}',
            data: {
                'category': category,
                'priority': priority,
                'id' : id
            },
            success: function(data) {
               if(data == 'true'){

                    $(':input[type="submit"]').prop('disabled', true);
                    if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                    {
                        $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                    }
                    
               }else{
                    $(':input[type="submit"]').prop('disabled',false);
                    $(".priority").parent().next(".validation").remove();
               }
            }
        });
    }
});*/
</script>
@endsection