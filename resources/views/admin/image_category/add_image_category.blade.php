@extends('layouts.admin.admin')
@section('title','Add Image Category | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Add Image Category</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Add Image Category
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.saveImageCategory') }}" enctype="multipart/form-data" id="category_image">
                            @csrf
                            <div class="form-group">
                                <label for="inputProjectName">Category Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control category" name="category_name" id="inputProjectName" placeholder="Project Name" required>
                            </div>

                            <div class="form-group">
                                <label for="inputPriority">Priority</label>
                                <input type="tel" class="form-control priority" name="priority" oninvalid="this.setCustomValidity('Please Enter Priority')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputPriority"  placeholder="Enter Priority" >
                                   
                            </div>
                                
                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Catergory Cover<span class="mendetory">*</span> (1900x400)</label>
                                <input type="file" class="form-control dropify" name="category_banner" required="">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="submit" name="add_new" value="add_new" class="btn btn-info">Save & Add New</button>
                                <a href="{{route('admin.imageCategoryList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function() {
    $(".priority").keyup(function(){

        $(".priority").parent().next(".validation").remove();

        var priority = $(".priority").val();

        if(priority != ''){
            $.ajax({
                type: 'post',
                url: 'admin-image-category-priority',
                data: {
                    /*'category': category,*/
                    'priority': priority
                },
                success: function(data) {
                   if(data == 'true'){

                        $(':input[type="submit"]').prop('disabled', true);
                        if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                        {
                            $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                        }
                        
                        $('.priority').focus();
                   }else{
                        $(':input[type="submit"]').prop('disabled',false);
                        $(".priority").parent().next(".validation").remove();
                   }
                }
            });
        }
    });
});

/*$(document).on('focusout','.category',function(){

    var category = $(".category").val();
    var priority = $(".priority").val();
    if(priority != ''){

            $.ajax({
            type: 'post',
            url: 'admin-image-category-priority',
            data: {
                'category': category,
                'priority': priority
            },
            success: function(data) {
               if(data == 'true'){

                    $(':input[type="submit"]').prop('disabled', true);
                    if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                    {
                        $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                    }
                    
               }else{
                    $(':input[type="submit"]').prop('disabled',false);
                    $(".priority").parent().next(".validation").remove();
               }
            }
        });
    }
});*/
</script>
@endsection