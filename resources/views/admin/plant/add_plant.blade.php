@extends('layouts.admin.admin')
@section('title','Add Plant| Hindustan RMC')
@section('content')
<style type="text/css">
#map-canvas {
margin-top: 15px !important;
margin-bottom: 15px !important;
height: 300px;
}
.gmnoprint .gm-style-mtc,.gmnoprint .gm-style-mtc,.gmnoprint .gm-style-cc, .gmnoprint .gm-style-cc{
    display: none;
}
</style>
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Add Plant</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Add Plant
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.insertPlant') }}" enctype="multipart/form-data" id="plantForms">
                            @csrf

                            <div class="form-group">
                                <label for="inputPlantName">Plant Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control plant_name" name="plant_name" id="inputPlantName" placeholder="Plant Name" required>
                            </div>

                            <div class="form-group">
                                <label for="inputPriority">Priority</label>
                                <input type="tel" class="form-control priority" name="priority" oninvalid="this.setCustomValidity('Please Enter Priority')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputPriority"  placeholder="Enter Priority">
                            </div>
                            
                            <div class="form-group">
                                <label class="cstm-switch" style="margin-top: 10px;">
                                    <input type="checkbox" name="plant_status" class="cstm-switch-input" id="cstm-switch-input-credit">
                                    <span class="cstm-switch-indicator"></span>
                                    <span class="cstm-switch-description">Working / Upcoming</span>
                                </label>
                            </div>

                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Plant Image<span class="mendetory">*</span> (370x220)</label>
                                <input type="file" class="form-control dropify" name="plant_image" required="">
                            </div>

                           
                            <div class="form-group">
                                <label class="inputProjectGallery">Plant Gallery Photos (400x520)</label>
                                <div class="col-md-12" id="fine-uploader-gallery">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputTelephone">Telephone<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="telephone"  id="inputTelephone"  placeholder="Enter Telephone">
                            </div>
                                
                            <input type="hidden" name="latitude" id="latitude"> 
                  
                            <input type="hidden" name="longitude" id ="longitude">  

                            <input type="hidden" id ="city" name="city_name"/>
                          
                            <input type="hidden" id ="state" name="state_name"/>

                            <input type="hidden" id ="country" name="country"/>

                            <div class="form-group">
                                <label for="inputPlantAddress">Address<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="address" id="pacinput" placeholder="Address" required="required">
                            </div>  

                            <div class="form-group">
                                <div id="map-canvas"></div>
                            </div>

                            <div class="form-group">
                                <label for="inputPlantEmail">Email ID<span class="mendetory">*</span></label>
                                <input type="email" class="form-control email" name="email" id="inputPlantEmail" placeholder="Email ID" required>
                            </div>

                            <div class="form-group">
                                <label for="inputPlantInchanrge">Plant Incharge<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="plant_incharge" id="inputPlantInchanrge" placeholder="Plant Inchanrge" required>
                            </div>

                            <div class="form-group">
                                <label for="inputQualityHead">Quality Head<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="quality_head" id="inputQualityHead" placeholder="Plant Inchanrge" required>
                            </div>

                            <div class="form-group">
                                <label for="inputCertification">ISO Certification</label>
                                <input type="text" class="form-control" name="iso_certification" id="inputCertification" placeholder="ISO Certification">
                            </div>

                            <div class="form-group">
                                <label for="inputDate">Year of Establishment</label>
                                <input type="text" class="form-control" name="year_of_establish" placeholder="Year Of Establishment">
                            </div>

                            <div class="form-group">
                                <label for="inputDate">Track Record</label>
                                <input type="text" class="form-control" name="track_record" placeholder="Track Record">
                            </div>

                            <div class="form-group">
                                <label for="inputGovernMentApproval">Government Approval</label>
                                <input type="text" class="form-control" name="government_approval" id="inputGovernMentApproval" placeholder="Government Approval">
                            </div>

                            <div class="form-group">
                                <label for="inputPlantMake">Plant Make</label>
                                <input type="text" class="form-control" name="plant_make" id="inputPlantMake" placeholder="Plant Make">
                            </div>

                            <div class="form-group">
                                <label for="inputPlantCapacity">Plant Capacity</label>
                                <input type="text" class="form-control" name="plant_capacity" id="inputPlantCapacity" placeholder="Plant Capacity">
                            </div>

                            <div class="form-group">
                                <label for="inputMaximumSupply">Maximum Supply/Day</label>
                                <input type="text" class="form-control" name="maximum_supply" id="inputMaximumSupply" placeholder="Maximum Supply/Day">
                            </div>

                            <div class="form-group">
                                <label for="inputElecticSupplyCondition">Electric Supply Condition</label>
                                <input type="text" class="form-control" name="electric_supply_condition" id="inputElecticSupplyCondition" placeholder="Electric Supply Condition">
                            </div>

                            <div class="form-group">
                                <label for="inputWaterSupplyCondition">Water Supply Condition</label>
                                <input type="text" class="form-control" name="water_supply_condition" id="inputWaterSupplyCondition" placeholder="Water Supply Condition">
                            </div>

                            <div class="form-group">
                                <label for="inputNoOfTransistMixer">No. of Transist Mixer</label>
                                <input type="text" class="form-control" name="no_trans_mixer" id="inputNoOfTransistMixer" placeholder="No. of Transist Mixer">
                            </div>

                            <div class="form-group">
                                <label for="inputNoOfBoomPlacer">No. of Boom Placer</label>
                                <input type="text" class="form-control" name="no_boom_placer" id="inputNoOfBoomPlacer" placeholder="No. of Boom Placer">
                            </div>

                            <div class="form-group">
                                <label for="inputNoOfConcretePump">No. of Concrete Pump</label>
                                <input type="text" class="form-control" name="no_concrete_pump" id="inputNoOfConcretePump" placeholder="No. of Concrete Pump">
                            </div>

                            <div class="form-group">
                                <label for="inputPipeLineLength">Pipe Line Length (Metre)</label>
                                <input type="text" class="form-control" name="pipeline_length" id="inputPipeLineLength" placeholder="Pipe Line Length (Metre)">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.plantList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function() {
    $(".priority").keyup(function(){

        $(".priority").parent().next(".validation").remove();
        var priority = $(".priority").val();

        if(priority != ''){
            $.ajax({
                type: 'post',
                url: 'admin-plant-priority',
                data: {
                    'priority': priority
                },
                success: function(data) {
                   if(data == 'true'){

                        $(':input[type="submit"]').prop('disabled', true);
                        if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                        {
                            $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                        }
                        
                        $('.priority').focus();
                   }else{
                        $(':input[type="submit"]').prop('disabled',false);
                        $(".priority").parent().next(".validation").remove();
                   }
                }
            });
        }
    });
});
/*
$(document).on('focusout', '.plant_name', function() {
    
    var plant_name = $(".plant_name").val();
    var priority = $(".priority").val();
    if(priority != ''){

            $.ajax({
            type: 'post',
            url: 'admin-plant-priority',
            data: {
                'plant_name': plant_name,
                'priority': priority
            },
            success: function(data) {
               if(data == 'true'){

                    $(':input[type="submit"]').prop('disabled', true);
                    if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                    {
                        $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                    }
                    
               }else{
                    $(':input[type="submit"]').prop('disabled',false);
                    $(".priority").parent().next(".validation").remove();
               }
            }
        });
    }
});*/
</script>
@endsection