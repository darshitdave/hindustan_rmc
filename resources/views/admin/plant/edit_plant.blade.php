@extends('layouts.admin.admin')
@section('title','
Edit Plant| Hindustan RMC')
@section('content')
<style type="text/css">
#map-canvas {
margin-top: 15px !important;
margin-bottom: 15px !important;
height: 300px;
}
.gmnoprint .gm-style-mtc,.gmnoprint .gm-style-mtc,.gmnoprint .gm-style-cc, .gmnoprint .gm-style-cc{
    display: none;
}
</style>
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                    Edit Plant</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Edit Plant
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.updatePlant') }}" enctype="multipart/form-data" id="plantForms">
                            @csrf

                            <div class="form-group">
                                <label for="inputPlantName">Plant Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control plant_name" name="plant_name" id="inputPlantName" placeholder="Plant Name" value="{{$get_plant->plant_name}}" required>
                                <input type="hidden" name="id" id="id" value="{{$get_plant->id}}">
                            </div>

                            <div class="form-group">
                                <label for="inputPriority">Priority</label>
                                <input type="tel" class="form-control priority" name="priority" oninvalid="this.setCustomValidity('Please Enter Priority')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputPriority"  placeholder="Enter Priority" value="{{$get_plant->priority}}">
                            </div>

                            <div class="form-group">
                                <label class="cstm-switch" style="margin-top: 10px;">
                                    <input type="checkbox" name="plant_status" class="cstm-switch-input" id="cstm-switch-input-credit" @if($get_plant->plant_status == 1) checked=checked @endif>
                                    <span class="cstm-switch-indicator"></span>
                                    <span class="cstm-switch-description">Working / Upcoming</span>
                                </label>
                            </div>

                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Plant Image<span class="mendetory">*</span> (370x220)</label>
                                <input type="file" class="form-control dropify" name="plant_image" data-show-remove="false"   @if($get_plant->plant_image != '') data-show-remove="false"  data-default-file="{{asset('uploads/plant_image/'.$get_plant->plant_image)}}" @else required="required" @endif>
                            </div>

                            <div class="form-group">
                                <label class="PlantGalleryPhotos">Plant Gallery Photos (400x520)</label>
                                <div class="col-md-12" id="fine-uploader-gallery">
                                </div>
                            </div>

                            @if(count($get_plant_image) > 0)
                            <div class="form-group"> 
                                <label class="PlantGalleryPhotos">Plant Gallery Photos</label>
                                @foreach($get_plant_image as $key => $plant_id)
                                  <div class="col-md-3 remove_image_{{$plant_id->id}}">
                                    
                                    <a href="javascript:void(0);" id="close" class="remove" data-id="{{$plant_id->id}}" data-value="{{$plant_id->plant_image}}"><i class="mdi mdi-close-box" style="margin-left: 74px;"></i></a>

                                    
                                    <a href="{{asset('uploads/Plant_Galllery/'.$plant_id->plant_image)}}" target="_blank"><img src="{{asset('uploads/Plant_Galllery/'.$plant_id->plant_image)}}" target="_blank" class="img-thumbnail img-fluid" style="width: 100%;"></a>

                                   
                                  </div>
                                @endforeach
                            </div>
                            @endif

                            <div class="form-group">
                                <label for="inputTelephone">Telephone<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="telephone"  id="inputTelephone"  placeholder="Enter Telephone" value="{{$get_plant->telephone}}">
                            </div>
                                
                            <input type="hidden" name="latitude" id="latitude" value="{{$get_plant->latitude}}"> 
                  
                            <input type="hidden" name="longitude" id ="longitude" value="{{$get_plant->longitude}}">  

                            <input type="hidden" id ="city" name="city_name" value="{{$get_plant->city_name}}"/>
                          
                            <input type="hidden" id ="state" name="state_name" value="{{$get_plant->state_name}}"/>

                            <input type="hidden" id ="country" name="country" value="{{$get_plant->country}}" />

                            <div class="form-group">
                                <label for="inputPlantAddress">Address<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="address" id="pacinput" placeholder="Address" required="required" value="{{$get_plant->address}}">
                            </div>  

                            <div class="form-group">
                                <div id="map-canvas"></div>
                            </div>

                            <!-- <div class="form-group">
                                <label for="inputPlantAddress">Address<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="address" placeholder="Address" id="inputPlantAddress" spellcheck="false" required="required">{{$get_plant->address}}</textarea>
                            </div>   -->

                            <div class="form-group">
                                <label for="inputPlantEmail">Email ID<span class="mendetory">*</span></label>
                                <input type="email" class="form-control email" name="email" id="inputPlantEmail" placeholder="Email ID" required value="{{$get_plant->email_id}}">
                            </div>

                            <div class="form-group">
                                <label for="inputPlantInchanrge">Plant Incharge<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="plant_incharge" id="inputPlantInchanrge" placeholder="Plant Inchanrge" value="{{$get_plant->plant_incharge}}" required>
                            </div>

                            <div class="form-group">
                                <label for="inputQualityHead">Quality Head<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="quality_head" id="inputQualityHead" placeholder="Plant Inchanrge" required value="{{$get_plant->quality_head}}" required>
                            </div>

                            <div class="form-group">
                                <label for="inputCertification">ISO Certification</label>
                                <input type="text" class="form-control" name="iso_certification" id="inputCertification" placeholder="ISO Certification" value="{{$get_plant->iso_certificate}}">
                            </div>

                            <div class="form-group">
                                <label for="inputDate">Year of Establishment</label>
                                <input type="text" class="form-control" name="year_of_establish" placeholder="Year Of Establishment" value="{{$get_plant->year_of_establish}}">
                            </div>

                            <div class="form-group">
                                <label for="inputDate">Track Record</label>
                                <input type="text" class="form-control" name="track_record" placeholder="Track Record" value="{{$get_plant->track_record}}">
                            </div>

                            <div class="form-group">
                                <label for="inputGovernMentApproval">Government Approval</label>
                                <input type="text" class="form-control" name="government_approval" id="inputGovernMentApproval" placeholder="Government Approval" value="{{$get_plant->government_approval}}">
                            </div>

                            <div class="form-group">
                                <label for="inputPlantMake">Plant Make</label>
                                <input type="text" class="form-control" name="plant_make" id="inputPlantMake" placeholder="Plant Make" value="{{$get_plant->plant_make}}">
                            </div>

                            <div class="form-group">
                                <label for="inputPlantCapacity">Plant Capacity</label>
                                <input type="text" class="form-control" name="plant_capacity" id="inputPlantCapacity" placeholder="Plant Capacity" value="{{$get_plant->plant_capacity}}">
                            </div>

                            <div class="form-group">
                                <label for="inputMaximumSupply">Maximum Supply/Day</label>
                                <input type="text" class="form-control" name="maximum_supply" id="inputMaximumSupply" placeholder="Maximum Supply/Day" value="{{$get_plant->maximum_supply}}">
                            </div>

                            <div class="form-group">
                                <label for="inputElecticSupplyCondition">Electric Supply Condition</label>
                                <input type="text" class="form-control" name="electric_supply_condition" id="inputElecticSupplyCondition" placeholder="Electric Supply Condition" value="{{$get_plant->electric_supply_condition}}">
                            </div>

                            <div class="form-group">
                                <label for="inputWaterSupplyCondition">Water Supply Condition</label>
                                <input type="text" class="form-control" name="water_supply_condition" id="inputWaterSupplyCondition" placeholder="Water Supply Condition" value="{{$get_plant->water_supply_condition}}">
                            </div>

                            <div class="form-group">
                                <label for="inputNoOfTransistMixer">No. of Transist Mixer</label>
                                <input type="text" class="form-control" name="no_trans_mixer" id="inputNoOfTransistMixer" placeholder="No. of Transist Mixer" value="{{$get_plant->no_trans_mixer}}">
                            </div>

                            <div class="form-group">
                                <label for="inputNoOfBoomPlacer">No. of Boom Placer</label>
                                <input type="text" class="form-control" name="no_boom_placer" id="inputNoOfBoomPlacer" placeholder="No. of Boom Placer" value="{{$get_plant->no_boom_placer}}">
                            </div>

                            <div class="form-group">
                                <label for="inputNoOfConcretePump">No. of Concrete Pump</label>
                                <input type="text" class="form-control" name="no_concrete_pump" id="inputNoOfConcretePump" placeholder="No. of Concrete Pump" value="{{$get_plant->no_concrete_pump}}">
                            </div>

                            <div class="form-group">
                                <label for="inputPipeLineLength">Pipe Line Length (Metre)</label>
                                <input type="text" class="form-control" name="pipeline_length" id="inputPipeLineLength" placeholder="Pipe Line Length (Metre)" value="{{$get_plant->pipeline_length}}">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.plantList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function() {
    $(".priority").keyup(function(){

        $(".priority").parent().next(".validation").remove();

        var priority = $(".priority").val();
        var id = $("#id").val();

        if(priority != ''){
            $.ajax({
                type: 'post',
                url: 'admin-plant-priority',
                data: {
                    'priority': priority,
                    'id' : id
                },
                success: function(data) {
                    if(data == 'true'){

                        $(':input[type="submit"]').prop('disabled', true);
                        if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                        {
                            $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                        }
                        
                        $('.priority').focus();
                    }else{
                        $(':input[type="submit"]').prop('disabled',false);
                        $(".priority").parent().next(".validation").remove();
                    }
                }
            });
        }
    });
});

/*$(document).on('focusout', '.plant_name', function() {
    
    var plant_name = $(".plant_name").val();
    var priority = $(".priority").val();
    var id = $("#id").val();

    if(priority != ''){

            $.ajax({
            type: 'post',
            url: 'admin-plant-priority',
            data: {
                'plant_name': plant_name,
                'priority': priority,
                'id' : id
            },
            success: function(data) {
               if(data == 'true'){

                    $(':input[type="submit"]').prop('disabled', true);
                    if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                    {
                        $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                    }
                    
               }else{
                    $(':input[type="submit"]').prop('disabled',false);
                    $(".priority").parent().next(".validation").remove();
               }
            }
        });
    }
});*/
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDbX_JirTlqgj9BO002nMah8CQSD7f4ypI&signed_in=true&libraries=places"></script>
<script type="text/javascript">
    var isPlaceAuthentic = false;
    var lastPlaceAuthenticated = '';
   
    function initialize() {
        var input = (document.getElementById('pacinput'));
        var autocomplete = new google.maps.places.Autocomplete(input);
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            infowindow.close();
            var place = autocomplete.getPlace();
            isPlaceAuthentic = true;
            lastPlaceAuthenticated = $('#pacinput').val();
            isPlaceAuthentic = false;
            var address = '';
            var toInsertData = place.adr_address;
            var latti = place.geometry.location.lat();
            var longi = place.geometry.location.lng();
            var addressToInsert = toInsertData.substr(0, toInsertData.indexOf('<')).trim();
            if (toInsertData.indexOf('<') !== 1) {
                toInsertData = toInsertData.substr(toInsertData.indexOf('<'));
            }
            var streetAddress = $(toInsertData).filter('.street-address').text().trim();
            var extendedAddress = $(toInsertData).filter('.extended-address').text().trim();
            var cityName = $(toInsertData).filter('.locality').text().trim();
            var stateName = $(toInsertData).filter('.region').text().trim();
            var countryName = $(toInsertData).filter('.country-name').text().trim();

            $("#latitude").val(latti);
            $("#longitude").val(longi);
            $("#city").val(cityName);
            $("#state").val(stateName);
            $("#country").val(countryName);
            initMap(latti,longi);
            var appenedAddress = addressToInsert.concat(streetAddress);
            appenedAddress = appenedAddress.concat(extendedAddress);
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
    $("#area_submit").on('click',function(e){
        if(($("#latitude").val() == "" || $("#longitude").val() == "")){
            alert("Please Enter Valid Address");
            e.preventDefault();
        }
    });
    

    
    function initMap(latitude,longitude) {
        var myLatLng = {lat: latitude, lng: longitude};
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 18,
          center: myLatLng
        });
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            document.getElementById("lat").value = this.getPosition().lat();
            document.getElementById("long").value = this.getPosition().lng();
        });
    }
</script>
@if($get_plant->latitude != '' && $get_plant->longitude != '')
    <script type="text/javascript">
        initMap({{ $get_plant->latitude }},{{ $get_plant->longitude }});
    </script>
@endif

<script>
$(document).on('click', '.remove', function() {
    $.ajaxSetup({ 
          headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
    });
    
    if(confirm("Are you sure want to delete this file ?")){
        var id = $(this).data('id');
        var current_image = $(this).data('value');
       
        $.ajax({
            type: 'delete',
            url: '{{route("admin.plant.removefile")}}',
            data: {
                'id': id
            },
            success: function(data) {
               if(data == 'true'){
                
                $('.remove_image_'+id).remove(); 

              }
            }
        });
    }
});
</script>
@endsection