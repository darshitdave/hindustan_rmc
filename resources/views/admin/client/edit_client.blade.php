@extends('layouts.admin.admin')
@section('title','Edit Client | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Edit Client</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Edit Client
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.saveEditedClient') }}" enctype="multipart/form-data" id="category_image">
                            @csrf
                            <div class="form-group">
                                <label for="inputProjectName">Client Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="client_name" id="inputProjectName" placeholder="Client Name" value="{{$get_client->client_name}}" required>
                                <input type="hidden" name="id" id="id" value="{{$get_client->id}}">
                            </div>

                            <div class="form-group">
                                <label for="inputPriority">Priority</label>
                                <input type="tel" class="form-control priority" name="priority" oninvalid="this.setCustomValidity('Please Enter Priority')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputPriority"  placeholder="Enter Priority" value="{{$get_client->priority}}">
                                
                            </div>
                                
                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Catergory Cover<span class="mendetory">*</span> (180x70)</label>
                                <input type="file" class="form-control dropify" name="company_logo"   @if($get_client->company_logo != '') data-show-remove="false"  data-default-file="{{asset('uploads/company_logo/'.$get_client->company_logo)}}" @endif>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.clientList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function() {
    $(".priority").keyup(function(){

        $(".priority").parent().next(".validation").remove();

        var priority = $(".priority").val();
        var id = $("#id").val();

        if(priority != ''){
            $.ajax({
                type: 'post',
                url: '{{route("admin.ClientPriority")}}',
                data: {
                    'priority': priority,
                    'id' : id
                },
                success: function(data) {
                   if(data == 'true'){

                        $(':input[type="submit"]').prop('disabled', true);
                        if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                        {
                            $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                        }
                        
                        $('.priority').focus();
                   }else{
                        $(':input[type="submit"]').prop('disabled',false);
                        $(".priority").parent().next(".validation").remove();
                   }
                }
            });
        }
    });
});

</script>
@endsection