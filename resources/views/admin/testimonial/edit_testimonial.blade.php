@extends('layouts.admin.admin')
@section('title','Edit Certificates and Recognitions| Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Edit Certificates and Recognitions</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Edit Certificates and Recognitions
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.updateTestimonial') }}" enctype="multipart/form-data" id="testimonialForms">
                            @csrf

                            <div class="form-group">
                                <label for="inputCategoryId">Category Name<span class="mendetory">*</span></label>
                                <select class="form-control category" name="category_id" required="required">
                                    <option value="" >Select Category Name</option>
                                    <option value="0" @if($get_testimonial->category_type == 0) selected="selected" @endif>Certificates</option>
                                    <option value="1" @if($get_testimonial->category_type == 1) selected="selected" @endif>Recognitions</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="inputSiteName">Site Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control site_name" name="site_name" id="inputSiteName" placeholder="Site Name" required value="{{$get_testimonial->site_name}}">
                                <input type="hidden" name="id" id="id" value="{{$get_testimonial->id}}">
                            </div>

                            <div class="form-group">
                                <label for="inputCompanyName">Company Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="company_name" id="inputSiteName" placeholder="Company Name" required value="{{$get_testimonial->company_name}}">
                            </div>

                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Image<span class="mendetory">*</span>  (260x270)</label>
                                <input type="file" class="form-control dropify" name="image" data-show-remove="false"   @if($get_testimonial->image != '') data-show-remove="false"  data-default-file="{{asset('uploads/testimonial_image/'.$get_testimonial->image)}}" @else required="required" @endif>
                            </div>

                            <div class="form-group">
                                <label for="inputPriority">Priority</label>
                                <input type="tel" class="form-control priority" name="priority" oninvalid="this.setCustomValidity('Please Enter Priority')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputPriority"  placeholder="Enter Priority" value="{{$get_testimonial->priority}}">
                            </div>

                            <div class="form-group">
                                <label for="inputProjectDescription">Description<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control" name="description" placeholder="Description" id="inputProjectDescription" spellcheck="false" required="required">{{$get_testimonial->description}}</textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.testimonialList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function() {
    $(".priority").keyup(function(){

        $(".priority").parent().next(".validation").remove();

        var priority = $(".priority").val();
        var category_id = $(".category").val();
        var id = $("#id").val();

        if(priority != ''){
            $.ajax({
                type: 'post',
                url: '{{route("admin.checkTestimonialPriority")}}',
                data: {
                    'priority': priority,
                    'category_id': category_id,
                    'id': id
                },
                success: function(data) {
                   if(data == 'true'){

                        $(':input[type="submit"]').prop('disabled', true);
                        if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                        {
                            $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                        }
                        
                        $('.priority').focus();
                   }else{
                        $(':input[type="submit"]').prop('disabled',false);
                        $(".priority").parent().next(".validation").remove();
                   }
                }
            });
        }
    });
});

$(document).on('change', '.category', function() {

    var category_id = $(".category").val();
    var priority = $(".priority").val();
    var id = $("#id").val();

    if(priority != ''){

            $.ajax({
            type: 'post',
            url: "{{route('admin.checkTestimonialPriority')}}",
            data: {
                'category_id': category_id,
                'priority': priority,
                'id' : id
            },
            success: function(data) {
               if(data == 'true'){

                    $(':input[type="submit"]').prop('disabled', true);
                    if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                    {
                        $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                    }
                    
               }else{
                    $(':input[type="submit"]').prop('disabled',false);
                    $(".priority").parent().next(".validation").remove();
               }
            }
        });
    }
});
</script>
@endsection