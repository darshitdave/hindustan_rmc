@extends('layouts.admin.admin')
@section('title','Edit Gallery | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Edit Gallery</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Edit Gallery
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.saveEditedGallery') }}" enctype="multipart/form-data" id="projectForms">
                            @csrf
                            
                            <input type="hidden" name="id" value="{{ $editGallery->id }}">
                                
                            <div class="form-group">
                                <label for="inputProjectProfilePhoto">Gallery Image<span class="mendetory">*</span> (1170x680)</label>
                                <input type="file" class="form-control dropify" name="gallery_image"  data-default-file="{{ asset('uploads/gallery_image') }}/{{ $editGallery->image }}" required="">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.galleryList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
