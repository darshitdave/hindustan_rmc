@extends('layouts.admin.admin')
@section('title','NewsLetter Subscription List | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        NewsLetter Subscription List
                    </h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container pull-up">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Email ID</th>
                                    <th>Date of subscription</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($new_letter_list))
                                    @foreach($new_letter_list as $nk => $nv)
                                    
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $nv->email_id }}</td>
                                            <?php $date1 = date('d-m-Y h:i:s',strtotime($nv->created_at)); ?>
                                            <td>{{ $date1 }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')

@endsection