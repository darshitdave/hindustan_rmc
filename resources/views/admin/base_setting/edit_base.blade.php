@extends('layouts.admin.admin')
@section('title','Base Setting | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Base Setting</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Base Setting
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.baseSetting') }}" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    Cement Rate :
                                </div>
                                <div class="form-group col-md-2">
                                    
                                    <input type="text" name="cement_rate" class="form-control cement_rate" @if(isset($get_last->cement_rate) && $get_last->cement_rate != '') value="{{$get_last->cement_rate}}" @else value="" @endif>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control" placeholder="/50 Kg" disabled="disabled">
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" name="cement_kg" class="form-control cement_kg" @if(isset($get_last->cement_kg) && $get_last->cement_kg != '') value="{{$get_last->cement_kg}}" @else value="" @endif>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control" placeholder="/Kg" disabled="disabled">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    Flyash Rate : 
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" name="flyash_rate" class="form-control flyash_rate" @if(isset($get_last->flyash_rate) && $get_last->flyash_rate != '') value="{{$get_last->flyash_rate}}" @else value="" @endif>
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" placeholder="/1000 Kg" disabled="disabled">
                                </div>
                                <div class="form-group col-md-2">

                                    <input type="text" name="flyash_kg" class="form-control flyash_kg" @if(isset($get_last->flyash_kg) && $get_last->flyash_kg != '') value="{{$get_last->flyash_kg}}" @else value="" @endif>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control" placeholder="/Kg" disabled="disabled">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    GST :
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text" name="gst_percent" class="form-control" @if(isset($get_last->gst) && $get_last->gst != '') value="{{$get_last->gst}}" @else value="" @endif>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control" placeholder="in %" disabled="disabled" >
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function() {
    $(".cement_rate").keyup(function(){
        
        var cement_rate = $(".cement_rate").val();

        if(cement_rate != ''){
           total_kg =  parseInt(cement_rate) / 50;
           
           $('.cement_kg').val(total_kg);
        }
    });
});

$(document).ready(function() {
    $(".flyash_rate").keyup(function(){
        
        var flyash_rate = $(".flyash_rate").val();

        if(flyash_rate != ''){
           total_kg =  parseInt(flyash_rate) / 1000;
           
           $('.flyash_kg').val(total_kg);
        }
    });
});
</script>
@endsection