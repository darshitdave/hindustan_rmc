@extends('layouts.admin.admin')
@section('title','Edit Blog| Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Edit Blog</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                            Edit Blog
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.updateBlog') }}" enctype="multipart/form-data" id="blogForms">
                            @csrf

                            <div class="form-group">
                                <label>Title<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="title" placeholder="Title" value="{{$get_blog->title}}" required>
                            </div>

                            <div class="form-group">
                                <label for="inputPriority">Priority</label>
                                <input type="tel" class="form-control priority" name="priority" oninvalid="this.setCustomValidity('Please Enter Priority')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" id="inputPriority"  placeholder="Enter Priority" value="{{$get_blog->priority}}">
                                <input type="hidden" name="id" id="id" value="{{$get_blog->id}}">
                            </div>

                            <div class="form-group">
                                <label for="inputDate">Date<span class="mendetory">*</span></label>
                                <input type="text" class="js-datepicker form-control" name="blog_date" placeholder="Date" value="{{$get_blog->date}}">
                            </div>

                            <div class="form-group">
                                <label>Author Name<span class="mendetory">*</span></label>
                                <input type="text" class="form-control" name="author_name" placeholder="Author Name" value="{{$get_blog->author_name ? $get_blog->author_name : 'Hindustan RMC'}}">
                            </div>

                            <div class="form-group">
                                <label>Blog Image<span class="mendetory">*</span> (1170x684)</label>
                                <input type="file" class="form-control dropify" name="blog_image" @if($get_blog->blog_image != '') data-show-remove="false" data-default-file="{{asset('uploads/blog_image/'.$get_blog->blog_image)}}" @endif>
                            </div>

                            <div class="form-group">
                                <label>Description<span class="mendetory">*</span></label>
                                <textarea type="text" class="form-control ckeditor" name="description" placeholder="Description">{{$get_blog->description}}</textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('admin.blogList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).ready(function() {
    $(".priority").keyup(function(){

        $(".priority").parent().next(".validation").remove();

        var priority = $(".priority").val();

        if(priority != ''){
            $.ajax({
                type: 'post',
                url: "{{route('admin.blogPriority')}}",
                data: {
                    'priority': priority
                },
                success: function(data) {
                   if(data == 'true'){

                        $(':input[type="submit"]').prop('disabled', true);
                        if ($(".priority").parent().next(".validation").length == 0) // only add if not added
                        {
                            $(".priority").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Priority Already Exists!</div>");
                        }
                        
                        $('.priority').focus();
                   }else{
                        $(':input[type="submit"]').prop('disabled',false);
                        $(".priority").parent().next(".validation").remove();
                   }
                }
            });
        }
    });
});

</script>
@endsection