@extends('layouts.admin.admin')
@section('title','Blog List | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">
                        Blog List
                    </h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container pull-up">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive p-t-10">
                            <table id="example-multi" class="table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Title</th>
                                    <th>Priotity</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($get_blog))
                                    @foreach($get_blog as $gk => $gv)
                                    
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $gv->title }}</td>
                                            <td>{{ $gv->priority }}</td>
                                            <td>{{ $gv->date }}</td>
                                            <td>
                                                @php $checked = ''; @endphp
                                                @if($gv->is_active == 1)
                                                  @php $checked = 'checked'; @endphp
                                                @endif
                                                <label class="cstm-switch ">
                                                    <input type="checkbox" name="option" value="1" class="cstm-switch-input blog_content" data-id="{{ $gv->id }}" {{ $checked }}>
                                                    <span class="cstm-switch-indicator size-md "></span>
                                                    <span class="cstm-switch-description"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.editBlog',$gv->id) }}" class="btn m-b-15 ml-2 mr-2 btn-dark"><i class="fe fe-edit"></i></a>
                                                <a href="{{ route('admin.removeBlog',$gv->id) }}" class="btn m-b-15 ml-2 mr-2 btn-dark" onclick="return confirm('Are your sure want to delete this Blog Detail ?')"><i class="fe fe-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).on('change', '.blog_content', function() {

    var id = $(this).data('id');
    var current_status = $(this).data('value');
    var status;
   
    if($(this).prop("checked") == true){
      status = 1;
    } else {
      status = 0;
    }

    $.ajax({
        type: 'post',
        url: 'change-blog-status',
        data: {
            'id': id,
            'status': status
        },
        success: function(data) {
           if(data == 'true'){
                if(status == 1){
                    msg = 'Status Enabled!';
                } else {
                    msg = 'Status Disabled!';
                }
            } else {        
               msg = 'Something Went Wrong';
            }

            $.notify({ title: '', message: msg},{
            placement: {
                align: "right",
                from: "top"
            },
                timer: 500,
                type: 'success',
            });
        }
    });
});
</script>
@endsection