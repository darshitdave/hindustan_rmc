@extends('layouts.admin.admin')
@section('title','Hindustan RMC | Admin Change Password')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">  Change Password</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container pull-up">
        <div class="row">
            <div class="col-lg-6 offset-3">
                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                           Change Password
                        </h5>
                        <p class="m-b-0 text-muted">
                        </p>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" method="POST" action="{{ route('admin.updateAdminPassword') }}" id="changepassword">
                            @csrf   
                            <input type="hidden" name="id" value="{{ Auth::guard('admin')->user()->id }}"> 
                            
                            <div class="form-group">
                                <label for="inputOldpass">Old Password</label>
                                <input type="password" name="old_pass" class="form-control" id="password" placeholder="Password" >
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="inputNewpass">New Password</label>
                                <input type="password" name="new_pass" class="form-control" id="new_pass" placeholder="Confirm Password" >
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="inputNewpass">Confirm Password</label>
                                <input type="password" name="pass_conf" class="form-control" id="password_confirmation" placeholder="Confirm Password" >
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>


                            <div class="form-group">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection  