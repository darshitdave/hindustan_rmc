@extends('layouts.admin.admin')
@section('title','Hindustan RMC | Admin Update Profile')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class=""> Update Profile</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container pull-up">
        <div class="row">
            <div class="col-lg-6 offset-3">
                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                           Profile
                        </h5>
                        <p class="m-b-0 text-muted">
                        </p>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" method="POST" action="{{ route('admin.update.profile') }}" id="profile">
                            @csrf   
                            <input type="hidden" name="id" value="{{ Auth::guard('admin')->user()->id }}"> 

                            <div class="form-group">
                                <label for="inputName">Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{$admin_profile->name}}">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="inputEmail">Email</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{$admin_profile->email}}" readonly>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection  