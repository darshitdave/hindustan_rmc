@extends('layouts.admin.admin')
@section('title','Add Product Setting | Hindustan RMC')
@section('content')
<section class="admin-content">
    <div class="bg-dark">
        <div class="container  m-b-30">
            <div class="row">
                <div class="col-12 text-white p-t-40 p-b-90">
                    <h4 class="">Add Product Setting</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container  pull-up">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">

                <!--widget card begin-->
                <div class="card m-b-30">
                    <div class="card-header">
                        <h5 class="m-b-0">
                          Add Product Setting
                        </h5>
                    </div>
                    <div class="card-body ">
                        <form method="post" action="{{ route('admin.productSetting') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    Select Product Type :
                                </div>
                                <div class="form-group col-md-8">
                                    <select class="form-control category" name="category_id" required="required">
                                        <option value="" selected="selected">Select Product Type</option>
                                        @foreach($sub_products as $sk => $sv)
                                            <option value="{{$sv->id}}">{{$sv->sub_products}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @foreach($product_type as $pk => $pv)
                            <div class="form-row">
                                    <div class="form-group col-md-1">
                                        <input type="checkbox" class="form-control check product_id_{{$pv->id}}" name="product_type_id[]" value="{{$pv->id}}" data-id="{{$pv->id}}"> 
                                    </div>
                                    <div class="form-group col-md-3">
                                            {{$pv->product_types}}
                                    </div>
                                    <div class="form-group col-md-4">
                                        <input type="text" class="form-control basic_rate_{{$pv->id}}" name="basic_rate[]" data-id="{{$pv->id}}" placeholder="Basic Rate" oninvalid="this.setCustomValidity('Please Enter Basic Rate')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" disabled="disabled">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <input type="text" class="form-control minimum_rate_{{$pv->id}}" name="minimum_rate[]" data-id="{{$pv->id}}" placeholder="Minimum Quantity" oninvalid="this.setCustomValidity('Please Enter Minimum Quantity')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" disabled="disabled">
                                    </div>
                            </div>
                            @endforeach
                            <div class="form-group">
                                <button class="btn btn-primary">Save</button>
                                <a href="{{route('admin.productSettingList')}}"  class="btn btn-danger">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
$(document).on('click', '.check', function() {
    var id = $(this).data('id');

    if ($(this).is(":checked")) {

        $('.basic_rate_'+id).removeAttr("disabled");
        $('.minimum_rate_'+id).removeAttr("disabled");
        $('.minimum_rate_'+id).attr("required",true);
        $('.basic_rate_'+id).attr("required",true);
    }else{
        
        $('.basic_rate_'+id).attr("disabled", true);
        $('.basic_rate_'+id).val('');
        $('.minimum_rate_'+id).attr("disabled", true);
        $('.minimum_rate_'+id).val('');
        $('.basic_rate_'+id).attr("required",false);
        $('.minimum_rate_'+id).attr("required",false);
    }
    
});


</script>
@endsection