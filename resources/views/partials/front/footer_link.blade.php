<!-- Modernizer JS -->
<script src="{{ asset('assets/front/js/vendor/modernizr-2.8.3.min.js') }}"></script>

<!-- jQuery JS -->
<script src="{{ asset('assets/front/js/vendor/jquery-3.4.1.min.js') }}"></script>

<!-- Bootstrap JS -->
<script src="{{ asset('assets/front/js/vendor/bootstrap.min.js') }}"></script>

<!-- Popper JS -->
<script src="{{ asset('assets/front/js/vendor/popper.min.js') }}"></script>

<!-- Swiper Slider JS -->
<script src="{{ asset('assets/front/js/plugins/swiper.min.js') }}"></script>

<!-- Tippy JS -->
<script src="{{ asset('assets/front/js/plugins/tippy.min.js') }}"></script>

<!-- Light gallery JS -->
<script src="{{ asset('assets/front/js/plugins/lightgallery.min.js') }}"></script>

<!-- Light gallery video JS -->
<script src="{{ asset('assets/front/js/plugins/lg-video.min.js') }}"></script>

<!-- Waypoints JS -->
<script src="{{ asset('assets/front/js/plugins/waypoints.min.js') }}"></script>

<!-- Counter up JS -->
<script src="{{ asset('assets/front/js/plugins/counterup.min.js') }}"></script>

<!-- Appear JS -->
<script src="{{ asset('assets/front/js/plugins/appear.min.js') }}"></script>

<!-- Gmap3 JS -->
<script src="{{ asset('assets/front/js/plugins/gmap3.min.js') }}"></script>

<!-- Isotope JS -->
<script src="{{ asset('assets/front/js/plugins/isotope.min.js') }}"></script>

<!-- Mailchimp JS -->
<script src="{{ asset('assets/front/js/plugins/mailchimp-ajax-submit.min.js') }}"></script>

<!-- Main JS -->
<script src="{{ asset('assets/front/js/main.js') }}"></script>

<!-- Revolution Slider JS -->
<script src="{{ asset('assets/front/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('assets/front/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('assets/front/revolution/revolution-active.js') }}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="{{ asset('assets/front/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ asset('assets/front/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('assets/front/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('assets/front/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('assets/front/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('assets/front/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!--=====  End of JS files ======-->

