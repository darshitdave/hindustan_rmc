    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/vendor/bootstrap.min.css') }}">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/vendor/font-awesome.min.css') }}">

    <!-- Ionicons CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/vendor/ionicons.min.css') }}">

    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/vendor/flaticon.min.css') }}">

    <!-- Icomoon CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/vendor/icomoon.min.css') }}">

    <!-- Tractor icon CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/vendor/tractor-icon.min.css') }}">

    <!-- Swiper slider CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/plugins/swiper.min.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/plugins/animate.min.css') }}">

    <!-- Light gallery CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/plugins/lightgallery.min.css') }}">


    <!-- Vendor & Plugins CSS (Please remove the comment from below vendor.min.css & plugins.min.css for better website load performance and remove css files from avobe) -->
    <!--
	<link rel="stylesheet" href="assets/css/vendor/vendor.min.css">
	<link rel="stylesheet" href="assets/css/plugins/plugins.min.css">
    -->

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/front/css/style.css') }}">

    <!-- Revolution Slider CSS -->
    <link href="{{ asset('assets/front/revolution/css/settings.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/revolution/css/navigation.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/revolution/custom-setting.css') }}" rel="stylesheet">
    <link href="{{ asset('css/developer.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <!--For toaster -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    