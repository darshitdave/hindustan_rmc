
<div class="footer-area section-space--inner--top--120 dark-bg--style3" style="padding-top: 0px;padding-bottom: 0px !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer-content-wrapper">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <!-- footer intro wrapper -->
                            <div class="footer-intro-wrapper">
                                <div class="footer-logo">
                                    <a href="{{route('front.home.page')}}">
                                        <img src="{{ asset('assets/front/img/logo/HRMC_footer_logo.png') }}" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="footer-desc footer-desc--style2">
                                    Hindustan RMC is one of the largest Ready Mix Concrete manufacturers in Ahmedabad. We own 5 plants covering entire Ahmedabad - Gandhinagar belt. QCI certification, 50 Transit Mixers, 13 Pumps, 24x7 Mobile Quality Van.
                                </div>
                                <a href="{{route('front.company.page')}}" class="see-more-link see-more-link--style3">SEE MORE <i class="ion-android-arrow-forward"></i></a>

                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <!-- footer widget -->
                            <div class="footer-widget">
                                <h4 class="footer-widget__title footer-widget__title--style2">CONTACT US</h4>
                                <ul class="footer-widget__navigation footer-widget__navigation--address">
                                    <!-- <li><i class="ion-android-map"></i><h6>Office Address </h6></li>
                                    <li>Survey No. 414/2, 
Off Shantipura To Bopal 
S.P. Ring Road, Shela, 
Dist. Ahmedabad – 380051 (Guj.)</li> -->
                                    <li><i class="ion-ios-telephone"></i><a href="tel:1800123999977">1800-123-9999-77</a></li>  
                                    <li><i class="ion-android-mail"></i><a href="mailto:info@hindustanrmc.com"> info@hindustanrmc.com</a></li>
                                    <div class="social-links social-links--style3">
                                        <ul>
                                            <li><a href="https://www.facebook.com/Hindustanrmc/" data-tippy="Facebook" data-tippy-inertia="false" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder__black" data-tippy-placement="top"><i class="ion-social-facebook"></i></a></li>
                                            <li><a href="https://twitter.com/hindustanrmc" data-tippy="Twitter" data-tippy-inertia="false" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder__black" data-tippy-placement="top"><i class="ion-social-twitter"></i></a></li>
                                            <li><a href="https://www.instagram.com/hindustanrmc/" data-tippy="Instagram" data-tippy-inertia="false" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder__black" data-tippy-placement="top"><i class="ion-social-instagram"></i></a></li>
                                            <li><a href="https://www.linkedin.com/company/hindustan-infrastructure-solution" data-tippy="Linkedin" data-tippy-inertia="false" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder__black" data-tippy-placement="top"><i class="ion-social-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <!-- footer widget -->
                            <div class="footer-widget">
                                <h4 class="footer-widget__title footer-widget__title--style2">QUICK LINKS</h4>
                                <ul class="footer-widget__navigation footer-widget__navigation--style2">

                                    <li><a href="{{route('front.our_ups.page')}}">USP</a></li>
                                    <li><a href="{{route('front.service_details.page')}}">Services</a></li>

                                    <li><a href="{{route('front.about.technology.page')}}">RMC Process</a></li>
                                    <li><a href="{{route('front.blog_list.page')}}">Blogs</a></li>
                                    

                                    <li><a href="{{route('front.select_calculation.page')}}">RMC Calculator</a></li>
                                    <li><a href="{{route('front.aboutFaq.page')}}">FAQs</a></li>
                                    

                                    <li><a href="{{route('front.client.page')}}">Clients</a></li>
                                    <li><a href="{{route('front.comCareer.page')}}">Careers</a></li>
                                    
                                    
                                    <li><a href="{{route('front.about.milestones.page')}}">Milestones</a></li>
                                    <li><a href="{{route('front.contact_us.page')}}">Contact US</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <!-- footer widget -->
                            <div class="footer-widget">
                                <h4 class="footer-widget__title footer-widget__title--style2">NEWSLETTER</h4>
                                <div class="footer-widget__newsletter">
                                        <input type="email" placeholder="Enter your email" class="news_email" name="EMAIL">
                                        <button type="submit" class="ht-btn ht-btn--default ht-btn--default--style3 border-0 news_button" style="background-color: #EF7F1B;color: #ffff;">Subscribe</button>
                                    
                                    <!-- mailchimp-alerts Start -->
                                    <div class="mailchimp-alerts">
                                        <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                        <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                        <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                                    </div>
                                    <!-- mailchimp-alerts end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="footer-copyright-wrapper">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="footer-copyright-left">
                                © 2020 Hindustan Infrastructure Solution
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="footer-copyright-right">
                                <div class="social-links social-links--style3">
                                    <ul>
                                        <li>
                                            <a href="https://www.finlark.com/" target="_blank" style="font-size: 0.875rem;">
                                                <p>
                                                    <span style="color: #f1f1f1;">Designed & Developed by</span> 
                                                    <b style="color: #ff4157;">Finlark</b>
                                                </p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  mobile menu overlay ====================-->
<div class="mobile-menu-overlay" id="mobile-menu-overlay">
    <div class="mobile-menu-overlay__header">
        <div class="container-fluid--cp-60">
            <div class="row align-items-center">
                <div class="col-md-4 col-sm-6 col-9">
                    <!-- logo -->
                    <div class="logo">
                        <a href="{{route('front.home.page')}}">
                            <img src="{{ asset('assets/front/img/logo/hrmc_logo.png') }}" class="img-fluid" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-md-8 col-sm-6 col-3">
                    <!-- mobile menu content -->
                    <div class="mobile-menu-content">
                        <a class="mobile-navigation-close-icon" id="mobile-menu-close-trigger" href="javascript:void(0)">
                            <i class="ion-ios-close-empty"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-menu-overlay__body">
        <nav class="offcanvas-navigation">
            <ul>
                <li class="">
                    <a href="{{route('front.home.page')}}">Home</a>
                </li>
                <li class="">
                    <a href="{{route('front.company.page')}}">About</a>
                </li>
                
                <li class="">
                    <a href="{{route('front.location_plants.page')}}">Plants</a>
                </li>
                <li class="">
                    <a href="{{route('front.about_projects.page')}}">Projects</a>
                </li>
                
                <li class="has-children">
                    <a href="JavaScript:Void(0);">RMC</a>
                    <ul class="sub-menu">
                        <li><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li><a href="{{route('front.service_details.page')}}">Services</a></li>
                        <li><a href="{{route('front.plant_comparison.page')}}">Plant Comparison</a></li>
                        <li><a href="{{route('front.our_ups.page')}}">Our USPs</a></li>
                    </ul>
                </li>
                <li class="">
                    <a href="{{route('front.comCareer.page')}}">Careers</a>
                </li>
                <li class="">
                    <li><a href="{{route('front.contact_us.page')}}">Contact Us</a></li>
                </li>
                <li class="">
                    <a href="{{route('front.select_calculation.page')}}">GET A QUOTE </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!--====================  End of mobile menu overlay  ====================-->
<!--====================  scroll top ====================-->
<a href="#" class="scroll-top" id="scroll-top">
    <i class="ion-android-arrow-up"></i>
</a>
<!--====================  End of scroll top  ====================-->
