<div class="header-area header-sticky">
    <div class="header-area__desktop">
        <!--=======  header top bar  =======-->
        
        <!--=======  End of header top bar  =======-->
        <!--=======  header navigation area  =======-->
        <div class="header-navigation-area header-navigation-area--style2 position-relative">
            <div class="row no-gutters">
                <div class="col-lg-12 position-static">
                    <div class="header-navigation-container">
                        <!-- logo -->
                        <div class="logo">
                            <a href="{{route('front.home.page')}}">
                                <img src="{{ asset('assets/front/img/logo/hrmc_logo.png') }}" class="img-fluid" alt="" style="height: 35px;">
                            </a>
                        </div>
                        <div class="header-navigation-wrapper header-navigation-wrapper--style4">
                            <div class="header-navigation-wrapper--style3__left">
                                <div class="header-navigation__nav header-navigation__nav--style5 position-static">
                                    <nav>
                                        <ul>
                                            <li class="">
                                                <a href="{{route('front.home.page')}}" class="header_menu">Home</a>
                                            </li>
                                            <li class="">
                                                <a href="{{route('front.company.page')}}" class="header_menu">About</a>
                                            </li>
                                            
                                            <li class="">
                                                <a href="{{route('front.location_plants.page')}}" class="header_menu">Plants</a>
                                            </li>
                                            <li class="">
                                                <a href="{{route('front.about_projects.page')}}" class="header_menu">Projects</a>
                                            </li>
                                            <li class="has-children has-children--multilevel-submenu">
                                                <a href="JavaScript:Void(0);" class="header_menu">RMC</a>
                                                <ul class="submenu">
                                                    <li><a href="{{route('front.about_product.page')}}" class="header_menu">Products</a></li>
                                                    <li><a href="{{route('front.service_details.page')}}">Services</a></li>
                                                    <li><a href="{{route('front.plant_comparison.page')}}" class="header_menu">Plant Comparison</a></li>
                                                    <li><a href="{{route('front.our_ups.page')}}" class="header_menu">USP</a></li>
                                                </ul>
                                            </li>
                                            <li class="">
                                                <a href="{{route('front.comCareer.page')}}" class="header_menu">Careers</a>
                                            </li>
                                            <li class="">
                                                <li><a href="{{route('front.contact_us.page')}}" class="header_menu">Contact Us</a></li>
                                            </li>
                                            
                                        </ul>
                                    </nav>
                                </div>
                                
                            </div>
                            <div class="header-navigation-wrapper--style3__right">
                                <ul class="topbar-info topbar-info--style2" style="margin: auto;">
                                    
                                    <li><a href="tel:1800123999977" style="color: #222;font-weight: 700;"><i class="ion-ios-telephone"></i>1800-123-9999-77</a></li>
                                </ul>
                                <div class="get-quote-button-wrapper">
                                    <a href="{{route('front.select_calculation.page')}}">RMC Calculator <i class="ion-arrow-right-c"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--=======  End of header navigation area =======-->
    </div>
    <div class="header-area__mobile">
        <!--=======  mobile menu  =======-->
        <div class="mobile-menu-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4 col-sm-6 col-9">
                        <!-- logo -->
                        <div class="logo">
                            <a href="{{route('front.home.page')}}">
                                <img src="{{ asset('assets/front/img/logo/hrmc_logo.png') }}" class="img-fluid" alt="" >
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-6 col-3">
                        <!-- mobile menu content -->
                        <div class="mobile-menu-content">
                            <div class="social-links">
                                <ul>
                                    <li><a href="https://www.facebook.com/Hindustanrmc/" data-tippy="Facebook" data-tippy-inertia="false" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder__yellow" data-tippy-placement="bottom"><i class="ion-social-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/hindustanrmc" data-tippy="Twitter" data-tippy-inertia="false" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder__yellow" data-tippy-placement="bottom"><i class="ion-social-twitter"></i></a></li>
                                    <li><a href="https://www.instagram.com/hindustanrmc/" data-tippy="Instagram" data-tippy-inertia="false" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder__yellow" data-tippy-placement="bottom"><i class="ion-social-instagram"></i></a></li>
                                    <li><a href="https://www.linkedin.com/company/hindustan-infrastructure-solution/" data-tippy="Linkedin" data-tippy-inertia="false" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder__yellow" data-tippy-placement="bottom"><i class="ion-social-linkedin"></i></a></li>
                                </ul>
                            </div>
                            <div class="mobile-navigation-icon" id="mobile-menu-trigger">
                                <i></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--=======  End of mobile menu  =======-->
    </div>
</div>