<aside class="admin-sidebar">
    <div class="admin-sidebar-brand">
        <!-- begin sidebar branding-->
        <a href="{{ route('admin.dashboard') }}">
            <img class="admin-brand-logo" src="{{ asset('assets/front/img/logo/hrmc_logo.png') }}"  alt="atmos Logo"></a>
            <!-- end sidebar branding-->
            <div class="ml-auto">
                <!-- sidebar pin-->
                <a href="#" class=""></a>
                <!-- sidebar close for mobile device-->
                <a href="#" class="admin-close-sidebar"></a>
            </div>
        </div>
        <div class="admin-sidebar-wrapper js-scrollbar">
            <ul class="menu">
                
                <li class="menu-item @if(route::is('admin.dashboard')) active @endif ">
                    <a href="{{ route('admin.dashboard') }}" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Dashboard</span>
                        </span>
                        <span class="menu-icon">
                           <i class="icon-placeholder mdi mdi-chart-areaspline"></i>
                       </span>
                   </a>
                </li>  

                <li class="menu-item @if(route::is('admin.genral_inquiry_list') || route::is('admin.rmc_inquiry_list')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Inquiry
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-chat-alert"></i>
                        </span>
                    </a>
                    <ul class="sub-menu" @if(route::is('admin.genral_inquiry_list') || route::is('admin.rmc_inquiry_list')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.genral_inquiry_list')) active @endif">
                            <a href="{{ route('admin.genral_inquiry_list') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">General Inquiry List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-chat-processing"></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.rmc_inquiry_list')) active @endif">
                            <a href="{{ route('admin.rmc_inquiry_list') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">RMC Inquiry List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-chat"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item @if(route::is('admin.base.setting')) active @endif ">
                    <a href="{{ route('admin.base.setting') }}" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Base Setting</span>
                        </span>
                        <span class="menu-icon">
                           <i class="icon-placeholder mdi mdi-settings"></i>
                       </span>
                   </a>
                </li>  
                <li class="menu-item @if(route::is('admin.productSettingList') || route::is('admin.product.setting')|| route::is('admin.editProductSetting')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Product Setting
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-shape"></i>
                        </span>
                    </a>
                    <ul class="sub-menu" @if(route::is('admin.productSettingList') || route::is('admin.product.setting')|| route::is('admin.editProductSetting')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.product.setting')) active @endif">
                            <a href="{{ route('admin.product.setting') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Product Setting</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-server-plus"></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.productSettingList')) active @endif">
                            <a href="{{ route('admin.productSettingList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Product Setting List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-format-list-bulleted"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.plantList') || route::is('admin.addPlant')|| route::is('admin.editPlant')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Plant
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-pin"></i>
                        </span>
                    </a>
                    <ul class="sub-menu" @if(route::is('admin.plantList') || route::is('admin.addPlant')|| route::is('admin.editPlant')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.addPlant')) active @endif">
                            <a href="{{ route('admin.addPlant') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Plant</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-tooltip-edit"></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.plantList')) active @endif">
                            <a href="{{ route('admin.plantList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Plant List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-format-list-bulleted "></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.projectList') || route::is('admin.addProject')|| route::is('admin.editProject')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Project
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-bridge"></i>
                        </span>
                    </a>

                    <ul class="sub-menu" @if(route::is('admin.projectList') || route::is('admin.addProject')|| route::is('admin.editProject')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.addProject')) active @endif">
                            <a href="{{ route('admin.addProject') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Project</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-tooltip-edit "></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.projectList')) active @endif">
                            <a href="{{ route('admin.projectList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Project List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder fe fe-list "></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.clientList') || route::is('admin.addClient')|| route::is('admin.editClient')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Client
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-account-plus"></i>
                        </span>
                    </a>
                    <ul class="sub-menu" @if(route::is('admin.clientList') || route::is('admin.addClient')|| route::is('admin.editClient')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.addClient')) active @endif">
                            <a href="{{ route('admin.addClient') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Client</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-account-plus"></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.clientList')) active @endif">
                            <a href="{{ route('admin.clientList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Client List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-format-list-bulleted"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.imageCategoryList') || route::is('admin.addImageCategory')|| route::is('admin.editImageCategory')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Image Categories
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-camera-image"></i>
                        </span>
                    </a>
                    <ul class="sub-menu" @if(route::is('admin.imageCategoryList') || route::is('admin.addImageCategory')|| route::is('admin.editImageCategory')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.addImageCategory')) active @endif">
                            <a href="{{ route('admin.addImageCategory') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Category</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-camera-image"></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.imageCategoryList')) active @endif">
                            <a href="{{ route('admin.imageCategoryList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Image Categories List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-format-list-bulleted"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.testimonialOtherList') || route::is('admin.addTestimonialOther')|| route::is('admin.editTestimonialOther')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Testimonial
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-comment-account-outline"></i>
                        </span>
                    </a>
                    <ul class="sub-menu" @if(route::is('admin.testimonialOtherList') || route::is('admin.addTestimonialOther')|| route::is('admin.editTestimonialOther')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.addTestimonialOther')) active @endif">
                            <a href="{{ route('admin.addTestimonialOther') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Testimonial</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-message-reply"></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.testimonialOtherList')) active @endif">
                            <a href="{{ route('admin.testimonialOtherList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Testimonial List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-message-bulleted"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.testimonialList') || route::is('admin.addTestimonial')|| route::is('admin.editTestimonial')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Certificates and Recognitions
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-certificate"></i>
                        </span>
                    </a>
                    <ul class="sub-menu" @if(route::is('admin.testimonialList') || route::is('admin.addTestimonial')|| route::is('admin.editTestimonial')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.addTestimonial')) active @endif">
                            <a href="{{ route('admin.addTestimonial') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Certificates and Recognitions</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-message-reply"></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.testimonialList')) active @endif">
                            <a href="{{ route('admin.testimonialList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Certificates and Recognitions List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-message-bulleted"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.blogList') || route::is('admin.addBlog')|| route::is('admin.editBlog')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Blog
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-blogger"></i>
                        </span>
                    </a>
                    <ul class="sub-menu" @if(route::is('admin.blogList') || route::is('admin.addBlog')|| route::is('admin.editBlog')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.addBlog')) active @endif">
                            <a href="{{ route('admin.addBlog') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Blog</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-message-reply"></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.blogList')) active @endif">
                            <a href="{{ route('admin.blogList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Blog List </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-message-bulleted"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item @if(route::is('admin.newsLetterList')) active @endif ">
                    <a href="{{ route('admin.newsLetterList') }}" class="menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Newsletter Subscription</span>
                        </span>
                        <span class="menu-icon">
                           <i class="icon-placeholder mdi mdi-newspaper"></i>
                       </span>
                   </a>
                </li> 
                
                <!-- <li class="menu-item @if(route::is('admin.galleryList') || route::is('admin.addGallery')) active opened @endif">
                    <a href="#" class="open-dropdown menu-link">
                        <span class="menu-label">
                            <span class="menu-name">Life @ Hindustan RMC
                                <span class="menu-arrow"></span>
                            </span>
                        </span>
                        <span class="menu-icon">
                            <i class="icon-placeholder mdi mdi-trophy"></i>
                        </span>
                    </a>
                    <ul class="sub-menu" @if(route::is('admin.addGallery') || route::is('admin.galleryList')) style="display: block;" @endif>
                        <li class="menu-item @if(route::is('admin.addGallery')) active @endif">
                            <a href="{{ route('admin.addGallery') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Add Gallery </span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-camera-image"></i>
                                </span>
                            </a>
                        </li>
                        <li class="menu-item @if(route::is('admin.galleryList')) active @endif">
                            <a href="{{ route('admin.galleryList') }}" class=" menu-link">
                                <span class="menu-label">
                                    <span class="menu-name">Gallery List</span>
                                </span>
                                <span class="menu-icon">
                                    <i class="icon-placeholder mdi mdi-format-list-bulleted"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li> -->
            </ul>   
        </div>
</aside>