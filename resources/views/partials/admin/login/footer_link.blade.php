
<script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/vendor/popper/popper.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-scrollbar/jquery.scrollbar.min.js') }}"   ></script>
<script src="{{ asset('assets/vendor/listjs/listjs.min.js') }}"></script>
<script src="{{ asset('assets/vendor/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

<script src="{{ asset('assets/vendor/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/front/datatable-data.js') }}"></script>

<!-- <script src="{{ asset('js/front/atmos.min.js')}}"></script> -->
<script src="{{ asset('js/front/atmos.js')}}"></script>
<script src="{{ asset('assets/vendor/apexchart/apexcharts.min.js') }}"></script>

<!-- page level-->
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/pages/login.js') }}"></script>
<script src="{{ asset('js/pages/validation.js') }}"></script>

<!-- dashboard chart -->
<script src="{{asset('js/front/dashboard-01.js')}}"></script>
<!-- page level-->
<!--Additional Page includes-->
<script src="{{asset('assets/vendor/apexchart/apexcharts.min.js')}}"></script>
<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/developer.js') }}"></script>
@if(route::is('admin.login'))
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@endif
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{ asset('assets/js/apps.min.js') }}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<!-- <script>
  $(document).ready(function() {
    App.init();
  });
</script> -->
<script type="text/javascript">
  @if(Session::has('messages'))
    
     jQuery(document).ready(function() {
       @foreach(Session::get('messages') AS $msg) 
          toastr['{{$msg["type"]}}']('{{$msg["message"]}}');
       @endforeach

     });
     
   @endif
   @if (count($errors) > 0) 
    
    jQuery(document).ready(function() {
    
        @foreach($errors->all() AS $error)
         toastr.error('{{$error}}');
     @endforeach     
      });
     
@endif
</script>

@if(route::is('admin.addProject') || route::is('admin.addPlant'))
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDbX_JirTlqgj9BO002nMah8CQSD7f4ypI&signed_in=true&libraries=places"></script>
<script type="text/javascript">
    var isPlaceAuthentic = false;
    var lastPlaceAuthenticated = '';
   
    function initialize() {

        var input = (document.getElementById('pacinput'));

        var autocomplete = new google.maps.places.Autocomplete(input);

        var infowindow = new google.maps.InfoWindow();

        google.maps.event.addListener(autocomplete, 'place_changed', function () {

            infowindow.close();

            var place = autocomplete.getPlace();

            isPlaceAuthentic = true;

            lastPlaceAuthenticated = $('#pacinput').val();

            isPlaceAuthentic = false;

            var address = '';

            var toInsertData = place.adr_address;

            var latti = place.geometry.location.lat();

            var longi = place.geometry.location.lng();

            var addressToInsert = toInsertData.substr(0, toInsertData.indexOf('<')).trim();

            if (toInsertData.indexOf('<') !== 1) {

                toInsertData = toInsertData.substr(toInsertData.indexOf('<'));

            }

            var streetAddress = $(toInsertData).filter('.street-address').text().trim();

            var extendedAddress = $(toInsertData).filter('.extended-address').text().trim();

            var cityName = $(toInsertData).filter('.locality').text().trim();

            var stateName = $(toInsertData).filter('.region').text().trim();

            var countryName = $(toInsertData).filter('.country-name').text().trim();

            $("#latitude").val(latti);

            $("#longitude").val(longi);

            $("#city").val(cityName);

            $("#state").val(stateName);

            $("#country").val(countryName);

            initMap(latti,longi);

            var appenedAddress = addressToInsert.concat(streetAddress);

            appenedAddress = appenedAddress.concat(extendedAddress);

            if (place.address_components) {

                address = [

                    (place.address_components[0] && place.address_components[0].short_name || ''),

                    (place.address_components[1] && place.address_components[1].short_name || ''),

                    (place.address_components[2] && place.address_components[2].short_name || '')

                ].join(' ');
            }

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    $("#area_submit").on('click',function(e){
        if(($("#latitude").val() == "" || $("#longitude").val() == "")){
            alert("Please Enter Valid Address");
            e.preventDefault();
        }
    });
    
    initMap(23.0300,72.5800);

    function initMap(latitude,longitude) {
      
      var myLatLng = {lat: latitude, lng: longitude};

      var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 18,
        center: myLatLng
      });

      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        draggable: true
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        document.getElementById("lat").value = this.getPosition().lat();
        document.getElementById("long").value = this.getPosition().lng();
      });
  }

</script>
@endif

@if(route::is('admin.addPlant') || route::is('admin.editPlant'))

<script>
   var galleryUploader = new qq.FineUploader({
       element: document.getElementById("fine-uploader-gallery"),
       template: 'qq-template-gallery',
       request: {
           endpoint: "{{route('admin.plant.multiple')}}",
            
       },
       multiple: true,
       thumbnails: {
         placeholders: {
          waitingPath: "{{asset('assets/vendor/fine-uploader/placeholders/waiting-generic.png')}}",
          notAvailablePath: "{{asset('assets/vendor/fine-uploader/placeholders/not_available-generic.png')}}"
         }
       },
       validation: {
           allowedExtensions: ['jpeg', 'jpg', 'gif', 'png','pdf'],
           itemLimit: 8,
           sizeLimit: 5217152,
       },
       deleteFile: {
           enabled: true,
           method: "POST",
           forceConfirm: true,
           endpoint: "{{route('admin.plant.removefile')}}",
           customHeaders: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       },
       text: {
           uploadButton: 'Click or Drop for upload images',
           waitingForResponse: 'Processing...',
           retryButton: 'Retry',
           cancelButton: 'Cancel',
           failUpload: 'Upload failed',
           deleteButton: 'Delete',
           deletingStatusText: 'Deleting...',
           formatProgress: '{percent}% of {total_size}'
       },
       
       debug: true,
       callbacks: {
           onComplete:function(id, fileName, responseJSON){
             if (responseJSON.success) {
                 var li = $('.qq-upload-list li')[id];
                 $(li).append('<input type="hidden" name="ajax_photos[]" value="'+responseJSON.name+'"></input>');
             }
           },
           onDelete: function(id) {
             
           },
       },
   });

</script>
<!-- end -->
@endif

@if(route::is('admin.addCategoryImage') || route::is('admin.editCategoryImage'))
  
  <script>
   var galleryUploader = new qq.FineUploader({
       element: document.getElementById("fine-uploader-gallery"),
       template: 'qq-template-gallery',
       request: {
           endpoint: "{{route('admin.multipleCategoryImage')}}",
            
       },
       multiple: true,
       thumbnails: {
         placeholders: {
          waitingPath: "{{asset('assets/vendor/fine-uploader/placeholders/waiting-generic.png')}}",
          notAvailablePath: "{{asset('assets/vendor/fine-uploader/placeholders/not_available-generic.png')}}"
         }
       },
       validation: {
           allowedExtensions: ['jpeg', 'jpg', 'gif', 'png','pdf'],
           itemLimit: 8,
           sizeLimit: 5217152,
       },
       deleteFile: {
           enabled: true,
           method: "POST",
           forceConfirm: true,
           endpoint: "{{route('admin.removeCategoryImage')}}",
           customHeaders: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       },
       text: {
           uploadButton: 'Click or Drop for upload images',
           waitingForResponse: 'Processing...',
           retryButton: 'Retry',
           cancelButton: 'Cancel',
           failUpload: 'Upload failed',
           deleteButton: 'Delete',
           deletingStatusText: 'Deleting...',
           formatProgress: '{percent}% of {total_size}'
       },
       
       debug: true,
       callbacks: {
           onComplete:function(id, fileName, responseJSON){
             if (responseJSON.success) {
                 var li = $('.qq-upload-list li')[id];
                 $(li).append('<input type="hidden" name="ajax_photos[]" value="'+responseJSON.name+'"></input>');
             }
           },
           onDelete: function(id) {
             
           },
       },
   });

</script>

@endif
