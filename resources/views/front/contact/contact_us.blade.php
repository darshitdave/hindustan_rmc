@extends('layouts.front.front')
@section('title','Hindustan RMC | Contact Us')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/contact_banner.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Contact Us</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="javascript:void(0);">Support</a></li>
                    <li>Contact Us</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper message_contact section-space--inner--120" style="padding-top: 120px;padding-bottom: 33px;">
    <div class="contact-form-area section-space--top--80">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="career-title-area text-center section-space--bottom--50">
                        <h2 class="title mb-0">Drop us a line</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 mx-auto">
                    <div class="contact-form-wrapper">
                        <form method="post" action="{{ route('admin.insertContactDetail') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="text" name="con_name" id="con_name" placeholder="Name *" required>
                                </div>
                                <div class="col-lg-6">
                                    <input type="email" name="con_email" id="con_email" placeholder="Your Email *" required>
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" name="con_phone" id="con_phone" maxlength="10" pattern="^\d{10}$" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" placeholder="Phone Number">
                                </div>
                                <div class="col-lg-12">
                                    <textarea placeholder="Message" name="con_message" id="con_message"></textarea>
                                </div>
                                <div class="col-lg-12">
                                    <label for="ReCaptcha">Recaptcha:</label>
                                    <div class="g-recaptcha" data-sitekey="{{env('NOCAPTCHA_SITEKEY')}}"></div>
                                    <button type="submit" value="submit" id="submit" name="submit" class="ht-btn ht-btn--default" style="margin-top: 1%;color: #ffff;">SEND MESSAGE</button>
                                </div>
                            </div>
                        </form>
                        <p class="form-message"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  icon info area ====================-->
<div class="icon-info-area section-space--inner--120 grey-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="icon-info-wrapper">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="single-icon-info">
                                <div class="glyph-icon icomoon-placeholder" style="margin-top:-15px;color:#ffc246;"></div>
                                <div class="single-icon-info__content">
                                    <h4 class="single-icon-info__title single-icon-info__title--black2">Registered Office</h4>
                                    <p class="single-icon-info__text">Survey No.414/2, Opp. Mahindra Service Center,<br>Off Shantipura To Bopal S.P. Ring Road, Shela, <br> Dist. Ahmedabad – 380051, Gujarat.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="single-icon-info">
                                <div class="single-icon-info__image">
                                    <img src="{{ asset('assets/front/img/icons/support-yellow.png')}}" class="img-fluid" alt="">
                                </div>
                                <div class="single-icon-info__content">
                                    <h4 class="single-icon-info__title single-icon-info__title--black2">24/7 Support</h4>
                                    <p class="single-icon-info__text"><a href="tel:1800123999977" style="color: #888 !important;">1800-123-9999-77</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="single-icon-info">
                                <div class="single-icon-info__image">
                                    <img src="{{ asset('assets/front/img/icons/message-yellow.png')}}" class="img-fluid" alt="">
                                </div>
                                <div class="single-icon-info__content">
                                    <h4 class="single-icon-info__title single-icon-info__title--black2">Email Us</h4>
                                    <p class="single-icon-info__text"><a href="mailto:info@hindustanrmc.com" style="color: #888 !important;">info@hindustanrmc.com</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of icon info area  ====================-->
<div class="page-content-wrapper message_contact section-space--inner--120" style="padding-top: 120px;padding-bottom: 34px;">
    <div class="contact-form-area section-space--top--80">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="career-title-area text-center section-space--bottom--50">
                        <h2 class="title mb-0">Follow us</h2>
                        <ul class="contact-social-link-list">
                            
                            <li>
                                <a href="https://www.facebook.com/Hindustanrmc/">
                                    <i class="ion-social-facebook link-icon"></i><span class="link-text">Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/hindustanrmc">
                                    <i class="ion-social-twitter link-icon"></i><span class="link-text">Twitter</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/hindustanrmc/">
                                    <i class="ion-social-instagram link-icon"></i><span class="link-text">Instagram</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/hindustan-infrastructure-solution">
                                    <i class="ion-social-linkedin link-icon"></i><span class="link-text">Linkedin</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content-wrapper message_contact section-space--inner--120" style="padding-bottom: 0px;">
    
        <!-- google map -->
        <div class="google-map google-map--style-2" id="google-map-one" data-width="100%" data-zoom_enable="" data-zoom="10" data-map_type="roadmap"></div>
    
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection
@section('js')

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDbX_JirTlqgj9BO002nMah8CQSD7f4ypI&signed_in=true&libraries=places"></script>
<script type="text/javascript">
var data = {!! json_encode($latLong) !!};
var locations = data; 

//alert(locations);
var map = new google.maps.Map(document.getElementById('google-map-one'), { 
    zoom: 11, 
    center: new google.maps.LatLng(23.033863, 72.585022), 
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    options: {
        gestureHandling: 'greedy'
    },
    scrollwheel: false 
}); 
   
var infowindow = new google.maps.InfoWindow(); 

var marker, i; 

for (i = 0; i < locations.length; i++) { 
    marker = new google.maps.Marker({ 
        position: new google.maps.LatLng(locations[i][1], locations[i][2]), 
        map: map,
        icon: {
            url: "http://maps.gstatic.com/mapfiles/markers2/marker.png"
        },
        title: String(locations[i][0]),
        url : locations[i][4] 
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) { 
        return function() { 
            infowindow.setContent(this.get('title'));
            marker.setTitle(this.get('title'));
            infowindow.open(map, marker);
            window.location.href = this.url; 
            window.open = this.url; 
        } 
    })(marker, i));

     markerYellow_first = new google.maps.Marker({ 
        position: new google.maps.LatLng('23.1345003', '72.5825692'), 
        map: map,
        icon: {
          url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
        },
        title : 'Upcoming'/*,
        url : 'http://hindustanrmc.homipod.com/plants-location' */
    });

    markerYellow = new google.maps.Marker({ 
        position: new google.maps.LatLng('23.1279494', '72.55819269999999'), 
        map: map,
        icon: {
          url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
        },
        title : 'Upcoming'/*,
        url : 'http://hindustanrmc.homipod.com/plants-location'  */
    });

    google.maps.event.addListener(markerYellow_first, 'click', (function(markerYellow_first, i) { 
        return function() { 
            
            infowindow.setContent(this.get('title'));
            markerYellow_first.setTitle(this.get('title'));
            infowindow.open(map, markerYellow_first);
            /*window.location.href = this.url; 
            window.open = this.url;*/
        } 
    })(markerYellow_first, i));

    google.maps.event.addListener(markerYellow, 'click', (function(markerYellow, i) { 
        return function() { 
            
            infowindow.setContent(this.get('title'));
            markerYellow.setTitle(this.get('title'));
            infowindow.open(map, markerYellow);
            /*window.location.href = this.url; 
            window.open = this.url;*/
        } 
    })(markerYellow, i));

} 

var email = $('#con_email').val();
if(email != ''){
    
    $(document).on('click', '#submit', function() {
        toastr.success('Thank you for reaching out to us.<br>We will soon get in touch!');
    });    
}

$('form').on('submit', function(e) {
  if(grecaptcha.getResponse() == "") {
    e.preventDefault();
    toastr.error('Please Check Captcha');
  } else {
    
  }
});

</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection