@extends('layouts.front.front')
@section('title','Hindustan RMC | Company')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg bg-img section-space--inner--bottom--50 section-space--inner--top--120" data-bg="{{ asset('assets/front/img/backgrounds/about_company.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Company</h2>
            </div>
            <div class="col-lg-12">
                <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                    <li class="has-children"><a href="{{route('front.company.page')}}">About</a></li>
                    <li>Company</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="image-list-text mt-0 section-space--bottom--60">
                    
                    <p>Hindustan Infrastructure Solution is one of the largest Ready-Mix Concrete (RMC) manufacturers in Ahmedabad. Set up way back in 2011, HINDUSTAN RMC expanded business rapidly and currently owns a total of 5 RMC plants covering entire Ahmedabad - Gandhinagar belt from Changodar to Gandhinagar to Ramol. The total serviceable area amounts to more than 10,000 sq. km.<br><br>Our plants are located at Shela, Adalaj, Gandhinagar and at Adalaj each leading to a combined monthly production amounting to nearly 20,000 cmt.</p>
                </div>
                <div class="service-details-tab section-space--bottom--80">
                    <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                        <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">COMPANY STRENGTH</a>
                        <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">VISION</a>
                        <a class="nav-item nav-link" id="service-details-item3-tab" data-toggle="tab" href="#service-details-item3" role="tab" aria-selected="false">MISSION</a>
                    </div>
                    <div class="tab-content service-details-tab__content-wrapper">
                        <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                          We have 5 RMC manufacturing Plants, 60 Transit Mixers, 15 Concrete Pumps, 5 Wheel Loaders, 5 Pump Towing Vehicles, 3 DG's for Electric back-up, Water Chilling Plant for Temperature Controller Concrete and a strong family of around 200 members. <br><br>
                          We are ISO 9001:2015 Certified Company and we are producing and supplying concrete by the brand name of "Hindustan RMC". We are also the only QCI RMCMA certified plant in the whole city with an in-depth focus on systematic processes, product quality, technology, innovation and customer service.


                        </div>
                        <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                            We aspire to be one of the leading brands in RMC manufacturing in Gujarat region for all the iconic construction projects by 2025.
                            
                        </div>
                        <div class="tab-pane fade" id="service-details-item3" role="tabpanel" aria-labelledby="service-details-item3-tab">
                            We add value to our clients by manufacturing best-in-class and cost-effective concrete and providing timely & quality services.
                        </div>
                    </div>
                </div>
                <div class="service-details-text-wrapper section-space--bottom--80">
                    <h2 class="title">Complete Concrete Solution</h2>
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- solution grid wrapper -->
                            <div class="solution-grid-wrapper">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="count-wrap">
                                            <div class="dot"></div>
                                        </div>
                                        <div class="solution-grid-single">
                                            <h3 class="solution-grid-single__title">Concrete Production</h3>
                                            <p class="solution-grid-single__content">We make multiple types of concrete which are known for quality.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="count-wrap">
                                            <div class="dot"></div>
                                        </div>
                                        <div class="solution-grid-single">
                                            <h3 class="solution-grid-single__title">Concrete Transportation</h3>
                                            <p class="solution-grid-single__content">We transport the concrete to your premises whenever you seek.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="count-wrap">
                                            <div class="dot"></div>
                                        </div>
                                        <div class="solution-grid-single">
                                            <h3 class="solution-grid-single__title">Concrete Placement</h3>
                                            <p class="solution-grid-single__content">We place the concrete at the exact location via pumping or dumping.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 about_sidebar">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;color: #ffffff;"></i>
                                </div>
                                <h3 class="page-title" style="color: #ffffff;">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
<!--====================  cta area ====================-->
<div class="cta-area bg-img video_company section-space--inner--190" data-bg="{{ asset('assets/front/img/backgrounds/company_footer.png') }}" >
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <!-- cta content erapper -->
                <div class="cta-content__wrapper text-center">
                    <h2 class="cta-content__title section-space--bottom--40">Director's Message</h2>
                    <div class="video-play-button-wrapper video-popup">
                        <a class="d-block" href="#">
                            <div class="video-play-icon">
                                <i class="ion-ios-play"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of cta area  ====================-->

<div class="page-content-wrapper section-space--inner--120 about_sidebar1">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
            </div>
            <div class="col-lg-4">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;color: #ffffff;"></i>
                                </div>
                                <h3 class="page-title" style="color: #ffffff;">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection  

