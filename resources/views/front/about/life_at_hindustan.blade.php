@extends('layouts.front.front')
@section('title','Hindustan RMC | Life at Hindustan RMC')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg bg-img section-space--inner--bottom--50 section-space--inner--top--120" data-bg="{{ asset('assets/front/img/backgrounds/Life_at_Hindustan_RMC.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Life at Hindustan RMC</h2>
            </div>
            <div class="col-lg-12">
                <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                    <li class="has-children"><a href="{{route('front.company.page')}}">About</a></li>
                    <li>Life at Hindustan RMC</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="service-details-page-content">
                    <div class="service-details-text-wrapper">
                        <h2 class="title">We’ve got the perfect work - fun balance.</h2>
                        <p>Mechanical engineers make a difference. That's because mechanical engineering careers center on creating technologies to meet human needs. Virtually every product or service in modern life has probably been touched in some way by a mechanical engineer to help humankind.</p>
                        <div class="split-text section-space--top--60">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="single-split-text">
                                        <h4 class="title">Our Solutions</h4>
                                        <p>Review the findings from our survey of early technology adopters, and find out how to get closer to your fastest‑moving customers.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-split-text other_text">
                                        <h4 class="title">Industry Benefits</h4>
                                        <p>A forecasting and planning solution that helps the restaurant industry build a highly accurate inventory and sales forecast</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="split-text section-space--top--60">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="single-split-text">
                                        <h4 class="title">Our Solutions</h4>
                                        <p>Review the findings from our survey of early technology adopters, and find out how to get closer to your fastest‑moving customers.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-split-text other_text">
                                        <h4 class="title">Industry Benefits</h4>
                                        <p>A forecasting and planning solution that helps the restaurant industry build a highly accurate inventory and sales forecast</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="split-text section-space--top--60">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="single-split-text">
                                        <h4 class="title">Our Solutions</h4>
                                        <p>Review the findings from our survey of early technology adopters, and find out how to get closer to your fastest‑moving customers.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-split-text other_text">
                                        <h4 class="title">Industry Benefits</h4>
                                        <p>A forecasting and planning solution that helps the restaurant industry build a highly accurate inventory and sales forecast</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 life_at_sidebar">
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
<!--====================  page content wrapper ====================-->
@if(isset($gallery) && count($gallery) >= 1)
<div class="page-content-wrapper multi_image_life_at section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="service-details-page-content">
                    <div class="service-details-gallery-slider-wrapper section-space--bottom--60">
                        <div class="image-gallery-slider-big__container-area section-space--bottom--30">
                            <div class="swiper-container image-gallery-slider-big__container">
                                <div class="swiper-wrapper image-gallery-slider-big__wrapper">
                                    @foreach($gallery as $gk => $gv)
                                        <div class="swiper-slide">
                                            <div class="image-gallery-slider-big__single-slide">
                                                <img src="{{ asset('uploads/gallery_image') }}/{{ $gv->image }}" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="ht-swiper-button-prev ht-swiper-button-prev-12 ht-swiper-button-nav"><i class="ion-ios-arrow-left"></i></div>
                            <div class="ht-swiper-button-next ht-swiper-button-next-12 ht-swiper-button-nav"><i class="ion-ios-arrow-forward"></i></div>
                        </div>
                        <div class="image-gallery-slider-small__container-area">
                            <div class="swiper-container image-gallery-slider-small__container">
                                <div class="swiper-wrapper image-gallery-slider-small__wrapper">
                                    @foreach($gallery as $gk => $gv)
                                         <div class="swiper-slide">
                                            <div class="image-gallery-slider-small__single-slide">
                                                <img src="{{ asset('uploads/gallery_image') }}/{{ $gv->image }}" class="img-fluid" alt="">.
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!--====================  End of page content wrapper  ====================-->
<!--====================  page content wrapper ====================-->

<!--====================  End of page content wrapper  ====================-->
@endsection  

