@extends('layouts.front.front')
@section('title','Hindustan RMC | Leadership')
@section('content')
<!--====================  breadcrumb area ====================-->
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/Leadership.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Leadership</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="Javascript:void(0);">Home</a></li>
                    <li>Leadership</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  End of breadcrumb area  ====================-->
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="common-page-content">
                    <div class="leadership__wrapper">
                        <div class="leadership__single">
                            <div class="image">
                                <img src="{{ asset('assets/front/img/team/CHIRAG.png')}}" class="img-fluid" alt="">
                            </div>
                            <div class="content">
                                <div class="identity-wrapper has-border-left">
                                    <h3 class="name">Chirag Thakkar</h3>
                                    <span class="designation">Managing Director | Business Development</span>
                                </div>
                                <p class="brief-desc">Chirag is Managing Partner for the entire Hindustan Group. He overlooks primarily Business Development working on setting up new plants, forward and backward integration, new businesses to enter and also troubleshoots for the various operational issues. and also troubleshoots for the various operational issues. He has been pivotal to take the group turnover to 100 crores today.</p>
                                <!-- <div class="social-links social-links--white-topbar d-inline-block">
                                    <ul>
                                        <li><a href="//facebook.com"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="//twitter.com"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="//linkedin.com"><i class="ion-social-linkedin"></i></a></li>
                                        <li><a href="//skype.com"><i class="ion-social-skype"></i></a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                        <div class="leadership__single">
                            <div class="image">
                                <img src="{{ asset('assets/front/img/team/CHINTAN.jpg')}}" class="img-fluid" alt="">
                            </div>
                            <div class="content">
                                <div class="identity-wrapper has-border-left">
                                    <h3 class="name">Chintan Thakkar</h3>
                                    <span class="designation">Shela In-Charge | Operations & Maintenance (O&M)</span>
                                </div>
                                <p class="brief-desc">Chintan is Founding Partner for the group who is also the go to guy for Tech and geeky stuff. He overlooks Operations and Maintenance across plants, dismantling of completed plants, setting up of new plants and purchase of all technical infrastructure materials. Over and above this, he also manages the Shela Plant entirely end-to-end.</p>
                                <!-- <div class="social-links social-links--white-topbar d-inline-block">
                                    <ul>
                                        <li><a href="//facebook.com"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="//twitter.com"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="//linkedin.com"><i class="ion-social-linkedin"></i></a></li>
                                        <li><a href="//skype.com"><i class="ion-social-skype"></i></a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                        <div class="leadership__single">
                            <div class="image">
                                <img src="{{ asset('assets/front/img/team/Keval.jpg')}}" class="img-fluid" alt="">
                            </div>
                            <div class="content">
                                <div class="identity-wrapper has-border-left">
                                    <h3 class="name">Keval Thakkar</h3>
                                    <span class="designation">Adalaj In-Charge | Sales & Marketing</span>
                                </div>
                                <p class="brief-desc">Keval is the power house who holds experience of managing multiple plants at Adalaj and Sarthak. He currently oversees Group Sales and Marketing. Coming from a Finance background yet he holds profound grip for Operations and Sales. He is currently in-charge at Adalaj. He is a go to guy for any sorts of trouble at the plant and people love how he simplifies things from them.</p>
                                <!-- <div class="social-links social-links--white-topbar d-inline-block">
                                    <ul>
                                        <li><a href="//facebook.com"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="//twitter.com"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="//linkedin.com"><i class="ion-social-linkedin"></i></a></li>
                                        <li><a href="//skype.com"><i class="ion-social-skype"></i></a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                        <div class="leadership__single">
                            <div class="image">
                                <img src="{{ asset('assets/front/img/team/Krunal_Thakar.jpg')}}" class="img-fluid" alt="">
                            </div>
                            <div class="content">
                                <div class="identity-wrapper has-border-left">
                                    <h3 class="name">Krunal Thakkar</h3>
                                    <span class="designation">Changodar In-Charge | Production</span>
                                </div>
                                <p class="brief-desc">Krunal is an amazing source of energy who breathes the business. He is rumoured to be available 24x7 for his team and never misses a call. He currently oversees Group Production of RMC across the plants. He holds a unique rapport amongst all the Production and Operations guys across plants and is the go to guy for any kind of issues. He is currently in-charge at Changodar (Navapura plant).</p>
                                <!-- <div class="social-links social-links--white-topbar d-inline-block">
                                    <ul>
                                        <li><a href="//facebook.com"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="//twitter.com"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="//linkedin.com"><i class="ion-social-linkedin"></i></a></li>
                                        <li><a href="//skype.com"><i class="ion-social-skype"></i></a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                        <div class="leadership__single">
                            <div class="image">
                                <img src="{{ asset('assets/front/img/team/SAMIP.jpg')}}" class="img-fluid" alt="">
                            </div>
                            <div class="content">
                                <div class="identity-wrapper has-border-left">
                                    <h3 class="name">Samip Mulani</h3>
                                    <span class="designation">Ognaj In-Charge | Purchase</span>
                                </div>
                                <p class="brief-desc">Samip is the most fun guy to be around who is happy go lucky and handles Purchase of Raw Materials for the group. An amazing manager - he is said to handle work even remotely ensuring work flows smoothly even when he is not around. He is currently in-charge of the newly set up Ognaj plant. He holds quite infectious energy and always makes everyone around laugh their hearts out maintaining the fun quotient in the organization.</p>
                                <!-- <div class="social-links social-links--white-topbar d-inline-block">
                                    <ul>
                                        <li><a href="//facebook.com"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="//twitter.com"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="//linkedin.com"><i class="ion-social-linkedin"></i></a></li>
                                        <li><a href="//skype.com"><i class="ion-social-skype"></i></a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                        <div class="leadership__single">
                            <div class="image">
                                <img src="{{ asset('assets/front/img/team/Nanu_bhai.jpg')}}" class="img-fluid" alt="">
                            </div>
                            <div class="content">
                                <div class="identity-wrapper has-border-left">
                                    <h3 class="name">Nanubhai Thakkar</h3>
                                    <span class="designation">Finance & HR</span>
                                </div>
                                <p class="brief-desc">Profoundly knowledgeable, he brings the depth of experience in the group and hence handles the most serious portfolios of Finance and HR. He is both strict and amicable. Mostly people are in awe of him or scared of him but everybody respects for the pearls of wisdom he shares with all of us. Being a grounds up fellow, he maintains the tempo of work and constantly motivates everyone to be on their toes and focused on group mission.</p>
                                <!-- <div class="social-links social-links--white-topbar d-inline-block">
                                    <ul>
                                        <li><a href="//facebook.com"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="//twitter.com"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="//linkedin.com"><i class="ion-social-linkedin"></i></a></li>
                                        <li><a href="//skype.com"><i class="ion-social-skype"></i></a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                        <div class="leadership__single">
                            <div class="image">
                                <img src="{{ asset('assets/front/img/team/NIS_5375_copy.jpg')}}" class="img-fluid" alt="">
                            </div>
                            <div class="content">
                                <div class="identity-wrapper has-border-left">
                                    <h3 class="name">Rushiraj Patel</h3>
                                    <span class="designation">Strategy</span>
                                </div>
                                <p class="brief-desc">Rushiraj brings valuable knowledge of management and oversees Group Business Strategy helping to figure out on how to important business deals and how to consistently expand the business whilst not missing out on all the minor details of business. A data - oriented guy, he constantly strives to improve processes and tracking of important information and analyse it to drive data based decisions in the business.</p>
                                <!-- <div class="social-links social-links--white-topbar d-inline-block">
                                    <ul>
                                        <li><a href="//facebook.com"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="//twitter.com"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="//linkedin.com"><i class="ion-social-linkedin"></i></a></li>
                                        <li><a href="//skype.com"><i class="ion-social-skype"></i></a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                        <div class="leadership__single">
                            <div class="image">
                                <img src="{{ asset('assets/front/img/team/nayan.png')}}" class="img-fluid" alt="">
                            </div>
                            <div class="content">
                                <div class="identity-wrapper has-border-left">
                                    <h3 class="name">Nayan Thakkar</h3>
                                    <span class="designation">Support & Tech</span>
                                </div>
                                <p class="brief-desc">Nayan holds the key piece of Support & Tech and he ensures all pieces of puzzle fit together and business is continuously achieving its objectives. He also oversees the Tech (ERP) implementation and monitoring across plants and departments ensuring key and vital information is available on a timely manner.</p>
                                <!-- <div class="social-links social-links--white-topbar d-inline-block">
                                    <ul>
                                        <li><a href="//facebook.com"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="//twitter.com"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="//linkedin.com"><i class="ion-social-linkedin"></i></a></li>
                                        <li><a href="//skype.com"><i class="ion-social-skype"></i></a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;color: #ffffff;"></i>
                                </div>
                                <h3 class="page-title" style="color: #ffffff;color: #ffffff;">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection