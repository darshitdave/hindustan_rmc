@extends('layouts.front.front')
@section('title','Hindustan RMC | Milestones')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg bg-img section-space--inner--bottom--50 section-space--inner--top--120" data-bg="{{ asset('assets/front/img/backgrounds/milestone_banner.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Milestones</h2>
            </div>
            <div class="col-lg-12">
                <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                    <li class="has-children"><a href="{{route('front.company.page')}}">About</a></li>
                    <li>Milestones</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="common-page-content">
                    <div class="common-page-text-wrapper section-space--bottom--50">
                        <h2 class="common-page-title section-space--bottom--40">The journey is always memorable than the destination.</h2>
                        <p>Started out with a single small plant of 30 cubic metre per hour capacity and growing to 5 plants with more than 150 cubic metre per hour capacity has been a really memorable journey and here is a time - capsule that shall remind us of the milestones we achieved along the way.</p>
                    </div>

                    <div class="history-grid__wrapper row">
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2010 - Ideation
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/1.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        Chirag and Chintan Thakkar saw the potential of RMC while in Cement Trading Business. 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2011 - Hindustan RMC was born
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/born.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        We started first plant with 30 cu. mt. capacity at Sanand with 4 Transit Mixers.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2012 - Stronger Infrastructure
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/largest_infra.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        We added 8 Transit Mixers and 2 Concrete Pumps in our Family.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2013 - 2nd Plant at Adalaj 
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/adalaj_1.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        Another plant of 30 CMT capacity was added to serve Adalaj-Gandhinagar and East Ahmedabad.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2014 - 3rd Plant at Gota
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/gota1.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        3rd plant at Gota of 45 CMT capacity in association with B Safal group for multiple projects. 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2015 - 4 lac concreting
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/4lakh.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        Total 4 lac CMT concreting done so far by Hindustan RMC. 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2016 - East Ebony Project work 
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/east_bony1.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        Associated with True Value Group and plant setup for concreting of entire East Ebony project.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2016 - Largest Plant setup at Shela
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/shela1.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        75 CMT capacity plant was set up at Shela and Head Office shifted here.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2017 - Plant setup at Sanand GIDC
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/sanand1.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        Associated with Japanese giant Kajima (MamyPoko Pants) to set up its factory.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2018 - 2 more plants added.
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/other_2_plants.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        Two more plants came up at Sarthak in Gandhinagar and Navapura in Changodar.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper" style="margin-top: 30px;">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2019 - 2 lac CMT in 1 year
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/25.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        First time, 2 lac CMT concreting done in single year by all 4 plants combined.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 history-grid__single-wrapper">
                            <div class="dot"></div>
                            <div class="history-grid__single">
                                <div class="content-head">
                                    2020 - Ognaj Plant Set up
                                </div>
                                <div class="content-body">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/img/timeline/ognaj1.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="content">
                                        Currently, we have 5 RMC manufacturing Plants, 60 Transit Mixers, 15 Concrete Pumps, 5 Wheel Loaders, 8 Pump Towing Vehicles, 3 DG's for Electric back-up, Water Chilling Plant for Temperature Controller Concrete and a strong family of around 200 members.
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;color: #ffffff;"></i>
                                </div>
                                <h3 class="page-title" style="color: #ffffff;">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection 