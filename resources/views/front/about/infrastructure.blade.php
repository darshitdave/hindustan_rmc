@extends('layouts.front.front')
@section('title','Hindustan RMC | Infrastructure')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg bg-img section-space--inner--bottom--50 section-space--inner--top--120" data-bg="{{ asset('assets/front/img/backgrounds/Infrastructure.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Infrastructure</h2>
            </div>
            <div class="col-lg-12">
                <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                    <li class="has-children"><a href="{{route('front.company.page')}}">About</a></li>
                    <li>Infrastructure</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="image-list-text mt-0 section-space--bottom--60">
                    <div class="image-wrapper">
                        <div class="image">
                            <img src="{{ asset('assets/front/img/blog/tiles1.png') }}" class="img-fluid" alt="">
                        </div>
                        <div class="image-text">5 RMC manufacturing Plants</div>
                    </div>
                    <div class="image-wrapper">
                        <div class="image">
                            <img src="{{ asset('assets/front/img/blog/tiles2.png') }}" class="img-fluid" alt="">
                        </div>
                        <div class="image-text">60 Transit Mixers </div>
                    </div>
                </div>
                <div class="image-list-text mt-0 section-space--bottom--80">
                    <div class="image-wrapper">
                        <div class="image">
                            <img src="{{ asset('assets/front/img/blog/tiles3.png') }}" class="img-fluid" alt="">
                        </div>
                        <div class="image-text"> 15 Concrete Pumps</div>
                    </div>
                    <div class="image-wrapper">
                        <div class="image">
                            <img src="{{ asset('assets/front/img/blog/tiles4.png') }}" class="img-fluid" alt="">
                        </div>
                        <div class="image-text">5 Wheel Loaders</div>
                    </div>
                </div>
                <div class="image-list-text mt-0 section-space--bottom--80">
                    <div class="image-wrapper">
                        <div class="image">
                            <img src="{{ asset('assets/front/img/blog/7.jpg') }}" class="img-fluid" alt="">
                        </div>
                        <div class="image-text">5 Pump Towing Vehicles</div>
                    </div>
                    <div class="image-wrapper">
                        <div class="image">
                            <img src="{{ asset('assets/front/img/blog/tiles6.png') }}" class="img-fluid" alt="">
                        </div>
                        <div class="image-text">3 DG's for Electric back-up</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 infrastructure_sidebar">
                <div class="page-sidebar">
                    <!-- page sidebar -->
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
<!--====================  project counter area ====================-->
<div class="project-counter-area counter_infrastructure section-space--inner--100 grey-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="project-counter-single-content-wrapper">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <!-- project counter single content -->
                            <div class="project-counter-single-content project-counter-single-content--style2">
                                <!-- <div class="project-counter-single-content__image">
                                    <img src="{{ asset('assets/front/img/icons/oil-dark.png') }}" class="img-fluid" alt="">
                                </div> -->
                                <div class="project-counter-single-content__content">
                                    <span class="project-counter-single-content__project-count counter" style="font-size: 2.0rem;">400</span><span class="project-counter-single-content__project-count" style="font-size: 2.0rem;">m<sup>3</sup></span>
                                    <h5 class="project-counter-single-content__project-title">Top TM capacity</h5>
                                    <!-- <p class="project-counter-single-content__subtext">With a countrywide network of power plants, we've doing our best to power our community.</p> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <!-- project counter single content -->
                            <div class="project-counter-single-content project-counter-single-content--style2">
                                <!-- <div class="project-counter-single-content__image">
                                    <img src="{{ asset('assets/front/img/icons/home-dark.png') }}" class="img-fluid" alt="">
                                </div> -->
                                <div class="project-counter-single-content__content">
                                    <span class="project-counter-single-content__project-count counter" style="font-size: 2.0rem;">150</span><span class="project-counter-single-content__project-count" style="font-size: 2.0rem;">m<sup>3</sup>/hr</span>
                                    <h5 class="project-counter-single-content__project-title">Concreting capacity </h5>
                                    <!-- <p class="project-counter-single-content__subtext">We have always been proud of what we have accomplished influential changes.</p> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <!-- project counter single content -->
                            <div class="project-counter-single-content project-counter-single-content--style2">
                                <!-- <div class="project-counter-single-content__image">
                                    <img src="{{ asset('assets/front/img/icons/tree-dark.png') }}" class="img-fluid" alt="">
                                </div> -->
                                <div class="project-counter-single-content__content">
                                    <span class="project-counter-single-content__project-count counter" style="font-size: 2.0rem;">400</span><span class="project-counter-single-content__project-count" style="font-size: 2.0rem;">m</span>
                                    <h5 class="project-counter-single-content__project-title">Horizontal pumping capacity</h5>
                                    <!-- <p class="project-counter-single-content__subtext">We have always been proud of what we have accomplished influential changes.</p> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <!-- project counter single content -->
                            <div class="project-counter-single-content project-counter-single-content--style2">
                                <!-- <div class="project-counter-single-content__image">
                                    <img src="{{ asset('assets/front/img/icons/reuse-dark.png') }}" class="img-fluid" alt="">
                                </div> -->
                                <div class="project-counter-single-content__content">
                                    <span class="project-counter-single-content__project-count counter" style="font-size: 2.0rem;">150</span><span class="project-counter-single-content__project-count" style="font-size: 2.0rem;">m</span>
                                    <h5 class="project-counter-single-content__project-title">Vertical pumping capacity</h5>
                                    <!-- <p class="project-counter-single-content__subtext">We have always been proud of what we have accomplished influential changes.</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of project counter area  ====================-->
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper slider_infrastructure section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- common blog carousel -->
                <div class="common-blog-carousel">
                    <div class="swiper-container common-blog-carousel__container">
                        <div class="swiper-wrapper common-blog-carousel__wrapper">
                            <div class="swiper-slide common-blog-carousel__single-slide">
                                <div class="row image-popup">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{ asset('assets/front/img/blog/slider/Infra_Gallery_1.png') }}" class="single-gallery-thumb">
                                                <img src="{{ asset('assets/front/img/blog/slider/Infra_Gallery_1.png') }}" class="img-fluid" alt="" style="height: 238px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide common-blog-carousel__single-slide">
                                <div class="row image-popup">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{ asset('assets/front/img/blog/slider/Infra_Gallery_3.png') }}" class="single-gallery-thumb">
                                                <img src="{{ asset('assets/front/img/blog/slider/Infra_Gallery_2.png') }}" class="img-fluid" alt="" style="height: 238px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide common-blog-carousel__single-slide">
                                <div class="row image-popup">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{ asset('assets/front/img/blog/slider/Infra_Gallery_3.png') }}" class="single-gallery-thumb">
                                                <img src="{{ asset('assets/front/img/blog/slider/Infra_Gallery_3.png') }}" class="img-fluid" alt="" style="height: 238px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide common-blog-carousel__single-slide">
                                <div class="row image-popup">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{ asset('assets/front/img/blog/slider/Infra_Gallery_4.png') }}" class="single-gallery-thumb">
                                                <img src="{{ asset('assets/front/img/blog/slider/Infra_Gallery_4.png') }}" class="img-fluid" alt="" style="height: 238px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide common-blog-carousel__single-slide">
                                <div class="row image-popup">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{ asset('assets/front/img/blog/slider/Infra_Gallery_5.png') }}" class="single-gallery-thumb">
                                                <img src="{{ asset('assets/front/img/blog/slider/Infra_Gallery_5.png') }}" class="img-fluid" alt="" style="height: 238px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide common-blog-carousel__single-slide">                                    
                                <div class="row image-popup">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{ asset('assets/front/img/blog/slider/Infra_Gallery_6.png') }}" class="single-gallery-thumb">
                                                <img src="{{ asset('assets/front/img/blog/slider/Infra_Gallery_6.png') }}" class="img-fluid" alt="" style="height: 238px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination swiper-pagination-12"></div>
                    <div class="ht-swiper-button-prev ht-swiper-button-prev-13 ht-swiper-button-nav d-none d-lg-block"><i class="ion-ios-arrow-left"></i></div>
                    <div class="ht-swiper-button-next ht-swiper-button-next-13 ht-swiper-button-nav d-none d-lg-block"><i class="ion-ios-arrow-forward"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120 infrastructure_sidebar1">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
            </div>
            <div class="col-lg-4">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection  

