@extends('layouts.front.front')
@section('title','Hindustan RMC | Ready Mix Concrete Process')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg bg-img section-space--inner--bottom--50 section-space--inner--top--120" data-bg="{{ asset('assets/front/img/backgrounds/RMC_Process_banner.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Ready Mix Concrete Process</h2>
            </div>
            <div class="col-lg-12">
                <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                    <li class="has-children"><a href="{{route('front.company.page')}}">About</a></li>
                    <li>Ready Mix Concrete Process</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120 grey-bg" style="padding-bottom: 100px;padding-top: 60px;
">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="case-study__page-content-wrapper section-space--top--80" style="margin-top: 40px;">
                    <p class="section-title-content">Ready-Mix Concrete is a major part of any concrete construction project. It is a convenient and efficient tool for building. The concrete is delivered to the site mixed and ready to be cast which saves time and money.</p>
                    <h3>There are two types of ready-mix plants :</h3>
                    
                    <div class="case-study__subcontent-wrapper section-space--top--60" style="margin-top: 25px;">
                        <div class="case-study__subcontent-single section-space--bottom--40">
                            <div class="case-study__marker">
                                <i class="ion-flash"></i>
                            </div>
                            <div class="case-study__subcontent-content">
                                <!-- <h4 class="case-study__subtitle">Improve sales and operations and production planning:</h4> -->
                                <p class="section-title-content">A central mixing plant uses a stationary mixer to mix the concrete. The concrete is then transferred into the transportation vehicle. </p>
                            </div>
                        </div>
                        <div class="case-study__subcontent-single" style="margin-top: -36px;">
                            <div class="case-study__marker">
                                <i class="ion-flash"></i>
                            </div>
                            <div class="case-study__subcontent-content">
                                
                                <p class="section-title-content">A truck mixing plant places all unmixed dry materials into a truck mixer. Water is added and mixing is then done in the truck.</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="case-study__page-content-wrapper section-space--top--80" style="margin-top: 40px;">
                    
                    <h4>Many central mix plants have the ability to bypass the stationary mixer, thus making them a hybrid of the two types.</h4>
                    
                    <div class="case-study__subcontent-wrapper section-space--top--60" style="margin-top: 25px;">
                        <!-- <div class="case-study__subcontent-single section-space--bottom--40">
                            <div class="case-study__marker">
                                <i class="ion-flash"></i>
                            </div>
                            <div class="case-study__subcontent-content"> -->
                                <!-- <h4 class="case-study__subtitle">Improve sales and operations and production planning:</h4> -->
                                <!-- <p class="section-title-content">Truck mixing plants are simpler and less expensive than central mix plants because they do not have a stationary mixer. This is important because less machinery means less potential problems. For long trips the dry materials can be held in the truck until it reaches the site where water is then added and mixing can be completed. The advantage of central mixing lies within the fact that a mixing truck can hold more completely mixed concrete than unmixed concrete. This means that there can be more concrete delivered with fewer trucks.</p> -->
                            <!-- </div>
                        </div> -->
                        <div class="case-study__subcontent-single section-space--bottom--40" >
                            <div class="case-study__marker">
                                <i class="ion-flash"></i>
                            </div>
                            <div class="case-study__subcontent-content">
                                
                                <p class="section-title-content">Besides a few small exceptions, all mixing plants have the same basic layout. The plants consist of aggregate storage, cement/pozzolan storage, batchers (aggregate batching, cement batching, water and admixture batching), dust collection system, feed system, and some sort of mixer (truck mixer or stationary mixer). Due to modern technology, the ready-mix plant can be operated from a remote location. A batch operator uses video equipment and computer systems to control the crucial areas of the plant.</p>
                            </div>
                        </div>
                        <div class="case-study__subcontent-single">
                            <div class="case-study__marker">
                                <i class="ion-flash"></i>
                            </div>
                            <div class="case-study__subcontent-content">
                                
                                <p class="section-title-content">The process of producing concrete in measured batches is called batching. Every ready-mix concrete plant uses this process to produce their concrete. Weigh Batching uses a hopper and a scale to batch all of the dry materials. The scale gives a readout of the weight of material in the hopper. Due to its consistent nature, water can be batched by weigh batching or volumetric batching.</p>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-4 technology_sidebar">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;color: #ffffff;"></i>
                                </div>
                                <h3 class="page-title" style="color: #ffffff;">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120" >
    <!--=======  shopping cart wrapper  =======-->
    <div class="shopping-cart-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="title" style="margin-bottom: 28px;">DIFFERENCE BETWEEN SMC & RMC</h2>
                    <!--=======  cart table  =======-->
                    <div class="myaccount-table table-responsive text-center">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <th style="background-color: #495057;color: #ffffff;">PARTICULARS</th>
                                    <th style="background-color: #495057;color: #ffffff;">SITE MIX CONCRETE (SMC)</th>
                                    <th style="background-color: #495057;color: #ffffff;">READY MIX CONCRETE (RMC)</th>
                                    
                                </tr>
                                <tr>
                                    <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                    <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                    <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                </tr>
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;">
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                            <div class="single-contact-icon-info__image">
                                                <i class="glyph-icon flaticon-039-factory-1" style="font-size: 35px;"></i>
                                            </div>
                                            <div class="single-contact-icon-info__content">
                                                <p><h4 class="title">PRODUCTION</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation table_tr_color">Raw Material</span>
                                    </td>
                                    <td>Near Mixer at Site</td>
                                    <td>At Batching Plant</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Weighing</span>
                                    </td>
                                    <td>Manual</td>
                                    <td>Computerised</td>
                                </tr>
                                <tr>    
                                    <td class="table_tr_color">
                                        <span class="product-variation">Moisture Control</span>
                                    </td>
                                    <td>Approximate</td>
                                    <td>Lab Controlled</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">W/C ratio</span>
                                    </td>
                                    <td>Manual(Approx)</td>
                                    <td>Computerised</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">    
                                        <span class="product-variation">Dosing Of Admixtures</span>
                                    </td>
                                    <td>Manual(Approx)</td>
                                    <td>Computerised</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Mixing</span>
                                    </td>
                                    <td>Tilting Mixer</td>
                                    <td>Non Tilting Single Shaft Mixer</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Discharge</span>
                                    </td>
                                    <td>Platform</td>
                                    <td>Transit Mixer / Pump</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">    
                                        <span class="product-variation">Rate</span>
                                    </td>
                                    <td>6 Cmt. Per. Hr.</td>
                                    <td>30 Cmt. Per. Hr.</td>
                                </tr>
                            </tbody>
                            <tr>
                                <th style="border-color: #ffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                            </tr>
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;">
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                            <div class="single-contact-icon-info__image">
                                                
                                                <i class="glyph-icon flaticon-012-scheme" style="font-size: 35px;"></i>
                                            </div>
                                            <div class="single-contact-icon-info__content">
                                                
                                                <p><h4 class="title">QUALITY</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Degree Of Control</span>
                                    </td>
                                    <td>Fair</td>
                                    <td>Excellent</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Yield</span>
                                    </td>
                                    <td>Varaible</td>
                                    <td>Consistent</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">    
                                        <span class="product-variation">Testing Of Fresh Concrete</span>
                                    </td>
                                    <td>Once in 25 Batches</td>
                                    <td>Every Batch</td>
                                </tr>
                            </tbody>
                            <tr>
                                <th style="border-color: #ffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                            </tr>
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;"> 
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                            <div class="single-contact-icon-info__image">
                                                <i class="glyph-icon flaticon-015-cart" style="font-size: 35px;"></i>
                                            </div>
                                            <div class="single-contact-icon-info__content">
                                                <p><h4 class="title">RAW MATERIAL</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Selection / Sourcing</span>
                                    </td>
                                    <td>Depending On Availability</td>
                                    <td>Pre Determined Sources</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">    
                                        <span class="product-variation">Testing</span>
                                    </td>
                                    <td>Random</td>
                                    <td>Regular</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">    
                                        <span class="product-variation">Storage</span>
                                    </td>
                                    <td>Multi Location</td>
                                    <td>Single Point</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Storage Space Requirement</span>
                                    </td>
                                    <td>Large</td>
                                    <td>Limited & Compact</td>
                                </tr>
                            </tbody>
                            <tr>
                                <th style="border-color: #ffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                            </tr>
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;">
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                                <div class="single-contact-icon-info__image">
                                                    
                                                    <i class="glyph-icon flaticon-015-cart" style="font-size: 35px;"></i>
                                                </div>
                                            <div class="single-contact-icon-info__content">                    
                                                <p><h4 class="title">DELIVERY</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Mode</span>
                                    </td>
                                    <td>Manual</td>
                                    <td>Transit Mixer</td>
                                </tr>
                            </tbody>
                            <tr>
                                <th style="border-color: #ffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                            </tr>
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;">
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                                <div class="single-contact-icon-info__image">
                                                    
                                                    <i class="glyph-icon flaticon-021-worker" style="font-size: 35px;"></i>
                                                </div>
                                            <div class="single-contact-icon-info__content">
                                                
                                                <p><h4 class="title">PLACING</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Mode</span>
                                    </td>
                                    <td>Manual / Hoist / Crane</td>
                                    <td>Concrete Pump</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Rate</span>
                                    </td>
                                    <td>5 Cmt. Per Hr.</td>
                                    <td>30 Cmt. Per. Hr.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--=======  End of cart table  =======-->
                </div>
            </div>
        </div>
    </div>
    <!--=======  End of shopping cart wrapper  =======-->
</div>
<!--====================  End of page content wrapper  ====================-->
<!--====================  cta area ====================-->
<div class="cta-area bg-img section-space--inner--190" data-bg="{{ asset('assets/front/img/backgrounds/RMC_Process_video.jpg') }}" style="margin-bottom: 0%;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <!-- cta content erapper -->
                <div class="cta-content__wrapper text-center">
                    <!-- <h2 class="cta-content__title section-space--bottom--40">Focusing on strategic decisions & practical actions</h2> -->
                    <div class="video-play-button-wrapper video-popup">
                        <a class="d-block" href="https://www.youtube.com/watch?v=iO9s0YXGnnM">
                            <div class="video-play-icon" style="">
                                <i class="ion-ios-play" style=""></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of cta area  ====================-->
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120 technology_sidebar1">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">      
            </div>
            <div class="col-lg-4">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;color: #ffffff;"></i>
                                </div>
                                <h3 class="page-title" style="color: #ffffff;">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection  

