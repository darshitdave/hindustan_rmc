@extends('layouts.front.front')
@section('title','Hindustan RMC | Equipment on Hire')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg bg-img section-space--inner--bottom--50 section-space--inner--top--120" data-bg="{{ asset('assets/front/img/industry/Web_Banners_equipment_on_hire.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Equipment on Hire</h2>
            </div>
            <div class="col-lg-12">
                <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                    <li class="has-children"><a href="{{route('front.home.page')}}">Home</a></li>
                    <li>Equipment on Hire</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="service-details-page-content">

                    <div class="service-details-text-wrapper section-space--bottom--80">
                        <h2 class="title">Construction Equipment on Hire </h2>
                        <p>We provide various of quality construction equipments on hire. Most of our equipments are top notch quality equipments built from reputed companies like Schwing Stetter. They are consistently used for us and well maintained to give optimal performance because of the value they carry for us. Renting equipment is a good idea compared to buying as it saves high initial purchase cost, freedom from storage and maintenance, no depreciation costs to be borne, transportation issue, space issue and no downtime costs as they are hired only for project durations.</p>
                    </div>
                    <div class="service-details-image-desc section-space--bottom--120">
                        <div class="service-details-image-desc__wrapper">
                            <div class="service-details-image-desc__single">
                                <div class="row row-35 align-items-center">
                                    <div class="col-lg-4 col-md-5">
                                        <div class="image">
                                            <img src="{{ asset('assets/front/img/banner/1_service.png') }}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-7">
                                        <div class="content">
                                            <div class="service-details-text-wrapper">
                                                <h2 class="title">Concrete Mixer (Transit Mixer - TM) on Hire</h2>
                                                <p>9 cu.m concrete carrying capacity. Built by Schwing Stetter. High loading volume and optimum drive characteristics. Wear resistant plates and Stetter T protection. More information here.</p>
                                                <p><a href="https://www.schwingstetterindia.com/products/concrete-mixer/" class="see-more-link see-more-link--dark">Know More <i class="ion-arrow-right-c"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="service-details-image-desc__single">
                                <div class="row row-35 align-items-center">
                                    <div class="col-lg-4 col-md-5">
                                        <div class="image">
                                            <img src="{{ asset('assets/front/img/banner/2_service.png') }}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-7">
                                        <div class="content">
                                            <div class="service-details-text-wrapper">
                                                <h2 class="title">Stationary Pump on Hire</h2>
                                                <p>The epitome of concrete pumps, only choice to pump concrete to record breaking heights. Built by Schwing Stetter. Pumping at minized fuel cost with proven Open circuit and rugged ROCK valve system. Theoritical pumping of up to 116 cu.m/hr or Maximum concrete pressure of up to 243 bar. More information here.</p>
                                                <p><a href="https://www.schwingstetterindia.com/products/stationary-pump/" class="see-more-link see-more-link--dark">Know More <i class="ion-arrow-right-c"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="service-details-image-desc__single">
                                <div class="row row-35 align-items-center">
                                    <div class="col-lg-4 col-md-5">
                                        <div class="image">
                                            <img src="{{ asset('assets/front/img/banner/3_service.png') }}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-7">
                                        <div class="content">
                                            <div class="service-details-text-wrapper">
                                                <h2 class="title">RMC Plant on Hire (with or without manpower)</h2>
                                                <p>60 cu.m/hr plant with conveyor feeding option with Stetter Twin Shaft Mixer. Built by Schwing Stetter. Ideal for construction project like Road, bridge, canals etc. Inline silo for storage of aggregates. MCI 550 control system with SCADA software with numerous features. More information here.</p>
                                                <p><a href="https://www.schwingstetterindia.com/products/30-60-cumhour/" class="see-more-link see-more-link--dark">Know More <i class="ion-arrow-right-c"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="service-details-image-desc__single">
                                <div class="row row-35 align-items-center">
                                    <div class="col-lg-4 col-md-5">
                                        <div class="image">
                                            <img src="{{ asset('assets/front/img/banner/Service_Changing_Boom.png') }}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-7">
                                        <div class="content">
                                            <div class="service-details-text-wrapper">
                                                <h2 class="title">Placing Boom on Hire</h2>
                                                <p>Placing booms provide a systematic method of concrete distribution. Built by Schwing Stetter. Octagonal tubular support mast with self climbing platform. Split boom option for faster and easier erection. 35 meter boom length reach. More information here.</p>
                                                <p><a href="https://www.schwingstetterindia.com/products/placing-boom/" class="see-more-link see-more-link--dark">Know More <i class="ion-arrow-right-c"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="service-details-image-desc__single">
                                <div class="row row-35 align-items-center">
                                    <div class="col-lg-4 col-md-5">
                                        <div class="image">
                                            <img src="{{ asset('assets/front/img/banner/5_service.png') }}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-7">
                                        <div class="content">
                                            <div class="service-details-text-wrapper">
                                                <h2 class="title">Truck Mounted Boom Pump on Hire</h2>
                                                <p>Concrete pump mounted 4 axle truck with RZ fold placing boom. Built by Schwing Stetter. Up to 43 meter boom with curved super x outriggers in front. Up to 164 cu.m / hr long stroking with Big rock valve ensuring maximum filling efficiency. High volume pours in infrastructure work, commercial projects and bridges. More information here.</p>
                                                <p><a href="https://www.schwingstetterindia.com/products/truck-mounted-pump/" class="see-more-link see-more-link--dark">Know More <i class="ion-arrow-right-c"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="service-details-image-desc__single">
                                <div class="row row-35 align-items-center">
                                    <div class="col-lg-4 col-md-5">
                                        <div class="image">
                                            <img src="{{ asset('assets/front/img/banner/6_service.png') }}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-7">
                                        <div class="content">
                                            <div class="service-details-text-wrapper">
                                                <h2 class="title">Wheel loader on Hire</h2>
                                                <p>1 -3 cu. mt. bucket carrying capacity. Built by Schwing Stetter. Strong and relaiable power train. Z - bar linkage for greater digging power. Up to 170 KN of bucket breakforce. More information here.</p>
                                                <p><a href="https://www.schwingstetterindia.com/products/wheel-loader/" class="see-more-link see-more-link--dark">Know More <i class="ion-arrow-right-c"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="service-details-image-desc__single">
                                <div class="row row-35 align-items-center">
                                    <div class="col-lg-4 col-md-5">
                                        <div class="image">
                                            <img src="{{ asset('assets/front/img/banner/7_service.png') }}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-7">
                                        <div class="content">
                                            <div class="service-details-text-wrapper">
                                                <h2 class="title">Tower Crane on Hire</h2>
                                                <p>Maximum lift of 16 Ton at 3.0 m radius. Built by Schwing Stetter. Up to 2 ton tip load. 35 meter free standing height to 80 meter Jib length. Trolley equipped with front and rear trolley mechanism. More information here.</p>
                                                <p><a href="https://www.schwingstetterindia.com/products/tower-crane/" class="see-more-link see-more-link--dark">Know More <i class="ion-arrow-right-c"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="service-details-image-desc__single">
                                <div class="row row-35 align-items-center">
                                    <div class="col-lg-4 col-md-5">
                                        <div class="image">
                                            <img src="{{ asset('assets/front/img/banner/8_service.png') }}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-7">
                                        <div class="content">
                                            <div class="service-details-text-wrapper">
                                                <h2 class="title">Line Pump on Hire</h2>
                                                <p>Schwing line pump is a compact truck mounted pump. Built by Schwing Stetter. Deployment and set up time will be short. Optional pipe rack to carry up to 100 meters of pipelines. Water cleaning system also available to clean pipelines and other accessories. Rock valve with Fast switching System. More information available here.</p>
                                                <p><a href="https://www.schwingstetterindia.com/products/line-pump/" class="see-more-link see-more-link--dark">Know More <i class="ion-arrow-right-c"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
<!--====================  cta area ====================-->
<div class="cta-area section-space--inner--60 default-bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <!-- cta text -->
                <h2 class="cta-text text-center text-lg-left">Looking for quality construction partner?</h2>
            </div>
            <div class="col-lg-3 home_button text-center text-lg-right">
                <a href="{{route('front.select_calculation.page')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark">RMC CALCULATOR</a>
            </div>
            <div class="col-lg-2 text-center text-lg-right">
                <a href="{{route('admin.getQuoteCalculator')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark">ORDER NOW</a>
            </div>
        </div>
    </div>
</div>
<!--====================  End of cta area  ====================-->
@endsection  