@extends('layouts.front.front')
@section('title','Hindustan RMC | Category Image')
@section('content')

<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('uploads/image_category_banner') }}/{{ $category_name->category_banner }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">{{$category_name->category_name}}</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="{{route('front.comCareer.page')}}">Career</a></li>
                    <li>{{$category_name->category_name}}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@if(isset($category_images) && count($category_images) >= 1)
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper slider_infrastructure section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="case-study__image-gallery-wrapper section-space--top--80">
                    <div class="row image-popup">
                        @foreach($category_images as $ck => $cv)
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="case-study__single-gallery-image">
                                <a href="{{ asset('uploads/Category_Sub_Images') }}/{{ $cv->sub_images }}" class="single-gallery-thumb">
                                    <img src="{{ asset('uploads/Category_Sub_Images') }}/{{ $cv->sub_images }}" class="img-fluid" alt="">
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@else
<div class="page-content-wrapper slider_infrastructure section-space--inner--120">
</div>
@endif
@endsection  