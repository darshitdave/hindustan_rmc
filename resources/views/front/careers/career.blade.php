@extends('layouts.front.front')
@section('title','Hindustan RMC | Careers')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/Careers_banner.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Careers</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="javascript:void(0);">Support</a></li>
                    <li>Careers</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  featured project area ====================-->
<div class="featured-project-area bg-img section-space--inner--top--120 section-space--inner--bottom--300 career_image">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="featured-project-wrapper">
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- section title left align -->
                            <div class="section-title-area">
                                <!-- <h4 class="subtitle">FEATURED PROJECTS</h4> -->
                                <h2 class="title title--left">We're constantly looking for hard-working, ambitious and progressive candidates.</h2>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <!-- section title content -->
                            <div class="section-title-content-area">
                                <p class="section-title-content">Come and join our Hindustan RMC family where people have grown from ground staff to being handling bigger responsibilities and larger profiles whlle fulfilling their own dreams and pushing us closer to our vision. We have people who have been with us since years and have no intention of going elsewhere given the work environment we provide and growth oppportunities.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of featured project area  ====================-->
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper career-page">
    <div class="career-single__row-wrapper">
        <div class="row no-gutters">
            <div class="col-xl-3 col-md-6 order-2 order-md-1">
                <div class="career-single__content">
                    <h3 class="career-single__title">Internship Opportunities</h3>
                    <p class="career-single__text">If you are a graduate or undergraduate looking to gain hands-on experience at a reputed company, grab a chance to become an intern with Hindustan RMC.</p>
                    <a href="mailto:careers@hindustanrmc.com?subject=Internship Opportunities" class="ht-btn ht-btn--transparent ht-btn--transparent--dark-border">APPLY NOW</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 order-1 order-md-2">
                <div class="career-single__image bg-img" data-bg="{{ asset('assets/front/img/career/1_careers.png')}}"></div>
            </div>
            <div class="col-xl-3 col-md-6 order-3">
                <div class="career-single__content career-single__content--yellow-bg">
                    <h3 class="career-single__title" style="color: #eee;">Plant Manager</h3>
                    <p class="career-single__text" style="color: #eee;">Can run the entire plant from production, managing finances, handling customers and employees? Make sure you don't miss the chance to work with us.</p>
                    <a href="mailto:careers@hindustanrmc.com?subject=Plant Manager" class="ht-btn ht-btn--transparent ht-btn--transparent--dark-border" style="color: #eee;border: 2px solid #eee;">APPLY NOW</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 order-4">
                <div class="career-single__image bg-img" data-bg="{{ asset('assets/front/img/career/2_careers.png')}}"></div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-xl-3 col-md-6 order-2 order-md-1">
                <div class="career-single__content career-single__content--yellow-bg">
                    <h3 class="career-single__title" style="color: #eee;">Lab Technician</h3>
                    <p class="career-single__text" style="color: #eee;">Can you run RMC quality lab and ensure that the material dispatched is always top-notch quality? Learn from people who deliver quality concrete every time.</p>
                    <a href="mailto:careers@hindustanrmc.com?subject=Lab Technician" class="ht-btn ht-btn--transparent ht-btn--transparent--dark-border" style="color: #eee;border: 2px solid #eee;">APPLY NOW</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 order-1 order-md-2">
                <div class="career-single__image bg-img" data-bg="{{ asset('assets/front/img/career/3_careers.png')}}"></div>
            </div>

            <div class="col-xl-3 col-md-6 order-3">
                <div class="career-single__content">
                    <h3 class="career-single__title">Sales Executive</h3>
                    <p class="career-single__text">After water, concrete is the second-most consumed material in the world but selling it is a different story! If you can do it, let's talk.</p>
                    <a href="mailto:careers@hindustanrmc.com?subject=Sales Executive" class="ht-btn ht-btn--transparent ht-btn--transparent--dark-border">APPLY NOW</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 order-4">
                <div class="career-single__image bg-img" data-bg="{{ asset('assets/front/img/career/4_careers.png')}}"></div>
            </div>
        </div>
    </div>
</div>
<div class="page-content-wrapper career-page1" style="display: none;">
    <div class="career-single__row-wrapper">
        <div class="row no-gutters">
            <div class="col-xl-3 col-md-6 order-2 order-md-1">
                <div class="career-single__image bg-img" data-bg="{{ asset('assets/front/img/career/1_careers.png')}}"></div>
            </div>
            <div class="col-xl-3 col-md-6 order-1 order-md-2">
                <div class="career-single__content">
                    <h3 class="career-single__title">Internship Opportunities</h3>
                    <p class="career-single__text">If you are a graduate or undergraduate looking to gain hands-on experience at a reputed company, grab a chance to become an intern with Hindustan RMC.</p>
                    <a href="mailto:careers@hindustanrmc.com?subject=Internships Program" class="ht-btn ht-btn--transparent ht-btn--transparent--alt">APPLY NOW</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 order-3">
                <div class="career-single__content career-single__content--yellow-bg">
                    <h3 class="career-single__title" style="color: #eee;">Plant Manager</h3>
                    <p class="career-single__text" style="color: #eee;">Can run the entire plant from production, managing finances, handling customers and employees? Make sure you don't miss the chance to work with us.</p>
                    <a href="mailto:careers@hindustanrmc.com?subject=Internships Program" class="ht-btn ht-btn--transparent ht-btn--transparent--alt" style="color: #eee;border: 2px solid #eee;">APPLY NOW</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 order-4">
                <div class="career-single__image bg-img" data-bg="{{ asset('assets/front/img/career/2_careers.png')}}"></div>
            </div>

            <div class="col-xl-3 col-md-6 order-4 ">
                <div class="career-single__content">
                    <h3 class="career-single__title" >Lab Technician</h3>
                    <p class="career-single__text" >Can you run RMC quality lab and ensure that the material dispatched is always top-notch quality? Learn from people who deliver quality concrete every time.</p>
                    <a href="mailto:careers@hindustanrmc.com?subject=Internships Program" class="ht-btn ht-btn--transparent ht-btn--transparent--alt" >APPLY NOW</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 order-5 ">
                <div class="career-single__image bg-img" data-bg="{{ asset('assets/front/img/career/3_careers.png')}}"></div>
            </div>
            <div class="col-xl-3 col-md-6 order-6">
                <div class="career-single__content career-single__content--yellow-bg">
                    <h3 class="career-single__title" style="color: #eee;">Sales Executive</h3>
                    <p class="career-single__text" style="color: #eee;">After water, concrete is the second-most consumed material in the world but selling it is a different story! If you can do it, let's talk.</p>
                    <a href="mailto:careers@hindustanrmc.com?subject=Internships Program" class="ht-btn ht-btn--transparent ht-btn--transparent--alt" style="color: #eee;border: 2px solid #eee;">APPLY NOW</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 order-7">
                <div class="career-single__image bg-img" data-bg="{{ asset('assets/front/img/career/4_careers.png')}}"></div>
            </div>
        </div>
    </div>
</div>
<!--====================  end of page content wrapper ====================-->
<!--====================  featured project area ====================-->
<div class="featured-project-area bg-img section-space--inner--top--120 section-space--inner--bottom--300 career_image">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="featured-project-wrapper">
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- section title left align -->
                            <div class="section-title-area">
                                <!-- <h4 class="subtitle">FEATURED PROJECTS</h4> -->
                                <h2 class="title title--left">We’ve got the perfect work - fun balance.</h2>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <!-- section title content -->
                            <div class="section-title-content-area">
                                <p class="section-title-content">At Hindustan, we are like a work family which dreams together, works hard together and achieves milestones together. Starting from morning aarti to evening aarti and planning targets for the new year to celebrating the year end on 31st December, we breathe and share our lives together. Our employees feel like this is their second home and we ensure to give the best possible lives to them.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of featured project area  ====================-->
@if(isset($category) && count($category) >= 1)
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--60" style="padding-top: 18px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="image-list-text mt-0 section-space--bottom--60">
                @foreach($category as $ck => $cv)
                    <div class="image-wrapper">
                        <a href="{{route('front.comCareerImage.page',$cv->category_slug)}}">
                            <div class="image">

                                
                                @if(isset($cv->category->sub_images) && !is_null($cv->category->sub_images))
                                <img src="{{ asset('uploads/Category_Sub_Images') }}/{{ $cv->category->sub_images }}" class="img-fluid" alt="">
                                @endif
                            </div>
                            <div class="image-text">{{$cv->category_name}}</div>
                        </a>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endif
<!--====================  career background area ====================-->
<div class="career-background__area career-background__area-bg bg-img section-space--inner--120" data-bg="{{ asset('assets/front/img/backgrounds/lets_grow.png') }}" style="padding-top: 75px !important;">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-7 col-md-10">
                <!-- career background-content -->
                <div class="career-background__content">
                    <h3 class="title"> <span class="highlight">Let's Grow</span> Together</h3>
                    <p class="text">As you are looking to build a career, we are also dreaming with you to help you achieve your dreams, come and join us - so we can fulfill your dreams and together we can fulfil our dream of making Hindustan RMC one of the largest construction partners in the country.</p>
                    <!-- <a href="page-career.html" class="ht-btn ht-btn--dark ht-btn--dark--style2">TRACTOR CAREERS</a> -->
                    <a href="mailto:careers@hindustanrmc.com" style="color: #222 !important;"><h4 class="call-text">Write to us at: careers@hindustanrmc.com</h4></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of career background area  ====================-->

@endsection