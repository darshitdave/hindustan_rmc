@extends('layouts.front.front')
@section('title','Hindustan RMC | Blog Detail')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/banner/Blog_banner.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Blog Detail</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="{{route('front.blog_list.page')}}">Blog</a></li>
                    <li>Blog Detail</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <!-- blog content wrapper -->
                <div class="blog-content-wrapper">
                    <div class="single-list-blog-post mb-0">
                        <div class="single-list-blog-post__media section-space--bottom--50">
                            <img src="{{asset('uploads/blog_image/'.$get_blog->blog_image)}}" class="img-fluid" alt="">
                        </div>
                        <div class="single-list-blog-post__content single-list-blog-post__content--style2">
                            <h1 class="title">{{$get_blog->title}}</h1>
                            <div class="meta-list">
                                <div class="single-meta">
                                    <?php $date = date('M d Y',strtotime($get_blog->date)); ?>
                                    <p class="date">{{$date}}</p>
                                </div>
                                <div class="single-meta">
                                    <p class="author">BY <a href="#">{{$get_blog->author_name}}</a></p>
                                </div>
                            </div>
                            <div class="">
                                <?php echo $get_blog->description ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <div class="newsletter-box-wrapper bg-img" data-bg="assets/img/icons/message.png">
                            <h3 class="newsletter-title">GET IN TOUCH!</h3>
                            <div class="newsletter-form-wrapper infra_in_touch">
                                <form method="post" action="{{ route('admin.insertContactDetail') }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="text" name="con_name" placeholder="Name *" required="required">
                                    <input type="email" name="con_email" placeholder="Your Email *" class="contact_mail" required="required">
                                    <input type="tel" placeholder="Phone Number" name="con_phone" maxlength="10" pattern="^\d{10}$" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                                    <textarea placeholder="   Message" name="con_message" id="con_message" class="infra_message"></textarea>
                                    <div class="g-recaptcha" data-sitekey="{{env('NOCAPTCHA_SITEKEY')}}" style="transform:scale(0.86);-webkit-transform:scale(0.86);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                                    <button type="submit" id="submit">Submit</button>
                                </form>
                            </div>
                            <a href="tel:1800123999977" style="color: #222 !important;"><h3 class="call-text">Call Us: 1800-123-9999-77</h3></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection
@section('js')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>


$('form').on('submit', function(e) {
  if(grecaptcha.getResponse() == "") {
    e.preventDefault();
    toastr.error('Please Check Captcha');
  } else {
        toastr.success('Thank you for reaching out to us.<br>We will soon get in touch!');
  }
});
</script>
@endsection