@extends('layouts.front.front')
@section('title','Hindustan RMC | Blog List')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/banner/Blog_banner.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Blog List</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="javascript:void(0);">Blog</a></li>
                    <li>Blog List</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
@if(isset($get_blog) && count($get_blog) >= 1)
<div class="page-content-wrapper section-space--inner--120 " style="padding-top: 80px;padding-bottom: 80px;background-color: #fff;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- blog grid wrapper -->
                <div class="blog-grid-wrapper">
                    <div class="row">
                        @foreach($get_blog as $gk => $gv)
                        <div class="col-lg-4 col-md-6">
                            <div class="blog-post-slider__single-slide blog-post-slider__single-slide--grid-view">
                                <div class="blog-post-slider__image section-space--bottom--30">
                                    <a href="{{route('front.blog_detail.page',$gv->blog_slug)}}"><img src="{{asset('uploads/blog_image_thumb/'.$gv->blog_image)}}" class="img-fluid" alt="" ></a>
                                </div>
                                <div class="blog-post-slider__content">
                                    <?php $date = date('M d Y',strtotime($gv->date)); ?>
                                    <p class="post-date">{{$date}}</p>
                                    <h3 class="post-title">
                                        <a href="{{route('front.blog_detail.page',$gv->blog_slug)}}">{{$gv->title}}</a>
                                    </h3>
                                    <p class="post-excerpt">
                                        <?php

                                        $text = $gv->description;
                                        $text = strip_tags($text,"<style>");
                                        $substring = substr($text,strpos($text,"<style"),strpos($text,"</style>"));
                                        $text = str_replace($substring,"",$text);
                                        $text = str_replace(array("\t","\r","\n"),"",$text);
                                        $text = trim($text);
                                        $position=112; // Define how many character you want to display.
                                        $message= $text;
                                        $post = substr($message, 0, $position);

                                        echo strip_tags($post);
                                        echo "...";
                                        
                                        ?>
                                    </p>
                                    <a href="{{route('front.blog_detail.page',$gv->blog_slug)}}" class="see-more-link blog-readmore">Read More <i class="ion-arrow-right-c"></i></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!--====================  End of page content wrapper  ====================-->
@endsection