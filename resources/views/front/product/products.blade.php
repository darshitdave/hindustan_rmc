@extends('layouts.front.front')
@section('title','Hindustan RMC | Products')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg bg-img section-space--inner--bottom--50 section-space--inner--top--120" data-bg="{{ asset('assets/front/img/backgrounds/product_banner.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Products</h2>
            </div>
            <div class="col-lg-12">
                <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                    <li class="has-children"><a href="{{route('front.home.page')}}">Home</a></li>
                    <li>Products</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<!--====================  case study filter area ====================-->
<div class="page-content-wrapper section-space--inner--120 grey-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <!-- filter title -->
                <ul class="project-filter-menu section-space--bottom--80">
                    <!-- <li class="active" data-filter="*">All</li> -->
                    <li class="active" data-filter=".performance" data-id="0">Performance Based Concrete</li>
                    <li data-filter=".concrete">Usage Based Concrete</li>
                    <li data-filter=".decorative">Decorative Concrete</li>
                    <li data-filter=".green">Green Concrete</li>
                    <!-- <li data-filter=".oil-gas">OIL & GAS</li> -->
                </ul>

                <!-- filter content -->
                <div class="project-filter-content">
                    <div class="row project-isotope">
                        <div class="col-lg-4 col-md-6 performance">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','compacting-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/1.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 performance">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','strength-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/2.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 performance">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','durable-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/12.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 performance">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','corrosion-resistant-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/3.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 performance">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','early-strength-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/4.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 performance">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','performance-enhanced-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/7.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 concrete">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','waterproof-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/5.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 concrete">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','reinforced-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/8.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 concrete">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','fire-resistant-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/9.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 concrete">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','crack-resistant-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/10.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 concrete">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','durable-foundation-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/11.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 concrete">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','flowing-column-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/13.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 concrete">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','ice-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/6.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 decorative">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','color-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/product/14.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 green">
                            <!-- single project -->
                            <div class="single-case-study-project single-case-study-project--style2">
                                <a href="{{route('front.about_sub_product.page','lightweight-concrete')}}">
                                    <span class="case-readmore"><i class="ion-arrow-right-c"></i></span>
                                    <div class="single-case-study-project__image">
                                        <img src="{{ asset('assets/front/img/projects/last.png') }}" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of case study filter area  ====================-->
<!--====================  cta area ====================-->
<div class="cta-area section-space--inner--60 default-bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <!-- cta text -->
                <h2 class="cta-text  text-center text-lg-left">Looking for quality construction partner?</h2>
            </div>
            <div class="col-lg-3 home_button text-center text-lg-right">
                <a href="{{route('front.select_calculation.page')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark" style="color: #ffff;">RMC CALCULATOR</a>
            </div>
            <div class="col-lg-2 text-center text-lg-right">
                <a href="{{route('admin.getQuoteCalculator')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark" style="color: #ffff;">ORDER NOW</a>
            </div>
        </div>
    </div>
</div>
<!--====================  End of cta area  ====================-->
@endsection
@section('js')
<script>
$(document).ready(function() {

    $('[data-id="0"]').click();

});
</script>
@endsection