@extends('layouts.front.front')
@if($type == 'compacting-concrete')
    @section('title','Hindustan RMC | Self Compacting Concrete')
@elseif($type == 'strength-concrete')
    @section('title','Hindustan RMC | High Strength Concrete')
@elseif($type == 'durable-concrete')
    @section('title','Hindustan RMC | High Performance Durable Concrete')
@elseif($type == 'corrosion-resistant-concrete')
    @section('title','Hindustan RMC | Corrosion Resistant Concrete')
@elseif($type == 'early-strength-concrete')
    @section('title','Hindustan RMC | High Early Strength Concrete')
@elseif($type == 'performance-enhanced-concrete')
    @section('title','Hindustan RMC | Performance Enhanced Concrete')
@elseif($type == 'waterproof-concrete')
    @section('title','Hindustan RMC | Waterproof Concrete')
@elseif($type == 'reinforced-concrete')
    @section('title','Hindustan RMC | Fiber Reinforced Concrete')
@elseif($type == 'fire-resistant-concrete')
    @section('title','Hindustan RMC | Fire Resistant Concrete')
@elseif($type == 'crack-resistant-concrete')
    @section('title','Hindustan RMC | Crack Resistant Slab Concrete')
@elseif($type == 'durable-foundation-concrete')
    @section('title','Hindustan RMC | Durable Foundation Concrete')
@elseif($type == 'flowing-column-concrete')
    @section('title','Hindustan RMC | Flowing Column Concrete')
@elseif($type == 'color-concrete')
    @section('title','Hindustan RMC | Colour Concrete')
@elseif($type == 'lightweight-concrete')
    @section('title','Hindustan RMC | Lightweight Concrete')
@elseif($type == 'ice-concrete')
    @section('title','Hindustan RMC | Ice Concrete')
@endif
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg bg-img section-space--inner--bottom--50 section-space--inner--top--120" data-bg="{{ asset('assets/front/img/backgrounds/product_banner.png') }}">
    <div class="container">
        <div class="row align-items-center">
            @if($type == 'compacting-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Self Compacting Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Self Compacting Concrete</li>
                    </ul>
                </div>
            @elseif($type == 'strength-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">High Strength Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>High Strength Concrete</li>
                    </ul>
                </div>
            @elseif($type == 'durable-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">High Performance Durable Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>High Performance Durable Concrete</li>
                    </ul>
                </div>
            @elseif($type == 'corrosion-resistant-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Corrosion Resistant Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Corrosion Resistant Concrete</li>
                    </ul>
                </div>               
            @elseif($type == 'early-strength-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">High Early Strength Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>High Early Strength Concrete</li>
                    </ul>
                </div>            
            @elseif($type == 'performance-enhanced-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Performance Enhanced Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>High Early Strength Concrete</li>
                    </ul>
                </div>      
            @elseif($type == 'waterproof-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Waterproof Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Waterproof Concrete</li>
                    </ul>
                </div>          
            @elseif($type == 'reinforced-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Fiber Reinforced Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Fiber Reinforced Concrete</li>
                    </ul>
                </div>
            @elseif($type == 'fire-resistant-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Fire Resistant Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Fire Resistant Concrete</li>
                    </ul>
                </div>
            @elseif($type == 'crack-resistant-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Crack Resistant Slab Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Crack Resistant Slab Concrete</li>
                    </ul>
                </div>
            @elseif($type == 'durable-foundation-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Durable Foundation Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Durable Foundation Concrete</li>
                    </ul>
                </div> 
            @elseif($type == 'flowing-column-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Flowing Column Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Flowing Column Concrete</li>
                    </ul>
                </div>              
            @elseif($type == 'color-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Colour Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Colour Concrete</li>
                    </ul>
                </div>          
            @elseif($type == 'lightweight-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Lightweight Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Lightweight Concrete</li>
                    </ul>
                </div>      
            @elseif($type == 'ice-concrete')
                <div class="col-lg-12">
                    <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Ice Concrete</h2>
                </div>
                <div class="col-lg-12">
                    <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                        <li class="has-children"><a href="{{route('front.about_product.page')}}">Products</a></li>
                        <li>Ice Concrete</li>
                    </ul>
                </div>         
            @endif

        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<!--====================  featured project area ====================-->
<div class="featured-project-area bg-img section-space--inner--top--120 section-space--inner--bottom--300 sub_product_image" style="padding-bottom: 22px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="featured-project-wrapper">
                    <div class="row">
                        @if($type == 'compacting-concrete')
                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{asset('assets/front/img/projects/product/1.png')}}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                    <p class="section-title-content">Hindustan RMC Easy Flow is a special concrete, produced by using high-quality ingredients including the latest 4th generation acrylic based super plasticizers and viscosity modifying agents that lend it a unique self-compacting feature, which in turn allows complicated, intricate and elegant designs without leaving any voids.</p>
                                </div>
                            </div>
                        @elseif($type == 'strength-concrete')
                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/2.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                    <p class="section-title-content">Mazboot is defined as a High Strength concrete meeting special combination of performance and uniformity requirements that cannot always be achieved routinely using conventional constituents and normal mixing, placing, and curing practices.<br><br>
Hindustan RMC Mazboot is produced by using multiple blends of cementitious materials along with advanced-quality superplasticizers. This results in refinement of the micro-pore structure of the concrete, leading to very dense, impervious and long-lasting structures. It is a boon for modern constructions that call for innovative sleek designs with maximum space utilization.</p>
                                </div>
                            </div>
                        @elseif($type == 'durable-concrete')
                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/12.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                    <p class="section-title-content">Hindustan RMC Heavy Duty is made by careful proportioning of binder in order to control heat of hydration to help reduce micro-cracks and enhance the durability of structure. This concrete has excellent workability and controlled temperature rise leads to superior durability. Its superior durability makes it ideal for concrete roads and mass concreting jobs like foundation rafts.</p>
                                </div>
                            </div>
                        @elseif($type == 'corrosion-resistant-concrete')

                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/3.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                    <p class="section-title-content">The corrosion of reinforcement steel in concrete is a universal problem. It is one of the major reasons for failure of concrete due to loss of durability. If concrete is compacted well, leaving no voids, then the rate of corrosion can be controlled. For all practical purposes, even well compacted concrete is likely to have 1-2% voids through which ingress of moisture can take place, which leads to corrosion of steel. Steel embedded in concrete can expand 5-6 times more than its original volume due to corrosion, causing internal stresses and cracks in the concrete cover. These cracks further aggravate the corrosion process by allowing exposure of the steel.<br><br>
To overcome the ill effects of corrosion of the steel reinforcement, Hindustan RMC has introduced a special concrete called 'Rust Proof'.<br><br>
Hindustan RMC Rust Proof is a special concrete that uses high-quality, corrosion-inhibiting agents that impart protection to the steel reinforcement by providing a protective coating to it, reducing the rate of corrosion of steel embedded in the concrete. The result - the steel stays strong for long and so does the entire structure.</p>
                                </div>
                            </div>

                        @elseif($type == 'early-strength-concrete')

                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/4.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                    <p class="section-title-content">Hindustan RMC Time Saver is a special concrete solution designed to achieve high early strength and hence facilitate early stripping time. This increases the speed of construction. It is specially used in concrete repairs at busy traffic areas like roads, airports and runways. Hindustan RMC Time Saver also aids faster loading of structural elements viz. columns, beams, walls and floors. It comes with a dual advantage of workability and high early strength gain specially achieved through a judicious selection of special high performance additives and superior mix formulation techniques used by experts.</p>
                                </div>
                            </div>
                        @elseif($type == 'performance-enhanced-concrete')

                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/7.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                     <p class="section-title-content">Hindustan RMC Booster Concrete is a special concrete with an enhanced performance with respect to slump retention, early age strength and plastic shrinkage. It is specially designed with careful selection of raw material proportions and special additives to provide this enhanced performance. The special mix design of “Hindustan RMC” enables achieving excellent workability retentions without any ill-effect on the early strength of concrete. “Hindustan RMC Booster Concrete” also provides extended time for placing, compaction and finishing of concrete. There is a considerable change in the hydration process of concrete, which reduces incidence of plastic shrinkage cracking. This in turn improves the structural durability.</p>
                                </div>
                            </div>
                        @elseif($type == 'waterproof-concrete')

                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/5.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                     <p class="section-title-content">Hindustan RMC H<sub>2</sub>O Proof Concrete is an innovative waterproofing solution that alters the concrete’s performance from by transforming it from a hydrophilic sponge into a hydrophobic, waterproof, construction material. Hindustan RMC H<sub>2</sub>O Proof Concrete is produced by introducing special additives that shut down the active capillary transport mechanism and reduces absorption.</p>
                                </div>
                            </div>

                        @elseif($type == 'reinforced-concrete')

                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/8.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                     <p class="section-title-content">Hindustan RMC Fibrous is an enhanced crack resistant and relatively ductile concrete, produced by using different types of fibres like polyester, polypropylene, glass & steel, etc. that minimizes the problems of plastic shrinkage cracks of the concrete in the green stage and substantially improves its static and dynamic properties like flexural impact and tensile strength.</p>
                                </div>
                            </div>

                        @elseif($type == 'fire-resistant-concrete')

                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/9.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                     <p class="section-title-content">Hindustan RMC Fire Fighter helps in passive fire protection with its carefully chosen mix constituents that can withstand high elevated temperatures.<br><br>
These control fire spread and its effect by providing sufficient fire resistance to prevent loss of structural stability within a prescribed time period, which is based on the building’s occupancy and fire safety objectives.</p>
                                </div>
                            </div>
                        @elseif($type == 'crack-resistant-concrete')
                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/10.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                     <p class="section-title-content">Hindustan RMC Crackless has been designed for all kind of roofs and slabs. It is impervious, durable and has been designed by adding special additives and compounds that minimize chances of water seepage in the slab.<br><br>
Made with high precision engineered control cement, the additives contained in it prevents corrosion of the reinforcements and thereby increase durability of the structures.Crackless is designed with finely engineered fibres which helps in minimizing shrinkage cracks in the concrete, thereby enhancing the life of the roof. Since outside water does not seep in, it reduces the need for frequent repair or painting of internal walls.</p>
                                </div>
                            </div>
                        @elseif($type == 'durable-foundation-concrete')

                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/11.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                     <p class="section-title-content">Hindustan RMC Long-Lasting is a specially designed durable and impervious concrete for all types of substructure and foundations. It is manufactured by mixing high-precision engineered cement, suitably graded fine coarse aggregates and special additives. This makes it a very cohesive and a uniform mix. It is pumpable and can be poured with ease into places that are difficult to reach. It helps in maintaining uniform levels and is very compact. It reduces permeability of concrete, thereby preventing chloride and sulphate ion attack on reinforcements.</p>
                                </div>
                            </div>

                        @elseif($type == 'flowing-column-concrete')
                            <div class="col-md-6">
                                <!-- feature background -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/13.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="section-title-content-area">
                                     <p class="section-title-content">Hindustan RMC Column Con is an easy flowing concrete, specially designed for column applications aimed at achieving early stripping times, better surface finish and extended placement duration. Hindustan RMC Column Con gives you durable concrete which is able to withstand water and co<sub>2</sub> ingress and provides better corrosion resistant properties as compared to conventional mix.<br><br>
It helps cast slender columns with dense reinforcements and reduces risk of honeycombing while ensuring better appearance.</p>
                                </div>
                            </div>
                        @elseif($type == 'color-concrete')
                            <div class="col-lg-6">
                                <!-- section title left align -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/14.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-lg-6">
                                <!-- section title content -->
                                <div class="section-title-content-area">
                                    <p class="section-title-content">Hindustan RMC Colour Plus is coloured concrete available in a wide palette of fascinating colours, with customized shades being made available as per architectural requirements. It uses consistent quality UV-resistant pigments that help in retaining the true colour / shades for longer duration.</p>
                                </div>
                            </div>          
                        @elseif($type == 'lightweight-concrete')
                            <div class="col-lg-6">
                            <!-- section title left align -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/last.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-lg-6">
                                <!-- section title content -->
                                <div class="section-title-content-area">
                                    <p class="section-title-content">Hindustan RMC Lite-Wt. is a light weight concrete for non-load bearing elements such as roofs, architectural facing works and levelling courses that sustainably reduce the dead weight. This ingenious concrete is made by distributing foam or polystyrene beads in a uniform manner throughout a precisely designed mix. Hindustan RMC Lite-Wt. possesses excellent workability and can be placed in any desired shape. It also provides excellent heat and sound insulation as well as better fire resistance than ordinary concrete.</p>
                                </div>
                            </div>        
                        @elseif($type == 'ice-concrete')
                            <div class="col-lg-6">
                            <!-- section title left align -->
                                <div class="feature-background__image bg-img" data-bg="{{ asset('assets/front/img/projects/product/6.png') }}" style="width: 88%"></div>
                            </div>
                            <div class="col-lg-6">
                                <!-- section title content -->
                                <div class="section-title-content-area">
                                    <p class="section-title-content">Wherever mass concreting work is involved, large differences in temperature between the core and surface of structures results in thermal cracks. Higher concrete temperature means a lot of disadvantages. For example, high temperature leads to faster drying of concrete, loss of workability, formation of cold joints, excessive plastic shrinkage cracks, rapid evaporation of curing water and difficulties in finishing concrete and overall reduction in the 28 day compressive strength.</p>
                                    <p class="section-title-content">To overcome these disadvantages, Hindustan RMC has introduced a special concrete ‘Hindustan RMC Ice Concrete’</p>
                                    <p class="section-title-content">Hindustan RMC Ice Concrete is produced using chilled water or ice flakes that helps control the temperature of the concrete and maintain it within acceptable limits. The other ingredients used in this concrete are also thermally controlled to bring down the temperature within acceptable limits at the time of placing. This considerably reduces the ill-effects of high temperature like thermal cracks, plastic shrinkage, faster drying of concrete, formation of cold joints, etc.</p>
                                </div>
                            </div>          
                        @endif

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of featured project area  ====================-->
@if($type == 'compacting-concrete')
    <!--====================  page content wrapper ====================-->
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--60">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--60">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Highly effective for RCC members with intricate and complicated shapes and sizes</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--60">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Avoids defects such as segregation and honeycombing</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--60">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Superior surface finish enabling faster construction and lower manpower requirement</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--60">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Better degree of compaction, hence better impermeability & durability</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> A carefully proportioned concrete made with latest 4th generation super plasticizers and viscosity modifying agents that lend it a unique self-compacting feature</p></b>
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash"  style="color: #ef7f1b;"></i><b> This concrete is able to flow and compact under its own weight, without the need of any external vibration, whilst maintaining homogeneity</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Easy_Flow_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                    <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                        <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                        <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                        
                    </div>
                    <div class="tab-content service-details-tab__content-wrapper">
                        <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                          <i class="ion-flash" style="color: #ef7f1b;"></i><span> Concrete should not be allowed to dry and early curing should be commenced at site as soon as possible </span><br><br>
                          <i class="ion-flash" style="color: #ef7f1b;"></i><span> The shuttering should be properly designed to support the dead load and liquid pressure exerted by the concrete in green stage</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Shuttering needs to be erected and firmly supported to avoid bulging and dislocation</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Shuttering should be water tight</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Curing of concrete should be done as per IS: 456-2000. As concrete contains more powder content, prolonged curing is recommended</span>


                        </div>
                        <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                            <i class="ion-flash" style="color: #ef7f1b;"></i><span> In RCC members with heavy and congested reinforcement.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Architectural and complicated shape and design of intricate structure.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Retaining walls, raft, footings and pile foundations.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Repair, restoration and renewal of RCC structures.</span>
                            
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@elseif($type == 'strength-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--60">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--60">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Enhanced Load Carrying Capability enables innovative sleek designs with maximum space utilization</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--60">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Assurance of long-term durability</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--60">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Economy in the structural design due to reduction in dead loads</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> High compressive strength of M60 and above</p></b>
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash"  style="color: #ef7f1b;"></i><b> Dense and impervious concrete</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Majboot_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                            
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Placing of concrete should be done immediately after receiving the concrete at site </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Delayed, over/under compaction of the concrete should be avoided</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Finishing should be done immediately after compaction</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Ensure early and initial curing by way of covering the concrete with wet hessian cloth</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Broom finish texture should be provided for the finished concrete surface of slab in particular</span>


                            </div>
                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> RCC columns and beams in high rise buildings; columns, beams and girders for mega-urban infrastructure projects.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Precast structural elements.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Marine structures.</span>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($type == 'durable-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--60">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Increases resistance to sulphates, reduces chloride-ion penetration, and improves resistance to aggressive environments</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Reduces the heat of hydration</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Provides excellent durability</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Improved workability, pumpability, and placeability during concrete pours</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6" style="margin-top: -4% !important;">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Reduces shrinkage</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Superior later age strength, ideal for the structures where loads are expected after a time gap.</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Heavy_Duty_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                            
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Early and prolonged curing will result in better later-strength gain </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Moderate compaction to be done in order to avoid over or under compaction</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Proper mix designs need to be evaluated for all the concrete properties before the work commences</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Sampling and testing should be done as per IS specification to get desired results</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Recommended to use where lighter shades of concrete are required</span>


                            </div>
                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> Massive raft foundations and deep beams.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Bridge foundations and bridge piers.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Large retaining walls.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Mass concrete works in dams, hydro-electric and power projects.</span>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@elseif($type == 'corrosion-resistant-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--20">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Allows the concrete cover to remain intact for longer duration</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Improves the durability and service life of the structure</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Savings from elevation of pre-coating to steel bars and concrete surfaces</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Made from high-quality, corrosion-inhibiting agents that impart protection to the steel reinforcement by providing a protective coating to it</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Corrosion Resistant Concrete reduces the corrosion initiation & rate of corrosion of steel embedded in the concrete</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Rust_Proof_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Steel reinforcement used should be free of any rust </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Before concrete is placed, steel reinforcement should be given cement water slurry wash</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> The concrete should be compacted well, leaving practically no voids</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Specified depth of cover, as per BIS standards, should be strictly provided</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> During compaction it must be ensured that the cover blocks provided to the reinforcement are not displaced</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> If cover blocks are displaced, the same should be restored immediately before placing the concrete</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> In very aggressive and saline exposure condition, protective epoxy or equivalent coatings to the concrete may also be provided</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
@elseif($type == 'early-strength-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--60">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Concrete roads can be repaired in less than 24 hrs</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Time Saver can double the number of daily rotations of formwork for walls, columns, slabs on grade and grade beams, allowing greater flexibility in construction schedules in relation to the rotation of formwork.</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Opportunity to regain project time lost due to climatic conditions, breakdown etc.</b>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Very high early strength without affecting normal placement procedures</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Early stripping time</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Very high durability</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Superior finishes and guaranteed compressive strengths</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Greater immunity against environmental attacks</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Time_Saver_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                            
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Keep plastic covers handy while pouring concrete during the rainy season. Try not to drop the concrete from a height of more than 1 meter. Deshutter only as per recommended guidelines by your structural consultant/architect. Ensure that there are no honeycombs or voids in the concrete especially near joints/corners or at the base. </span><br><br>

                            </div>
                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> All building terrace slabs to mitigate plastic shrinkage cracking.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Industrial warehouses, container yards, railway platforms etc.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Concrete roads, beams, and precast concrete girders which require additional flexural strength.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Slab on grade: All types of concrete pavements, industrial floors, airport taxiways, hangars, etc.</span>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($type == 'performance-enhanced-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--60">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Excellent workability that makes compaction much easier and more effective</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Early curing in the form of sprinkling is strongly recommended</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Start of curing before loss of sheen/drying is highly recommended</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> It is designed to achieve 80% characteristic compressive strength in 7 days, hence formwork cycle time can be reduced accordingly</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Sampling and Testing shall be done as per IS specification to get desired results</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Excellent workability that makes compaction much easier and more effective</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Better setting pattern allowing ample time for placing, compaction and finishing</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Improved plastic shrinkage resistance of concrete</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Superior early strengths which enables early removal of form work</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Booster_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            
                            
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Keep plastic covers handy while pouring concrete during the rainy season. Try not to drop the concrete from a height of more than 1 meter. Deshutter only as per recommended guidelines by your structural consultant/architect. Ensure that there are no honeycombs or voids in the concrete especially near joints/corners or at the base. </span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Start of curing before loss of sheen/drying is highly recommended</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> It is designed to achieve 80% characteristic compressive strength in 7 days, hence formwork cycle time can be reduced accordingly</span><br><br><span> Sampling and Testing shall be done as per IS specification to get desired results</span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($type == 'waterproof-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--20">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Increase in concrete service life</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Resists extreme hydrostatic pressure from either positive or negative surface of the concrete</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Becomes an integral part of the substrate</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Highly resistant to aggressive chemicals</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Can seal static hairline cracks up to 0.4 mm</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Allows concrete to breathe</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Non-toxic</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Less costly to apply than most other methods</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Added into the concrete at time of batching and therefore is not subject to climatic restraints</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Increases flexibility in construction scheduling</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>

                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> During summer consider an early morning or evening placement schedule to avoid higher midday temperature </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Keep the concrete surface wet by covering with damp hessian cloth, or burlap. You can also cover with a polythene sheet</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Cover the entire surface with hessian or jute cloth and keep it saturated with water throughout the curing period</span>
                            </div>

                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> Basement Retaining Walls</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Lift Pits</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Swimming Pools</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Water retaining structures</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Sewage & Water Treatment Plants</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Foundations</span>
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
@elseif($type == 'reinforced-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--20">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Fibres increase in the homogeneity and cohesiveness of concrete</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Improved crack resistance, fire resistance, impact & blast resistance</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Additional flexural strength makes it ideal for abrasive surfaces like roads, pavements and industrial floors.</b>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> A concrete made with randomly dispersed discrete discontinuous fibres</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Fibres arrest plastic shrinkage cracks of the concrete in the green stage</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Fibours_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Concrete should not be allowed to dry in the green stage </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Curing should commence immediately after the concrete starts showing signs of dryness </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Concrete should be thoroughly mixed at the time of placement for further uniform redistribution of fibre in concrete mass </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Finally, use ‘broom’ finish </span><br><br>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
@elseif($type == 'fire-resistant-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--20">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> It can be used to insulate industrial boilers</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> It can be used in sandwich construction</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> It is ideal for swimming pool bases</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Pizza & Bakery Ovens can be insulated by Fire Safe for safety & can get maximum output of fuel. </b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12" style="margin-top: 2%;">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> When tested Hindustan RMC Fire Resistant Concrete achieves 4-Hour fire ratings to systems that employed vermiculite as one of the components</p></b>
                                                </div>
                                                <div class="col-lg-12" style="margin-top: 2%;">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Hindustan RMC Fire Resistant Concrete has excellent insulating properties. Three inches of Fire Resistant concrete is equivalent to 1 1/2" of rigid board insulation layer over steel decks. One inch of vermiculite concrete is equal in insulating value to 20 inches of regular concrete</p></b>
                                                </div>
                                                <div class="col-lg-12" style="margin-top: 2%;">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> When compared with structural grade concrete, Hindustan RMC Fire Resistant Concrete is 60% of the weight. This results in considerable savings from the footings through the structural steel</p></b>
                                                </div>
                                                <div class="col-lg-12" style="margin-top: 2%;">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Hindustan RMC Fire Resistant Concrete can be applied over a variety of bases, allowing architects and engineers ample flexibility in their design criteria. The thickness of the concrete can be varied to permit necessary slope to drain</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/FireFighter_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Start curing after initial setting time of concrete </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Cover concrete after finishing by gunny bags or polythene sheets</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Plan to consume concrete within stipulated time</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Use tested non-absorbent shuttering material</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Deploy skilled labours for placing</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Do not vibrate the concrete mechanically</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Don’t let open finished concrete up to final setting time</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Don’t remove the formwork until the concrete is strong enough to do so</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Sampling and Testing shall be done as per IS specification to get desired results</span>
                            </div>
                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> Rain water harvesting in housing societies</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Play grounds.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Nurseries and Gardens.</span>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      
@elseif($type == 'crack-resistant-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--20">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Reduced shrinkage cracks and maintenance cost</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Enhanced durability features. Continues to gain strength in long term</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Easy placement</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Increased water-tightness</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Made with high modified cement content and supplementary binder materials.</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> The additives contain new engineering fibre which help in preventing cracks</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> The additives help in preventing corrosion of the reinforcements and thereby increase durability of the structures.</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Crackless_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> During summer consider an early morning or evening placement schedule to avoid a higher midday temperature </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Keep the concrete surface wet by covering with damp hessian cloth, or burlap. You can also cover it with a polythene sheet</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Cover the entire surface with hessian or jute cloth and keep it saturated with water throughout the curing period</span>
                            </div>
                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> All building terrace slabs to mitigate plastic shrinkage cracking.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Industrial warehouses, container yards, railway platforms etc.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Concrete roads, beams, and precast concrete girders which require additional flexural strength.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Slab on grade: All types of concrete pavements, industrial floors, airport taxiways, hangars, etc.</span>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
@elseif($type == 'durable-foundation-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--20">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Reduction in thermal cracks due to low heat of hydration</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Longer durability of structure</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Minimizes water ingress and permeability</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Higher resistance to chloride and sulphate attack</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Made with enhanced SCM contents</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Reduction in placing temperature</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> May also contain water-proofing chemicals</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Long_Lasting_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Ensure adequate access to the pour or discharge point for personnel and equipment </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Wherever possible, use the truck mixer chute to discharge concrete directly into its final position. Avoid dropping the concrete from a height</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Start pouring from the corners/edges and move towards the center</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Don't allow the concrete to pile up in large heaps or sloping layers</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Place the concrete in a series of layers of equal depth - not more than 300 mm to 500 mm deep - only after the underlying layer is properly compacted</span>
                            </div>
                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> Structures near the coastline.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Marine structures.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Sewage treatment plants.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Bridges in marine environment.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Sea walls and Armour units.</span>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
@elseif($type == 'flowing-column-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--60">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Enhanced surface finish</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Avoids slump drop and therefore eliminates need for retempering at site</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Faster construction and deshuttering time saves on labour cost</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Allows easy flow of concrete,better pumpability and placement of concrete</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Reduced honeycombing</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Reduces cost</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Higher retention</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Easy flowing</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Column_Con_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Start curing immediately or as soon as possible </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> During summer consider an early morning or evening placement schedule to avoid the higher midday temperature</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Keep the concrete surface wet by covering with damp hessian cloth, or burlap. You can also cover with a polythene sheet</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Cover the entire surface with hessian or jute cloth and keep it saturated with water throughout the curing period</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Use cords to tie the Polythene to columns. Exposed edges and corners should be well fixed down to avoid wind and drought conditions</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Leaving the formwork in place for a longer time would help in avoiding evaporation of water from the concrete</span>
                            </div>
                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> Precast facilities: Beams, panels and specific elements.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Pavements and trafficked roads.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Concrete casting using Aluform systems.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Climbing and Gliding formwork systems.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> PT Slabs</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Machine Foundation.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Tunnels and Subways.</span>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>       
@elseif($type == 'color-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--60">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Highly aesthetic rich designs are possible for elevational treatment in high end projects</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Enhancement in the pleasing appearance of the structure</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Requires much lesser maintenance and is more durable</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Ideal for driveways, patios, walkways, swimming pool deck, car parks, industrial floors and exteriors like walls</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Highly aesthetic rich designs are possible for elevational treatment in high end projects</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Enhancement in the pleasing appearance of the structure</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Requires much lesser maintenance and is more durable</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Ideal for driveways, patios, walkways, swimming pool deck, car parks, industrial floors and exteriors like walls</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Highly aesthetic rich designs are possible for elevational treatment in high end projects</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Enhancement in the pleasing appearance of the structure</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Requires much lesser maintenance and is more durable</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Ideal for driveways, patios, walkways, swimming pool deck, car parks, industrial floors and exteriors like walls</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Manufactured by using very consistent quality UV resistant pigments sourced from selected trustworthy manufacturers</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Concrete retains the original shades for longer duration</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Even customized shades can be made available as per customer’s requirements</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Colour_Plus_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Concrete must be finished before lapse of final setting time </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Cleaning and washing of coloured surfaces with clean water at a regular interval is highly recommended</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Water used for curing srhould be free from any contamination</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> While finishing the concrete, use of grey cement or any other similar material should be strictly avoided</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> For cleaning the coloured surfaces, use of acid and alkalis should be avoided</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> The finished concrete should be protected by polythene sheet so as to avoid damage to finished surface</span>
                            </div>
                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> Pedestrian walkways, footpaths and cycle tracks.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Floorings for parking areas, basements, etc.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Artistic precast panels and products.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Civic monuments and surrounds.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> PT Slabs</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Commercial and domestic flooring.</span>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>     
@elseif($type == 'lightweight-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--20">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Possesses excellent workability and can be placed in any desired shape</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Provides excellent heat and sound insulation</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> This concrete has better fire resistance</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Can be applied with all traditional surface finishes - paint, tiles, carpets, etc.</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Can be used as a leveling course without adding dead weight</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> MA light weight concrete produced by distributing foam / polystyrene bits in a uniform manner throughout a precisely designed concrete</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Hindustan RMC lightweight Concrete has a density ranging from 500 Kg per cum and above with a strength ranging from 1.5 to 4.5 Mpa</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Lite_WT_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Avoid mechanical vibrators for light weight concrete </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Curing should be done as per norms</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Don't allow the concrete to pile up in large heaps or sloping layers</span>
                            </div>
                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> As an insulating material for roofs, floors and electrical cables.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Construction of partition walls.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Lightweight filler material.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Cover for reservoir to reduce evaporation losses</span>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>     
@elseif($type == 'ice-concrete')
    <div class="industry-grid-area section-space--inner--120 grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <!-- section title -->
                    <div class="section-title-area section-space--bottom--60">
                        <h2 class="title title--left section-space--bottom--30">Advantages <span class="highlight"></span></h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- industry grid wrapper -->
                    <div class="industry-grid-wrapper">
                        <div class="row row-35">
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> No rapid loss of slump and drying of concrete</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> No early setting and stiffening of concrete</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> Reduced plastic shrinkage and Ease for finishing</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="single-industry-grid section-space--bottom--20">
                                    <div class="single-industry-grid__content" style="font-size: 0.94rem;">
                                        <i class="ion-flash" style="line-height: 1.1;color: #EF7F1B;"></i><b> No loss of compressive strength at 28 days</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--====================  feature background area ====================-->
        <div class="feature-background__area">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="feature-background__wrapper bg-img" data-bg="{{asset('assets/front/img/backgrounds/14.png')}}">
                        <div class="row no-gutters">
                            <div class="col-lg-7">
                                

                                <!-- feature content -->
                                <div class="feature-background__content-area feature-background__content-area--style2">
                                    <div class="feature-background__content-wrapper section-space--inner--190">
                                        <!-- section title area -->
                                        <div class="section-title-area section-space--bottom--60">
                                            <h2 class="title title--style4 title--style4--about" style="color: #fffdfb">Useful in a Variety of Situations</h2>
                                        </div>

                                        <!-- feature single content -->
                                        <div class="feature-background__single-content-wrapper">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> This concrete is specially designed by lowering the placing temperature with careful proportioning of binder and cooling the ingredients</p></b>
                                                </div>
                                                <div class="col-lg-12">
                                                    <p style="color: #fffdfb;font-size: 0.94rem;"><i class="ion-flash" style="color: #ef7f1b;"></i><b> Ideal for mass concreting hence enhancing the durability of the structure</p></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <!-- feature background -->
                                <div class="feature-background__image feature-background__image--style2 bg-img" data-bg="{{asset('assets/front/img/backgrounds/products/Ice_variety.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of feature background area  ====================-->
    </div>

    <div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-details-tab section-space--bottom--80">
                        <div class="nav nav-tabs service-details-tab__navigation-wrapper" id="nav-tab3" role="tablist">
                            <a class="nav-item nav-link active" id="service-details-item1-tab" data-toggle="tab" href="#service-details-item1" role="tab" aria-selected="true">How To Use</a>
                            <a class="nav-item nav-link" id="service-details-item2-tab" data-toggle="tab" href="#service-details-item2" role="tab" aria-selected="false">Application</a>
                        </div>
                        <div class="tab-content service-details-tab__content-wrapper">
                            <div class="tab-pane fade show active" id="service-details-item1" role="tabpanel" aria-labelledby="service-details-item1-tab">
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Steel reinforcement and shuttering should be sprinkled with cool water, before placing the concrete </span><br><br>
                              <i class="ion-flash" style="color: #ef7f1b;"></i><span> Placing of concrete should be done immediately after receiving the concrete at site</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Delayed compaction of the concrete should be avoided</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Finishing should be done immediately after compaction</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Early curing is highly recommended by way of covering the concrete with wet hessian cloth</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Broom finish texture should be provided for the finished concrete surface of slab in particular</span>
                            </div>
                            <div class="tab-pane fade" id="service-details-item2" role="tabpanel" aria-labelledby="service-details-item2-tab">
                                <i class="ion-flash" style="color: #ef7f1b;"></i><span> Massive raft foundations and deep beams.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Bridge foundations and bridge piers.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Large retaining walls.</span><br><br><i class="ion-flash" style="color: #ef7f1b;"></i><span> Mass concrete works in dams, hydro-electric and power projects.</span>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      
@endif

<!--====================  cta area ====================-->
<div class="cta-area section-space--inner--60 default-bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <!-- cta text -->
                <h2 class="cta-text  text-center text-lg-left">Looking for quality construction partner?</h2>
            </div>
            <div class="col-lg-3 home_button text-center text-lg-right">
                <a href="{{route('front.select_calculation.page')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark" style="color: #ffff;">RMC CALCULATOR</a>
            </div>
            <div class="col-lg-2 text-center text-lg-right">
                <a href="{{route('admin.getQuoteCalculator')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark" style="color: #ffff;">ORDER NOW</a>
            </div>
        </div>
    </div>
</div>
<!--====================  End of cta area  ====================-->
@endsection  


