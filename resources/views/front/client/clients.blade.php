@extends('layouts.front.front')
@section('title','Hindustan RMC | Clients')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/client_banner.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Clients</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="{{route('front.home.page')}}">Home</a></li>
                    <li>Clients</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <div class="container">
        <div class="row">
            @if(isset($get_client) && count($get_client) >= 1)
            <div class="col-lg-8">
                <div class="common-page-content">
                    <!-- <div class="common-page-text-wrapper section-space--bottom--50">
                        <h2 class="common-page-title">Partnerships & Affiliations</h2>
                        <p>Our industry professionals are able to deliver better ideas and solutions embedded with a deep understanding of each client’ business and industry. The industry focus of our experts allows us to offer comprehensive solutions.</p>
                    </div> -->
                    <div class="brand-logo-grid__wrapper section-space--bottom--50">
                        <div class="row">
                            @foreach($get_client as $gk => $gv)
                                <div class="col-md-4 col-6">
                                    <div class="brand-logo-grid__single">
                                        <div class="brand-logo-slider__single">
                                            <div class="image">
                                                <img src="{{ asset('uploads/company_logo') }}/{{ $gv->company_logo }}" class="img-fluid" alt="">
                                            </div>
                                            <div class="image-hover">
                                                <img src="{{ asset('uploads/company_logo') }}/{{ $gv->company_logo }}" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            @endforeach
                            

                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-lg-4">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title" style="color: #ffffff;">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;color: #ffffff;"></i>
                                </div>
                                <h3 class="page-title" style="color: #ffffff;">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection