@extends('layouts.front.front')
@section('title','Hindustan RMC | Recognitions')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/Recognitions_banner.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Recognitions</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="{{route('front.home.page')}}">Home</a></li>
                    <li>Recognitions</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                @if(isset($get_testimonial_certificate) && count($get_testimonial_certificate) >= 1)
                <div class="common-page-content">
                    <div class="common-page-text-wrapper section-space--bottom--80">
                        <h2 class="common-page-title" style="margin-bottom: -12%;">Certification</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="case-study__image-gallery-wrapper section-space--top--80">
                                <div class="row image-popup">
                                    @foreach($get_testimonial_certificate as $gk => $cv)
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{asset('uploads/testimonial_image/'.$cv->image)}}" class="single-gallery-thumb">
                                                <img src="{{asset('uploads/testimonial_image/'.$cv->image)}}" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-6 col-sm-6">
                                        <div class="content">
                                            <h3 class="title">{{$cv->company_name}}</h3>
                                            <p>{{$cv->description}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(isset($get_testimonial_recognitation) && count($get_testimonial_recognitation) >= 1)
                    <div class="common-page-content" style="margin-top: 71px;">
                        <div class="common-page-text-wrapper section-space--bottom--80">
                            <h2 class="common-page-title" style="margin-bottom: -12%;">Thank You from Clients</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="case-study__image-gallery-wrapper section-space--top--80">
                                    <div class="row image-popup">
                                        @foreach($get_testimonial_recognitation as $gk => $rv)
                                        <div class="col-lg-4 col-md-6 col-sm-6">
                                            <div class="case-study__single-gallery-image">
                                                <a href="{{asset('uploads/testimonial_image/'.$rv->image)}}" class="single-gallery-thumb">
                                                    <img src="{{asset('uploads/testimonial_image/'.$rv->image)}}" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-md-6 col-sm-6">
                                            <div class="content">
                                                <h3 class="title">{{$rv->company_name}}</h3>
                                                <p>{{$rv->description}}</p>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="common-page-content">
                    </div>
                @endif
            </div>
            <div class="col-lg-4 appraciation_sidebar">
                <!-- page sidebar -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="page-sidebar">
                        <!-- single sidebar widget -->
                        <div class="single-sidebar-widget">
                            <div class="sidebar-other-page__wrapper">
                                <a href="{{route('front.company.page')}}" class="sidebar-other-page__single">
                                    <div class="icon">
                                        <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;"></i>
                                    </div>
                                    <h3 class="page-title">Company</h3>
                                </a>
                                <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                    <div class="icon">
                                        <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                    </div>
                                    <h3 class="page-title">Leadership</h3>
                                </a>
                                <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                    <div class="icon">
                                        <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                    </div>
                                    <h3 class="page-title">Milestones</h3>
                                </a>
                                <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single">
                                    <div class="icon">
                                        <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                    </div>
                                    <h3 class="page-title">Infrastructure</h3>
                                </a>
                                <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single">
                                    <div class="icon">
                                        <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;"></i>
                                    </div>
                                    <h3 class="page-title">RMC Process</h3>
                                </a>
                                <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single active">
                                    <div class="icon">
                                        <i class="glyph-icon tractor-bookmark" style="font-size: 50px;color: #ffffff;"></i>
                                    </div>
                                    <h3 class="page-title" style="color: #ffffff;">Recognitions</h3>
                                </a>
                                <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                    <div class="icon">
                                        <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                    </div>
                                    <h3 class="page-title">Clients</h3>
                                </a>
                                <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                    <div class="icon">
                                        <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                    </div>
                                    <h3 class="page-title">FAQs</h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@if(isset($get_testimonial_other) && count($get_testimonial_other) >= 1)
<div class="testimonial-brand-slider-area section-space--inner--120 grey-bg" style="padding-bottom: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- testimonial slider -->
                <div class="testimonial-slider__body-wrapper section-space--bottom--60">
                    <!-- testimonial slider title -->
                    <div class="testimonial-slider__title-wrapper section-space--bottom--60">
                        <h2 class="testimonial-slider__title" style="color: #222;">Testimonials</h2>
                        <div class="testimonial-slider__nav-container">
                            <div class="ht-swiper-button-prev ht-swiper-button-prev-3 ht-swiper-button-nav"><i class="ion-chevron-left"></i></div>
                            <div class="ht-swiper-button-next ht-swiper-button-next-3 ht-swiper-button-nav"><i class="ion-chevron-right"></i></div>
                        </div>
                    </div>
                    <div class="testimonial-slider__content-area">
                        <div class="swiper-container testimonial-slider__container">
                            <div class="swiper-wrapper testimonial-slider__wrapper">
                            @foreach($get_testimonial_other as $gk => $gv)
                                <div class="swiper-slide">
                                    <div class="testimonial-slider__single-item">
                                        <div class="testimonial-slider__single-item__image">
                                            <img src="{{asset('uploads/testimonial_other_image/'.$gv->image)}}" class="img-fluid" alt="">
                                        </div>
                                        <div class="testimonial-slider__single-item__content">
                                            <h4 class="testimonial-name"> {{$gv->name}} <span class="designation">{{$gv->    designation}}</span></h4>
                                            <div class="rating" style="color: #EF7F1B">
                                                @if($gv->rating == 1 || $gv->rating >= 1)
                                                    <i class="ion-ios-star"></i>
                                                @else
                                                    <i class="ion-ios-star-outline"></i>
                                                @endif

                                                @if($gv->rating == 2 || $gv->rating >= 2)
                                                    <i class="ion-ios-star"></i>
                                                @else
                                                    <i class="ion-ios-star-outline"></i>
                                                @endif

                                                @if($gv->rating == 3 || $gv->rating >= 3)
                                                    <i class="ion-ios-star"></i>
                                                @else
                                                    <i class="ion-ios-star-outline"></i>
                                                @endif

                                                @if($gv->rating == 4 || $gv->rating >= 4)
                                                    <i class="ion-ios-star"></i>
                                                @else
                                                    <i class="ion-ios-star-outline"></i>
                                                @endif

                                                @if($gv->rating == 5 || $gv->rating >= 5)
                                                    <i class="ion-ios-star"></i>
                                                @else
                                                    <i class="ion-ios-star-outline"></i>
                                                @endif
                                            </div>
                                            <p class="text">{{$gv->description}}</p>
                                            @if($gv->video_url != '')
                                            <div class="video-button-container video-popup">
                                                <a href="{{ $gv->video_url }}" class="section-title-video-button">
                                                    <div class="video-play bg-img" data-bg="{{ asset('assets/img/icons/video-play.png') }}">
                                                        <i class="ion-ios-play"></i>
                                                    </div>
                                                    <div class="video-text"> Watch Video <i class="ion-arrow-right-c"></i></div>
                                                </a>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                        <div class="swiper-pagination swiper-pagination-3"></div>
                    </div>
                </div>
            </div>
        </div><br><br>
    </div>
</div>
@endif
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120 appraciation_sidebar1">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
            </div>
            <div class="col-lg-4">
                <!-- single sidebar widget -->
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;color: #ffffff;"></i>
                                </div>
                                <h3 class="page-title" style="color: #ffffff;">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection