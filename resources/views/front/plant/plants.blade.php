@extends('layouts.front.front')
@section('title','Hindustan RMC | Plants')
@section('content')
<div class="breadcrumb-area section-space--inner--80 bg-img" style="background-color: #f7f7f7 !important;">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6 section-title-area">
                <h2 class="title"><?php echo substr($plant_detail->plant_name,2); ?></h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children" style="color: #222;"><a href="{{route('front.location_plants.page')}}" style="color: #222;">Plants</a></li>
                    <li style="color: #222;"><?php echo substr($plant_detail->plant_name,2); ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <!-- blog content wrapper -->
                <div class="blog-content-wrapper">
                    <div class="single-list-blog-post mb-0">
                        <div class="single-list-blog-post__media section-space--bottom--50">
                            <div class="product-details__image-slider">
                                
                                <div class="swiper-container product-details__image-slider-container">
                                    <div class="swiper-wrapper product-details__image-slider-wrapper">
                                        <div class="swiper-slide">
                                            <div class="product-details__image-single">
                                                <img src="{{asset('uploads/plant_image/'.$plant_detail->plant_image)}}" class="img-fluid" alt="" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="swiper-pagination swiper-pagination-15"></div> -->
                            </div>
                        </div>

                        <div class="single-list-blog-post__content single-list-blog-post__content--style2">
                            @if((isset($plant_detail->year_of_establish) && $plant_detail->year_of_establish != '') || (isset($plant_detail->track_record) && $plant_detail->track_record != '') || (isset($plant_detail->government_approval) && $plant_detail->government_approval != '') || (isset($plant_detail->plant_make) && $plant_detail->plant_make != '') || (isset($plant_detail->maximum_supply) && $plant_detail->maximum_supply != '') || (isset($plant_detail->electric_supply_condition) && $plant_detail->electric_supply_condition != '') || (isset($plant_detail->water_supply_condition) && $plant_detail->water_supply_condition != '') || (isset($plant_detail->no_trans_mixer) && $plant_detail->no_trans_mixer != '') || (isset($plant_detail->no_boom_placer) && $plant_detail->no_boom_placer != '') || (isset($plant_detail->no_concrete_pump) && $plant_detail->no_concrete_pump != '') || (isset($plant_detail->pipeline_length) && $plant_detail->pipeline_length != ''))
                            <h2 class="title" style="margin-bottom: 28px;font-size: 1.5625rem;">Plant Description</h2>
                            <div class="myaccount-table table-responsive text-center" style="width: 88%;">
                                <table class="table table-bordered">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Heads</th>
                                            <th>Details</th>                  
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(isset($plant_detail->year_of_establish) && $plant_detail->year_of_establish != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Year of Establishment</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->year_of_establish}}</td>
                                        </tr>
                                        @endif
                                        @if(isset($plant_detail->track_record) && $plant_detail->track_record != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Track Record</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->track_record}}</td>
                                        </tr>
                                        @endif
                                        @if(isset($plant_detail->government_approval) && $plant_detail->government_approval != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Government Approval</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->government_approval}}</td>
                                        </tr>
                                        @endif
                                        @if(isset($plant_detail->plant_make) && $plant_detail->plant_make != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Plant Make</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->plant_make}}</td>
                                        </tr>
                                        @endif
                                        @if(isset($plant_detail->maximum_supply) && $plant_detail->maximum_supply != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Maximum Supply/Day</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->maximum_supply}}</td>
                                        </tr>
                                        @endif
                                        @if(isset($plant_detail->electric_supply_condition) && $plant_detail->electric_supply_condition != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Electric Supply Condition</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->electric_supply_condition}}</td>
                                        </tr>
                                        @endif
                                        @if(isset($plant_detail->water_supply_condition) && $plant_detail->water_supply_condition != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Water Supply Condition</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->water_supply_condition}}</td>
                                        </tr>
                                        @endif
                                        @if(isset($plant_detail->no_trans_mixer) && $plant_detail->no_trans_mixer != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">No. of Transit Mixer</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->no_trans_mixer}}</td>
                                        </tr>
                                        @endif
                                        @if(isset($plant_detail->no_boom_placer) && $plant_detail->no_boom_placer != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">No. of Boom Placer</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->no_boom_placer}}</td>
                                        </tr>
                                        @endif
                                        @if(isset($plant_detail->no_concrete_pump) && $plant_detail->no_concrete_pump != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">No. of Concrete Pump</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->no_concrete_pump}}</td>
                                        </tr>
                                        @endif
                                        @if(isset($plant_detail->pipeline_length) && $plant_detail->pipeline_length != '')
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Pipe Line Length (Metre)</td>
                                            <td style="color: #888;font-size: 15px;">{{$plant_detail->pipeline_length}}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </div>  
                        @if(count($get_plant_image) > 0)
                        <div class="single-service-gallery-wrapper section-space--bottom--60" style="margin-top: 20px;margin-bottom: 0px !important;">
                            <h2 class="title" style="margin-bottom: 15px;">Plant Gallery</h2>
                            <div class="case-study__image-gallery-wrapper">
                                <div class="row image-popup">
                                    @foreach($get_plant_image as $key => $plant_id)
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{asset('uploads/Plant_Galllery/'.$plant_id->plant_image)}}" class="single-gallery-thumb">
                                                <img src="{{asset('uploads/Plant_Galllery/'.$plant_id->plant_image)}}" class="img-fluid" alt="" style="height: 238px;">
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <!-- page sidebar -->
                <div class="page-sidebar">      
                    <!-- single sidebar widget -->
                    @if((isset($plant_detail->address) && $plant_detail->address != '') || (isset($plant_detail->telephone) && $plant_detail->telephone != '') || (isset($plant_detail->email_id) && $plant_detail->email_id != '') || (isset($plant_detail->plant_incharge) && $plant_detail->plant_incharge != '') || (isset($plant_detail->quality_head) && $plant_detail->quality_head != ''))
                    <div class="single-sidebar-widget">
                        <h2 class="widget-title">Plant Details</h2>
                        <ul class="sidebar-category">
                            @if(isset($plant_detail->address) && $plant_detail->address != '')
                            <li><a href="javascript:void(0);" style="color: #333;font-weight: bold;">Address</a>
                            <a href="javascript:void(0);" style="color: #888;font-size: 15px;">{{$plant_detail->address}}</a></li>
                            @endif
                            @if(isset($plant_detail->telephone) && $plant_detail->telephone != '')
                            <li><a href="javascript:void(0);" style="color: #333;font-weight: bold;">Telephone</a>
                            <?php $phone = explode('-', $plant_detail->telephone); $phone1 = implode('', $phone);?>
                            <a href="tel:{{$phone1}}" style="color: #888;font-size: 15px;">{{$plant_detail->telephone}}</a></li>
                            @endif
                            @if(isset($plant_detail->email_id) && $plant_detail->email_id != '')
                            <li><a href="javascript:void(0);" style="color: #333;font-weight: bold;">Email</a>
                            <a href="mailto:{{$plant_detail->email_id}}" style="color: #888;font-size: 15px;">{{$plant_detail->email_id}}</a></li>
                            @endif
                            @if(isset($plant_detail->plant_incharge) && $plant_detail->plant_incharge != '')
                            <li><a href="javascript:void(0);" style="color: #333;font-weight: bold;">Plant Inchanrge</a>
                            <a href="javascript:void(0);" style="color: #888;font-size: 15px;">{{$plant_detail->plant_incharge}}</a></li>
                            @endif
                            @if(isset($plant_detail->quality_head) && $plant_detail->quality_head != '')
                            <li><a href="javascript:void(0);" style="color: #333;font-weight: bold;">Quality Department</a>
                            <a href="javascript:void(0);" style="color: #888;font-size: 15px;">{{$plant_detail->quality_head}}</a></li>
                            @endif
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
<div class="page-content-wrapper section-space--inner--120 grey-bg" style="padding-top: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="related-product-slider-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section-title-area section-space--bottom--50">
                                    <h2 class="title" style="margin-top: 56px;">Other Plants</h2>
                                </div>
                            </div>
                        </div>
                        <div class="related-product-slider">
                            <div class="swiper-container related-product-slider__container">
                                <div class="swiper-wrapper related-product-slider__wrapper">
                                    @foreach($get_plant as $gk => $gv)
                                    <div class="swiper-slide">
                                        <div class="shop-single-product mb-0">
                                            <div class="shop-single-product__thumb-wrapper">
                                                <div class="shop-single-product__image">
                                                    <a href="{{route('front.about_plants.page',$gv->slug)}}">
                                                        <img src="{{asset('uploads/plant_image/'.$gv->plant_image)}}" class="img-fluid" alt="" style="height: 142px !important;">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="shop-single-product__content">
                                                <h3 class="shop-single-product__title"><a href="{{route('front.about_plants.page',$gv->slug)}}">{{$gv->plant_name}}</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="ht-swiper-button-prev ht-swiper-button-prev-15 ht-swiper-button-nav" style="background-color:#EF7F1B;border-color: #EF7F1B;"><i class="ion-chevron-left"></i></div>
                            <div class="ht-swiper-button-next ht-swiper-button-next-15 ht-swiper-button-nav" style="background-color:#EF7F1B;border-color: #EF7F1B;"><i class="ion-chevron-right"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection