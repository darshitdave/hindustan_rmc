@extends('layouts.front.front')
@section('title','Hindustan RMC | Our Plants')
@section('content')
<div class="breadcrumb-area section-space--inner--80 bg-img" style="background-color: #f7f7f7 !important;">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6 section-title-area">
                <h2 class="title">Our Plants</h2>
                <!-- <h2 class="breadcrumb-page-title">Blog List</h2> -->
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children" style="color: #222;"><a href="javascript:void(0);" style="color: #222;">Plants</a></li>
                    <li style="color: #222;">Our Plants</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
@if(isset($get_plant) && count($get_plant) > 1)
    <div class="contact-location-grid__wrapper"style="margin-top: 9%;">
        <div class="container">
            <div class="row contact_location">
                @foreach($get_plant as $gk => $gv)
                    <div class="col-md-4">
                        <div class="contact-location-grid__single">
                            @if($gv->plant_status == 0)
                            <a href="{{route('front.about_plants.page',$gv->slug)}}">
                            @endif
                                <div class="contact-location-grid__image">
                                    <img src="{{asset('uploads/plant_image/'.$gv->plant_image)}}" class="img-fluid" alt="" style="height: 219px !important;">
                                </div>
                            @if($gv->plant_status == 0)
                            </a>
                            @endif
                            <div class="contact-location-grid__content">
                                @if($gv->plant_status == 0)
                                <a href="{{route('front.about_plants.page',$gv->slug)}}">
                                @endif
                                    <h2 class="title">{{$gv->plant_name}}</h2>
                                @if($gv->plant_status == 0)
                                </a>
                                @endif
                                @if($gv->plant_status == 0)
                                <a href="{{route('front.about_plants.page',$gv->slug)}}">
                                @endif
                                    <ul class="info-list">
                                        <li class="address" style="color: #888;">{{$gv->address}}</li>
                                        <li class="email"><a href="mailto:{{$gv->email_id}}" style="color: #888 !important;">{{$gv->email_id}}</a></li>
                                        <?php $phone = explode('-', $gv->telephone); $phone1 = implode('', $phone);?>
                                       <li class="phone"><a href="tel:<?php echo $phone1; ?>" style="color: #888 !important;">{{$gv->telephone}}</a></li>
                                    </ul>
                                @if($gv->plant_status == 0)
                                </a>
                                <a href="{{route('front.about_plants.page',$gv->slug)}}" class="see-more-link see-more-link--dark  see-more-link--dark--style2" style="margin-bottom: 12px;">KNOW MORE<i class="ion-arrow-right-c"></i></a>
                                <input type="hidden" name="latitude" id="latitude" value="{{$gv->latitude}}"> 
                                <input type="hidden" name="longitude" id ="longitude" value="{{$gv->longitude}}">  
                                @else
                                <a href="javascript:void(0);" class="see-more-link see-more-link--dark  see-more-link--dark--style2" style="margin-bottom: 12px;">Coming Soon</a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div><br><br><br><br>
    </div>
<div class="page-content-wrapper map_home section-space--inner--120 grey-bg">
    <!-- google map -->
    <div class="google-map google-map--style-2" id="google-map-one" data-width="100%" data-zoom_enable="" data-zoom="10" data-map_type="roadmap">
    </div>
</div>
@endif
<!--====================  End of page content wrapper  ====================-->
@endsection
@section('js')

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDbX_JirTlqgj9BO002nMah8CQSD7f4ypI&signed_in=true&libraries=places"></script>
<script type="text/javascript">
var data = {!! json_encode($latLong) !!};
var locations = data; 
//alert(locations);
var map = new google.maps.Map(document.getElementById('google-map-one'), { 
    zoom: 11, 
    center: new google.maps.LatLng(23.033863, 72.585022), 
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    options: {
        gestureHandling: 'greedy'
    },
    scrollwheel: false
}); 
   
   var infowindow = new google.maps.InfoWindow(); 
   
   var marker, i; 
   
for (i = 0; i < locations.length; i++) { 
    marker = new google.maps.Marker({ 
        position: new google.maps.LatLng(locations[i][1], locations[i][2]), 
        map: map,
        icon: {
            url: "http://maps.gstatic.com/mapfiles/markers2/marker.png"
        },
        title: String(locations[i][0]),
        url : locations[i][4]
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) { 
        return function() { 
            
            infowindow.setContent(this.get('title'));
            marker.setTitle(this.get('title'));
            infowindow.open(map, marker);
            window.location.href = this.url; 
            window.open = this.url;
        } 
    })(marker, i)); 

    markerYellow_first = new google.maps.Marker({ 
        position: new google.maps.LatLng('23.1345003', '72.5825692'), 
        map: map,
        icon: {
          url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
        },
        title : 'Upcoming'/*,
        url : 'http://hindustanrmc.homipod.com/plants-location'*/
    });

    markerYellow = new google.maps.Marker({ 
        position: new google.maps.LatLng('23.1279494', '72.55819269999999'), 
        map: map,
        icon: {
          url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
        },
        title : 'Upcoming'/*,
        url : 'http://hindustanrmc.homipod.com/plants-location' */
    });

    google.maps.event.addListener(markerYellow_first, 'click', (function(markerYellow_first, i) { 
        return function() { 
            
            infowindow.setContent(this.get('title'));
            markerYellow_first.setTitle(this.get('title'));
            infowindow.open(map, markerYellow_first);
            /*window.location.href = this.url; 
            window.open = this.url;*/
        } 
    })(markerYellow_first, i));

    google.maps.event.addListener(markerYellow, 'click', (function(markerYellow, i) { 
        return function() { 
            
            infowindow.setContent(this.get('title'));
            markerYellow.setTitle(this.get('title'));
            infowindow.open(map, markerYellow);
            /*window.location.href = this.url; 
            window.open = this.url;*/
        } 
    })(markerYellow, i));
} 
</script>
@endsection