@extends('layouts.front.front')
@section('title','Hindustan RMC | RMC Calculator')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/RMC_Calculator.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">RMC Calculator</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="{{route('front.home.page')}}">Home</a></li>
                    <li>RMC Calculator</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <!--====================  page content area ====================-->
    <div class="page-content-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  page wrapper  =======-->
                    <div class="page-wrapper">
                        <div class="page-content-wrapper">
                            <div class="row">
                                <!-- My Account Tab Menu Start -->
                                <div class="col-lg-3 col-12">
                                    <div class="myaccount-tab-menu nav" role="tablist">
                                        <a href="JavaScript:Void(0);" class="active" ><i class="glyph-icon icomoon-calculator" style="float: left;margin-top: -8px;margin-right: 4px;"></i>RMC Quantity Calculator</a>

                                        <a href="{{ route('admin.getQuoteCalculator') }}" ><i class="glyph-icon icomoon-edit" style="float: left;margin-top: -8px;margin-right: 4px;"></i> Get Quote</a>

                                    </div>
                                </div>
                                <!-- My Account Tab Menu End -->

                                <!-- My Account Tab Content Start -->
                                <div class="col-lg-9 col-12">
                                    <div class="tab-content" id="myaccountContent">
                                        
                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Calculate RMC Requirement</h3>

                                                <div class="account-details-form">
                                                    <form method="post" action="{{ route('admin.insertRmcCalculator') }}" enctype="multipart/form-data" id="rmc_calculator">
                                                        @csrf
                                                        <div class="row">
                                                            <div class="col-lg-12 col-12">
                                                                <!-- <h4 class="checkout-title">Terms And Condition</h4><br> -->

                                                                <div class="checkout-cart-total">
                                                                    <h4 class="title" style="margin-bottom: 8px;">Terms And Conditions</h4>
                                                                    Rate applicable for up to 20 km distance from nearest plant.
                                                                        Minor variations possible in quote subject to variation in distance, product mix, payment terms etc. 
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="checkout-form">
                                                            <div id="billing-form" class="billing-form">
                                                                <!-- <h4 class="checkout-title">Step 1</h4> -->
                                                                <div class="row">
                                                                    
                                                                    <div class="col-12">
                                                                        <label>Select Structure  <span class="mendetory">*</span> </label>
                                                                        <select class="nice-select" name="select_shape" id="shape_value">
                                                                            <option>Select Structure Type</option>
                                                                            @foreach($get_shape_types as $gk => $gv)
                                                                                <option value="{{$gv->id}}">{{$gv->shape_type}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- slab image -->
                                                        <div class="row slab_image" style="display: none;">
                                                            <div class="col-lg-8 col-12">
                                                                <div class="about-box-image-single">
                                                                    <div class="about-box-image-single__image">
                                                                        <img src="{{ asset('assets/front/img/shape_images/Slab _ Square Footing.png') }}" class="img-fluid" alt="">
                                                                    </div>
                                                                    <!-- <div class="about-box-image-single__content">
                                                                        <h4 class="title">Leadership Role</h4>
                                                                        <p>We lead a powerfully growing company with an evident social improving role.</p>
                                                                    </div> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end of slab image -->

                                                        

                                                        <!-- beam image-->
                                                        <div class="row beam_image" style="display: none;">
                                                            <div class="col-lg-8 col-12">
                                                                <div class="about-box-image-single">
                                                                    <div class="about-box-image-single__image">
                                                                        <img src="{{ asset('assets/front/img/shape_images/Beam _ square Column.png') }}" class="img-fluid" alt="">
                                                                    </div>
                                                                    <!-- <div class="about-box-image-single__content">
                                                                        <h4 class="title">Leadership Role</h4>
                                                                        <p>We lead a powerfully growing company with an evident social improving role.</p>
                                                                    </div> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end of beam image -->

                                                        <!-- column image -->
                                                            
                                                        <!-- end of column image -->

                                                        <div class="checkout-form">
                                                            <div id="billing-form" class="billing-form">
                                                                <!-- start slab section -->
                                                                <div class="row slab" style="display: none;">
                                                                    <div class="col-lg-4 col-12">
                                                                        <label><b><u>Slab</u></b></label><br>
                                                                        <label><b>Thickness <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12">
                                                                        <label></label>
                                                                        <input class="thikness_slab" name="thikness_slab" placeholder="" type="text" required="required">
                                                                    </div>

                                                                    <div class="col-lg-4 col-12" >
                                                                        <label></label>
                                                                        <select class="nice-select thikness_unit"> 
                                                                            <option value="">Select Unit</option>
                                                                            <option value="1" selected="selected">Inch</option>
                                                                            <option value="2">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" name="total_slab_thikness" class="slab" id="total_thikness" style="display: none;">

                                                                    <div class="col-lg-4 col-12 slab">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Width <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 slab" >
                                                                        <label></label>
                                                                        <input type="text" name="slab_width" class="width_slab" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 slab" >
                                                                        <label></label>
                                                                        <select class="nice-select width_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="slab" name="total_slab_width" id="total_width" style="display: none;">

                                                                    <div class="col-lg-4 col-12 slab">
                                                                        <label></label><br>
                                                                        <label><b>Length <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 slab" >
                                                                        <label></label>
                                                                        <input type="text" class="length_slab" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 slab">
                                                                        <label></label>
                                                                        <select class="nice-select length_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="slab" name="total_slab_length" id="total_length" style="display: none;">

                                                                </div>
                                                                <!--end of slab section -->

                                                                <!-- start footing -->
                                                                <div class="row footing" style="display: none;">
                                                                    <div class="col-12 footing">
                                                                        <label>Footing</label>
                                                                        <select class="nice-select col-md-12" id="footing_value">
                                                                            <option>Select Footing Shape</option>
                                                                            <option value="1">Square</option>
                                                                            <option value="2">Trapezoidal</option>
                                                                        </select>
                                                                    </div>

                                                                    <!-- footing image -->
                                                                        <!-- trapezoidal-->
                                                        
                                                                    <div class="col-12 footing">
                                                                        <div class="col-8 footing_trapezoidal" style="display: none;">
                                                                            <div class="about-box-image-single">
                                                                                <div class="about-box-image-single__image">
                                                                                    <img src="{{ asset('assets/front/img/shape_images/Trapezoidal Footing.png') }}" class="img-fluid" alt="">
                                                                                </div>
                                                                                <!-- <div class="about-box-image-single__content">
                                                                                    <h4 class="title">Leadership Role</h4>
                                                                                    <p>We lead a powerfully growing company with an evident social improving role.</p>
                                                                                </div> -->
                                                                            </div>
                                                                        </div>
                                                            

                                                                        <!-- square -->
                                                                        <div class="col-8 footing_square" style="display: none;">
                                                                            <div class="about-box-image-single">
                                                                                <div class="about-box-image-single__image">
                                                                                    <img src="{{ asset('assets/front/img/shape_images/Slab _ Square Footing.png') }}" class="img-fluid" alt="">
                                                                                </div>
                                                                                <!-- <div class="about-box-image-single__content">
                                                                                    <h4 class="title">Leadership Role</h4>
                                                                                    <p>We lead a powerfully growing company with an evident social improving role.</p>
                                                                                </div> -->
                                                                            </div>
                                                                        </div>
                                                            
                                                                    </div>
                                                                    <!-- end of footing image-->

                                                                    <!-- square section --> 
                                                                    <div class="col-lg-4 col-12 square_footing" style="display: none;">
                                                                        <label><b><u>Square</u></b></label><br>
                                                                        <label><b>Thickness <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 square_footing" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="footing_thikness" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 square_footing" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select footing_thikness_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1" selected="selected">Inch</option>
                                                                            <option value="2">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="square_footing" name="footing_total_thikness" id="footing_total_thikness" style="display: none;">

                                                                    <div class="col-lg-4 col-12 square_footing" style="display: none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Width <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 square_footing" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="footing_width" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 square_footing" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select footing_width_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="square_footing" name="footing_total_width" id="footing_total_width" style="display: none;">


                                                                    <div class="col-lg-4 col-12 square_footing" style="display: none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Length <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 square_footing" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="footing_length" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 square_footing" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select footing_length_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="square_footing" name="footing_total_length" id="footing_total_length" style="display: none;">
                                                                    <!-- end of square section -->

                                                                    <!-- Trapezoidal section -->
                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label><b><u>Trapezoidal</u></b></label><br>
                                                                        <label><b>Base Length <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="base_length" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select base_length_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1" selected="selected">Inch</option>
                                                                            <option value="2">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="trapezoidal" name="base_total_length" id="base_total_length" style="display: none;">

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Base Width <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="base_width" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select base_width_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="trapezoidal" name="base_total_width" id="base_total_width" style="display: none;">

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Base Height <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="base_height" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select base_height_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="trapezoidal" name="base_total_height" id="base_total_height" style="display: none;">

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Vertical Slant Height <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="vertical_slant_height" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select vertical_slant_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="trapezoidal" name="vertical_total_slant" id="vertical_total_slant" style="display: none;">

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Pillar Length <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="pillar_length" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select pillar_length_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="trapezoidal" name="pillar_total_length" id="pillar_total_length" style="display: none;">

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Pillar Width <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="pillar_width" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 trapezoidal" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select pillar_width_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="trapezoidal" name="pillar_total_width" id="pillar_total_width" style="display: none;">
                                                                    
                                                                    <!-- end of trapezoidal section -->
                                                                </div>
                                                                <!-- end footing -->

                                                                <!-- start beam section -->
                                                                <div class="row beam" style="display:none;">
                                                                    <div class="col-lg-4 col-12 beam" >
                                                                        <label><b><u>Beam</u></b></label><br>
                                                                        <label><b>Thickness <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 beam" style="display:none;">
                                                                        <label></label>
                                                                        <input type="text" class="beam_thikness" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 beam" style="display:none;">
                                                                        <label></label>
                                                                        <select class="nice-select beam_thikness_unit">
                                                                            <option>Select Unit</option>
                                                                            <option selected="selected" value="1">Inch</option>
                                                                            <option value="2">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="beam" name="beam_total_thikness" id="beam_total_thikness" style="display:none;">
                                                                    <div class="col-lg-4 col-12 beam" style="display:none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Width <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 beam" style="display:none;">
                                                                        <label></label>
                                                                        <input type="text" class="beam_width" placeholder="" required="required">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 beam" style="display:none;">
                                                                        <label></label>
                                                                        <select class="nice-select beam_width_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="beam" name="beam_total_width" id="beam_total_width" style="display:none;">

                                                                    <div class="col-lg-4 col-12 beam" style="display:none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Length <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 beam" style="display:none;">
                                                                        <label></label>
                                                                        <input type="text" class="beam_length" placeholder="" required="required">

                                                                    </div>
                                                                    <div class="col-lg-4 col-12 beam" style="display:none;">
                                                                        <label></label>
                                                                        <select class="nice-select beam_length_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="beam" name="beam_total_length" id="beam_total_length" style="display:none;">
                                                                </div>
                                                                <!-- end beam-->

                                                                <!-- start column section -->
                                                                <div class="row column" >
                                                                    <div class="col-lg-12 col-12 column" style="display: none;">
                                                                        <label><b><u>Column</u></b></label><br>
                                                                        <label></label>
                                                                        <select class="nice-select" id="column_selection">
                                                                            <option>Select Column Shape</option>
                                                                            <option value="1">Square</option>
                                                                            <option value="2">Circle</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-12 column">
                                                                        <div class="col-8 column_circle_image" style="display: none;">
                                                                            <div class="about-box-image-single">
                                                                                <div class="about-box-image-single__image">
                                                                                    <img src="{{ asset('assets/front/img/shape_images/Circular Column.png') }}" class="img-fluid" alt="">
                                                                                </div>
                                                                                <!-- <div class="about-box-image-single__content">
                                                                                    <h4 class="title">Leadership Role</h4>
                                                                                    <p>We lead a powerfully growing company with an evident social improving role.</p>
                                                                                </div> -->
                                                                            </div>
                                                                        </div>
                                                                    
                                                                    
                                                                        <div class="col-8 column_square" style="display: none;">
                                                                            <div class="about-box-image-single">
                                                                                <div class="about-box-image-single__image">
                                                                                    <img src="{{ asset('assets/front/img/shape_images/Slab _ Square Footing.png') }}" class="img-fluid" alt="">
                                                                                </div>
                                                                                <!-- <div class="about-box-image-single__content">
                                                                                    <h4 class="title">Leadership Role</h4>
                                                                                    <p>We lead a powerfully growing company with an evident social improving role.</p>
                                                                                </div> -->
                                                                            </div>
                                                                        </div>
                                                                    
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 square_column" style="display: none;">
                                                                        <label><b><u>Square</u></b></label><br>
                                                                        <label><b>Thickness <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 square_column" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="column_thikness" placeholder="" required="required">
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 square_column" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select column_thikness_unit">
                                                                            <option>Select Unit</option>
                                                                            <option selected="selected" value="1">Inch</option>
                                                                            <option value="2">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="square_column" name="column_total_thikness" id="column_total_thikness" style="display: none;">

                                                                    <div class="col-lg-4 col-12 square_column" style="display: none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Width <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 square_column" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="column_width" placeholder="" required="required">
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 square_column" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select column_width_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="square_column" name="column_total_width" id="column_total_width" style="display: none;">

                                                                    <div class="col-lg-4 col-12 square_column" style="display: none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Length <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 square_column" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="column_length" placeholder="" required="required">
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 square_column" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select column_length_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="square_column" name="column_total_length" id="column_total_length" style="display: none;">

                                                                    <div class="col-lg-4 col-12 circle" style="display: none;">
                                                                        <label><b><u>Circle</u></b></label><br>
                                                                        <label><b>Diametre <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 circle" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="circle_diametre" placeholder="" required="required">
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 circle" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select circle_diametre_unit">
                                                                            <option>Select Unit</option>
                                                                            <option selected="selected" value="1">Inch</option>
                                                                            <option value="2">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="circle" name="circle_total_diametre" id="circle_total_diametre" style="display: none;">

                                                                    <div class="col-lg-4 col-12 circle" style="display: none;">
                                                                        <label><b><u></u></b></label><br>
                                                                        <label><b>Height <span class="mendetory">*</span> :</b></label>
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 circle" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" class="circle_height" placeholder="" required="required">
                                                                    </div>

                                                                    <div class="col-lg-4 col-12 circle" style="display: none;">
                                                                        <label></label>
                                                                        <select class="nice-select circle_height_unit">
                                                                            <option>Select Unit</option>
                                                                            <option value="1">Inch</option>
                                                                            <option value="2" selected="selected">Feet</option>
                                                                            <option value="3">Metre</option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" class="circle" name="circle_total_height" id="circle_total_height" style="display: none;">

                                                                </div>
                                                                <!-- end column section -->

                                                                <div class="row calculate" >
                                                                    <div class="col-lg-4 col-12 slab_total_value" style="display: none;margin-bottom: 10px;">
                                                                        <a href="javascript:void(0)" class="ht-btn ht-btn--dark border-0 mt-5 slab_total_value">Calculate</a>
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 footing_square_total_value" style="display: none;margin-bottom: 10px;">
                                                                        <a href="javascript:void(0)" class="ht-btn ht-btn--dark border-0 mt-5 footing_square_total_value">Calculate</a>
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 footing_trape_total_value" style="display: none;margin-bottom: 10px;">
                                                                        <a href="javascript:void(0)" class="ht-btn ht-btn--dark border-0 mt-5 footing_trape_total_value">Calculate</a>
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 beam_total_value" style="display: none;margin-bottom: 10px;">
                                                                        <a href="javascript:void(0)" class="ht-btn ht-btn--dark border-0 mt-5 beam_total_value">Calculate</a>
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 column_square_total_value" style="display: none;margin-bottom: 10px;">
                                                                        <a href="javascript:void(0)" class="ht-btn ht-btn--dark border-0 mt-5 column_square_total_value">Calculate</a>
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 column_circle_total_value" style="display: none;margin-bottom: 10px;">
                                                                        <a href="javascript:void(0)" class="ht-btn ht-btn--dark border-0 mt-5 column_circle_total_value">Calculate</a>
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 press_calculate" style="display: none;">
                                                                        <label>Required Volume In Cubic Metre</label>
                                                                        <input type="text" name="m_cube" id="m_cube" readonly="readonly">
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 press_calculate" style="display: none;">
                                                                        <label>Required Volume In Cubic feet</label>
                                                                        <input type="text" name="feet_cube" id="feet_cube" readonly="readonly">
                                                                    </div>
                                                                    <div class="col-lg-8 col-12 press_calculate" style="display: none;">
                                                                        
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 press_calculate" style="display: none;">
                                                                        <button name="submit" class="ht-btn ht-btn--dark border-0 mt-5 calculate_value">Get Quote &nbsp;&nbsp;<i class="ion-arrow-right-c"></i></a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->

                                    </div>
                                </div>
                                <!-- My Account Tab Content End -->
                            </div>
                        </div>
                    </div>
                    <!--=======  End of page wrapper  =======-->
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of page content area  ====================-->
</div>
<!--====================  End of page content wrapper  ====================-->

@endsection
@section('js')

<script>

</script>

@endsection