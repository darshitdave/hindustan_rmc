@extends('layouts.front.front')
@section('title','Hindustan RMC | Get Quote')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/RMC_Calculator.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Get Quote</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="{{route('front.home.page')}}">Home</a></li>
                    <li>Get Quote</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <!--====================  page content area ====================-->
    <div class="page-content-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  page wrapper  =======-->
                    <div class="page-wrapper">
                        <div class="page-content-wrapper">
                            <div class="row">
                                <!-- My Account Tab Menu Start -->
                                <div class="col-lg-3 col-12">
                                    <div class="myaccount-tab-menu nav" role="tablist">
                                        <a href="{{ route('front.select_calculation.page') }}"  ><i class="glyph-icon icomoon-calculator" style="float: left;margin-top: -8px;margin-right: 4px;"></i>RMC Quantity Calculator</a>

                                        <a href="JavaScript:Void(0);" class="active" ><i class="glyph-icon icomoon-edit" style="float: left;margin-top: -8px;margin-right: 4px;"></i> Get Quote</a>

                                    </div>
                                </div>
                                <!-- My Account Tab Menu End -->

                                <!-- My Account Tab Content Start -->
                                <div class="col-lg-9 col-12">
                                    <div class="tab-content" id="myaccountContent">
                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Get Quote</h3>

                                                <div class="account-details-form">
                                                    <form method="post" action="{{ route('admin.insertRmcDetail') }}" enctype="multipart/form-data">
                                                    @csrf
                                                        <div class="row">
                                                            <div class="col-lg-12 col-12">
                                                                <!-- <h4 class="checkout-title">Terms And Condition</h4><br> -->

                                                                <div class="checkout-cart-total">
                                                                    <h4 class="title" style="margin-bottom: 8px;">Terms And Conditions</h4>
                                                                    Rate applicable for up to 20 km distance from nearest plant.
                                                                        Minor variations possible in quote subject to variation in distance, product mix, payment terms etc. 
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="checkout-form">
                                                            <div id="billing-form" class="billing-form">
                                                                <!-- <h4 class="checkout-title">Step 1</h4> -->
                                                                <div class="row">
                                                                    
                                                                    <div class="col-lg-6 col-12">
                                                                        <label>Select Product <span class="mendetory">*</span></label>
                                                                        <select class="nice-select product" name="product_id" required="required">
                                                                            <option>Select Product Type</option>
                                                                            @foreach($get_product as $gk => $gv)
                                                                                <option value="{{$gv->id}}">{{$gv->sub_products}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6 col-12">
                                                                    </div>

                                                                    <div class="col-lg-6 col-12 concrete" style="display: none;">
                                                                        <label>Select Concrete Grade <span class="mendetory">*</span></label>
                                                                        <select class="nice-select" name="grade_id" id="product_type" required="required">
                                                                            <option>Select Concrete Grade Type</option>
                                                                           
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6 col-12 concrete" style="display: none;">
                                                                    </div>

                                                                    <div class="col-lg-6 col-12 concrete" style="display: none;">
                                                                        <label>RMC REQUIRED (in cubic meter)<span class="mendetory">*</span></label>
                                                                        <input type="text" name="m_cube" id="require_volume" required="required" value="@if(isset($m_cube)){{$m_cube}} @endif">
                                                                    </div>
                                                                    <div class="col-lg-6 col-12 concrete" style="display: none;">
                                                                        <input type="hidden" name="">
                                                                        <label class="rmc_calc_label" ><b style="color: #EF7F1B;border-bottom: 1px dotted;"><a href="{{ route('front.select_calculation.page') }}" style="color: #EF7F1B;">RMC CALCULATOR</a></b></label>
                                                                    </div>

                                                                    <div class="col-lg-5 col-12 concrete" style="display: none;margin-bottom: 20px;">
                                                                        <label for="accept_terms">Do You Want to Add Fly Ash?</label><br>
                                                                        <div class="check-box">
                                                                            <input type="checkbox" id="accept_terms" selected class="fly_ash_value" value="1">
                                                                            <label for="accept_terms" style="margin-right: 8px;">Yes</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-4 col-12 concrete" style="display: none;">
                                                                        <label></label><br>
                                                                        <div class="check-box">
                                                                            <input type="checkbox" id="accept_terms1" class="fly_ash_value" value="2">
                                                                            <label for="accept_terms1" class="fly_ash_check" style="margin-left: -249px;margin-top: 11px;">No</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-12 concrete" style="display: none;">
                                                                    </div>

                                                                    <div class="col-lg-6 col-12 fly_ash_include" style="display: none;">
                                                                        <label>Cement <span class="mendetory">*</span></label>
                                                                        <input type="text" name="cement_with_fly_ash" id="cement1" placeholder="In Kg" required="required">
                                                                        <input type="hidden" name="cement_rate" id="cement_rate1">
                                                                    </div>
                                                                    
                                                                    <div class="col-lg-6 col-12 fly_ash_include" style="display: none;">
                                                                        
                                                                    </div>

                                                                    <div class="col-lg-6 col-12 fly_ash_not_include" style="display: none;">
                                                                        <label>Cement <span class="mendetory">*</span></label>
                                                                        <input type="text" name="cement_with_out_fly_ash" id="cement2" placeholder="In Kg" required="required">
                                                                        <input type="hidden" name="basic_rate" id="cement_rate2">
                                                                        <input type="hidden" name="cement_rate" id="basic_cement_rate" value="{{$base_setting->cement_kg}}">
                                                                        <input type="hidden" name="total_cement" id="total_cement">
                                                                    </div>
                                                                    <div class="col-lg-3 col-12 fly_ash_not_include" style="display: none;">
                                                                        <label>Fly Ash <span class="mendetory">*</span></label>
                                                                        <input type="text" name="fly_ash" id="fly_ash"  placeholder="In Kg" required="required">
                                                                        <input type="hidden" name="fly_ash_rate" id="basic_fly_ash_rate" value="{{$base_setting->flyash_kg}}">
                                                                        <input type="hidden" name="gst_rate" id="gst_rate" value="{{$base_setting->gst}}">
                                                                    </div>
                                                                    <div class="col-lg-3 col-12 fly_ash_not_include" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" name="percent" class="percent_fly_ash" id="percent" placeholder="Percent" required="required">
                                                                    </div>     

                                                                    <div class="col-lg-6 col-12 placement_method" style="display: none;">
                                                                        <label>Select Concrete Placement Method <span class="mendetory">*</span></label>
                                                                        <select class="nice-select" name="placement_type" id="placement_type" required="required">
                                                                            <option value="" >Select Concrete Placement Method</option>
                                                                            <option value="1">Dumping</option>
                                                                            <option value="2">Pumping</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6 col-12 placement_method" style="display: none;">
                                                                    </div>
                                                                    <div class="col-lg-12 col-12 inqury_form" style="display: none;">
                                                                        <hr></hr>
                                                                    </div>
                                                                    <div class="col-lg-6 col-12 user_detail inqury_form" style="display: none;">
                                                                        <label>Contact Details <span class="mendetory">*</span></label>
                                                                        <input type="text" name="name" id="name" placeholder="Enter Name *" required="required">
                                                                    </div>
                                                                    
                                                                    <div class="col-lg-6 col-12 user_detail inqury_form" style="display: none;">
                                                                        <label></label>
                                                                        <input type="tel" name="mobile_no" id="mobile_no" placeholder="Enter Mobile Number *" required="required" maxlength="10" pattern="^\d{10}$" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                                                                    </div>

                                                                    <div class="col-lg-6 col-12 user_detail inqury_form" style="display: none;">
                                                                        <input type="email" name="email" id="email" placeholder="Enter Email ID *" required="required">
                                                                    </div>

                                                                    <div class="col-lg-6 col-12 user_detail inqury_form" style="display: none;">
                                                                        <input type="text" name="address" id="address" placeholder="Address (Optional)">
                                                                    </div>

                                                                    <div class="col-md-6 col-12 inqury_form" style="display: none;">
                                                                        <label for="ReCaptcha">Recaptcha:</label>
                                                                        <div class="g-recaptcha" data-sitekey="{{env('NOCAPTCHA_SITEKEY')}}"></div>
                                                                        <button name="submit" class="ht-btn ht-btn--dark border-0  calculate_value" id="submit" style="margin-top: 1%;margin-bottom: 16px;" >Get Price Quote</a>
                                                                    </div>

                                                                    <div class="col-md-6 col-12 inqury_form" style="display: none;">
                                                                    </div>

                                                                    <div class="col-md-6 col-12 inqury_form" style="display: none;">
                                                                        <div class="checkout-cart-total">
                                                                            <h4 class="title" style="margin-top: 0px; margin-bottom: : 9px;"><i class="ion-flash" style="line-height: 1.1;"></i>You will receive the price quote on your email address soon!</h4>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 col-12 inqury_form" style="display: none;">
                                                                    </div>

                                                                    <div class="col-md-4 col-12 price" style="display: none;">
                                                                        <label></label>
                                                                        <input type="text" name="m_cube" id="m_cube" readonly="readonly">
                                                                        <input type="text" name="with_gst" id="with_gst">
                                                                        <input type="hidden" name="with_out_gst" id="with_out_gst">
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->

                                        
                                    </div>
                                </div>
                                <!-- My Account Tab Content End -->
                            </div>
                        </div>
                    </div>
                    <!--=======  End of page wrapper  =======-->
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of page content area  ====================-->
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection  
@section('js')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
$(document).on('click', '#submit', function(e) {
  if(grecaptcha.getResponse() == "") {
    e.preventDefault();
    toastr.error('Please Check Captcha');
  } else {
    
  }
});

</script>
@endsection  