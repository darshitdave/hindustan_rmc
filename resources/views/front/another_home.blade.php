@extends('layouts.front.front')
@section('title','Hindustan RMC | Home')
@section('content')
<div class="hero-slider-area">
    <div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="home" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;">
        <!-- START REVOLUTION SLIDER 5.4.7 fullwidth mode -->
        <div id="rev_slider_2_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7">
            <ul>
            <!-- SLIDE  -->
             <li data-index="rs-30" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-thumb="{{ asset('assets/front/img/slider/one/HRMC_Banner_1.jpg') }}" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('assets/front/img/slider/one/HRMC_Banner_1.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="9" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption   tp-resizeme" id="slide-30-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['25','25','25','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="off" data-responsive="off"  data-frames='[{"delay":1330,"speed":600,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'  data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[10,10,10,10]" data-paddingright="[10,10,10,10]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[10,10,10,10]" style="z-index: 7; min-width: 901px; max-width: 901px; white-space: nowrap; font-size: 17px; line-height: 71px; font-weight: 700; color: #ffffff; letter-spacing: 0px; line-height: 12px;font-family:Roboto;border-color:rgba(255,255,255,0.2);border-style:solid;border-width:1px 1px 1px 1px;"> 360° Concrete Solution</div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption   tp-resizeme" id="slide-30-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['170','170','170','170']" data-fontsize="['60','50','40','35']" data-lineheight="['71','61','51','45']" data-width="['901','901','550','400']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":370,"speed":1000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; min-width: 901px; max-width: 901px; white-space: normal; font-size: 60px; line-height: 71px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Roboto;">Ahmedabad’s Largest Concreting Infrastructure</div>

                    <!-- LAYER NR. 3 -->
                   <a class="tp-caption rev-btn " href="{{route('front.about.infrastructure.page')}}" target="_self" id="slide-30-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='' data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":1300,"speed":600,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(34,34,34);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[20,20,20,20]" data-paddingright="[30,30,30,30]" data-paddingbottom="[20,20,20,20]" data-paddingleft="[30,30,30,30]" style="z-index: 9; white-space: nowrap; font-size: 12px; line-height: 12px; font-weight: 700; color: #ffffff; letter-spacing: 1px;font-family:Roboto;background-color: #EF7F1B;border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Our Infrastructure </a>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-5" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="{{ asset('assets/front/img/slider/one/HRMC_Banner_2.jpg') }}" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('assets/front/img/slider/one/HRMC_Banner_2.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption  " id="slide-5-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['25','25','25','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="off" data-responsive="off"  data-frames='[{"delay":1330,"speed":600,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'  data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[10,10,10,10]" data-paddingright="[10,10,10,10]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[10,10,10,10]" style="z-index: 7; min-width: 901px; max-width: 901px; white-space: nowrap; font-size: 17px; line-height: 71px; font-weight: 700; color: #ffffff; letter-spacing: 0px; line-height: 12px;font-family:Roboto;border-color:rgba(255,255,255,0.2);border-style:solid;border-width:1px 1px 1px 1px;">Realtime SMS Updates | GPS tracking | 24x7 Mobile Testing Van</div>

                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption  " id="slide-5-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['200','200','200','200']" data-fontsize="['60','50','40','35']" data-lineheight="['71','61','51','45']" data-width="['901','901','550','400']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":370,"speed":1000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; min-width: 901px; max-width: 901px; white-space: normal; font-size: 60px; line-height: 71px; font-weight: 700; color: #ffffff; letter-spacing: 4px !important;font-family:Roboto;">Building Trust Via Transparency</div>

                    <!-- LAYER NR. 8 -->
                    <!--  -->
                    <a class="tp-caption rev-btn-01 rev-btn " href="{{route('admin.getQuoteCalculator')}}" target="_self" id="slide-5-layer-4" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='' data-responsive_offset="off" data-responsive="off" data-frames='[{"delay":1350,"speed":300,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(34,34,34);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[20,20,20,20]" data-paddingright="[30,30,30,30]" data-paddingbottom="[20,20,20,20]" data-paddingleft="[30,30,30,30]" style="z-index: 9; white-space: nowrap; font-size: 12px; line-height: 12px; font-weight: 700; color: #ffffff; letter-spacing: 1px;font-family:Roboto;background-color: #EF7F1B;border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Order Now </a>

                    <!-- LAYER NR. 9 -->
                    
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-6" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="{{ asset('assets/front/img/slider/one/HRMC_Banner_3.jpg') }}" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('assets/front/img/slider/one/HRMC_Banner_3.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 10 -->
                    <div class="tp-caption  " id="slide-6-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['25','25','25','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="off" data-responsive="off"  data-frames='[{"delay":1330,"speed":600,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'  data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[10,10,10,10]" data-paddingright="[10,10,10,10]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[10,10,10,10]" style="z-index: 7; min-width: 901px; max-width: 901px; white-space: nowrap; font-size: 17px; line-height: 71px; font-weight: 700; color: #ffffff; letter-spacing: 0px; line-height: 12px;font-family:Roboto;border-color:rgba(255,255,255,0.2);border-style:solid;border-width:1px 1px 1px 1px;">100% Strength Guarantee</div>


                    <div class="tp-caption  " id="slide-6-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['200','200','200','200']" data-fontsize="['60','50','40','35']" data-lineheight="['71','61','51','45']" data-width="['901','901','550','400']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":370,"speed":1000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; min-width: 901px; max-width: 901px; white-space: normal; font-size: 60px; line-height: 71px; font-weight: 700; color: #ffffff; letter-spacing: 2px !important;font-family:Roboto;">Committed to Quality and
Timely Service</div>


                    <a class="tp-caption rev-btn-01 rev-btn " href="{{route('front.latter_of_appraciation.page')}}" target="_self" id="slide-6-layer-4" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='' data-responsive_offset="off" data-responsive="off" data-frames='[{"delay":1350,"speed":300,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(34,34,34);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[20,20,20,20]" data-paddingright="[30,30,30,30]" data-paddingbottom="[20,20,20,20]" data-paddingleft="[30,30,30,30]" style="z-index: 9; white-space: nowrap; font-size: 12px; line-height: 12px; font-weight: 700; color: #ffffff; letter-spacing: 1px;font-family:Roboto;background-color: #EF7F1B;border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">View Testimonials </a>
                </li>
                <li data-index="rs-7" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="{{ asset('assets/front/img/slider/one/HRMC_Banner_4.jpg') }}" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('assets/front/img/slider/one/HRMC_Banner_4.jpg') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 10 -->
                    <div class="tp-caption  " id="slide-6-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['25','25','25','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="off" data-responsive="off"  data-frames='[{"delay":1330,"speed":600,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'  data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[10,10,10,10]" data-paddingright="[10,10,10,10]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[10,10,10,10]" style="z-index: 7; min-width: 901px; max-width: 901px; white-space: nowrap; font-size: 17px; line-height: 71px; font-weight: 700; color: #ffffff; letter-spacing: 0px; line-height: 12px;font-family:Roboto;border-color:rgba(255,255,255,0.2);border-style:solid;border-width:1px 1px 1px 1px;">Batching Plant | Transit Mixer | Boom Pump | Stationary Pump</div>

                    <div class="tp-caption  " id="slide-6-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['200','200','200','200']" data-fontsize="['60','50','40','35']" data-lineheight="['71','61','51','45']" data-width="['901','901','550','400']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":370,"speed":1000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; min-width: 901px; max-width: 901px; white-space: normal; font-size: 60px; line-height: 71px; font-weight: 700; color: #ffffff; letter-spacing: 2px !important;font-family:Roboto;">Construction Equipment on Rent</div>


                    <a class="tp-caption rev-btn-01 rev-btn " href="{{route('front.service_details.page')}}" target="_self" id="slide-6-layer-4" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='' data-responsive_offset="off" data-responsive="off" data-frames='[{"delay":1350,"speed":300,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(34,34,34);bs:solid;bw:0 0 0 0;"}]' data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[20,20,20,20]" data-paddingright="[30,30,30,30]" data-paddingbottom="[20,20,20,20]" data-paddingleft="[30,30,30,30]" style="z-index: 9; white-space: nowrap; font-size: 12px; line-height: 12px; font-weight: 700; color: #ffffff; letter-spacing: 1px;font-family:Roboto;background-color: #EF7F1B;border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Services </a>
                </li>
            </ul>
            <div class="tp-bannertimer" style="height: 5px; background: rgba(0,0,0,0.15);"></div>
        </div>
    </div><!-- END REVOLUTION SLIDER -->
</div>
<!--====================  banner image three area ====================-->
<div class="banner-image-three-area bg-img grey-bg">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-6 ">
                <div class="banner-image-three__left ">
                    <div class="banner-image-three__content">
                        <h4 class="subtitle">OUR STORY</h4>
                        <h3 class="title">Market Leader in Ahmedabad <span class="highlight">Since 2011</span></h3>
                        <!-- <p class="subtitle-big">Industrial engineering is a branch of engineering which deals with the optimization.</p> -->
                        <p class="text" style="font-size: 16pt;">We have grown from a single plant to 5 plants today concreting more than 25,000 cubic metre per month and are continuously to further expand our presence across Gujarat. We are<b> now emerging as one face</b> for all your concrete requirements.</p>
                        <p class="text" style="font-size: 16pt;">We are one of the few QCI certified independent RMC manufacturing plants in India and only one in Ahmedabad. We focus on quality services and hence seek feedback constantly and in nearly a decade, we are proud of being able to maintain a high customer satisfaction level of 97%.</p>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-6 ">
                <div class="banner-image-three__right">
                    <div class="banner-image-three__image">
                        <div class="banner-image">
                            <img src="{{ asset('assets/front/img/banner/QCi.jpg') }}" class="img-fluid" alt="">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of banner image three area  ====================-->
<div class="page-content-wrapper grey-bg" style="overflow: hidden;">
        <div class="truck-animation"></div>
</div>
<!--====================  page content wrapper ====================-->
@if(isset($get_plant) && count($get_plant) >= 1)
<div class="page-content-wrapper plants_home section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- brand logo slider -->
                <div class="section-title-area section-title-area--style3 section-space--bottom--80">
                    <h2 class="title title--style7" style="color:#222;"> Our Plants </h2>
                    <div class="swiper-static-nav-wrap">
                        <a href="{{ route('front.location_plants.page') }}" class="ht-btn ht-btn--dark">VIEW ALL</a></center>
                    </div>
                </div>
                <!-- industry two slider -->
                <div class="industry-two-slider mb-0">
                    <div class="swiper-container industry-two-slider__container">
                        <div class="swiper-wrapper industry-two-slider__wrapper">
                            @foreach($get_plant as $gk => $gv)
                            <div class="swiper-slide">
                                <div class="industry-two-slider__single-item industry-two-slider__single-item--style2">
                                    <div class="industry-two-slider__single-item__image">
                                        @if($gv->plant_status == 0)
                                        <a href="{{route('front.about_plants.page',$gv->slug)}}">
                                        @endif
                                            <img src="{{asset('uploads/plant_image/'.$gv->plant_image)}}" class="img-fluid" alt="" >
                                        @if($gv->plant_status == 0)
                                        </a>
                                        @endif
                                    </div>
                                    <div class="industry-two-slider__single-item__content">
                                        <h3 class="title">
                                            @if($gv->plant_status == 0)
                                            <a href="{{route('front.about_plants.page',$gv->slug)}}">
                                            @endif
                                                {{$gv->plant_name}} 
                                            @if($gv->plant_status == 0)
                                            </a>
                                            @endif
                                        </h3>
                                        <p class="subtitle">{{$gv->address}}</p>
                                        @if($gv->plant_status == 0)
                                        <a href="{{route('front.about_plants.page',$gv->slug)}}" class="see-more-link see-more-link--dark">SEE MORE <i class="ion-arrow-right-c"></i></a>
                                        @else
                                        <a href="JavaScript:Void(0);" class="see-more-link see-more-link--dark">Coming Soon</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="swiper-pagination swiper-pagination-11"></div>
                    <div class="ht-swiper-button-prev ht-swiper-button-prev-4 ht-swiper-button-nav d-none d-lg-block" style="background-color: #EF7F1B;border-color: #EF7F1B;"><i class="ion-ios-arrow-left"></i></div>
                    <div class="ht-swiper-button-next ht-swiper-button-next-4 ht-swiper-button-nav d-none d-lg-block" style="background-color: #EF7F1B;border-color: #EF7F1B;"><i class="ion-ios-arrow-forward"></i></div>
                </div>

            </div>
        </div>
    </div>
</div>
@endif
<!--====================  End of page content wrapper  ====================-->


<!--====================  project counter area ====================-->
<div class="project-counter-area">
    <div class="row no-gutters">
        <div class="col-lg-12">
            <div class="project-counter-wrapper">
                <!-- project counter-bg -->
                <div class="project-counter-bg bg-img" data-bg="{{ asset('assets/front/img/backgrounds/home_project.JPG') }}"></div>
                <!-- project counter content -->
                <div class="project-counter-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="project-counter-single-content-wrapper">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- project counter single content -->
                                        <div class="project-counter-single-content">
                                            <div class="project-counter-single-content__image">
                                                <i class="glyph-icon icomoon-flag" style="color: #f8f9fa"></i>
                                            </div>
                                            <div class="project-counter-single-content__content">
                                                <span class="project-counter-single-content__project-count counter" style="font-size: 2.0rem;">400</span><span class="project-counter-single-content__project-count" style="font-size: 2.0rem;">+</span>
                                                <h5 class="project-counter-single-content__project-title">Projects </h5>
                                                <p class="project-counter-single-content__subtext">Done it all. Residential, Commercial, Industrial Plants, Bridges, RCC Roads.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <!-- project counter single content -->
                                        <div class="project-counter-single-content">
                                            <div class="project-counter-single-content__image">
                                                <i class="glyph-icon icomoon-shopping-cart" style="color: #f8f9fa"></i>
                                            </div>
                                            <div class="project-counter-single-content__content">
                                                <span class="project-counter-single-content__project-count counter" style="font-size: 2.0rem;">10,00,000</span><span class="project-counter-single-content__project-count" style="font-size: 2.0rem;">+</span>
                                                <h5 class="project-counter-single-content__project-title">Cubic Meter Concreting</h5>
                                                <p class="project-counter-single-content__subtext">10,000 dreams of families for their homes/offices fullfilled.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of project counter area  ====================-->

<!--====================  cta area ====================-->
<div class="cta-area section-space--inner--60 default-bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                
                <h2 class="cta-text text-center text-lg-left">Looking for quality construction partner?</h2>
            </div>
            <div class="col-lg-3 home_button text-center text-lg-right">
                <a href="{{route('front.select_calculation.page')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark">RMC CALCULATOR</a>
            </div>
            <div class="col-lg-2 text-center text-lg-right">
                <a href="{{route('admin.getQuoteCalculator')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark">ORDER NOW</a>
            </div>
        </div>
    </div>
</div>
<!--====================  End of cta area  ====================-->



<!--====================  industry grid area ====================-->
<div class="industry-grid-area">
    <div class="row no-gutters">
        <div class="col-lg-12">
            <div class="single-industry-grid-two-wrapper">
                <div class="single-industry-grid-two">
                    <div class="single-industry-grid-two__image bg-img" data-bg="{{ asset('assets/front/img/industry/stone_crushing.png') }}"></div>
                    <div class="single-industry-grid-two__content" style="background-color: white;">
                        <div class="content-wrapper">
                            <h2 class="title"><i class="glyph-icon icomoon-calendar"></i><a href="javascript:void(0);"><!-- Oil --> <span class="highlight" style="color:#222222;">Stone Crushing Plant</span></a></h2>
                            <p class="text">Backward Integration into stone crushing business under the name of New Rayka Stone LLP.</p>
                        </div>
                        <a href="{{ route('front.home.stone.page') }}" class="post-readmore-btn"> <span style="color: white;">READ MORE</span> <i class="ion-android-arrow-forward" style="color: white;"></i></a>
                    </div>
                </div>
                <!-- single industry grid -->
                <div class="single-industry-grid-two" style="background-color:#fff">
                    <div class="single-industry-grid-two__image bg-img" data-bg="{{ asset('assets/front/img/industry/home_projects_no_count.jpg') }}"></div>
                    <div class="single-industry-grid-two__content">
                        <div class="content-wrapper">
                            <h2 class="title"><i class="glyph-icon icomoon-flag"></i><a href="javascript:void(0);" style="color:#222;"><!-- Projects  --><span class="highlight">Projects</span></a></h2>
                            <p class="text">Residential, Commercial, Industrial, Bridges, Roads. We’ve done it all.</p>
                        </div>
                        <a href="{{ route('front.about_projects.page') }}" class="post-readmore-btn"> <span style="color: white;">READ MORE</span> <i class="ion-android-arrow-forward" style="color: white;"></i></a>
                    </div>
                </div>
                <!-- single industry grid -->
                <div class="single-industry-grid-two">
                    <div class="single-industry-grid-two__image bg-img" data-bg="{{ asset('assets/front/img/industry/Equipment_on_Hire_front.jpg') }}"></div>
                    <div class="single-industry-grid-two__content">
                        <div class="content-wrapper">
                            <h2 class="title"><i class="glyph-icon icomoon-calendar"></i><a href="javascript:void(0);"><!-- Oil --> <span class="highlight">Equipment on Hire</span></a></h2>
                            <p class="text">We not only supply RMC but also support your concrete needs by renting out our infrastructure.</p>
                        </div>
                        <a href="{{ route('front.service_details.page') }}" class="post-readmore-btn"> <span style="color: white;">READ MORE</span> <i class="ion-android-arrow-forward" style="color: white;"></i></a>
                    </div>
                </div>
                <!-- single industry grid -->
                <div class="single-industry-grid-two">
                    <div class="single-industry-grid-two__image bg-img" data-bg="{{ asset('assets/front/img/industry/product_home.jpg') }}"></div>
                    <div class="single-industry-grid-two__content" style="background-color: white;">
                        <div class="content-wrapper">
                            <h2 class="title"><i class="glyph-icon icomoon-layers"></i><a href="javascript:void(0);"><!-- Bridge --> <span class="highlight" style="color:#222222;">Products</span></a></h2>
                            <p class="text">Different RMC have different features, applications and advantages. Check out our various brands and products to find out which one suits you the best.</p>
                        </div>
                        <a href="{{ route('front.about_product.page') }}" class="post-readmore-btn"> <span style="color: white;">READ MORE</span> <i class="ion-android-arrow-forward" style="color: white;"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!--====================  End of industry grid area  ====================-->
@if(isset($get_client) && count($get_client) >= 1)
<div class="testimonial-brand-slider-area testimonial_home section-space--inner--120" style="margin-top: 0% !important;">
    <div class="container">
        <div class="row" >
            <div class="col-lg-12">
                <!-- brand logo slider -->
                <div class="section-title-area section-title-area--style3 section-space--bottom--80" style="margin-top:-3%;">
                    <h2 class="title title--style7" style="color: #222;"> Clients </h2>
                    
                    <div class="testimonial-slider__nav-container">
                        <div class="ht-swiper-button-prev ht-swiper-client-button-prev-3 ht-swiper-button-nav"><i class="ion-chevron-left"></i></div>
                        <div class="ht-swiper-button-next ht-swiper-client-button-next-3 ht-swiper-button-nav"><i class="ion-chevron-right"></i></div>
                    </div>
                </div>
                <div class="brand-logo-slider__container-area">
                    <div class="swiper-container brand-logo-slider__container">
                        <div class="swiper-wrapper brand-logo-slider__wrapper">
                            @foreach($get_client as $gk => $gv)
                            <div class="swiper-slide brand-logo-slider__single">
                                <div class="image">
                                    <img src="{{ asset('uploads/company_logo') }}/{{ $gv->company_logo }}" class="img-fluid" alt="">
                                </div>
                                <div class="image-hover">
                                    <img src="{{ asset('uploads/company_logo') }}/{{ $gv->company_logo }}" class="img-fluid" alt="">
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!--====================  End of testimonial brand slider area  ====================-->
<!--====================  testimonial brand slider area ====================-->
@if(isset($get_testimonial_other) && count($get_testimonial_other) >= 1)
<div class="testimonial-brand-slider-area testimonials_home_quote section-space--inner--120 grey-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- testimonial slider -->
                <div class="testimonial-slider__body-wrapper section-space--bottom--60">
                    <!-- testimonial slider title -->
                    <div class="testimonial-slider__title-wrapper section-space--bottom--60">
                        <h2 class="testimonial-slider__title" style="color: #222;">Testimonials</h2>
                        <div class="testimonial-slider__nav-container">
                            <div class="ht-swiper-button-prev ht-swiper-button-prev-3 ht-swiper-button-nav"><i class="ion-chevron-left"></i></div>
                            <div class="ht-swiper-button-next ht-swiper-button-next-3 ht-swiper-button-nav"><i class="ion-chevron-right"></i></div>
                        </div>
                    </div>

                    <div class="testimonial-slider__content-area">
                        <div class="swiper-container testimonial-slider__container">
                            <div class="swiper-wrapper testimonial-slider__wrapper">
                            @foreach($get_testimonial_other as $gk => $gv)
                                <div class="swiper-slide">
                                    <div class="testimonial-slider__single-item">
                                        <div class="testimonial-slider__single-item__image">
                                            <img src="{{asset('uploads/testimonial_other_image/'.$gv->image)}}" class="img-fluid" alt="">
                                        </div>
                                        <div class="testimonial-slider__single-item__content">
                                            <h4 class="testimonial-name"> {{$gv->name}} <span class="designation">{{$gv->    designation}}</span></h4>
                                            <div class="rating" style="color: #EF7F1B">
                                                @if($gv->rating == 1 || $gv->rating >= 1)
                                                    <i class="ion-ios-star"></i>
                                                @else
                                                    <i class="ion-ios-star-outline"></i>
                                                @endif

                                                @if($gv->rating == 2 || $gv->rating >= 2)
                                                    <i class="ion-ios-star"></i>
                                                @else
                                                    <i class="ion-ios-star-outline"></i>
                                                @endif

                                                @if($gv->rating == 3 || $gv->rating >= 3)
                                                    <i class="ion-ios-star"></i>
                                                @else
                                                    <i class="ion-ios-star-outline"></i>
                                                @endif

                                                @if($gv->rating == 4 || $gv->rating >= 4)
                                                    <i class="ion-ios-star"></i>
                                                @else
                                                    <i class="ion-ios-star-outline"></i>
                                                @endif

                                                @if($gv->rating == 5 || $gv->rating >= 5)
                                                    <i class="ion-ios-star"></i>
                                                @else
                                                    <i class="ion-ios-star-outline"></i>
                                                @endif
                                            </div>
                                            <p class="text">{{$gv->description}}</p>
                                            @if($gv->video_url != '')
                                            <div class="video-button-container video-popup">
                                                <a href="{{ $gv->video_url }}" class="section-title-video-button">
                                                    <div class="video-play bg-img" data-bg="{{ asset('assets/img/icons/video-play.png') }}">
                                                        <i class="ion-ios-play"></i>
                                                    </div>
                                                    <div class="video-text"> Watch Video <i class="ion-arrow-right-c"></i></div>
                                                </a>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                        <div class="swiper-pagination swiper-pagination-3"></div>
                    </div>

                </div>
            </div>
            
        </div><br><br>
    </div>
</div>
@endif
<!--====================  page content wrapper ====================-->
@if(isset($get_blog) && count($get_blog) >= 1)
<div class="page-content-wrapper blogs_home section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- blog grid wrapper -->
                <div class="section-title-area section-title-area--style3 section-space--bottom--80">
                    <h2 class="title title--style7" style="color:#222;"> Blogs </h2>
                    <div class="swiper-static-nav-wrap">
                        <a href="{{ route('front.blog_list.page') }}" class="ht-btn ht-btn--dark">VIEW ALL</a></center>
                    </div>
                </div>
                <div class="blog-grid-wrapper">
                    <div class="row">
                        @foreach($get_blog as $gk => $gv)
                        <div class="col-lg-4 col-md-6">
                            <div class="blog-post-slider__single-slide blog-post-slider__single-slide--grid-view">
                                <div class="blog-post-slider__image section-space--bottom--30">
                                    <a href="{{route('front.blog_detail.page',$gv->blog_slug)}}"><img src="{{asset('uploads/blog_image_thumb/'.$gv->blog_image)}}" class="img-fluid" alt=""></a>
                                </div>
                                <div class="blog-post-slider__content">
                                    <?php $date = date('M d Y',strtotime($gv->date)); ?>
                                    <p class="post-date"> {{$date}}</p>
                                    <h3 class="post-title">
                                        <a href="{{route('front.blog_detail.page',$gv->blog_slug)}}">{{$gv->title}}</a>
                                    </h3>
                                    <p class="post-excerpt">
                                        <?php

                                        $text = $gv->description;

                                        $text = strip_tags($text,"<style>");

                                        $substring = substr($text,strpos($text,"<style"),strpos($text,"</style>"));

                                        $text = str_replace($substring,"",$text);
                                        $text = str_replace(array("\t","\r","\n"),"",$text);
                                        $text = trim($text);

                                        

                                        $position=112; // Define how many character you want to display.

                                        $message= $text;
                                        $post = substr($message, 0, $position);

                                        echo strip_tags($post);
                                        echo "...";
                                        ?></p>
                                    <a href="{{route('front.blog_detail.page',$gv->blog_slug)}}" class="see-more-link blog-readmore">SEE MORE <i class="ion-arrow-right-c"></i></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="page-content-wrapper map_home section-space--inner--120 grey-bg">
    <!-- google map -->
    <div class="google-map google-map--style-2" id="google-map-one" data-width="100%" data-zoom_enable="" data-zoom="10" data-map_type="roadmap">
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection  

@section('js')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDbX_JirTlqgj9BO002nMah8CQSD7f4ypI&signed_in=true&libraries=places"></script>
<script type="text/javascript">
var data = {!! json_encode($latLong) !!};
var locations = data; 


//alert(locations);
var map = new google.maps.Map(document.getElementById('google-map-one'), { 
    zoom: 11, 
    center: new google.maps.LatLng(23.033863, 72.585022), 
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    options: {
        gestureHandling: 'greedy'
    },
    scrollwheel: false
}); 
   
var infowindow = new google.maps.InfoWindow(); 

var marker, i; 

for (i = 0; i < locations.length; i++) { 
    console.log(locations[i][4]);
    marker = new google.maps.Marker({ 
        position: new google.maps.LatLng(locations[i][1], locations[i][2]), 
        map: map,
        icon: {
            url: "http://maps.gstatic.com/mapfiles/markers2/marker.png"
        }, 
        title: String(locations[i][0]),
        url : locations[i][4]
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) { 
        return function() { 
            
            infowindow.setContent(this.get('title'));
            marker.setTitle(this.get('title'));
            infowindow.open(map, marker);
            window.location.href = this.url; 
            window.open = this.url;
        } 
    })(marker, i));

    markerYellow_first = new google.maps.Marker({ 
        position: new google.maps.LatLng('23.1345003', '72.5825692'), 
        map: map,
        icon: {
          url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
        },
        title : 'Upcoming'/*,
        url : 'http://hindustanrmc.homipod.com/plants-location'*/
    });

    markerYellow = new google.maps.Marker({ 
        position: new google.maps.LatLng('23.1279494', '72.55819269999999'), 
        map: map,
        icon: {
          url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
        }, 
        title : 'Upcoming'/*,
        url : 'http://hindustanrmc.homipod.com/plants-location'*/
    });

    google.maps.event.addListener(markerYellow_first, 'click', (function(markerYellow_first, i) { 
        return function() { 
            
            infowindow.setContent(this.get('title'));
            markerYellow_first.setTitle(this.get('title'));
            infowindow.open(map, markerYellow_first);
            /*window.location.href = this.url; 
            window.open = this.url;*/
        } 
    })(markerYellow_first, i));

    google.maps.event.addListener(markerYellow, 'click', (function(markerYellow, i) { 
        return function() { 
            
            infowindow.setContent(this.get('title'));
            markerYellow.setTitle(this.get('title'));
            infowindow.open(map, markerYellow);
           /* window.location.href = this.url; 
            window.open = this.url;*/
        } 
    })(markerYellow, i));
} 


</script>
@endsection

