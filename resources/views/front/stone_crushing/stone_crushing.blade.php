@extends('layouts.front.front')
@section('title','Hindustan RMC | Stone Crushing Plant')
@section('content')
<div class="breadcrumb-area section-space--inner--80 bg-img" style="background-color: #f7f7f7 !important;">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6 section-title-area">
                <h2 class="title">Stone Crushing Plant</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children" style="color: #222;"><a href="{{route('front.home.page')}}" style="color: #222;">Home</a></li>
                    <li style="color: #222;">Stone Crushing Plant</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120" style="padding-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <!-- blog content wrapper -->
                <div class="blog-content-wrapper">
                    <div class="single-list-blog-post mb-0">
                        <div class="single-list-blog-post__media section-space--bottom--50">
                            <div class="product-details__image-slider">
                                
                                <div class="swiper-container product-details__image-slider-container">
                                    <div class="swiper-wrapper product-details__image-slider-wrapper">
                                        <div class="swiper-slide">
                                            <div class="product-details__image-single">
                                                <img src="{{ asset('assets/front/img/industry/stone_crushing.png') }}" class="img-fluid" alt="" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="swiper-pagination swiper-pagination-15"></div> -->
                            </div>
                        </div>

                        <div class="single-list-blog-post__content single-list-blog-post__content--style2">
                            <h2 class="title" style="margin-bottom: 28px;font-size: 1.5625rem;">Plant Description</h2>
                            <div class="myaccount-table table-responsive text-center" style="width: 88%;">
                                <table class="table table-bordered">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Heads</th>
                                            <th>Details</th>                  
                                        </tr>
                                    </thead>

                                    <tbody>
                                        
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Year of Establishment</td>
                                            <td style="color: #888;font-size: 15px;">Jan - 2019</td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Track Record</td>
                                            <!-- <td style="color: #888;font-size: 15px;">3 lac m3 in 5 Years</td> -->
                                            <td style="color: #888;font-size: 15px;">--------------</td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Government Approval</td>
                                            <td style="color: #888;font-size: 15px;">Gujarat Pollution Control Board</td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Plant Make</td>
                                            <td style="color: #888;font-size: 15px;">Puzzolana 200 Tph Stone Crusher (Jaw Crusher)</td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Maximum Supply/Day</td>
                                            <td style="color: #888;font-size: 15px;">2400 MT</td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Electric Supply Condition</td>
                                            <td style="color: #888;font-size: 15px;">GEB & Diesel Generator</td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Water Supply Condition</td>
                                            <td style="color: #888;font-size: 15px;">Borewell Water</td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">No. of Transit Mixer</td>
                                            <td style="color: #888;font-size: 15px;">15</td>
                                        </tr>
                                        
                                        
                                        <!--<tr>
                                            <td style="color: #333;font-weight: bold;">No. of Boom Placer</td>
                                            <td style="color: #888;font-size: 15px;">5</td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">No. of Concrete Pump</td>
                                            <td style="color: #888;font-size: 15px;">5</td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td style="color: #333;font-weight: bold;">Pipe Line Length (Metre)</td>
                                            <td style="color: #888;font-size: 15px;">600</td>
                                        </tr> -->
                                        
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>  
                        
                        <!-- <div class="single-service-gallery-wrapper section-space--bottom--60" style="margin-top: 20px;margin-bottom: 0px !important;">
                            <h2 class="title" style="margin-bottom: 15px;">Plant Gallery</h2>
                            <div class="case-study__image-gallery-wrapper">
                                <div class="row image-popup">
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{asset('assets/front/img/backgrounds/stone_plant.jpg')}}" class="single-gallery-thumb">
                                                <img src="{{asset('assets/front/img/backgrounds/stone_plant.jpg')}}" class="img-fluid" alt="" style="height: 238px;">
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{asset('assets/front/img/backgrounds/stone_plant.jpg')}}" class="single-gallery-thumb">
                                                <img src="{{asset('assets/front/img/backgrounds/stone_plant.jpg')}}" class="img-fluid" alt="" style="height: 238px;">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="case-study__single-gallery-image">
                                            <a href="{{asset('assets/front/img/backgrounds/stone_plant.jpg')}}" class="single-gallery-thumb">
                                                <img src="{{asset('assets/front/img/backgrounds/stone_plant.jpg')}}" class="img-fluid" alt="" style="height: 238px;">
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div> -->
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <!-- page sidebar -->
                <div class="page-sidebar">      
                    <!-- single sidebar widget -->
                    
                    <div class="single-sidebar-widget">
                        <h2 class="widget-title">Plant Details</h2>
                        <ul class="sidebar-category">
                            
                            <li><a href="javascript:void(0);" style="color: #333;font-weight: bold;">Address</a>
                            <!-- <a href="javascript:void(0);" style="color: #888;font-size: 15px;">Hindustan RMC (Shela Plant), Sardar Patel Ring Road, Ahmedabad, Gujarat, India</a></li> -->

                            <a href="javascript:void(0);" style="color: #888;font-size: 15px;">

                            Survey No. 160, Mota Kerala Road,
Opp. Satyanaran Stone ,Nr. Sudamda Road,
Sayla Highway Cokadi, Mota Kerala ,
Tal. Sayala , Dis.Surandranagar, Pin.-363430</a></li>


                            
                            
                            <li><a href="javascript:void(0);" style="color: #333;font-weight: bold;">Telephone</a>
                            
                            <a href="tel:9033333563" style="color: #888;font-size: 15px;">9033-333-563</a></li>
                            
                            
                            <li><a href="javascript:void(0);" style="color: #333;font-weight: bold;">Email</a>
                            <a href="mailto:raykastone@gmail.com" style="color: #888;font-size: 15px;">raykastone@gmail.com</a></li>
                            
                            
                            <li><a href="javascript:void(0);" style="color: #333;font-weight: bold;">Plant Inchanrge</a>
                            <a href="javascript:void(0);" style="color: #888;font-size: 15px;">Mr. Vijay Bhai</a></li>
                            
                            
                            <!-- <li><a href="javascript:void(0);" style="color: #333;font-weight: bold;">Quality Department</a>
                            <a href="javascript:void(0);" style="color: #888;font-size: 15px;">Mr. Mehul Vyas</a></li> -->
                            
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->

@endsection