@extends('layouts.front.front')
@section('title','Hindustan RMC | Plant Comparison')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/banner/plant_compare_banner.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Plant Comparison</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="javascipt:void(0);">RMC</a></li>
                    <li>Plant Comparison</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120" >
    <!--=======  shopping cart wrapper  =======-->
    <div class="shopping-cart-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- <h2 class="title" style="margin-bottom: 28px;">DIFFERENCE BETWEEN SMC & RMC</h2> -->
                    <!--=======  cart table  =======-->
                    <div class="myaccount-table table-responsive text-center">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <th style="background-color: #495057;color: #ffffff;">PARTICULARS</th>
                                    <th style="background-color: #495057;color: #ffffff;">HINDUSTAN RMC</th>
                                    <th style="background-color: #495057;color: #ffffff;">OTHER RMC</th>
                                    
                                </tr>
                                <tr>
                                    <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                    <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                    <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                </tr>
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;">
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                            <div class="single-contact-icon-info__image">
                                                <i class="glyph-icon flaticon-010-tank-1" style="font-size: 35px;"></i>
                                            </div>
                                            <div class="single-contact-icon-info__content">
                                                <p><h4 class="title">PLANTS</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation table_tr_color">Number of Plants</span>
                                    </td>
                                    <td>5</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Coverage Area</span>
                                    </td>
                                    <td>5000 + Sq. km.</td>
                                    <td>25 Sq. km.</td>
                                </tr>
                            </tbody>
                            <tr>
                                <th style="border-color: #ffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                            </tr>
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;">
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                            <div class="single-contact-icon-info__image">
                                                
                                                <i class="glyph-icon flaticon-025-meter" style="font-size: 35px;"></i>
                                            </div>
                                            <div class="single-contact-icon-info__content">
                                                
                                                <p><h4 class="title">CAPACITY</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Manufacturing Capacity</span>
                                    </td>
                                    <td>180 m³/hr</td>
                                    <td>60 m³/hr</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Max. Output Capacity</span>
                                    </td>
                                    <td>150 m³/hr</td>
                                    <td>50 m³/hr</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">    
                                        <span class="product-variation">Max. TM Capacity</span>
                                    </td>
                                    <td>9 m³</td>
                                    <td>6-7 m³</td>
                                </tr>
                            </tbody>
                            <tr>
                                <th style="border-color: #ffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                            </tr>
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;"> 
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                            <div class="single-contact-icon-info__image">
                                                <i class="glyph-icon flaticon-034-industrial-robot" style="font-size: 35px;"></i>
                                            </div>
                                            <div class="single-contact-icon-info__content">
                                                <p><h4 class="title">INFRASTRUCTURE</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Number of Transit Mixer (TM)</span>
                                    </td>
                                    <td>60</td>
                                    <td>5</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">    
                                        <span class="product-variation">Concrete Pumps</span>
                                    </td>
                                    <td>18</td>
                                    <td>2</td>
                                </tr>
                            </tbody>
                            <tr>
                                <th style="border-color: #ffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                            </tr>
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;">
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                                <div class="single-contact-icon-info__image">
                                                    
                                                    <i class="glyph-icon flaticon-016-gear" style="font-size: 35px;"></i>
                                                </div>
                                            <div class="single-contact-icon-info__content">                    
                                                <p><h4 class="title">EXPERIENCE</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Year of Establishment</span>
                                    </td>
                                    <td>2010</td>
                                    <td>----</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Reputed Industrial Clients</span>
                                    </td>
                                    <td>Kajima India, Ford, Tata Nano, Hitachi</td>
                                    <td>Not Known</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">After Sales Service</span>
                                    </td>
                                    <td>✔</td>
                                    <td>✘</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Customer Satisfaction Ratio</span>
                                    </td>
                                    <td>97% Based on Feedback </td>
                                    <td>No Such Data Collected</td>
                                </tr>
                            </tbody>
                            <tr>
                                <th style="border-color: #ffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                            </tr>
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;">
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                                <div class="single-contact-icon-info__image">
                                                    
                                                    <i class="glyph-icon flaticon-020-planning" style="font-size: 35px;"></i>
                                                </div>
                                            <div class="single-contact-icon-info__content">
                                                
                                                <p><h4 class="title">CERTIFICATIONS</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">QCI Approved Plant</span>
                                    </td>
                                    <td>✔</td>
                                    <td>✘</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">ISO Certification</span>
                                    </td>
                                    <td>✔</td>
                                    <td>✘</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">EHS Standards</span>
                                    </td>
                                    <td>✔</td>
                                    <td>✘</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">GPCB Clearances</span>
                                    </td>
                                    <td>✔</td>
                                    <td>✘</td>
                                </tr>
                            </tbody>
                            <tr>
                                <th style="border-color: #ffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                            </tr>
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;">
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                                <div class="single-contact-icon-info__image">
                                                    
                                                    <i class="glyph-icon flaticon-004-walkie-talkie" style="font-size: 35px;"></i>
                                                </div>
                                            <div class="single-contact-icon-info__content">
                                                
                                                <p><h4 class="title">USP</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Chilling Plant for Temperature Controlled Concrete</span>
                                    </td>
                                    <td>✔</td>
                                    <td>✘</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Mobile Concrete Testing Lab</span>
                                    </td>
                                    <td>✔</td>
                                    <td>✘</td>
                                </tr>
                            </tbody>
                            <tr>
                                <th style="border-color: #ffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                                <th style="background-color:#ffffff;border-color: #ffffff;"></th>
                            </tr>
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="3" style="border-color: #dee2e6;">
                                        <div class="single-contact-icon-info single-contact-icon-info--style2" style="padding: 0px 41%;background-color: #e9ecef;">
                                                <div class="single-contact-icon-info__image">
                                                    
                                                    <i class="glyph-icon flaticon-021-worker" style="font-size: 35px;"></i>
                                                </div>
                                            <div class="single-contact-icon-info__content">
                                                
                                                <p><h4 class="title">TEAM</h4></p>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Team Strength</span>
                                    </td>
                                    <td>100</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td class="table_tr_color">
                                        <span class="product-variation">Lab Technicians</span>
                                    </td>
                                    <td>12</td>
                                    <td>2</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--=======  End of cart table  =======-->
                </div>
            </div>
        </div>
    </div>
    <!--=======  End of shopping cart wrapper  =======-->
</div>
<!--====================  End of page content wrapper  ====================-->
<!--====================  cta area ====================-->
<div class="cta-area section-space--inner--60 default-bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <!-- cta text -->
                <h2 class="cta-text text-center text-lg-left">Looking for quality construction partner?</h2>
            </div>
            <div class="col-lg-3 home_button text-center text-lg-right">
                <a href="{{route('front.select_calculation.page')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark">RMC CALCULATOR</a>
            </div>
            <div class="col-lg-2 text-center text-lg-right">
                <a href="{{route('admin.getQuoteCalculator')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark">ORDER NOW</a>
            </div>
        </div>
    </div>
</div>
<!--====================  End of cta area  ====================-->
@endsection