@extends('layouts.front.front')
@section('title','Hindustan RMC | USP')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/USP_banner.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">USP</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="javascipt:void(0);">RMC</a></li>
                    <li>USP</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  featured project area ====================-->
<div class="featured-project-area bg-img section-space--inner--top--120 section-space--inner--bottom--300 ups_image">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="featured-project-wrapper">
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- section title left align -->
                            <div class="section-title-area">
                                <h2 class="title title--left">We are constantly striving to make our customers lives easier.</h2>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <!-- section title content -->
                            <div class="section-title-content-area">
                                <p class="section-title-content">We are the only company that can provide ice concrete and colour concrete. We provide SMS Updates to promote transparency and quick updates on material dispatch and also provide with GPS tracking. We are the only one that has 24x7 mobile testing van that enables proper testing and quality assurance to our customers. We know the importance of concrete in creating the dream building and our plants being certified by  GPCB, ISO approved and RMCMA QCI Certified plants is a testimony to our commitment to maintain the highest standards of excellence.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of featured project area  ====================-->
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120 grey-bg" style="padding-bottom: 240px;padding-top: 80px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- blog grid wrapper -->
                <div class="blog-grid-wrapper blog-grid-wrapper--style5">
                    <div class="row">
                        <div class="col-lg-4 col-md-6" style="margin-top:50px;">
                            <div class="">
                                <div class="single-grid-blog-post__image single-grid-blog-post__image--style2">
                                    <!-- <span class="post-date" style="height: 54px;">1</span> -->
                                    <div class="post-image">
                                        <a href="JavaScript:Void(0);">
                                            <img src="{{asset('assets/front/img/blog/slider/1_usp_page.png')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="single-grid-blog-post__content single-grid-blog-post__content--style2">
                                    <h3 class="post-title">24x7 Mobile Testing Van</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6" style="margin-top:50px;">
                            <div class="">
                                <div class="single-grid-blog-post__image single-grid-blog-post__image--style2">
                                    <!-- <span class="post-date" style="height: 54px;">2 </span><span>Aug</span> --> 
                                    <div class="post-image">
                                        <a href="JavaScript:Void(0);">
                                            <img src="{{ asset('assets/front/img/blog/slider/2_usp_page.png')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="single-grid-blog-post__content single-grid-blog-post__content--style2">
                                    <h3 class="post-title">SMS Updates</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6" style="margin-top:50px;">
                            <div class="">
                                <div class="single-grid-blog-post__image single-grid-blog-post__image--style2">
                                    <!-- <span class="post-date" style="height: 54px;">3</span> -->
                                    <div class="post-image">
                                        <a href="JavaScript:Void(0);">
                                            <img src="{{asset('assets/front/img/blog/slider/3_ups.png')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="single-grid-blog-post__content single-grid-blog-post__content--style2">
                                    <h3 class="post-title">GPS Tracking</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6" style="margin-top:50px;">
                            <div class="">
                                <div class="single-grid-blog-post__image single-grid-blog-post__image--style2">
                                    <!-- <span class="post-date" style="height: 54px;">4</span> -->
                                    <div class="post-image">
                                        <a href="JavaScript:Void(0);">
                                            <img src="{{asset('assets/front/img/blog/slider/4_usp_page.png')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="single-grid-blog-post__content single-grid-blog-post__content--style2">
                                    <h3 class="post-title">Chilling Plant</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6" style="margin-top:50px;">
                            <div class="">
                                <div class="single-grid-blog-post__image single-grid-blog-post__image--style2">
                                    <!-- <span class="post-date" style="height: 54px;">5</span> -->
                                    <div class="post-image">
                                        <a href="JavaScript:Void(0);">
                                            <img src="{{asset('assets/front/img/blog/slider/5_ups.png')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="single-grid-blog-post__content single-grid-blog-post__content--style2">
                                    <h3 class="post-title">Colour Concrete</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6" style="margin-top:50px;">
                            <div class="">
                                <div class="single-grid-blog-post__image single-grid-blog-post__image--style2">
                                    <!-- <span class="post-date" style="height: 54px;">6</span> -->
                                    <div class="post-image">
                                        <a href="JavaScript:Void(0);">
                                            <img src="{{asset('assets/front/img/blog/slider/6_ups.png')}}" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="single-grid-blog-post__content single-grid-blog-post__content--style2">
                                    <h3 class="post-title">Certifications</h3>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
<!--====================  cta area ====================-->
<div class="cta-area section-space--inner--60 default-bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <!-- cta text -->
                <h2 class="cta-text text-center text-lg-left">Looking for quality construction partner?</h2>
            </div>
            <div class="col-lg-3 home_button text-center text-lg-right">
                <a href="{{route('front.select_calculation.page')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark" style="color: #ffff;">RMC CALCULATOR</a>
            </div>
            <div class="col-lg-2 text-center text-lg-right">
                <a href="{{route('admin.getQuoteCalculator')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark" style="color: #ffff;">ORDER NOW</a>
            </div>
        </div>
    </div>
</div>
<!--====================  End of cta area  ====================-->
@endsection