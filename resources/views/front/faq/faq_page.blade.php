@extends('layouts.front.front')
@section('title','Hindustan RMC | FAQs')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/FAQ.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">FAQs</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="javascript:void(0);">Support</a></li>
                    <li>FAQs</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="common-page-content">
                    <!-- <div class="common-page-text-wrapper section-space--bottom--50">
                        <h2 class="common-page-title">Frequently Asked Questions</h2>
                        <p>Our industry professionals are able to deliver better ideas and solutions embedded with a deep understanding of each client’ business and industry. The industry focus of our experts allows us to offer comprehensive solutions.</p>
                    </div> -->
                    <div class="faq-wrapper section-space--bottom--60">

                        <div id="accordion">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="font-size: 0.9rem;">
                                            What are the advantages of using ready-mixed concrete over site-mixed concrete? <span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Ready-mix concrete is sometimes preferred over on-site concrete mixing because of the precision of the mixture and reduced work site confusion. However, using a predetermined concrete mix reduces flexibility, both in the supply chain and in the actual components of the concrete. Hindustan RMC offer different types of concrete according to user’s mix design or industrial standard.</p>
                                        <h5 class="case-study__page-content-title" style="font-size: 0.9rem;margin-top: 1px;">Advantages of Ready-mix concrete over Site-mix concrete:</h5>
                                        <div class="case-study__subcontent-wrapper section-space--top--60" style="margin-top: 0px;">
                                            <div class="case-study__subcontent-single section-space--bottom--40">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">A carefully proportioned concrete made with latest 4th generation super plasticizers and viscosity modifying agents that lend it a unique self-compacting feature</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">The plants are located in areas zoned for industrial use, and yet the delivery trucks can service residential districts or inner cities.</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Better quality concrete is produced.</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Elimination of storage space for necessary materials at the site.</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Elimination of procurement/hiring of plant and machinery</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Wastage of basic materials is avoided.</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Labor associated with the production of concrete is eliminated.</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">The time required is greatly reduced.</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Noise and dust pollution at the site is reduced.</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Reduce cost.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <p>All these advantages clearly establish the technical superiority of ready-mixed concrete over site-mixed concrete. As regards cost, ready-mixed is slightly costlier than site-mixed concrete. This increased cost is mainly on account of government taxation.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="font-size: 0.9rem;">Is ready mixed concrete a well-established technology? How long this technology is being used in practice in India? <span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Yes, ready-mixed concrete is a well-established technology. It was first patented in Germany, way back in 1903. By 1950s, the use of ready mixed concrete picked up in most of the advanced countries in Europe and America. Compared to the advanced countries, India was a late starter in ready-mixed concrete. RMC technology came to India in 1996. Technologically speaking, the production process of ready mixed concrete has been continuously been upgraded since 1996, leading to improvement in product quality and uniformity. Today's plants in India are highly automated and consist of state-of-the-art machinery with computerized controls over the entire process of production.</p>
                                        <p>Set up way back in 2011, we expanded our business and currently own a total of 5 RMC plants covering entire Ahmedabad - Gandhinagar belt in India.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="font-size: 0.9rem;">
                                            What equation should I use to find out how much concrete I need? How can we arrive at the exact quantity of concrete required at our site? <span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <h5 class="case-study__page-content-title" style="font-size: 0.9rem;margin-top: 1px;">Calculate concrete:</h5>
                                        <div class="case-study__subcontent-wrapper section-space--top--60" style="margin-top: 0px;">
                                            
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Determine how thick you want the concrete</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Measure the length and width that you’d like to cover</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Multiply the length by the width to determine square footage</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Convert the thickness from inches to feet</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Multiply the thickness in feet by the square footage to determine cubic feet</p>
                                                </div>
                                            </div>
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content">Convert cubic feet to cubic yards by multiplying by .037</p>
                                                </div>
                                            </div>
                                        </div>
                                        <p>Generally your architect/engineers need to figure out the grade-wise concrete requirements based on plan of the structure and communicate the same to our plant. They are the right people and are in a better position to decide the quantities required. Concrete is generally specified in terms of the 28-day compressive strength and workability (usually slump) at placement point. These two are basic minimum requirements for us to design concrete mixes for you.</p>
                                        <p>Hindustan RMC well qualified engineers are always available to assist you with the concrete requirements for your site.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour" style="font-size: 0.9rem;">
                                            Can we base the measurements of concrete on the simple L (length) x B (breadth) x H (height) multiplication? <span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>All over the world, the basis of purchase and hence measurement of ready-mixed concrete is the volume of freshly mixed concrete. The Indian Standard on ready-mixed concrete, IS 4926:2003 states, “All concrete will be supplied and invoiced in terms of cubic meters (full or part) of compacted fresh concrete”. If the measurement is based on the simple L x D x H multiplication, it would be unfair to the RMC producer as it would not include the wastage and spillage of concrete, miscalculations in form volume, deflection or distortions of forms, settlement of wet mixes, over excavation, loss of entrained air, etc. In most of these cases, the responsibility does not lie with the RMC producer. Improper excavation is a common site problem. Deflection or distortion of forms is also quite common. It is observed that if the centre of 150-mm thick formwork of a slab gets deflected by 5 mm, an increased volume of 3 % would be required at the centre, decreasing towards the edges. Therefore ready-mixed concrete should only be measured based on the actual volume of fresh concrete supplied through transit mixers.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFive">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive" style="font-size: 0.9rem;">
                                            How can a buyer of ready-mixed concrete be assured of the quantity supplied? <span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The buyer can ensure the quantity supplied by checking the weight of the vehicle in a weighbridge before unloading and after unloading in order to obtain a net weight. Another alternative is to cross check the dimensions of the slab area.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSix">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix" style="font-size: 0.9rem;">
                                            How do we assure the quality of Ready Mix Concrete before using for Construction work? <span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Generally, Ready Mix Concrete is designed by an engineer which is generally known as concrete mix design of a specific grade viz M25, M30 etc... The design reports show the proportions of the various ingredients (coarse and fine aggregates, cement, water, often fly ash, admixture) of concrete based on the laboratory test mix for a particular grade.</p>
                                        <p>The slump test and compression test are two of the most common types of tests used for checking the quality of ready mix concrete. While a slump test is performed on-site, a compression test is performed in a lab. A supervisor performs the slump test before the concrete is about to be inserted into the formwork. However, if during the concrete laying process if the supervisor finds that the green concrete is getting dry or if the concrete placement is interrupted, the remaining concrete is tested again.</p>
                                        <p>The compression test is conducted for determining the characteristic strength of concrete. The result of this test is important for accepting in-site concrete work as it determines the concrete's strength. Historical slump and strength data are also taken into consideration. Moreover, for large projects trial mixes are often required.</p>
                                        <p>Hindustan RMC ensure after exhaustive laboratory and plant trials are conducted, and honestly reported the ready mixed concrete mixes are supplied to ensure good quality.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSeven">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven" style="font-size: 0.9rem;">
                                            Why is it essential to employ trained personnel for sampling and preparation of test cubes? <span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Although sampling and test cube preparation are simple methods, the procedures are operator-sensitive; hence it is essential to employ trained personnel. Take for example, extraction of a “representative” sample. You know that correct procedure involves taking four incremental samples from the middle 80% of the load, ignoring the first and the last 10% portion coming out from the truck mixer and then thoroughly mixing the incremental samples. Unless workmen are trained in this operation, they tend to violate the correct procedures. Similarly, standard procedures for making cube test specimens (filling cubes in layers, tamping each layer 25 times, adequate protection and curing of test specimens, etc.) tend to get violated on many occasions. This is mainly because the workmen at site are not trained adequately to do these jobs. If sampling and cube preparation work are done incorrectly, test results could be erroneous and the producer may get punished for no fault his own. Therefore, it is our considered opinion that sampling and cube preparation should be done only by the trained and experienced personnel.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEight">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight" style="font-size: 0.9rem;">How long does ready mix concrete need to set? <span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The concrete will begin to set once water is added at the site. For average, you will have to lay and level the ready mix concrete between 1 and 2 hours before the work ability is greatly reduced.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEight">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight" style="font-size: 0.9rem;">How can you assure us about the long-term durability of our structures with your concrete?<span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Long-term durability of your structures is dependent upon a variety of factors. Important amongst them are the exposure conditions to which the structure will be subjected to during service life, structural design and detailing, quality of concrete and precautions taken during placement, compaction and curing of concrete.</p>
                                        <p>Hindustan RMC continuously strive to focus on quality services and hence seek feedback constantly and in nearly a decade, we are proud of being able to maintain a high customer satisfaction level of 97%.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingNine">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine" style="font-size: 0.9rem;">Are there quality standards for ready mix concrete?<span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Yes, we are one of the few QCI certified independent RMC manufacturing plants in India and only one in Ahmedabad, which is proof of our commitment to the best standards for our ready mix.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTen">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen" style="font-size: 0.9rem;">Can we arrange for transportation of concrete from your plant to our site in our transit mixer/dumper?<span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Sure. But as you know the successful mixing and transporting of concrete requires the careful management of many factors.You would therefore understand that our liability for concrete consistency (slump, density, and compressive strength) will only be true when we pour concrete into your vehicles.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEleven">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven" style="font-size: 0.9rem;">Do we have to start curing earlier for ready-mixed concrete after placement?<span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Curing of concrete is the process of maintaining moisture inside the freshly casted concrete to continue the hydration process to achieve the desire properties of hardened concrete. Curing of concrete must begin as soon as possible after placement & finishing and must continue for a reasonable period of time as per the relevant standards, for the concrete to achieve its desired strength and durability. It is suggested that the curing process for concrete should commence immediately once the concrete surface looses its water and undergo shrinkage, which is normally within 2 hours after finishing compaction of concrete. Else, it will lead to plastic shrinkage cracks.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwelve">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve" style="font-size: 0.9rem;">What are the compliance criteria for ready-mix concrete?<span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>During delivery, the most common compliance conditions are slump value of concrete on site and concrete installation temperature, depending on the specifications and requirements of the client. Other than that, for compliance purposes, the quality of the concrete supplied will be judged on the basis of the compressive strength of concrete cube 28 days against the concrete grade ordered.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThirteen">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen" style="font-size: 0.9rem;">How the use of fly ash enhances quality of concrete? How to ensure good quality fly ash?<span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Fly ash benefits fresh concrete by reducing the mixing water requirement and improving the paste flow behaviour. The resulting benefits are as follows:</p>
                                        <div class="case-study__subcontent-wrapper section-space--top--60" >
                                            
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content"><b>Improved workability.</b> The spherical shaped particles of fly ash act as miniature ball bearings within the concrete mix, thus providing a lubricant effect. This same effect also improves concrete pumpability by reducing frictional losses during the pumping process and flat work finish ability.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="case-study__subcontent-wrapper section-space--top--60" style="margin-top: 0px;">
                                            
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content"><b>Decreased water demand.</b>The replacement of cement by fly ash reduces the water demand for a given slump. When fly ash is used at about 20 percent of the total cementitious, water demand is reduced by approximately 10 percent. Higher fly ash contents will yield higher water reductions. The decreased water demand has little or no effect on drying shrinkage/cracking. Some fly ash is known to reduce drying shrinkage in certain situations.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="case-study__subcontent-wrapper section-space--top--60" style="margin-top: 0px;">
                                            
                                            <div class="case-study__subcontent-single section-space--bottom--40" style="margin-top: -21px;">
                                                <div class="case-study__marker">
                                                    <i class="ion-flash"></i>
                                                </div>

                                                <div class="case-study__subcontent-content">
                                                    <p class="section-title-content"><b>Reduced heat of hydration.</b>Replacing cement with the same amount of fly ash can reduce the heat of hydration of concrete. This reduction in the heat of hydration does not sacrifice long-term strength gain or durability. The reduced heat of hydration lessens heat rise problems in mass concrete placements.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <p>The physical properties of fly ash as well as its uniformity requirements are checked from time to time to ensure compliance with the requirements of IS 3812:2003.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingForteen">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseForteen" aria-expanded="false" aria-controls="collapseForteen" style="font-size: 0.9rem;">How is ready mix eco-friendly?<span> <i class="ion-plus"></i>
                                            <i class="ion-minus"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseForteen" class="collapse" aria-labelledby="headingForteen" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Ready-mix concrete is eco-friendly in a lot of ways. Concrete's ingredients (water, aggregate, and cement) are plentiful in supply and have a lesser impact on the environment in their extraction than other building materials. Quarries, the main source of raw materials, can be quickly reclaimed for residential, recreational and commercial use. They can also be refurbished to their natural condition. Plus, leftovers are readily reused or recycled.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-sidebar">
                    <!-- single sidebar widget -->
                    <div class="single-sidebar-widget">
                        <div class="sidebar-other-page__wrapper">
                            <a href="{{route('front.company.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-009-storage" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Company</h3>
                            </a>
                            <a href="{{route('front.about.leadership.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-001-worker-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Leadership</h3>
                            </a>
                            <a href="{{route('front.about.milestones.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-042-monitor" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Milestones</h3>
                            </a>
                            <a href="{{route('front.about.infrastructure.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-032-gears-1" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Infrastructure</h3>
                            </a>
                            <a href="{{route('front.about.technology.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon flaticon-033-laser" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">RMC Process</h3>
                            </a>
                            <a href="{{route('front.latter_of_appraciation.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-bookmark" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Recognitions</h3>
                            </a>
                            <a href="{{route('front.client.page')}}" class="sidebar-other-page__single">
                                <div class="icon">
                                    <i class="glyph-icon tractor-network" style="font-size: 50px;"></i>
                                </div>
                                <h3 class="page-title">Clients</h3>
                            </a>
                            <a href="{{route('front.aboutFaq.page')}}" class="sidebar-other-page__single active">
                                <div class="icon">
                                    <i class="glyph-icon tractor-push-pin" style="font-size: 50px;color: #ffffff;"></i>
                                </div>
                                <h3 class="page-title" style="color: #ffffff;">FAQs</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
<!--====================  cta area ====================-->
<div class="cta-area section-space--inner--60 default-bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <!-- cta text -->
                <h2 class="cta-text text-center text-lg-left">Looking for quality construction partner?</h2>
            </div>
            <div class="col-lg-3 home_button text-center text-lg-right">
                <a href="{{route('front.select_calculation.page')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark" style="color: #ffffff;">RMC CALCULATOR</a>
            </div>
            <div class="col-lg-2 text-center text-lg-right">
                <a href="{{route('admin.getQuoteCalculator')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark" style="color: #ffffff;">ORDER NOW</a>
            </div>
        </div>
    </div>
</div>
<!--====================  End of cta area  ====================-->
@endsection