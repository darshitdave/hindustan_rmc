@extends('layouts.front.front')

@if($get_sub_project->category_id == 0)
    @section('title','Hindustan RMC | Residential Projects')
@elseif($get_sub_project->category_id == 1) 
    @section('title','Hindustan RMC | Commercial Projects')
@elseif($get_sub_project->category_id == 2) 
    @section('title','Hindustan RMC | Industrial Projects ')
@elseif($get_sub_project->category_id == 3) 
    @section('title','Hindustan RMC | Roads & Bridges Projects ')
@elseif($get_sub_project->category_id == 4) 
    @section('title','Hindustan RMC | Other Projects')
@endif

@section('content')
<!-- <div class="breadcrumb-area breadcrumb-area-bg bg-img section-space--inner--bottom--50 section-space--inner--top--120" data-bg="{{ asset('assets/front/img/backgrounds/20.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <h2 class="breadcrumb-page-title breadcrumb-page-title--style2">Sub Project</h2>
            </div>
            <div class="col-lg-12">
                <ul class="breadcrumb-page-list breadcrumb-page-list--style2 text-uppercase">
                    <li class="has-children"><a href="{{route('front.about_projects.page')}}">@if($get_sub_project->category_id == 0)Residential @elseif($get_sub_project->category_id == 1) Commercial @elseif($get_sub_project->category_id == 2) Industrial @elseif($get_sub_project->category_id == 3) Roads & Bridges @elseif($get_sub_project->category_id == 2) Others @endif</a></li>
                    <li>{{$get_sub_project->project_name}}</li>
                </ul>
            </div>
        </div>
    </div>
</div> -->
@if($get_sub_project->category_id == 1) 
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/commercial_project.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Commercial Projects</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="{{route('admin.projectList')}}">Projects</a></li>
                    <li>Commercial Projects</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@elseif($get_sub_project->category_id == 3)
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/bridge_other.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Roads & Bridges Projects</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="#">Projects</a></li>
                    <li>Roads & Bridges Projects</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@elseif($get_sub_project->category_id == 4)
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/bridge_other.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Other Projects</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="#">Projects</a></li>
                    <li>Other Projects</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@elseif($get_sub_project->category_id == 2)
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/Industrial_Project_banner_2.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Industrial Projects</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="#">Projects</a></li>
                    <li>Industrial Projects</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@elseif($get_sub_project->category_id == 0)
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/Residentional.jpg') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Residential Projects</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="#">Projects</a></li>
                    <li>Residential Projects</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endif
<!--====================  page content wrapper ====================-->
<div class="page-content-wrapper section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="case-study__page-content-wrapper">
                    <h3 class="case-study__page-content-title">{{$get_sub_project->project_name}}</h3>
                    <p>{{$get_sub_project->project_description}}</p>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="case-study-table">
                    @if(isset($get_sub_project->construction_date) && $get_sub_project->construction_date != '')
                    <div class="case-study-table__row">
                        <div class="case-study-table__icon">
                            <i class="ion-calendar"></i>
                        </div>
                        <?php $date = date('M Y',strtotime($get_sub_project->construction_date)); ?>
                        <div class="case-study-table__details"> <span class="highlight">Construction End Date:</span> {{$date}}</div>
                    </div>
                    @endif
                    @if(isset($get_sub_project->builder_name) && $get_sub_project->builder_name != '')
                    <div class="case-study-table__row">
                        <div class="case-study-table__icon">
                            <i class="ion-link"></i>
                        </div>
                        <div class="case-study-table__details"> <span class="highlight">Builder Name:</span> {{$get_sub_project->builder_name}}</div>
                    </div>
                    @endif
                    @if(isset($get_sub_project->location) && $get_sub_project->location != '')
                    <div class="case-study-table__row">
                        <div class="case-study-table__icon">
                            <i class="ion-ios-location"></i>
                        </div>
                        <div class="case-study-table__details"> <span class="highlight">Location:</span> {{$get_sub_project->location}}</div>
                    </div>
                    @endif
                    @if(isset($get_sub_project->concrete_consume) && $get_sub_project->concrete_consume != '')
                    <div class="case-study-table__row">
                        <div class="case-study-table__icon">
                            <i class="ion-cash"></i>
                        </div>
                        <div class="case-study-table__details"> <span class="highlight">Concrete Consume:</span> {{$get_sub_project->concrete_consume}}</div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="case-study__image-gallery-wrapper section-space--top--80">
                    <div class="row image-popup">
                    @if($get_sub_project->gallery_one != '')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="case-study__single-gallery-image">
                                <img src="{{asset('uploads/project_gallary/'.$get_sub_project->gallery_one)}}" class="img-fluid" alt="" style="max-width:80% !important;height: 251px!important;">
                            </div>
                        </div>
                    @endif
                    @if($get_sub_project->gallery_two != '')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="case-study__single-gallery-image">
                                <img src="{{asset('uploads/project_gallary/'.$get_sub_project->gallery_two)}}" class="img-fluid" alt="" style="max-width:80% !important;height: 251px!important;">
                            </div>
                        </div>
                    @endif
                    @if($get_sub_project->gallery_three != '')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="case-study__single-gallery-image">
                                <img src="{{asset('uploads/project_gallary/'.$get_sub_project->gallery_three)}}" class="img-fluid" alt="" style="max-width:80% !important;height: 251px!important;">
                            </div>
                        </div>
                    @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of page content wrapper  ====================-->
@endsection