@extends('layouts.front.front')
@section('title','Hindustan RMC | Projects')
@section('content')
<div class="breadcrumb-area breadcrumb-area-bg section-space--inner--80 bg-img" data-bg="{{ asset('assets/front/img/backgrounds/project_banner.png') }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h2 class="breadcrumb-page-title">Projects</h2>
            </div>
            <div class="col-sm-6">
                <ul class="breadcrumb-page-list text-uppercase">
                    <li class="has-children"><a href="{{route('front.home.page')}}">Home</a></li>
                    <li>Projects</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--====================  page content wrapper ====================-->
<!--====================  case study filter area ====================-->
@if(isset($get_projects) && count($get_projects) >= 1)
<div class="page-content-wrapper section-space--inner--120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- filter title -->
                <ul class="project-filter-menu section-space--bottom--80">
                    <li class="category" data-filter=".0" data-id="0">Residential</li>
                    <li class="category" data-filter=".1" data-id="1">Commercial</li>
                    <li class="category" data-filter=".2" data-id="2">Industrial</li>
                    <li class="category" data-filter=".3" data-id="3">Roads & Bridges</li>
                    <li class="category" data-filter=".4" data-id="4">Others</li>
                </ul>
                <!-- filter content -->
                <div class="project-filter-content">
                    <div class="row project-isotope">
                        @foreach($get_projects as $gk => $gv)
                        <a href="{{ route('front.about_sub_project.page',$gv->slug) }}" class="custom-card">
                            <div class="col-lg-4 col-md-6 {{$gv->category_id}}">
                                <!-- single project -->
                                <div class="single-case-study-project">
                                    <div class="single-case-study-project__image">
                                        <img src="{{asset('uploads/project_photo/'.$gv->project_profile)}}" class="img-fluid" alt="" style="height: 231px;">
                                    </div>        

                                    <div class="single-case-study-project__content">
                                        <h3 class="title"><a href="{{ route('front.about_sub_project.page',$gv->slug) }}">{{$gv->project_name}}</a></h3>
                                        <a href="{{ route('front.about_sub_project.page',$gv->slug) }}" class="category">{{$gv->builder_name}}</a>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!--====================  End of case study filter area  ====================-->
<!--====================  cta area ====================-->
<div class="cta-area section-space--inner--60 default-bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <!-- cta text -->
                <h2 class="cta-text  text-center text-lg-left">Looking for quality construction partner?</h2>
            </div>
            <div class="col-lg-3 home_button text-center text-lg-right">
                <a href="{{route('front.select_calculation.page')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark">RMC CALCULATOR</a>
            </div>
            <div class="col-lg-2 text-center text-lg-right">
                <a href="{{route('admin.getQuoteCalculator')}}" class="ht-btn ht-btn--transparent ht-btn--transparent--alt-dark">ORDER NOW</a>
            </div>
        </div>
    </div>
</div>
<!--====================  End of cta area  ====================-->
@endsection  
@section('js')
<script>
$(document).ready(function() {

    $('[data-id="0"]').click();

});
</script>
@endsection