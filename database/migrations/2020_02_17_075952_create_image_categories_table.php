<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category_name');
            $table->integer('priority')->nullable();
            $table->string('category_banner');
            $table->tinyInteger('is_priority')->default(1); //1 for active 0 deactive
            $table->tinyInteger('is_active')->default(0); //1 for active 0 deactive
            $table->tinyInteger('is_delete')->default(0);//0 for not delete 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_categories');
    }
}
