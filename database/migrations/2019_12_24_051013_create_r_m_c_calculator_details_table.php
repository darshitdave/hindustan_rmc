<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRMCCalculatorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_m_c_calculator_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_name');
            $table->integer('mobile_no');
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->integer('product_id');
            $table->integer('grade_id');
            $table->string('cement');
            $table->string('fly_ash');
            $table->string('fly_ash_rate');
            $table->integer('placement_type');
            $table->string('without_gst_price');
            $table->string('total_calculation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_m_c_calculator_details');
    }
}
