<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category_id');
            $table->string('project_name');
            $table->integer('priority');
            $table->string('project_profile');
            $table->string('project_description');
            $table->string('builder_name');
            $table->string('location');
            $table->string('construction_date');
            $table->string('concrete_consume');
            $table->tinyInteger('is_active')->default(0); //1 for active 0 deactive
            $table->tinyInteger('is_delete')->default(0);//0 for not delete
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
