<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('plant_name');
            $table->integer('priority')->nullable();
            $table->string('plant_image');
            $table->text('address');
            $table->string('telephone');
            $table->string('email_id');
            $table->string('plant_incharge');
            $table->string('quality_head');
            $table->string('iso_certificate')->nullable();
            $table->string('year_of_establish')->nullable();
            $table->string('track_record')->nullable();
            $table->string('government_approval')->nullable();
            $table->string('plant_make')->nullable();
            $table->string('plant_capacity')->nullable();
            $table->string('maximum_supply')->nullable();
            $table->string('electric_supply_condition')->nullable();
            $table->string('water_supply_condition')->nullable();
            $table->string('no_trans_mixer')->nullable();
            $table->string('no_boom_placer')->nullable();
            $table->string('no_concrete_pump')->nullable();
            $table->string('pipeline_length')->nullable();
            $table->tinyInteger('is_priority')->default(1); //1 for active 0 deactive
            $table->tinyInteger('is_active')->default(0); //1 for active 0 deactive
            $table->tinyInteger('is_delete')->default(0);//0 for not delete 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plants');
    }
}
