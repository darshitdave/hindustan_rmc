<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('cement_rate',10,2)->default(0.00);
            $table->double('cement_kg',10,2)->default(0.00);
            $table->double('fly_wash_rate',10,2)->default(0.00);
            $table->double('flyash_kg',10,2)->default(0.00);
            $table->double('gst',10,2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_settings');
    }
}
