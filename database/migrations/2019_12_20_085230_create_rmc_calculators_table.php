<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmcCalculatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rmc_calculators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('total_slab_thikness',10,2)->default(0.00);
            $table->double('slab_width',10,2)->default(0.00);
            $table->double('total_slab_width',10,2)->default(0.00);
            $table->double('total_slab_length',10,2)->default(0.00);
            $table->double('footing_total_thikness',10,2)->default(0.00);
            $table->double('footing_total_width',10,2)->default(0.00);
            $table->double('footing_total_length',10,2)->default(0.00);
            $table->double('base_total_length',10,2)->default(0.00);
            $table->double('base_total_width',10,2)->default(0.00);
            $table->double('base_total_height',10,2)->default(0.00);
            $table->double('vertical_total_slant',10,2)->default(0.00);
            $table->double('pillar_total_length',10,2)->default(0.00);
            $table->double('pillar_total_width',10,2)->default(0.00);
            $table->double('beam_total_thikness',10,2)->default(0.00);
            $table->double('beam_total_width',10,2)->default(0.00);
            $table->double('beam_total_length',10,2)->default(0.00);
            $table->double('column_total_thikness',10,2)->default(0.00);
            $table->double('column_total_width',10,2)->default(0.00);
            $table->double('column_total_length',10,2)->default(0.00);
            $table->double('circle_total_diametre',10,2)->default(0.00);
            $table->double('circle_total_height',10,2)->default(0.00);
            $table->double('m_cube',10,2)->default(0.00);
            $table->double('feet_cube',10,2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rmc_calculators');
    }
}
