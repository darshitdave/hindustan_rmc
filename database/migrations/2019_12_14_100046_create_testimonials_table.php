<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('site_name');
            $table->string('image');
            $table->integer('priority')->nullable();
            $table->tinyInteger('is_priority')->default(1); //1 for active 0 deactive
            $table->tinyInteger('is_active')->default(1); //1 for active 0 deactive
            $table->tinyInteger('is_delete')->default(0);//0 for not delete 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonials');
    }
}
