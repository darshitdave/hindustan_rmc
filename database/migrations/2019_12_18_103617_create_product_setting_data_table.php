<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSettingDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_setting_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_setting_id');
            $table->integer('sub_product_id');
            $table->integer('product_type_id');
            $table->double('basic_rate',10,2)->default(0.00);
            $table->double('minimum_qty',10,2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_setting_data');
    }
}
