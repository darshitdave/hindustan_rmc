<?php
namespace App\Http\Controllers;

use Mail;
use preg_replace;
use DateInterval;
use DateTime;
use DatePeriod;
use App\Model\User;
use App\Model\UserToken;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class GlobalController extends Controller
{   
    //Upload Image 
    public function uploadImage($image,$path){

       	$imagedata = $image;
        $destinationPath = 'uploads/'.$path;
        $thumbpath = $destinationPath."_thumb/";
        $extension = $imagedata->getClientOriginalExtension(); 
        $fileName = rand(11111,99999).time().'.'.$extension;

        $thumbpath = public_path($thumbpath).$fileName;
        $thumb = Image::make($imagedata)->resize(470, 298);
        $thumb->save($thumbpath);

        $destinationPath =  public_path($destinationPath)."/".$fileName;
        $normal = Image::make($imagedata);
        $normal->save($destinationPath);

        return $fileName;
    }

    //Upload Image 
    public function uploadImageData($image,$path){

       $imagedata = $image;
       
       $destinationPath = 'uploads/'.$path; 

       $extension = $imagedata->getClientOriginalExtension(); 

       $fileName = rand(11111,99999).'.'.$extension;

       $imagedata->move($destinationPath, $fileName);
        
       return $fileName;
    }

    public function sendMail($templete,$mail_message,$sub,$email = null,$mail_type){
        
       $maildata = array('bodymessage' => $mail_message);

        //customer
        Mail::send('mail.'.$templete, $maildata, function($msg) use ($email,$sub) {
           $msg->to($email)->subject($sub);
           $msg->from('inquiry@hindustanrmc.com','Hindustan RMC');
        });

        //admin
        $emails = ['inquiry@hindustanrmc.com','darshit.finlark@gmail.com'];
        if($mail_type == 0){  
          $templete1 = 'admin_null_temp';
          $sub1 = 'New General Inquiry <> hindustanrmc.com';
        }else{
          $templete1 = 'admin_rmc_inquiries';
          $sub1 = 'New RMC Inquiry <> hindustanrmc.com';
        }
        foreach ($emails as $ek => $ev) {
          Mail::send('mail.'.$templete1, $maildata, function($msg) use ($ev,$sub1) {
             $msg->to($ev, 'Site Admin');
             $msg->subject($sub1);
             $msg->from('inquiry@hindustanrmc.com','Hindustan RMC');
          }); 
        }
    }
    
    public static function slugify($text)
    {
      // replace non letter or digits by -
      $text = preg_replace('~[^\pL\d]+~u', '-', $text);

      // transliterate
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

      // remove unwanted characters
      $text = preg_replace('~[^-\w]+~', '', $text);

      // trim
      $text = trim($text, '-');

      // remove duplicate -
      $text = preg_replace('~-+~', '-', $text);

      // lowercase
      $text = strtolower($text);

      if (empty($text)) {
        return 'n-a';
      }

      return $text;
    }

    
}
