<?php

namespace App\Http\Controllers;

use App\Model\Blog;
use App\Model\Product;
use App\Model\Project;
use App\Model\Plant;
use App\Model\Client;
use App\Model\Gallery;
use App\Model\ShapeType;
use App\Model\BaseSetting;
use App\Model\Testimonial;
use App\Model\ProductType;
use App\Model\RmcCalculator;
use App\Model\NewsLatter;
use Illuminate\Http\Request;
use App\Model\ProductSetting;
use App\Model\TestimonialOther;
use App\Model\ImageCategory;
use App\Model\ProductSettingData;
use App\Model\ProjectProfilePhoto;
use App\Model\RMCCalculatorDetail;
use App\Model\GenralContactDetail;
use App\Model\PlantGalleryPhotos;
use App\Model\CategoryWiseImage;
use App\Http\Controllers\GlobalController;

class HomeController extends GlobalController
{
    public function indexPage(){
        $get_testimonial_other = TestimonialOther::where('is_delete',0)->where('is_active',1)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

        $get_blog = Blog::where('is_delete',0)->where('is_active',1)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->limit(3)->get();
        
        $get_plant = Plant::where('is_active',1)->where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

        $latLong = array();

        if(!is_null($get_plant)){
            foreach($get_plant as $gk => $gv){
                $data_array = array();
                $data_array[] = $gv->plant_name;
                $data_array[] = $gv->latitude;
                $data_array[] = $gv->longitude;
                $data_array[] = $gv->id;
                $data_array[] = '/plants-detail/'.$gv->slug;
                $latLong[] = $data_array;
            }
        }

        // echo "<pre>";
        // print_r($latLong);
        // exit;
        
        $get_client = Client::where('is_delete',0)->where('is_active',1)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

    	return view('front.another_home',compact('get_testimonial_other','get_blog','get_plant','latLong','get_client'));
    }

    /*public function indexAnotherPage(){

        $get_testimonial_other = TestimonialOther::where('is_delete',0)->where('is_active',1)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();
        $get_blog = Blog::where('is_delete',0)->where('is_active',1)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->limit(3)->get();
        
        $get_plant = Plant::where('is_active',1)->where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

        $latLong = array();

        if(!is_null($get_plant)){
            foreach($get_plant as $gk => $gv){
                $data_array = array();
                $data_array[] = $gv->plant_name;
                $data_array[] = '/plants-detail'.$gv->slug;
                $data_array[] = $gv->latitude;
                $data_array[] = $gv->longitude;
                $data_array[] = $gv->id;
                $latLong[] = $data_array;
            }
        }
       
        return view('front.another_home',compact('get_testimonial_other','get_blog','get_plant','latLong'));
    }*/

    public function stonePlantPage(){

        return view('front.stone_crushing.stone_crushing');
    }

    //about pages
    public function aboutUs(){
    	return view('front.about.about');	
    }

    public function aboutTechonology(){
    	return view('front.about.technology');		
    }

    public function aboutInfrastructure(){
    	return view('front.about.infrastructure');			
    }

    public function aboutLifeAtHindustan(){

        $gallery = Gallery::all();

    	return view('front.about.life_at_hindustan',compact('gallery'));	
    }

    public function aboutMilestones(){
    	return view('front.about.milestones');		
    }

    public function aboutLeadership(){
        
        return view('front.about.leadership');         
    }

    //product page
    public function aboutProduct(){
    	return view('front.product.products');		
    }

    public function aboutSubProduct($id){
        $type = $id;
        
    	return view('front.product.sub_products',compact('type'));			
    }

    //projects
    public function aboutProjects(Request $request){

        $get_projects = Project::where('is_active',1)->where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();    
        
    	return view('front.project.projects',compact('get_projects'));			
    }

    public function aboutSubProject($slug){
        $get_sub_project = Project::where('slug',$slug)->first();
        $get_photo = ProjectProfilePhoto::where('project_id',$get_sub_project->id)->get();
        
    	return view('front.project.sub_projects',compact('get_sub_project','get_photo'));			
    }

    //plants
    public function locationPlants(){

        $get_plant = Plant::where('is_active',1)->where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

        $latLong = array();

        if(!is_null($get_plant)){
            foreach($get_plant as $gk => $gv){
                $data_array = array();
                $data_array[] = $gv->plant_name;
                $data_array[] = $gv->latitude;
                $data_array[] = $gv->longitude;
                $data_array[] = $gv->id;
                $data_array[] = '/plants-detail/'.$gv->slug;
                $latLong[] = $data_array;
            }
        }
    

        return view('front.plant.all_plants',compact('get_plant','latLong'));              
    }

    public function aboutPlants($slug){
        $plant_detail = Plant::where('slug',$slug)->first();
        
        $get_plant = Plant::where('id','!=',$plant_detail->id)->where('is_active',1)->where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

        $get_plant_image = PlantGalleryPhotos::where('plant_id',$plant_detail->id)->get();

    	return view('front.plant.plants',compact('plant_detail','get_plant','get_plant_image'));				
    }

    //why us
    public function plantComparison(){
    	return view('front.why_us.plant_compare');		
    }

    public function ourUps(){
    	return view('front.why_us.our_ups');			
    }

    //contact us
    public function contactUs(){

        $get_plant = Plant::where('is_active',1)->where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

        $latLong = array();

        if(!is_null($get_plant)){
            foreach($get_plant as $gk => $gv){
                $data_array = array();
                $data_array[] = $gv->plant_name;
                $data_array[] = $gv->latitude;
                $data_array[] = $gv->longitude;
                $data_array[] = $gv->id;
                $data_array[] = '/plants-detail/'.$gv->slug;
                $latLong[] = $data_array;
            }
        }

    	return view('front.contact.contact_us',compact('latLong'));				
    }

    //careers
    public function comCareer(){
        $gallery = Gallery::all();
        $category = ImageCategory::with(['category'])->where('is_active',1)->where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();
        
    	return view('front.careers.career',compact('gallery','category'));					
    }

    public function comCareerImage(Request $request){

        
        $category_name = ImageCategory::where('category_slug',$request->id)->first();
        
        $category_images = CategoryWiseImage::where('category_id',$category_name->id)->where('is_active',1)->where('is_delete',0)->orderBy('created_at','ASC')->get();   
        
        return view('front.careers.career_images',compact('category_images','category_name'));                     
    }

    //faq
    public function aboutFaq(){
    	return view('front.faq.faq_page');						
    }

    //blog 
    public function blogList(){
        $get_blog = Blog::where('is_delete',0)->where('is_active',1)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

    	return view('front.blog.blog_list',compact('get_blog'));							
    }

    public function blogDetail($slug){
        
        $get_blog = Blog::where('blog_slug',$slug)->first();
        
    	return view('front.blog.blog_details',compact('get_blog'));								
    }

    //client
    public function client(){

        $get_client = Client::where('is_delete',0)->where('is_active',1)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

    	return view('front.client.clients',compact('get_client'));									
    }

    //service detail page
    public function serviceDetails(){
      
        return view('front.service.service');                                       
    }

    public function latterOfAppraciation(){
        
        $get_testimonial_certificate = Testimonial::where('is_active',1)->where('is_delete',0)->where('category_type','0')->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();
       
        $get_testimonial_recognitation = Testimonial::where('is_active',1)->where('is_delete',0)->where('category_type','1')->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

        $get_testimonial_other = TestimonialOther::where('is_delete',0)->where('is_active',1)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

        return view('front.appraciation.latter_of_appraciation',compact('get_testimonial_certificate','get_testimonial_recognitation','get_testimonial_other'));   
    }

    public function selectCalculation(){
        $get_shape_types = ShapeType::get();

        return view('front.calculator.calculator_type',compact('get_shape_types'));   
    }

    public function insertRmcCalculator(Request $request){

        $rmc_calculator = new RmcCalculator;
        $rmc_calculator->total_slab_thikness = $request->total_slab_thikness;
        $rmc_calculator->slab_width = $request->slab_width;
        $rmc_calculator->total_slab_width = $request->total_slab_width;
        $rmc_calculator->total_slab_length = $request->total_slab_length;
        $rmc_calculator->footing_total_thikness = $request->footing_total_thikness;
        $rmc_calculator->footing_total_width = $request->footing_total_width;
        $rmc_calculator->footing_total_length = $request->footing_total_length;
        $rmc_calculator->base_total_length = $request->base_total_length;
        $rmc_calculator->base_total_width = $request->base_total_width;
        $rmc_calculator->base_total_height = $request->base_total_height;
        $rmc_calculator->vertical_total_slant = $request->vertical_total_slant;
        $rmc_calculator->pillar_total_length = $request->pillar_total_length;
        $rmc_calculator->pillar_total_width = $request->pillar_total_width;
        $rmc_calculator->beam_total_thikness = $request->beam_total_thikness;
        $rmc_calculator->beam_total_width = $request->beam_total_width;
        $rmc_calculator->beam_total_length = $request->beam_total_length;
        $rmc_calculator->column_total_thikness = $request->column_total_thikness;
        $rmc_calculator->column_total_width = $request->column_total_width;
        $rmc_calculator->column_total_length = $request->column_total_length;
        $rmc_calculator->circle_total_diametre = $request->circle_total_diametre;
        $rmc_calculator->circle_total_height = $request->circle_total_height;
        $rmc_calculator->m_cube = $request->m_cube;
        $rmc_calculator->feet_cube = $request->feet_cube;
        $rmc_calculator->save();


        $m_cube = $request->m_cube;

        $get_product = Product::get();
        $base_setting = BaseSetting::orderBy('id', 'desc')->first();
        
        return view('front.calculator.get_quote_calculator',compact('m_cube','get_product','base_setting'));   
    }

    public function getSubProduct(Request $request){
        $get_product_type = ProductSetting::with(['product_type','product_data' => function($q) { $q->with(['product_name']); }])->where('is_delete',0)->get();

        if(!empty($get_product_type)){
            echo '<option selected="selected">Select Concrete Grade Type</option>';
            foreach($get_product_type as $gck => $gcv){
                
                foreach($gcv['product_data'] as $pk => $pv){
                  
                    if($pv != ''){
                        
                        echo '<option value="'.$pv['product_type_id'].'">'.$pv['product_name']['product_types'].'</option>';
                    }
                    
                }
            }
        }else{
            echo '<option value=" ">Action Not Available</option>';
        }

    }

    public function getSubProductQty(Request $request){

        $get_product_type = ProductSettingData::where('sub_product_id',$request->product)->where('product_type_id',$request->product_type)->first();

        $data['minimum_qty'] = $get_product_type->minimum_qty;
        $data['cement_rate'] = $get_product_type->basic_rate;

        return $data;
    }

    public function getQuoteCalculator(){

        $get_product = Product::get();
        $base_setting = BaseSetting::orderBy('id', 'desc')->first();
        
        return view('front.calculator.get_quote_calculator',compact('get_product','base_setting'));
    }

    public function insertQuoteValues(){

        return redirect(route('admin.getQuoteCalculator'))->with('messages', [
            [
                'type' => 'Quote',
                'title' => 'Quote',
                'message' => 'Quote Mailed Successfully!',
            ],
        ]);    
    }

    public function insertRmcDetail(Request $request){
        
        $rmc_calculator_detail = new RMCCalculatorDetail;
        $rmc_calculator_detail->user_name = $request->name;
        if(isset($request->email)){ 
            $rmc_calculator_detail->email = $request->email;
        }
        if(isset($request->address)){ 
            $rmc_calculator_detail->address = $request->address;
        }
        $rmc_calculator_detail->mobile_no = $request->mobile_no;

        $rmc_calculator_detail->product_id = $request->product_id;
        $rmc_calculator_detail->grade_id = $request->grade_id;
        if(isset($request->cement_with_out_fly_ash)){
            $rmc_calculator_detail->cement = $request->cement_with_out_fly_ash;
        }else{
            $rmc_calculator_detail->cement = $request->cement_with_fly_ash;
        }

        $rmc_calculator_detail->fly_ash = $request->fly_ash;
        $rmc_calculator_detail->fly_ash_rate = $request->fly_ash_rate;
        $rmc_calculator_detail->without_gst_price = $request->with_out_gst;
        $rmc_calculator_detail->with_gst_price = $request->with_gst;
        $rmc_calculator_detail->placement_type = $request->placement_type;
        $rmc_calculator_detail->total_calculation =  $request->m_cube;  

        $rmc_calculator_detail->save();

        $templete = 'rmc_inquiries';
        $email = $request->email;

        $sub = 'RMC Quotation <> Hindustan RMC';    

        $mail_message['name'] = $request->name;
        if(isset($request->email)){
            $mail_message['email'] = $request->email;
        }
        $mail_message['phone_number'] = $request->mobile_no;
        if(isset($request->address)){ 
            $mail_message['address'] = $request->address;   
        }
        $product_name = Product::where('id',$request->product_id)->first();
        $mail_message['product_name'] = $product_name->sub_products; 

        $grade_name = ProductType::where('id',$request->grade_id)->first();
        $mail_message['grade'] = $grade_name->product_types;   

        if(isset($request->cement_with_out_fly_ash)){
            $mail_message['cement_quantity'] = $request->cement_with_out_fly_ash;
        }else{
            $mail_message['cement_quantity'] = $request->cement_with_fly_ash;
        }
        $mail_message['fly_ash_quantity'] = $request->fly_ash;
        if($request->placement_type == 1){
            $mail_message['placement_type'] = 'Dumping';
        }else{
            $mail_message['placement_type'] = 'Pumping';
        }
        $mail_message['price_without_gst'] = $request->with_out_gst;
        
        $mail_message['price_with_gst_price'] = $request->with_gst;
        $mail_message['price_with_gst'] = $request->m_cube;
        $mail_type = '1';
        $this->sendMail($templete,$mail_message,$sub,$email,$mail_type);

        return redirect(route('admin.getQuoteCalculator'))->with('messages', [
            [
                'type' => 'Quote',
                'title' => 'Quote',
                'message' => 'Thank You For Visiting Us!',
            ],
        ]);
    }

    public function insertContactDetail(Request $request){
        
        $genral_contact_us = new GenralContactDetail;
        $genral_contact_us->name = $request->con_name;
        $genral_contact_us->email = $request->con_email;
        $genral_contact_us->phone_number = $request->con_phone;
        $genral_contact_us->message = $request->con_message;
        $genral_contact_us->save();

        $templete = 'null_temp';
        $email = $request->con_email;

        $sub = 'Message Received <> Hindustan RMC';    

        $mail_message['name'] = $request->con_name;
        $mail_message['email'] = $request->con_email;
        $mail_message['phone_number'] = $request->con_phone;
        $mail_message['message'] = $request->con_message;

        $mail_type = 0;
        $this->sendMail($templete,$mail_message,$sub,$email,$mail_type);

        return redirect(route('front.home.page'))->with('messages', [
            [
                'type' => 'success',
                'title' => 'Contact Us',
                'message' => 'Thank you for reaching out to us.We will soon get in touch!',
            ],
        ]); 
        return redirect()->route('front.home.page')->with(['message'=>'Details Successfully Send Thank You!','alert-type' => 'success']);
    }

    public function insertNewsLetterEmail(Request $request){
        
        $add_email = new NewsLatter;
        $add_email->email_id = $request->user_email;
        $add_email->save();

        if($add_email){
            return "true";
        }else{
            return "false";
        }
    }
}
