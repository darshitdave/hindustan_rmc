<?php

namespace App\Http\Controllers\Admin;

use App\Model\Project;
use Illuminate\Http\Request;
use App\Model\ProjectProfilePhoto;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;

class ProjectController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function projectList(){

    	$get_projects = Project::where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

    	return view('admin.project.project_list',compact('get_projects'));
    }

    public function addProject(){

    	return view('admin.project.add_project');	
    }

    public function insertProject(Request $request){
        
        $project_detail = new Project;
        $project_detail->category_id = $request->category_id;
        $project_detail->project_name = $request->project_name;
        $project_detail->slug = $this->slugify($request->project_name);
        $project_detail->priority = $request->priority;
        if(isset($request->project_profile_photo)){
          $fileName = $this->uploadImageData($request->project_profile_photo,'project_photo');
          $project_detail->project_profile = $fileName;         
        }
        $project_detail->project_description = $request->project_description;
        $project_detail->builder_name = $request->builder_name;
        $project_detail->city_name = $request->city_name;
        $project_detail->state_name = $request->state_name;
        $project_detail->country = $request->country;
        $project_detail->location = $request->location;
        $project_detail->latitude = $request->latitude;
        $project_detail->longitude = $request->longitude;
        $project_detail->construction_date = $request->construction_date;
        $project_detail->concrete_consume = $request->concrete_consume;
        if(isset($request->priority) && $request->priority != ''){
            $project_detail->is_priority = 0;
        }else{
            $project_detail->is_priority = 1;
        }
        if(isset($request->project_gallary_one)){
            $fileNameOne = $this->uploadImageData($request->project_gallary_one,'project_gallary');
            $project_detail->gallery_one = $fileNameOne;   
        }
        if(isset($request->project_gallary_two)){
            $fileNameTwo = $this->uploadImageData($request->project_gallary_two,'project_gallary');
            $project_detail->gallery_two = $fileNameTwo;   
        }
        if(isset($request->project_gallary_three)){
            $fileNameThree = $this->uploadImageData($request->project_gallary_three,'project_gallary');
            $project_detail->gallery_three = $fileNameThree;   
        }
        $project_detail->save();

        // if(!is_null($request->project_gallary)){
            
        //     foreach ($request->project_gallary as $pk => $pv) {
        //         $project_photo = new ProjectProfilePhoto;
        //         $project_photo->project_id = $project_detail->id;
        //         if(isset($pv)){
        //           $fileName = $this->uploadImageData($pv,'project_gallary');
        //           $project_photo->project_profile_photo = $fileName;         
        //         }
        //         $project_photo->save();
        //     }    
        // }
        
        if($request->add_new){

            return redirect(route('admin.addProject'))->with('messages', [
                  [
                      'type' => 'success', 
                      'title' => 'Project',
                      'message' => 'Project Detail Successfully Added!',
                  ],
            ]);

        } else {

            return redirect(route('admin.projectList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Project',
                      'message' => 'Project Detail Successfully Added!',
                  ],
            ]); 
        }

    }

    public function editProject(Request $request){

    	$get_project = Project::where('id',$request->id)->first();
        $get_project_photo = ProjectProfilePhoto::where('project_id',$request->id)->get();
       
    	return view('admin.project.edit_project',compact('get_project','get_project_photo'));		
    }

    public function updateProject(Request $request){

        $project_detail = Project::findOrFail($request->id);
        $project_detail->category_id = $request->category_id;
        $project_detail->project_name = $request->project_name;
        $project_detail->slug = $this->slugify($request->project_name);
        $project_detail->priority = $request->priority;
        if(isset($request->project_profile_photo)){
          $fileName = $this->uploadImageData($request->project_profile_photo,'project_photo');
          $project_detail->project_profile = $fileName;         
        }
        $project_detail->project_description = $request->project_description;
        $project_detail->builder_name = $request->builder_name;
        $project_detail->city_name = $request->city_name;
        $project_detail->state_name = $request->state_name;
        $project_detail->country = $request->country;
        $project_detail->location = $request->location;
        $project_detail->latitude = $request->latitude;
        $project_detail->longitude = $request->longitude;
        $project_detail->construction_date = $request->construction_date;
        $project_detail->concrete_consume = $request->concrete_consume;
        if(isset($request->priority) && $request->priority != ''){
            $project_detail->is_priority = 0;
        }else{
            $project_detail->is_priority = 1;
        }
        if(isset($request->project_gallary_one)){
            $fileNameOne = $this->uploadImageData($request->project_gallary_one,'project_gallary');
            $project_detail->gallery_one = $fileNameOne;   
        }
        if(isset($request->project_gallary_two)){
            $fileNameTwo = $this->uploadImageData($request->project_gallary_two,'project_gallary');
            $project_detail->gallery_two = $fileNameTwo;   
        }
        if(isset($request->project_gallary_three)){
            $fileNameThree = $this->uploadImageData($request->project_gallary_three,'project_gallary');
            $project_detail->gallery_three = $fileNameThree;   
        }
        $project_detail->save();

            // if(!is_null($request->project_gallary)){
            //     $project_photo = ProjectProfilePhoto::where('project_id',$request->id)->delete();
            //     foreach ($request->project_gallary as $pk => $pv) {
            //         $project_photo = new ProjectProfilePhoto;
            //         $project_photo->project_id = $project_detail->id;
            //         if(isset($pv)){
            //           $fileName = $this->uploadImageData($pv,'project_gallary');
            //           $project_photo->project_profile_photo = $fileName;         
            //         }
            //         $project_photo->save();
            //     }
            // }

        return redirect(route('admin.projectList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Project',
                  'message' => 'Project Detail Successfully Updated!',
              ],
        ]); 

    }

    public function removeProject(Request $request){

    	$remove_project = Project::where('id',$request->id)->update(['is_delete' => 1]);

    	if($remove_project){

            return redirect(route('admin.projectList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Project',
                      'message' => 'Project Detail Successfully Deleted!',
                  ],
            ]); 
        }
    }	

    //change doctor status
    public function changeProjectStatus(Request $request){

        $project_active = Project::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($project_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function checkCategoryPriority(Request $request){

        if(isset($request->id)){
            $category_priority = Project::where('id','!=',$request->id)->where('category_id',$request->category)->where('priority',$request->priority)->where('is_delete',0)->first();
        }else{
            $category_priority = Project::where('priority',$request->priority)->where('category_id',$request->category)->where('is_delete',0)->first();
        }

        if(!is_null($category_priority)){
            return 'true';
        } else {
            return 'false';
        }
    }
}
