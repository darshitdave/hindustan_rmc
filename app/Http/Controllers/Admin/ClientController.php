<?php

namespace App\Http\Controllers\Admin;

use App\Model\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;

class ClientController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function clientList(){

    	$get_client = Client::where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();
      
    	return view('admin.client.client_list',compact('get_client'));
    }

    public function addClient(){

    	return view('admin.client.add_client');	
    }

    public function saveClient(Request $request){
    	
    	$client_details = new Client;
    	$client_details->client_name = $request->client_name;
    	$client_details->priority = $request->priority;
    	if(isset($request->company_logo)){
          $fileName = $this->uploadImageData($request->company_logo,'company_logo');
          $client_details->company_logo = $fileName;         
        }
        
        $client_details->save();
        if($request->add_new){

            return redirect(route('admin.addClient'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Client',
                    'message' => 'Client Detail Successfully Added!',
                ],
            ]); 

        }else{
        
          return redirect(route('admin.clientList'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Client',
                    'message' => 'Client Detail Successfully Added!',
                ],
          ]); 
        }
    }

    public function editClient(Request $request){

    	$get_client = Client::where('id',$request->id)->first();
        
    	return view('admin.client.edit_client',compact('get_client'));
    }

    public function saveEditedClient(Request $request){

    	$client_details = Client::findOrFail($request->id);
    	$client_details->client_name = $request->client_name;
    	$client_details->priority = $request->priority;
    	if(isset($request->company_logo)){
          $fileName = $this->uploadImageData($request->company_logo,'company_logo');
          $client_details->company_logo = $fileName;         
        }
        
        $client_details->save();

        if($client_details){

            return redirect(route('admin.clientList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Client',
                      'message' => 'Client Detail Successfully Update!',
                  ],
            ]); 
        }

    }

    public function deleteClient(Request $request){

    	$remove_client = Client::where('id',$request->id)->update(['is_delete' => 1]);

    	if($remove_client){

            return redirect(route('admin.clientList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Client',
                      'message' => 'Client Detail Successfully Deleted!',
                  ],
            ]); 
        }
    }

    public function changeClientStatus(Request $request){

    	$client_active = Client::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($client_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function ClientPriority(Request $request){
    
        if(isset($request->id)){

            $client_priority = Client::where('id','!=',$request->id)->where('priority',$request->priority)->where('is_delete',0)->first();
        }else{
        	
            $client_priority = Client::where('priority',$request->priority)->where('is_delete',0)->first();
        }

        if(!is_null($client_priority)){
            return 'true';
        } else {
            return 'false';
        }
    }
}
