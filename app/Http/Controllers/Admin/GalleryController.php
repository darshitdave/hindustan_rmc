<?php

namespace App\Http\Controllers\Admin;

use App\Model\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;

class GalleryController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function galleryList(){

    	$gallery = Gallery::all();

    	return view('admin.gallery.gallery',compact('gallery'));
    }

    public function addGallery(){

    	return view('admin.gallery.add_gallery');
    }

    public function saveGallery(Request $request){

    	$gallery = new Gallery;
    	if(isset($request->gallery_image)){
	        $fileName = $this->uploadImageData($request->gallery_image,'gallery_image');
          	$gallery->image = $fileName;         
        }
        $gallery->save();

    	if($request->add_new){

            return redirect(route('admin.addProject'))->with('messages', [
                  [
                      'type' => 'success', 
                      'title' => 'Project',
                      'message' => 'Gallery Image Successfully Added!',
                  ],
            ]);

        } else {

            return redirect(route('admin.galleryList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Gallery',
                      'message' => 'Gallery Image Successfully Added!',
                  ],
            ]); 
        }
    }

    public function editGallery($id){

    	$editGallery = Gallery::where('id',$id)->first();

    	return view('admin.gallery.edit_gallery',compact('editGallery'));
    }

    public function saveEditedGallery(Request $request){

    	$gallery = Gallery::findOrFail($request->id);
    	if(isset($request->gallery_image)){
	        $fileName = $this->uploadImageData($request->gallery_image,'gallery_image');
          	$gallery->image = $fileName;         
        }
        $gallery->save();

    	return redirect(route('admin.galleryList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Gallery',
                  'message' => 'Gallery Image Successfully Updated!',
              ],
        ]); 
    }

    public function deleteGallery($id){

    	$deleteImage = Gallery::where('id',$id)->delete();

    	return redirect(route('admin.galleryList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Gallery',
                  'message' => 'Gallery Image Successfully Deleted!',
              ],
        ]); 
    }
}