<?php

namespace App\Http\Controllers\Admin;

use App\Model\ImageCategory;
use Illuminate\Http\Request;
use App\Model\CategoryWiseImage;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;

class ImageCategoryController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function imageCategoryList(){

    	$get_category = ImageCategory::where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

    	return view('admin.image_category.image_category_list',compact('get_category'));
    }

    public function addImageCategory(){

    	return view('admin.image_category.add_image_category');	
    }

    public function saveImageCategory(Request $request){
    	
    	$category_image = new ImageCategory;
    	$category_image->category_name = $request->category_name;
        $category_image->category_slug = $this->slugify($request->category_name);
    	$category_image->priority = $request->priority;
    	if(isset($request->category_banner)){
          $fileName = $this->uploadImageData($request->category_banner,'image_category_banner');
          $category_image->category_banner = $fileName;         
        }
        if(isset($request->priority) && $request->priority != ''){
            $category_image->is_priority = 0;
        }else{
            $category_image->is_priority = 1;
        }
        $category_image->save();
        if($request->add_new){

            return redirect(route('admin.addImageCategory'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Image Category',
                    'message' => 'Image category Detail Successfully Added!',
                ],
            ]); 

        }else{
        
          return redirect(route('admin.imageCategoryList'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Image Category',
                    'message' => 'Image category Successfully Added!',
                ],
          ]); 
        }
    }

    public function editImageCategory(Request $request){

    	$category_image = ImageCategory::where('id',$request->id)->first();
        
    	return view('admin.image_category.edit_image_category',compact('category_image'));
    }

    public function saveEditedImageCategory(Request $request){

    	$category_image = ImageCategory::findOrFail($request->id);
    	$category_image->category_name = $request->category_name;
        $category_image->category_slug = $this->slugify($request->category_name);
    	$category_image->priority = $request->priority;
    	if(isset($request->category_banner)){
          $fileName = $this->uploadImage($request->category_banner,'image_category_banner');
          $category_image->category_banner = $fileName;         
        }
        if(isset($request->priority) && $request->priority != ''){
            $category_image->is_priority = 0;
        }else{
            $category_image->is_priority = 1;
        }
        $category_image->save();

        if($category_image){

            return redirect(route('admin.imageCategoryList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Image Category',
                      'message' => 'Image Category Detail Successfully Updated!',
                  ],
            ]); 
        }

    }

    public function deleteImageCategory(Request $request){

    	$remove_image_category = ImageCategory::where('id',$request->id)->update(['is_delete' => 1]);

    	if($remove_image_category){

            return redirect(route('admin.imageCategoryList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Image Category',
                      'message' => 'Image Category Detail Successfully Deleted!',
                  ],
            ]); 
        }
    }

    public function changeImageCategoryStatus(Request $request){

    	$image_category_active = ImageCategory::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($image_category_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function ImageCategoryPriority(Request $request){
    	
        if(isset($request->id)){
            $category_priority = ImageCategory::where('id','!=',$request->id)->where('priority',$request->priority)->where('is_delete',0)->first();
        }else{
            $category_priority = ImageCategory::where('priority',$request->priority)->where('is_delete',0)->first();
        }

        if(!is_null($category_priority)){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function viewCategoryImages(Request $request){
    
        $category_images = CategoryWiseImage::where('category_id',$request->id)->where('is_delete',0)->get();
        $category_id = $request->id;
        $category_name = ImageCategory::where('id',$request->id)->first();

        return view('admin.image_category.category_wise_image_list',compact('category_images','category_id','category_name'));
    }

    public function addCategoryImage(Request $request){

        $category_id = $request->id;

        return view('admin.image_category.add_category_wise_image',compact('category_id'));
    }

    public function saveCategoryImage(Request $request){

        if(count($request->ajax_photos) > 0){

            foreach ($request->ajax_photos as $ak => $av) {
            
                $category_image = new CategoryWiseImage;
                $category_image->sub_images = $av;
                $category_image->category_id = $request->category_id;
                $category_image->save();
            }    
        }
        
        return redirect(route('admin.viewCategoryImages',$request->category_id))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Category Image',
                  'message' => 'Category Image Successfully Uploaded!',
              ],
        ]);

    }

    public function editCategoryImage($id = null,$category_id = null){
        
        $editCategoryImage = CategoryWiseImage::where('id',$id)->where('category_id',$category_id)->first();

        return view('admin.image_category.edit_category_wise_image',compact('category_id','id','editCategoryImage'));
    }

    public function saveEditedCategoryImage(Request $request){

        $update_image = CategoryWiseImage::findOrFail($request->id);
        if(isset($request->sub_images)){
            $fileName = $this->uploadImageData($request->sub_images,'Category_Sub_Images');
            $update_image->sub_images = $fileName;         
        }
        $update_image->save();

        if($update_image){

            return redirect(route('admin.viewCategoryImages',$request->category_id))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Category Image',
                      'message' => 'Category Image Successfully Uploaded!',
                  ],
            ]);
        }
    }

    public function deleteCategoryImage($id = null,$category_id = null){

        $remove_category_image = CategoryWiseImage::where('id',$id)->update(['is_delete' => 1]);

        if($remove_category_image){

            return redirect(route('admin.viewCategoryImages',$category_id))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Category Image',
                      'message' => 'Category Image Detail Successfully Deleted!',
                  ],
            ]); 
        }

    }

    public function categoryStatusImage(Request $request){

        $image_category_active = CategoryWiseImage::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($image_category_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function multipleCategoryImage(Request $request){

        $files = $request->qqfile;
        $destinationPath = 'uploads/Category_Sub_Images';
        $extension = $files->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $files->move($destinationPath, $fileName);

        $data['success'] = true;
        $data['name'] = $fileName;
        return $data;
    }

    public function removeCategoryImage(Request $request){

    }
}
