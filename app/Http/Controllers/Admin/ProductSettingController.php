<?php

namespace App\Http\Controllers\Admin;

use Redirect;
use App\Model\Product;
use App\Model\ProductType;
use Illuminate\Http\Request;
use App\Model\ProductSetting;
use App\Model\ProductSettingData;
use App\Http\Controllers\Controller;

class ProductSettingController extends Controller
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function productSettingList(){

    	$get_products = ProductSetting::with(['product_type','product_data' => function($q) { $q->with(['product_name']); }])->where('is_delete',0)->get();

    	return view('admin.product_setting.product_setting_list',compact('get_products'));
    }

    public function addProductSetting(){

    	$sub_products = Product::get();
    	$product_type = ProductType::get();

    	return view('admin.product_setting.add_product_setting',compact('sub_products','product_type'));
    }

    public function insertProductSetting(Request $request){
    		
      $category_exists = ProductSettingData::where('sub_product_id',$request->category_id)->first();
      if(is_null($category_exists))
      {

        $productData = array();

        if(!is_null($request->product_type_id)){
          foreach($request->product_type_id as $pk => $pv){
            $productData[$pk]['id'] = $pv;
          }
        }

        if(!is_null($request->minimum_rate)){
          foreach($request->minimum_rate as $mk => $mv){
            $productData[$mk]['minimum_rate'] = $mv;
          }
        }

        if(!is_null($request->basic_rate)){
          foreach($request->basic_rate as $bk => $bv){
            $productData[$bk]['basic_rate'] = $bv;
          }
        }

        $product = new ProductSetting;
        $product->sub_product_id = $request->category_id;
        $product->save();

        foreach ($productData as $pk => $pv) {
          
          $product_setting_data = new ProductSettingData;
          $product_setting_data->product_setting_id = $product->id;
          $product_setting_data->sub_product_id = $request->category_id;
          $product_setting_data->product_type_id = $pv['id'];
          $product_setting_data->basic_rate = $pv['basic_rate'];
          $product_setting_data->minimum_qty = $pv['minimum_rate'];
          $product_setting_data->save();
        }

        return redirect(route('admin.productSettingList'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Product Setting',
                    'message' => 'Product Setting Successfully Added!',
                ],
          ]);   

      }else{

      return Redirect::back()->with('messages', [
              [
                'type' => 'success',
                'title' => 'Product Setting',
                'message' => 'Product Already Added!',
              ],
        ]);   
      }
    	
    	
    }

   	public function editProductSetting(Request $request){

   		$sub_products = Product::get();
    	$product_type = ProductType::get();

   		$product = ProductSetting::where('id',$request->id)->first();

   		$get_product_data = ProductSettingData::where('product_setting_id',$request->id)->get();
		
		$productData = array();

		if(!is_null($get_product_data)){
			foreach($get_product_data as $gk => $gv){
				$productData[$gv->product_type_id]['basic_rate'] = $gv->basic_rate;
				$productData[$gv->product_type_id]['minimum_qty'] = $gv->minimum_qty;
			}
		}

   		return view('admin.product_setting.edit_product_setting',compact('get_product_data','sub_products','product_type','product','productData'));
   	} 

   	public function updateProductSetting(Request $request){
   		
   		$product_details = ProductSetting::findOrFail($request->id);
    	$product_details->sub_product_id = $request->category_id;
    	$product_details->save();

   		$productData = array();

      if(!is_null($request->product_type_id)){
        foreach($request->product_type_id as $pk => $pv){
          $productData[$pk]['id'] = $pv;
        }
      }

      if(!is_null($request->minimum_rate)){
        foreach($request->minimum_rate as $mk => $mv){
          $productData[$mk]['minimum_rate'] = $mv;
        }
      }

      if(!is_null($request->basic_rate)){
        foreach($request->basic_rate as $bk => $bv){
          $productData[$bk]['basic_rate'] = $bv;
        }
      }

      $product_data = ProductSettingData::where('product_setting_id',$request->id)->delete();
    	foreach ($productData as $pk => $pv) {
    		
    		$product_setting_data = new ProductSettingData;
    		$product_setting_data->product_setting_id = $request->id;
    		$product_setting_data->sub_product_id = $request->category_id;
    		$product_setting_data->product_type_id = $pv['id'];
    		$product_setting_data->basic_rate = $pv['basic_rate'];
    		$product_setting_data->minimum_qty = $pv['minimum_rate'];
    		$product_setting_data->save();
    	}
    	
    	return redirect(route('admin.productSettingList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Product Setting',
                  'message' => 'Product Setting Successfully Update!',
              ],
        ]); 
   	}

   	public function removeProductData(Request $request){

   		$remove_product = ProductSetting::where('id',$request->id)->update(['is_delete' => 1]);

        $delete_record = ProductSettingData::where('product_setting_id',$request->id)->delete();
    	if($delete_record){

            return redirect(route('admin.productSettingList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Product Setting',
                      'message' => 'Product Setting Detail Successfully Deleted!',
                  ],
            ]); 
        }

   	}
 	
 	public function changeProductStatus(Request $request){

 		$product_status = ProductSetting::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($product_status){
            return 'true';
        } else {
            return 'false';
        }

 	}

}
