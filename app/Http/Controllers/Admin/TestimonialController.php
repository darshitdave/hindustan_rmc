<?php

namespace App\Http\Controllers\Admin;

use App\Model\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;

class TestimonialController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function testimonialList(){

    	$get_testimonial = Testimonial::where('is_delete',0)->orderBy('category_type','ASC')->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

    	return view('admin.testimonial.testimonial_list',compact('get_testimonial'));
    }

    public function addTestimonial(){

    	return view('admin.testimonial.add_testimonail');	
    }

    public function insertTestimonial(Request $request){
        
        $testimonial_detail = new Testimonial;
        $testimonial_detail->category_type = $request->category_id;
        $testimonial_detail->site_name = $request->site_name;
        $testimonial_detail->company_name = $request->company_name;
        $testimonial_detail->description = $request->description;
        $testimonial_detail->priority = $request->priority;
        if(isset($request->image)){
          $fileName = $this->uploadImageData($request->image,'testimonial_image');
          $testimonial_detail->image = $fileName;         
        }
        if(isset($request->priority) && $request->priority != ''){
            $testimonial_detail->is_priority = 0;
        }else{
            $testimonial_detail->is_priority = 1;
        }
        $testimonial_detail->save();
        if($request->add_new){

            return redirect(route('admin.addTestimonial'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Latter of Appreciation',
                    'message' => 'Latter of Appreciation Detail Successfully Added!',
                ],
            ]);   

        }else{

          return redirect(route('admin.testimonialList'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Latter of Appreciation',
                    'message' => 'Latter of Appreciation Detail Successfully Added!',
                ],
          ]);   
        }
        

    }

    public function editTestimonial(Request $request){

    	$get_testimonial = Testimonial::where('id',$request->id)->first();
       
    	return view('admin.testimonial.edit_testimonial',compact('get_testimonial'));		
    }

    public function updateTestimonial(Request $request){
        
        $testimonial_detail = Testimonial::findOrFail($request->id);
        $testimonial_detail->category_type = $request->category_id;
        $testimonial_detail->site_name = $request->site_name;
        $testimonial_detail->company_name = $request->company_name;
        $testimonial_detail->description = $request->description;
        $testimonial_detail->priority = $request->priority;
        if(isset($request->image)){
          $fileName = $this->uploadImageData($request->image,'testimonial_image');
          $testimonial_detail->image = $fileName;         
        }       
        if(isset($request->priority) && $request->priority != ''){
            $testimonial_detail->is_priority = 0;
        }else{
            $testimonial_detail->is_priority = 1;
        }
        $testimonial_detail->save();

        return redirect(route('admin.testimonialList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Latter of Appreciation',
                  'message' => 'Latter of Appreciation Detail Successfully Updated!',
              ],
        ]); 

    }

    public function removeTestimonial(Request $request){

    	$remove_testimonial = Testimonial::where('id',$request->id)->update(['is_delete' => 1]);

    	if($remove_testimonial){

            return redirect(route('admin.testimonialList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Latter of Appreciation',
                      'message' => 'Latter of Appreciation Detail Successfully Deleted!',
                  ],
            ]); 
        }
    }	

    //change doctor status
    public function changeTestimonialStatus(Request $request){

        $testimonial_active = Testimonial::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($testimonial_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function checkTestimonialPriority(Request $request){

        if(isset($request->id)){
            $testimonial_priority = Testimonial::where('id','!=',$request->id)->where('category_type',$request->category_id)->where('priority',$request->priority)->where('is_delete',0)->first();
            
        }else{
            $testimonial_priority = Testimonial::where('category_type',$request->category_id)->where('priority',$request->priority)->where('is_delete',0)->first();
        }

        if(!is_null($testimonial_priority)){
            return 'true';
        } else {
            return 'false';
        }
    }
}
