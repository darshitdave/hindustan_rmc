<?php

namespace App\Http\Controllers\Admin;

use App\Model\Plant;
use App\Model\PlantGalleryPhotos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;

class PlantController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function plantList(){

    	$get_plants = Plant::where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

    	return view('admin.plant.plant_list',compact('get_plants'));
    }

    public function addPlant(){

    	return view('admin.plant.add_plant');	
    }

    public function insertPlant(Request $request){

        $plant_detail = new Plant;
        $plant_detail->plant_name = $request->plant_name;
        $plant_detail->slug = $this->slugify($request->plant_name);
        $plant_detail->priority = $request->priority;
        if(isset($request->plant_status)){
            $plant_detail->plant_status = 1;
        } else {
            $plant_detail->plant_status = 0;
        }
        $plant_detail->city_name = $request->city_name;
        $plant_detail->state_name = $request->state_name;
        $plant_detail->country = $request->country;
        $plant_detail->address = $request->address;
        $plant_detail->latitude = $request->latitude;
        $plant_detail->longitude = $request->longitude;

        $plant_detail->email_id = $request->email;
        $plant_detail->plant_incharge = $request->plant_incharge;
        $plant_detail->iso_certificate = $request->iso_certification;
        $plant_detail->telephone = $request->telephone;
        $plant_detail->quality_head = $request->quality_head;
        $plant_detail->year_of_establish = $request->year_of_establish;
        $plant_detail->track_record = $request->track_record;
        $plant_detail->government_approval = $request->government_approval;
        $plant_detail->plant_make = $request->plant_make;
        $plant_detail->plant_capacity = $request->plant_capacity;
        $plant_detail->maximum_supply = $request->maximum_supply;
        $plant_detail->electric_supply_condition = $request->electric_supply_condition;
        $plant_detail->water_supply_condition = $request->water_supply_condition;
        $plant_detail->no_trans_mixer = $request->no_trans_mixer;
        $plant_detail->no_boom_placer = $request->no_boom_placer;
        $plant_detail->no_concrete_pump = $request->no_concrete_pump;
        $plant_detail->pipeline_length = $request->pipeline_length;
        if(isset($request->plant_image)){
          $fileName = $this->uploadImageData($request->plant_image,'plant_image');
          $plant_detail->plant_image = $fileName;         
        }
        if(isset($request->priority) && $request->priority != ''){
            $plant_detail->is_priority = 0;
        }else{
            $plant_detail->is_priority = 1;
        }
        $plant_detail->save();

            if(count($request->ajax_photos) > 0){

                foreach ($request->ajax_photos as $ak => $av) {
                
                    $plant_id = new PlantGalleryPhotos;
                    $plant_id->plant_image = $av;
                    $plant_id->plant_id = $plant_detail->id;
                    $plant_id->save();

                }
            } 
           
        return redirect(route('admin.plantList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Plant',
                  'message' => 'Plant Detail Successfully Added!',
              ],
        ]); 

    }

    public function multiplePlant(Request $request){
        $files = $request->qqfile;
        $destinationPath = 'uploads/Plant_Galllery';
        $extension = $files->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $files->move($destinationPath, $fileName);

        $data['success'] = true;
        $data['name'] = $fileName;
        return $data;
    }

    public function editPlant(Request $request){

    	$get_plant = Plant::where('id',$request->id)->first();
        
        $get_plant_image = PlantGalleryPhotos::where('plant_id',$request->id)->get();


    	return view('admin.plant.edit_plant',compact('get_plant','get_plant_image'));		
    }

    public function updatePlant(Request $request){
    	
    	$plant_detail = Plant::findOrFail($request->id);
        $plant_detail->plant_name = $request->plant_name;
        $plant_detail->slug = $this->slugify($request->plant_name);
        $plant_detail->priority = $request->priority;
        
        if(isset($request->plant_status)){
            $plant_detail->plant_status = 1;
        } else {
            $plant_detail->plant_status = 0;
        }

        $plant_detail->city_name = $request->city_name;
        $plant_detail->state_name = $request->state_name;
        $plant_detail->country = $request->country;
        $plant_detail->address = $request->address;
        $plant_detail->latitude = $request->latitude;
        $plant_detail->longitude = $request->longitude;

        $plant_detail->email_id = $request->email;
        $plant_detail->plant_incharge = $request->plant_incharge;
        $plant_detail->iso_certificate = $request->iso_certification;
        $plant_detail->telephone = $request->telephone;
        $plant_detail->quality_head = $request->quality_head;
        $plant_detail->year_of_establish = $request->year_of_establish;
        $plant_detail->track_record = $request->track_record;
        $plant_detail->government_approval = $request->government_approval;
        $plant_detail->plant_make = $request->plant_make;
        $plant_detail->plant_capacity = $request->plant_capacity;
        $plant_detail->maximum_supply = $request->maximum_supply;
        $plant_detail->electric_supply_condition = $request->electric_supply_condition;
        $plant_detail->water_supply_condition = $request->water_supply_condition;
        $plant_detail->no_trans_mixer = $request->no_trans_mixer;
        $plant_detail->no_boom_placer = $request->no_boom_placer;
        $plant_detail->no_concrete_pump = $request->no_concrete_pump;
        $plant_detail->pipeline_length = $request->pipeline_length;
        if(isset($request->plant_image)){
          $fileName = $this->uploadImageData($request->plant_image,'plant_image');
          $plant_detail->plant_image = $fileName;         
        }
        if(isset($request->priority) && $request->priority != ''){
            $plant_detail->is_priority = 0;
        }else{
            $plant_detail->is_priority = 1;
        }
        $plant_detail->save();

            if(count($request->ajax_photos) > 0){
        
                foreach ($request->ajax_photos as $ak => $av) {
                    
                    $plant_id = new PlantGalleryPhotos;
                    $plant_id->plant_image = $av;
                    $plant_id->plant_id = $request->id;
                    $plant_id->save();
                }
            } 
        return redirect(route('admin.plantList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Plant',
                  'message' => 'Plant Detail Successfully Updated!',
              ],
        ]); 

    }

    public function removePlant(Request $request){

    	$remove_plant = Plant::where('id',$request->id)->update(['is_delete' => 1]);

    	if($remove_plant){

            return redirect(route('admin.plantList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Plant',
                      'message' => 'Plant Detail Successfully Deleted!',
                  ],
            ]); 
        }
    }	

    //change doctor status
    public function changePlantStatus(Request $request){

        $plant_active = Plant::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($plant_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function checkPlantPriority(Request $request){
    
        if(isset($request->id)){

            $plant_priority = Plant::where('id','!=',$request->id)->where('priority',$request->priority)->where('is_delete',0)->first();
        }else{
        	
            $plant_priority = Plant::where('priority',$request->priority)->where('is_delete',0)->first();
        }

        if(!is_null($plant_priority)){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function removePlantPhotos(Request $request){
        
        $delete_plant = PlantGalleryPhotos::where('id',$request->id)->delete();

        if($delete_plant){
            return 'true';
        }else{
            return 'false';   
        }
    }
}
