<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\NewsLatter;
use App\Http\Controllers\Controller;

class NewLatterController extends Controller
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function newsLetterList(){

    	$new_letter_list = NewsLatter::orderBy('created_at','DESC')->get();
      
    	return view('admin.news_letter.news_letter_list',compact('new_letter_list'));
    }
}
