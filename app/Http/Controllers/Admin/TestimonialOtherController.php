<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\TestimonialOther;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;

class TestimonialOtherController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function testimonialOtherList(Request $request){

    	$get_testimonial_other = TestimonialOther::where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();

    	return view('admin.testimonial_other.testimonial_other_list',compact('get_testimonial_other'));
    }

    public function addTestimonialOther(){
      
    	return view('admin.testimonial_other.add_testimonail_other');	
    }

    public function insertTestimonialOther(Request $request){
    	
        $testimonial_other = new TestimonialOther;
        $testimonial_other->name = $request->name;
        $testimonial_other->designation = $request->designation;
        $testimonial_other->priority = $request->priority;
        $testimonial_other->description = $request->description;
        if(isset($request->video_url) && $request->video_url != ''){
            $testimonial_other->video_url = $request->video_url;    
        }else{
            $testimonial_other->video_url = '';    
        }
        $testimonial_other->rating = $request->rating;
        if(isset($request->image)){
          $fileName = $this->uploadImageData($request->image,'testimonial_other_image');
          $testimonial_other->image = $fileName;         
        }
        $testimonial_other->save();
        if($request->add_new){
          return redirect(route('admin.addTestimonialOther'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Testimonial',
                  'message' => 'Testimonial Detail Successfully Added!',
              ],
          ]);
        }else{

          return redirect(route('admin.testimonialOtherList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Testimonial',
                  'message' => 'Testimonial Detail Successfully Added!',
              ],
          ]);
        }
    }

    public function editTestimonialOther(Request $request){

    	$get_testimonial_other = TestimonialOther::where('id',$request->id)->first();
       
    	return view('admin.testimonial_other.edit_testimonial_other',compact('get_testimonial_other'));		
    }

    public function updateTestimonialOther(Request $request){

    	$testimonial_other = TestimonialOther::findOrFail($request->id);
        $testimonial_other->name = $request->name;
        $testimonial_other->designation = $request->designation;
        $testimonial_other->priority = $request->priority;
        $testimonial_other->description = $request->description;
        if(isset($request->video_url) && $request->video_url != ''){
            $testimonial_other->video_url = $request->video_url;    
        }else{
            $testimonial_other->video_url = '';    
        }
        $testimonial_other->rating = $request->rating;
        if(isset($request->image)){
          $fileName = $this->uploadImageData($request->image,'testimonial_other_image');
          $testimonial_other->image = $fileName;         
        }
        $testimonial_other->save();
        
        return redirect(route('admin.testimonialOtherList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Testimonial',
                  'message' => 'Testimonial Detail Successfully Update!',
              ],
        ]);
    }

    public function removeTestimonialOther(Request $request){

    	$remove_testimonial_other = TestimonialOther::where('id',$request->id)->update(['is_delete' => 1]);

    	if($remove_testimonial_other){

            return redirect(route('admin.testimonialOtherList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Testimonial',
                      'message' => 'Testimonial Detail Successfully Deleted!',
                  ],
            ]); 
        }
    }

    //change doctor status
    public function changeTestimonialOtherStatus(Request $request){

        $testimonial_other_active = TestimonialOther::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($testimonial_other_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function checkTestimonialOtherPriority(Request $request){

      if(isset($request->id)){
            $testimonial_priority = TestimonialOther::where('id','!=',$request->id)->where('priority',$request->priority)->where('is_delete',0)->first();
        }else{
            $testimonial_priority = TestimonialOther::where('priority',$request->priority)->where('is_delete',0)->first();
           
        }

        if(!is_null($testimonial_priority)){
            return 'true';
        } else {
            return 'false';
        }
    }
}	
