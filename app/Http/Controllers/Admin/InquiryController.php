<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\RMCCalculatorDetail;
use App\Model\GenralContactDetail;
use App\Http\Controllers\Controller;

class InquiryController extends Controller
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function rmcInquiryList(Request $request){

        $filter = 0;
        $date_range = '';

        $query = RMCCalculatorDetail::query();

        if(isset($request->date_range) && $request->date_range != ''){
            $filter = 1;
            $date_range = $request->date_range;
            $date = explode('-',$request->date_range);
            $date1 = date('Y-m-d',strtotime(str_replace('/', '-',$date[0])."- 1 day"));
            $date2 = date('Y-m-d',strtotime(str_replace('/', '-',$date[1])."+ 1 day"));
            $query->whereBetween('created_at',[$date1,$date2]);
        }

        $query->with(['product_name','product_data' => function($q) { $q->with(['product_name']); }]);
        $rmc_details = $query->orderBy('created_at','DESC')->get();
        
        return view('admin.inquiry.rmc_inquiry_list',compact('rmc_details','date_range','filter'));
    }

    public function rmcInquiryDetail(Request $request){

        $rmc_details = RMCCalculatorDetail::where('id',$request->id)->first();          

        return view('admin.inquiry.rmc_inquiry_detail',compact('rmc_details'));
    }

    public function genralInquiryList(Request $request){

        $filter = 0;
        $date_range = '';

        $query = GenralContactDetail::query();

        if(isset($request->date_range) && $request->date_range != ''){
            $filter = 1;
            $date_range = $request->date_range;
            $date = explode('-',$request->date_range);
            $date1 = date('Y-m-d',strtotime(str_replace('/', '-',$date[0])."- 1 day"));
            $date2 = date('Y-m-d',strtotime(str_replace('/', '-',$date[1])."+ 1 day"));
            $query->whereBetween('created_at',[$date1,$date2]);
        }

        $genral_details = $query->orderBy('created_at','DESC')->get();

        return view('admin.inquiry.genral_inquiry_list',compact('genral_details','date_range','filter')); 
    }

    public function genralInquiryDetail(Request $request){
    
        $genral_details = GenralContactDetail::where('id',$request->id)->first();       
        
        return view('admin.inquiry.genral_inquiry_detail',compact('genral_details'));   
    }

}
