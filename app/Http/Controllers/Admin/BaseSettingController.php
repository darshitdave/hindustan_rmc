<?php

namespace App\Http\Controllers\Admin;

use App\Model\BaseSetting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseSettingController extends Controller
{
   	public function __construct(){
      $this->middleware('admin');
    }

    public function editBaseSetting(){

    	$get_last = BaseSetting::orderBy('id', 'desc')->first();
    	
    	return view('admin.base_setting.edit_base',compact('get_last'));
    }

    public function insertBaseSetting(Request $request){
  
    	$base_setting = new BaseSetting;
    	$base_setting->cement_rate = $request->cement_rate;
      	$base_setting->cement_kg = $request->cement_kg;
    	$base_setting->flyash_rate = $request->flyash_rate;
      	$base_setting->flyash_kg = $request->flyash_kg;
    	$base_setting->gst = $request->gst_percent;
      	$base_setting->save();

      if($base_setting){

          return redirect(route('admin.base.setting'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Base Setting',
                    'message' => 'Base Setting Successfully Added!',
                ],
          ]); 
      }
    }

}
