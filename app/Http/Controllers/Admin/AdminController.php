<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Auth;
use Redirect;
use Validator;
use App\Model\User;
use App\Model\Admin;
use App\Model\Doctor;
use App\Model\HelpSupport;
use Illuminate\Http\Request;
use App\Model\ChildrenDetail;
use App\Http\Controllers\GlobalController;
use Illuminate\Support\Facades\Input;

class AdminController extends GlobalController
{
    public function __construct(){

      $this->middleware('admin');
      
    }
    
    //Dashboard 
    public function index(){

      return view('admin.dashboard.dashboard');
    }

    //admin details
    public function adminProfile(){
        
        $admin_profile = Admin::where('id',Auth::guard('admin')->user()->id)->first();
        
        return view('admin.dashboard.admin_update_profile',compact('admin_profile'));
    }

    //update admin details
    public function adminProfileUpdate(Request $request){
        
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);

        $update_admin = Admin::findOrFail($request->id);
        $update_admin->name = $request->name;
        $update_admin->email = $request->email;
        $update_admin->save(); 

        return redirect()->route('admin.dashboard')->with(['message'=>'Profile successfully updated!','alert-type' => 'success']);
    }

    //change password
    public function changeAdminPassword(){

        return view('admin.dashboard.change_password');
    }

    //update password
    public function updateAdminPassword(Request $request){

        $this->validate($request, [
            'old_pass' => 'required',
            'new_pass' => 'required'
        ]);

        $user = Admin::where('id', '=', $request->id)->first();

        if(Hash::check($request->old_pass,$user->password)){

            $users = Admin::findOrFail($request->id);
            $users->password = Hash::make($request->new_pass);
            $users->save();

            return redirect(route('admin.dashboard'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Password',
                      'message' => 'Password Successfully changed!',
                  ],
            ]); 

        } else {
          
            return redirect()->back()->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Password',
                      'message' => 'Please Check Your Current Password!',
                  ],
            ]); 
        }
    }
   
}