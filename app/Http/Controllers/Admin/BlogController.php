<?php

namespace App\Http\Controllers\Admin;

use str_slug;
use App\Model\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;

class BlogController extends GlobalController
{
    public function __construct(){
      $this->middleware('admin');
    }

    public function blogList(){

    	$get_blog = Blog::where('is_delete',0)->orderBy('is_priority','ASC')->orderBy('priority','ASC')->get();
      
    	return view('admin.blog.blog_list',compact('get_blog'));
    }

    public function addBlog(){

    	return view('admin.blog.add_blog');	
    }


    public function insertBlog(Request $request){

    	$blog_details = new Blog;
    	$blog_details->title = $request->title;
        $blog_details->blog_slug = $this->slugify($request->title);
    	$blog_details->priority = $request->priority;
    	$blog_details->date = $request->blog_date;
    	$blog_details->author_name = $request->author_name;
    	$blog_details->description = $request->description;
    	if(isset($request->blog_image)){
          $fileName = $this->uploadImage($request->blog_image,'blog_image');
          $blog_details->blog_image = $fileName;         
        }
        
        $blog_details->save();
        if($request->add_new){

            return redirect(route('admin.addBlog'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Blog',
                    'message' => 'Blog Detail Successfully Added!',
                ],
            ]); 

        }else{
        
          return redirect(route('admin.blogList'))->with('messages', [
                [
                    'type' => 'success',
                    'title' => 'Blog',
                    'message' => 'Blog Detail Successfully Added!',
                ],
          ]); 
        }
    }

    public function editBlog(Request $request){

    	$get_blog = Blog::where('id',$request->id)->first();
        
    	return view('admin.blog.edit_blog',compact('get_blog'));
    }

    public function updateBlog(Request $request){

    	$blog_details = Blog::findOrFail($request->id);
    	$blog_details->title = $request->title;
        $blog_details->blog_slug = $this->slugify($request->title);
    	$blog_details->priority = $request->priority;
    	$blog_details->date = $request->blog_date;
    	$blog_details->author_name = $request->author_name;
    	$blog_details->description = $request->description;
    	if(isset($request->blog_image)){
          $fileName = $this->uploadImage($request->blog_image,'blog_image');
          $blog_details->blog_image = $fileName;         
        }
        
        $blog_details->save();

        if($blog_details){

            return redirect(route('admin.blogList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Blog',
                      'message' => 'Blog Detail Successfully Updated!',
                  ],
            ]); 
        }

    }

    public function removeBlog(Request $request){

    	$remove_blog = Blog::where('id',$request->id)->update(['is_delete' => 1]);

    	if($remove_blog){

            return redirect(route('admin.blogList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Blog',
                      'message' => 'Blog Detail Successfully Deleted!',
                  ],
            ]); 
        }
    }

    public function changeBlogStatus(Request $request){

    	$blog_active = Blog::where('id',$request->id)->update(['is_active'=> $request->status]);

        if($blog_active){
            return 'true';
        } else {
            return 'false';
        }
    }

    public function blogPriority(Request $request){
    
        if(isset($request->id)){

            $blog_priority = Blog::where('id','!=',$request->id)->where('priority',$request->priority)->where('is_delete',0)->first();
        }else{
        	
            $blog_priority = Blog::where('priority',$request->priority)->where('is_delete',0)->first();
        }

        if(!is_null($blog_priority)){
            return 'true';
        } else {
            return 'false';
        }
    }
}
