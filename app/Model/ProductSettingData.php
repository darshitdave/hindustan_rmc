<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductSettingData extends Model
{
    public function product_name(){
        return $this->hasOne('App\Model\ProductType','id','product_type_id');
    }
}
