<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductSetting extends Model
{
    public function product_type(){
        return $this->hasOne('App\Model\Product','id','sub_product_id');
    }

    public function product_data(){
        return $this->hasMany('App\Model\ProductSettingData','product_setting_id','id');
    }

    public function product_name(){
        return $this->hasOne('App\Model\ProductType','id','sub_product_id');
    }
}
