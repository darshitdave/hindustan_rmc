<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ImageCategory extends Model
{
    public function category(){
        return $this->hasOne('App\Model\CategoryWiseImage','category_id','id')->where('is_active',1)->where('is_delete',0);
    }
}
