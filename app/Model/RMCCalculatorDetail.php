<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RMCCalculatorDetail extends Model
{
    public function product_name(){
        return $this->hasOne('App\Model\Product','id','product_id');
    }

    public function product_data(){
        return $this->hasOne('App\Model\ProductSetting','sub_product_id','grade_id');
    }
}
