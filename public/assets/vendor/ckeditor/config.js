/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.filebrowserBrowseUrl = '/templateEditor/kcfinder/browse.php?opener=ckeditor&type=files';

    config.filebrowserImageBrowseUrl = '/templateEditor/kcfinder/browse.php?opener=ckeditor&type=images';

    config.filebrowserFlashBrowseUrl = '/templateEditor/kcfinder/browse.php?opener=ckeditor&type=flash';

    config.filebrowserUploadUrl = '/templateEditor/kcfinder/upload.php?opener=ckeditor&type=files';

    config.filebrowserImageUploadUrl = '/templateEditor/kcfinder/upload.php?opener=ckeditor&type=images';

    config.filebrowserFlashUploadUrl = '/templateEditor/kcfinder/upload.php?opener=ckeditor&type=flash';
	config.allowedContent = true;
	config.extraAllowedContent = '*(*)';
	config.protectedSource.push(/<i[^>]*><\/i>/g);
	config.extraPlugins = 'youtube';

	config.skin = 'moonocolor';
	config.removePlugins = 'sourcearea,save,about,print,preview,blockquote,showblocks,image,flash';
	
	 // Define changes to default configuration here:

	config.contentsCss = '/assets/admin_asset/vendors/bower_components/ckeditor/fonts/stylesheet.css';

	//the next line add the new font to the combobox in CKEditor

	//config.font_names = '<Cutsom Font Name>/<YourFontName>;' + config.font_names;

	config.font_names = 'Leaderspodium Font/proxima_nova_rgregular;' + config.font_names;
	

};
