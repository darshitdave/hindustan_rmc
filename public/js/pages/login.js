$(document).ready(function() {

    //custom validation method
    $.validator.addMethod("customemail", 
        function(value, element) {
            return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
        }, 
        "Please enter email id along with domain name."
    );

    // validate signup form on keyup and submit
    $("#loginForm").validate({
        errorElement: 'span',
        rules: {
            password: {
                required: true,
                minlength: 6
            },
            email: {
                required: true,
                email: true,
                customemail : true
            },
        },
        messages: {
            password:{
               required:"Please enter password",
               minlength:"Please enter minimum 6 character"
            },
            email:{
                required:"Please enter email address",
                email:"Please enter valid email address"
            } 
        }
    });

    // validate signup form on keyup and submit
    $("#register").validate({
        errorElement: 'span',
        rules: {
            
            mobile_number: {
                required: true,
                minlength: 10
            },
            otp: {
                required: true,
                minlength: 6
            },
        },
        messages: {
            
            mobile_number:{
                required:"Please Enter 10 Digit Mobile Number",
            },
            otp:{
                required:"Please Enter 6 Digit OTP",
            }, 
        }
    });

    //Forgot password
    $("#forgotForm").validate({
        errorElement: 'span',
        rules: {
            email: {
                required: true,
                email: true,
                customemail : true
            },
        },
        messages: {
            email:{
                required:"Please enter email address",
                email:"Please enter valid email address"
            } 
        }
    });

    $("#resetPassword").validate({
        errorElement: 'span',
        rules: {
            email: {
                required: true,
                email: true,
                customemail : true
            },
            password: {
                required: true,
                minlength: 6
            },
            password_confirmation:{
                required: true,
                minlength: 6,
                equalTo:"#password"  
            }
        },
        messages: {
            email:{
                required:"Please enter email address",
                email:"Please enter valid email address"
            },
            password:{
                required:"Please enter password",
            },
            password_confirmation:{
                required:"Please enter cofirm password",
                equalTo:"The password and confirmation password do not match!"
            }
        }
    });
    $("#changepassword").validate({
        errorElement: 'span',
        rules: {
            old_pass: {
                required: true,
                minlength: 6
            },
            new_pass: {
                required: true,
                minlength: 6
            },
            pass_conf:{
                required: true,
                minlength: 6,
                equalTo:"#new_pass" 
            }
        },
        messages: {
            old_pass:{
                required:"Please Enter Old Password",
            },
            new_pass:{
                required:"Please Enter New Password",
            },
            pass_conf:{
                required:"Please Enter Cofirm Password",
                equalTo:"The Password And Confirmation Password Do Not Match!"
            }
        }
    });
    $("#profile").validate({
        errorElement: 'span',
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true,
                customemail : true
            }
        },
        messages: {
            name:{
                required:"Please Enter Name",
            },
            email:{
                required:"Please Enter Email Address",
                email:"Please Enter Valid Email Address"
            }
        }
    });

});

