$(document).ready(function() {
	//custom validation method
    $.validator.addMethod("customemail", 
        function(value, element) {
            return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
        }, 
        "Please enter email id along with domain name."
    );

    $("#plantForms").validate({
        errorElement: 'span',
        rules: {
            plant_name: {
                required: true
            },
            telephone:{
            	required: true,
            	maxlength: 10,
            	minlength: 10	
            },
            address:{
            	required: true
            },
            email:{
            	required: true,
            	email: true,
                customemail : true	
            },
            plant_incharge:{
            	required: true
            },
            quality_head:{
            	required: true	
            }
        },
        messages: {
            plant_name:{
                required:"Enter Plant Name"
            },
            telephone:{
            	required:"Enter Telephone",
            	maxlength:"Enter 10 Digit Telephone",
            	minlength:"Enter Atleast 10 Digit Telephone"		
            },
            address:{
            	required:"Enter Address"
            },
            email:{
            	required:"Enter Email ID",
                email:"Enter Valid Email ID"
            },
            plant_incharge:{
            	required:"Enter Plant Incharge",
            },
            quality_head:{
            	required:"Enter Qulity Head",	
            }
        }
    });
    $("#projectForms").validate({
        errorElement: 'span',
        rules: {
        	category_id: {
                required: true
            },
            project_name:{
            	required: true	
            }
        },
        messages: {
            category_id:{
                required:"Select Category Name"
            },
            project_name:{
            	required:"Enter Project Name"		
            },
            project_profile_photo:{
                required:"Upload Project Profile Photo"
            }

        }
    });
    $("#testimonialForms").validate({
        errorElement: 'span',
        rules: {
            site_name: {
                required: true
            },
            company_name: {
              required: true  
            },
            description: {
              required: true  
            }
        },
        messages: {
            site_name:{
                required:"Enter Site Name"
            },
            company_name:{
              required:"Enter Company Name"  
            },
            description: {
              required:"Enter Description"  
            },
            image : {
                required:"Upload Image"  
            }
        }
    });
    $("#testimonialOtherForms").validate({
        errorElement: 'span',
        rules: {
            name: {
                required: true
            },
            designation : {
              required: true  
            },
            description : {
                required: true    
            },
            rating : {
              required: true,
               min : 1,
               max: 5      
            }
        },
        messages: {
            name:{
                required:"Enter Name"
            },
            designation:{
                required:"Enter Designation"
            },
            description : {
                required:"Enter Description"  
            },
            image : {
                required:"Upload Image"  
            },   
            rating : {
                required:"Enter Rating",  
                min : 'Enter Number Grater Than 0',
                max : 'Enter number Less or Equal to 5'
                
            }
        }
    });

    $("#blogForms").validate({
        errorElement: 'span',
        ignore: [],
        rules: {
            title: {
                required: true
            },
            blog_date : {
                required: true
            },
            description : {
              required: true,
              minlength:10
            }
        },
        messages: {
            title:{
                required:"Enter Title"
            },
            blog_date : {
              required:"Select Blog Date"  
            },
            blog_image : {
              required: "Upload Blog Image"  
            },
            description : {
              required: "Enter Description" ,
              minlength:"Please enter 10 characters"
            }    
        }
    });
    $("#rmc_calculator").validate({
        errorElement: 'span',
        rules: {
            select_shape: {
                required: true
            },
            thikness_slab : {
                required: true    
            }
        },
        messages: {
            select_shape:{
                required:"Select Shape"
            },
            thikness_slab : {
                required: "Enter Thikness"    
            }
        }
    });
    $("#category_image").validate({
        errorElement: 'span',
        rules: {
            category_name: {
                required: true
            }
        },
        messages: {
            category_name:{
                required:"Enter Category Name"
            },
            category_banner:{
                required:"Upload Category Cover Image"
            }
        }
    });
});