(function ($) {
    'use strict';
    $(document).ready(function () {
        $('#example').DataTable({
            //DataTable Options
        });
        $('#example-height').DataTable({
            scrollY:        '50vh',
            scrollCollapse: true,
            paging:         false
        });
        $('#example-multi').DataTable({
            "iDisplayLength": 50,
            "aLengthMenu": [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
        });
        /*$('#example-multi tbody').on( 'click', 'tr', function () {
            $(this).toggleClass('bg-gray-400');
        } );*/
    });

})(window.jQuery);